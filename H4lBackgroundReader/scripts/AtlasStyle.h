//
//   @file    AtlasStyle.h         
//   
//            ATLAS Style, based on a style file from BaBar
//
//
//   @author M.Sutton
// 
//   Copyright (C) 2010 Atlas Collaboration
//
//   $Id: AtlasStyle.h 101 2015-11-18 03:39:29Z inomidis $

#ifndef  __ATLASSTYLE_H
#define __ATLASSTYLE_H

#include "TStyle.h"

void SetAtlasStyle();

TStyle* AtlasStyle(); 

#endif // __ATLASSTYLE_H
