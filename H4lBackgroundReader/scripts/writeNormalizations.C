#include "../H4lBackgroundReader/H4lBkgEnums.h"
#include "TH2F.h"
#include "TFile.h"
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

using namespace H4lBkg;
using namespace Observable;
using namespace Category;
using namespace Channel;

void setBackgroundEstimation( EventType event_type, ProdType prod_type, float n, float stat, float syst );
void getBackgroundEstimation( EventType event_type, ProdType prod_type, float &n, float &stat, float &syst );
void getBinContents( EventType event_type, ProdType prod_type, float &x, float &xerr, float &xsyst, vector<TH2F*> h  )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  x = h[0]->GetBinContent(i,j);
  xerr = h[1]->GetBinContent(i,j);
  if(h.size()>2) xsyst = h[2]->GetBinContent(i,j);
}

void setBackgroundEstimationHM( EventType event_type, ProdTypeHM prod_type, float n, float stat, float syst );
void getBackgroundEstimationHM( EventType event_type, ProdTypeHM prod_type, float &n, float &stat, float &syst );
void getBinContentsHM( EventType event_type, ProdTypeHM prod_type, float &x, float &xerr, vector<TH2F*> h  )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  x = h[0]->GetBinContent(i,j);
  xerr = h[1]->GetBinContent(i,j);
}  

TH2F * h_n;
TH2F * h_stat;
TH2F * h_syst;

void writeNormalizations(int yr)
{
  vector<string> year={"2016","2017","2018","all"};
  float lumi=36.2;
  if(yr==1) lumi=44.3;
  else if(yr==2) lumi=59.9;
  else if(yr==3) lumi=139;
  //////////////////////////////////
  vector<ProdType> allProdTypes = getProdTypes();
  vector<EventType> allEventTypes = getEventTypes();
  static const unsigned int nProdTypes = allProdTypes.size();
  static const unsigned int nEventTypes = allEventTypes.size();
  
  h_n = new TH2F("h_n","", nProdTypes, 0,nProdTypes , nEventTypes,0,nEventTypes);
  int ii;
  ii=0;
  for (auto type : allProdTypes) 
    h_n->GetXaxis()->SetBinLabel( ++ii , toString( type ).Data() );
  ii=0;
  for (auto type : allEventTypes)
    h_n->GetYaxis()->SetBinLabel( ++ii, toString( type ).Data() );

  
  h_stat = (TH2F*) h_n->Clone("h_stat");
  h_syst = (TH2F*) h_n->Clone("h_syst");
  //////////////////////////////////////////////////////////////////////////////
  enum elFS{fs2l2e,fs4e,fs2mu2e};
  enum muFS{fs2l2mu,fs4mu,fs2e2mu};
  //llee
  //load gamma and light estimates and uncertainties for this year directly from the file
  string finalState[3]={"e","2e2e","2mu2e"};
  string line;
  float n2l2e[3]={0,0,0};  //total/4e/2mu2e
  float stat2l2e[3]={0,0,0};
  float syst2l2e[3]={0,0,0};
  for(int i=0;i<3;i++){
    //cout<<finalState[i]<<endl;
    string estfn="est"+year[yr].substr(2,2)+"_"+finalState[i]+".log";
    if(year[yr].compare("all")==0) estfn="est"+year[yr]+"_"+finalState[i]+".log";
    ifstream totalEst(estfn.c_str());
    if(totalEst.is_open()){
      while(getline(totalEst,line)){
	if(line.find("2d")==string::npos) continue;
	//cout<<line<<endl;
	int pos1=line.find("+");
	int pos2=line.find("+",pos1+1);
	n2l2e[i]+=stof(line.substr(9,pos1-9));
	//cout<<"add "<<line.substr(9,pos1-9)<<endl;
	stat2l2e[i]+=pow(stof(line.substr(pos1+2,pos2-(pos1+2)-7)),2);
	syst2l2e[i]+=pow(stof(line.substr(pos2+2,line.length()-(pos2-2)-7)),2);
      }
    }
    //cout<<"number in this final state: "<<n2l2e[i]<<endl;
    totalEst.close();
  }
  //load heavy estimates and uncertainties for this year directly from the file
  TFile* hfFile=TFile::Open(Form("heavyFlavorShapes_%s.root",year[yr].c_str()));
  TH1F* HFTot=(TH1F*)hfFile->Get("HFTot");
  for(int i=1;i<=2;i++){
    //cout<<"add "<<HFTot->GetBinContent(i)<<" to final state "<<finalState[i]<<endl;
    n2l2e[i]+=HFTot->GetBinContent(i);
    stat2l2e[i]+=pow(HFTot->GetBinError(i),2);
    syst2l2e[i]+=pow(0.3*HFTot->GetBinContent(i),2);
    n2l2e[elFS::fs2l2e]+=HFTot->GetBinContent(i);
    stat2l2e[elFS::fs2l2e]+=pow(HFTot->GetBinError(i),2);
    //cout<<"final state "<<finalState[i]<<" now has "<<n2l2e[i]<<" and the total has "<<n2l2e[elFS::fs2l2e]<<endl;
  }
  syst2l2e[elFS::fs2l2e]+=pow(0.3*HFTot->Integral(),2);
  for(int i=0;i<3;i++){
    stat2l2e[i]=sqrt(stat2l2e[i]);
    syst2l2e[i]=sqrt(syst2l2e[i]);
    cout<<finalState[i]<<": "<<n2l2e[i]<<endl;
  }
  string finalStateMu[3]={"mu","4mu","2e2mu"};
  float n2l2mu[3]={0,0,0};  //total/4mu/2e2mu
  float stat2l2mu[3]={0,0,0};
  float syst2l2mu[3]={0,0,0};
  for(int i=0;i<3;i++){
    string estfn="est"+year[yr].substr(2,2)+"_"+finalStateMu[i]+".txt";
    if(year[yr].compare("all")==0) estfn="est"+year[yr]+"_"+finalStateMu[i]+".txt";
    ifstream totalEst(estfn.c_str());
    if(totalEst.is_open()){
      while(getline(totalEst,line)){
	if(line.find(":")==string::npos) continue;
	int pos1=line.find(":");
	int pos2=line.find("+");
	int pos3=line.length();
	if(line.find("WZ")==string::npos) pos3=line.find("+",pos2+1);
	n2l2mu[i]+=stof(line.substr(pos1+1,pos2-pos1-1));
	stat2l2mu[i]+=pow(stof(line.substr(pos2+2,pos3-pos2-2)),2);
	if(pos3<line.length()) syst2l2mu[i]+=pow(stof(line.substr(pos3+2,line.length()-pos3-2)),2);
      }
    }
    totalEst.close();
  }
  for(int i=0;i<3;i++){
    stat2l2mu[i]=sqrt(stat2l2mu[i]);
    syst2l2mu[i]=sqrt(syst2l2mu[i]);
    cout<<finalStateMu[i]<<": "<<n2l2mu[i]<<endl;
  }
  TFile *f = TFile::Open(Form("data/bkg_perCategory_%s.root",year[yr].c_str()));
  TH2F* h_bkgFrac = (TH2F*)f->Get("h_bkgFrac");
  TH2F* h_bkgFracStatErr = (TH2F*)f->Get("h_bkgFracStatErr");
  TH2F* h_bkgFracSystErr = (TH2F*)f->Get("h_bkgFracSystErr");
  vector<TH2F*> vbkgFrac({h_bkgFrac,h_bkgFracStatErr,h_bkgFracSystErr});
  if (h_bkgFrac->GetXaxis()->GetNbins()!=nProdTypes || h_bkgFrac->GetXaxis()->GetNbins()!=nProdTypes) { 
    cout << "Wrong size for vector frac2l2e " << nProdTypes << " " << h_bkgFrac->GetXaxis()->GetNbins() << endl;
    exit(0);
  }

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  EventType channels[]={EventType::_2l2e,EventType::_2l2mu};
  float nIncl[] = { n2l2e[elFS::fs2l2e], n2l2mu[muFS::fs2l2mu] };
  float statIncl[] = { stat2l2e[elFS::fs2l2e], stat2l2mu[muFS::fs2l2mu] };
  float systIncl[] = { syst2l2e[elFS::fs2l2e], syst2l2mu[muFS::fs2l2mu] };
  ///////////////////////////////////////////////
  float systIncl4l = sqrt(syst2l2e[elFS::fs2l2e]*syst2l2e[elFS::fs2l2e] + syst2l2mu[muFS::fs2l2mu]*syst2l2mu[muFS::fs2l2mu]);
  /////
  
  //split 2l2e and 2l2mu in categories, full mass range
  for (int ichan=0; ichan<2; ichan++){
    for (auto ptype: allProdTypes) {
      if(!toString(ptype).Contains("BDT")){
	float f=0, ferr=0, fsyst=0;
	getBinContents( channels[ichan], ptype, f, ferr, fsyst, vbkgFrac );

	float nCateg = nIncl[ichan]*f;
	float relsystCateg = (nCateg>0) ? systIncl[ichan]/nIncl[ichan] : 0;
	float systCateg = nCateg* relsystCateg;
	float relstatCateg = (nCateg>0) ? sqrt(pow(statIncl[ichan]/nIncl[ichan],2)+pow(ferr/f,2) ) : 0;
	float statCateg=nCateg*relstatCateg;
	cout<<"f: "<<f<<" statIncl: "<<statIncl[ichan]<<"; ferr: "<<ferr<<endl;
    
	if (ptype==ProdType::prodInclusive) {
	  statCateg = statIncl[ichan];
	  systCateg = systIncl[ichan];
	}
	//don't include inclusive uncertainties in 2l2mu since each number is obtained independently
	if(channels[ichan]==EventType::_2l2mu){
	  statCateg=nIncl[ichan]*ferr;
	  systCateg=nIncl[ichan]*fsyst;
	}
      
	//if(channels[ichan]==EventType::_2l2mu && ptype==ProdType::_ttHLep) cout<<"set estimation for 2l2mu ttHLep"<<endl;
	setBackgroundEstimation( channels[ichan], ptype, nCateg, statCateg, systCateg );
	cout << toString(channels[ichan]) << " " << toString(ptype) << " " << nCateg << " +- " << statCateg << " (stat) +- " << systCateg << " (syst)" << endl;
	if (channels[ichan]==EventType::_2l2e) {
	  setBackgroundEstimation( EventType::_4e, ptype, f*n2l2e[elFS::fs4e], (f>0) ? f*n2l2e[elFS::fs4e]*sqrt(pow(stat2l2e[elFS::fs4e]/n2l2e[elFS::fs4e],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2e[elFS::fs4e]*f : 0);
	  setBackgroundEstimation( EventType::_2mu2e, ptype, f*n2l2e[elFS::fs2mu2e], (f>0) ? f*n2l2e[elFS::fs2mu2e]*sqrt(pow(stat2l2e[elFS::fs2mu2e]/n2l2e[elFS::fs2mu2e],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2e[elFS::fs2mu2e]*f : 0);
	} else {
	  setBackgroundEstimation( EventType::_4mu, ptype, f*n2l2mu[muFS::fs4mu], ferr*n2l2mu[muFS::fs4mu], fsyst*n2l2mu[muFS::fs4mu]);
	  setBackgroundEstimation( EventType::_2e2mu, ptype, f*n2l2mu[muFS::fs2e2mu], ferr*n2l2mu[muFS::fs2e2mu], fsyst*n2l2mu[muFS::fs2e2mu]);
	  //setBackgroundEstimation( EventType::_4mu, ptype, f*n2l2mu[muFS::fs4mu], (f>0) ? f*n2l2mu[muFS::fs4mu]*sqrt(pow(stat2l2mu[muFS::fs4mu]/n2l2mu[muFS::fs4mu],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2mu[muFS::fs4mu]*f : 0);
	  //setBackgroundEstimation( EventType::_2e2mu, ptype, f*n2l2mu[muFS::fs2e2mu], (f>0) ? f*n2l2mu[muFS::fs2e2mu]*sqrt(pow(stat2l2mu[muFS::fs2e2mu]/n2l2mu[muFS::fs2e2mu],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2mu[muFS::fs2e2mu]*f : 0);
	}
      }
    }
  }

  //for BDT categories it's the other way around, start with final states and add to get 2l2mu and 2l2e
  for(auto ptype : allProdTypes){
    if(!toString(ptype).Contains("BDT")) continue;
    float nbdt2l2mu=0,nbdt2l2e=0,nbdt4l=0,nbdt2l2mustat=0,nbdt2l2musyst=0,nbdt2l2estat=0,nbdt2l2esyst=0,nbdt4lstat=0,nbdt4lsyst=0;
    for(auto etype : allEventTypes){
      float f=0, ferr=0, fsyst=0;
      if(etype<EventType::_2l2mu){
	getBinContents( etype, ptype, f, ferr, fsyst, vbkgFrac );

	float nincl=0,systincl=0,statincl=0;
	if(etype==EventType::_4mu){
	  nincl=n2l2mu[muFS::fs4mu];
	  statincl=stat2l2mu[muFS::fs4mu];
	  systincl=syst2l2mu[muFS::fs4mu];
	}
	else if(etype==EventType::_2e2mu){
	  nincl=n2l2mu[muFS::fs2e2mu];
	  statincl=stat2l2mu[muFS::fs2e2mu];
          systincl=syst2l2mu[muFS::fs2e2mu];
	}
	else if(etype==EventType::_4e){
	  nincl=n2l2e[elFS::fs4e];
	  statincl=stat2l2e[elFS::fs4e];
          systincl=syst2l2e[elFS::fs4e];
	}
        else if(etype==EventType::_2mu2e){
	  nincl=n2l2e[elFS::fs2mu2e];
	  statincl=stat2l2e[elFS::fs2mu2e];
          systincl=syst2l2e[elFS::fs2mu2e];
	}
	float nCateg = nincl*f;
	float relsystCateg = (nCateg>0) ? systincl/nincl : 0;
	float systCateg = nCateg* relsystCateg;
	float relstatCateg = (nCateg>0) ? sqrt(pow(statincl/nincl,2)+pow(ferr/f,2) ) : 0;
	float statCateg=nCateg*relstatCateg;
	cout<<"f: "<<f<<" statIncl: "<<statincl<<"; ferr: "<<ferr<<endl;
	setBackgroundEstimation( etype, ptype, nCateg, statCateg, systCateg );
        cout << toString(etype) << " " << toString(ptype) << " " << nCateg << " +- " << statCateg << " (stat) +- " << systCateg << " (syst)" << endl;
	if(etype==EventType::_4e || etype==EventType::_2mu2e){
	  nbdt2l2e+=nCateg;
	  nbdt2l2estat+=pow(statCateg,2);
	  nbdt2l2esyst+=pow(systCateg,2);
	}
	else{
	  nbdt2l2mu+=nCateg;
          nbdt2l2mustat+=pow(statCateg,2);
          nbdt2l2musyst+=pow(systCateg,2);
	}
	nbdt4l+=nCateg;
	nbdt4lstat+=pow(statCateg,2);
	nbdt4lsyst+=pow(systCateg,2);
      }
      else if(etype==EventType::_2l2mu) setBackgroundEstimation( etype, ptype, nbdt2l2mu, sqrt(nbdt2l2mustat), sqrt(nbdt2l2musyst) );
      else if(etype==EventType::_2l2e) setBackgroundEstimation( etype, ptype, nbdt2l2e, sqrt(nbdt2l2estat), sqrt(nbdt2l2esyst) );
      else if(etype==EventType::_4l) setBackgroundEstimation( etype, ptype, nbdt4l, sqrt(nbdt4lstat), sqrt(nbdt4lsyst) );
    }
  }

  //add llee,llmumu to 4l
  for (auto ptype: allProdTypes) {
    if(!toString(ptype).Contains("BDT")){
      float n1=0,stat1=0,syst1=0; getBackgroundEstimation( EventType::_2l2mu, ptype, n1, stat1, syst1);
      float n2=0,stat2=0,syst2=0; getBackgroundEstimation( EventType::_2l2e,  ptype, n2, stat2, syst2);
      cout<<toString(ptype)<<" 2l2mu: "<<n1<<"+-"<<stat1*n1<<"+-"<<syst1*n1<<"; 2l2e: "<<n2<<"+-"<<stat2*n2<<"+-"<<syst2*n2<<endl;
      setBackgroundEstimation( EventType::_4l, ptype, n1+n2, stat1*n1+stat2*n2, sqrt(pow(syst1*n1,2)+pow(syst2*n2,2)) );
    }
  }

  //VHlep is negative for llee, set it to llmumu value+extra systematic to account for difference between llee and llmumu
  float ince,incmu,tstat,tsyst,vhlep_mu,vhlep_mu_stat,vhlep_mu_syst;
  getBackgroundEstimation(EventType::_2l2e,ProdType::prodInclusive,ince,tstat,tsyst);
  getBackgroundEstimation(EventType::_2l2mu,ProdType::prodInclusive,incmu,tstat,tsyst);
  getBackgroundEstimation(EventType::_2l2mu,ProdType::_VHlep,vhlep_mu,vhlep_mu_stat,vhlep_mu_syst);
  setBackgroundEstimation(EventType::_2l2e,ProdType::_VHlep,vhlep_mu,vhlep_mu_stat*vhlep_mu,sqrt(pow(vhlep_mu_syst*vhlep_mu,2)+pow(vhlep_mu*(ince-incmu)/incmu,2)));
  setBackgroundEstimation(EventType::_4l,ProdType::_VHlep,2*vhlep_mu,2*vhlep_mu_stat*vhlep_mu,sqrt(pow(2*vhlep_mu_syst*vhlep_mu,2)+pow(vhlep_mu*(ince-incmu)/incmu,2)));
  getBackgroundEstimation(EventType::_2l2mu,ProdType::_SB_VHLep,vhlep_mu,vhlep_mu_stat,vhlep_mu_syst);
  setBackgroundEstimation(EventType::_2l2e,ProdType::_SB_VHLep,vhlep_mu,vhlep_mu_stat*vhlep_mu,sqrt(pow(vhlep_mu_syst*vhlep_mu,2)+pow(vhlep_mu*(ince-incmu)/incmu,2)));
  setBackgroundEstimation(EventType::_4l,ProdType::_SB_VHLep,2*vhlep_mu,2*vhlep_mu_stat*vhlep_mu,sqrt(pow(2*vhlep_mu_syst*vhlep_mu,2)+pow(vhlep_mu*(ince-incmu)/incmu,2)));

  //////////////////////////////////////////////////////////////////
  ///categories with low stats
  // VHlep;
  //     fakes = 0.021 +- 0.027, HF = 0 +- 0.01 -> llee   = 0.021 +- 0.027
  //     Zjets = 0.008 +- 0.008, tt = 0 +- 0.01 -> llmumu = 0.008 +- 0.013
  //     -> 4l = 0.03 +- 0.03
  /*
  float n4l = 0.03, nerr4l = 0.03;
  setBackgroundEstimation( EventType::_4l, ProdType::_VHlep, n4l, 0, nerr4l );
  setBackgroundEstimation( EventType::_2l2e, ProdType::_VHlep, n4l/2., 0, nerr4l/sqrt(2) );
  setBackgroundEstimation( EventType::_2l2mu, ProdType::_VHlep, n4l/2., 0, nerr4l/sqrt(2) );
  setBackgroundEstimation( EventType::_2l2mu, ProdType::_ttHLep, n4l/2., 0, nerr4l/sqrt(2) );
  */
  //////////////////////////////////////////////////////////////////

  
  h_n->Draw("text");

  //normalize to 1/fb
  h_n->Scale(1./lumi);
  
  TFile *fout = TFile::Open(Form("data/normalizations_SR_%s.root",year[yr].c_str()),"recreate");
  h_n->Write();
  h_stat->Write();
  h_syst->Write();
  fout->Close();

  //now, repeat for HM
  std::cout<<"now HM"<<std::endl;
  vector<ProdTypeHM> allProdTypesHM = getProdTypesHM();
  static const unsigned int nProdTypesHM = allProdTypesHM.size();
  h_n = new TH2F("h_n_hm","", nProdTypesHM, 0,nProdTypesHM , nEventTypes,0,nEventTypes);
  ii=0;
  for (auto type : allProdTypesHM)
    h_n->GetXaxis()->SetBinLabel( ++ii , toString( type ).Data() );
  ii=0;
  for (auto type : allEventTypes)
    h_n->GetYaxis()->SetBinLabel( ++ii, toString( type ).Data() );

  h_stat = (TH2F*) h_n->Clone("h_hm_stat");
  h_syst = (TH2F*) h_n->Clone("h_hm_syst");

  TH2F* h_bkgFrac_HM = (TH2F*)f->Get("h_bkgFracHM");
  TH2F* h_bkgFracStatErr_HM = (TH2F*)f->Get("h_bkgFracStatErrHM");
  vector<TH2F*> vbkgFrac_HM({h_bkgFrac_HM,h_bkgFracStatErr_HM});
  if (h_bkgFrac_HM->GetXaxis()->GetNbins()!=nProdTypesHM || h_bkgFrac_HM->GetXaxis()->GetNbins()!=nProdTypesHM) {
    cout << "Wrong size for vector frac2l2e " << nProdTypesHM << " " << h_bkgFrac_HM->GetXaxis()->GetNbins() << endl;
    exit(0);
  }
  for (int ichan=0; ichan<2; ichan++){
    for (auto ptype: allProdTypesHM) {
      float f=0, ferr=0;
      getBinContentsHM( channels[ichan], ptype, f, ferr, vbkgFrac_HM );

      float nCateg = nIncl[ichan]*f;
      float relsystCateg = (nCateg>0) ? systIncl[ichan]/nIncl[ichan] : 0;
      float systCateg = nCateg* relsystCateg;
      float relstatCateg = (nCateg>0) ? sqrt(pow(statIncl[ichan]/nIncl[ichan],2)+pow(ferr/f,2) ) : 0;
      float statCateg=nCateg*relstatCateg;
      cout<<"f: "<<f<<" statIncl: "<<statIncl[ichan]<<"; ferr: "<<ferr<<endl;

      setBackgroundEstimationHM( channels[ichan], ptype, nCateg, statCateg, systCateg );
      cout << toString(channels[ichan]) << " " << toString(ptype) << " " << nCateg << " +- " << statCateg << " (stat) +- " << systCateg << " (syst)" << endl;
      if (channels[ichan]==EventType::_2l2e) {
        setBackgroundEstimationHM( EventType::_4e, ptype, f*n2l2e[elFS::fs4e], (f>0) ? f*n2l2e[elFS::fs4e]*sqrt(pow(stat2l2e[elFS::fs4e]/n2l2e[elFS::fs4e],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2e[elFS::fs4e]*f : 0);
	setBackgroundEstimationHM( EventType::_2mu2e, ptype, f*n2l2e[elFS::fs2mu2e], (f>0) ? f*n2l2e[elFS::fs2mu2e]*sqrt(pow(stat2l2e[elFS::fs2mu2e]/n2l2e[elFS::fs2mu2e],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2e[elFS::fs2mu2e]*f : 0);
      } else {
        setBackgroundEstimationHM( EventType::_4mu, ptype, f*n2l2mu[muFS::fs4mu], (f>0) ? f*n2l2mu[muFS::fs4mu]*sqrt(pow(stat2l2mu[muFS::fs4mu]/n2l2mu[muFS::fs4mu],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2mu[muFS::fs4mu]*f : 0);
        setBackgroundEstimationHM( EventType::_2e2mu, ptype, f*n2l2mu[muFS::fs2e2mu], (f>0) ? f*n2l2mu[muFS::fs2e2mu]*sqrt(pow(stat2l2mu[muFS::fs2e2mu]/n2l2mu[muFS::fs2e2mu],2)+pow(ferr/f,2)) : 0, (f>0) ? syst2l2mu[muFS::fs2e2mu]*f : 0);
      }
    }
  }

  //add llee,llmumu to 4l
  for (auto ptype: allProdTypesHM) {
    float n1=0,stat1=0,syst1=0; getBackgroundEstimationHM( EventType::_2l2mu, ptype, n1, stat1, syst1);
    float n2=0,stat2=0,syst2=0; getBackgroundEstimationHM( EventType::_2l2e,  ptype, n2, stat2, syst2);
    setBackgroundEstimationHM( EventType::_4l, ptype, n1+n2, stat1*n1+stat2*n2, sqrt(pow(syst1*n1,2)+pow(syst2*n2,2)) );  
  }

  h_n->Draw("text");

  //normalize to 1/fb
  h_n->Scale(1./lumi);

  TFile *fouthm = TFile::Open(Form("data/normalizations_HM_%s.root",year[yr].c_str()),"recreate");
  h_n->Write();
  h_stat->Write();
  h_syst->Write();
  fouthm->Close();
}

void setBackgroundEstimation( EventType event_type, ProdType prod_type, float n, float stat, float syst )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  cout<<"set estimate for "<<toString(event_type)<<", "<<toString(prod_type)<<" in bin "<<i<<","<<j<<" to "<<n<<endl;
  if (h_n->GetBinContent(i,j)!=0) cout << "WARNING! Overwriting bin content "<<i<<" , "<<j<<""<<toString(event_type) << " " << toString(prod_type) << " " << h_n->GetBinContent(i,j) << endl;
  h_n->SetBinContent( i, j, n );
  if(n!=0){
    h_stat->SetBinContent( i, j, stat/n );
    h_syst->SetBinContent( i, j, syst/n );
  }
  else{
    h_stat->SetBinContent( i, j, 0 );
    h_syst->SetBinContent( i, j, 0 );
  }
  //cout<<"set background estimate for "<<toString(event_type)<<","<<toString(prod_type)<<" to "<<h_n->GetBinContent( i, j)<<"+-"<<h_stat->GetBinContent( i, j)*100<<"%+-"<<h_syst->GetBinContent( i, j)*100<<"%"<<endl;
  
  return;
}
 
void getBackgroundEstimation( EventType event_type, ProdType prod_type, float &n, float &stat, float &syst )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  n = h_n->GetBinContent( i, j);
  stat = h_stat->GetBinContent( i, j);
  syst = h_syst->GetBinContent( i, j);
}

void setBackgroundEstimationHM( EventType event_type, ProdTypeHM prod_type, float n, float stat, float syst )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  if (h_n->GetBinContent(i,j)!=0) cout << "WARNING! Overwriting bin content " <<i<<" , "<<j<< " "<<toString(event_type)<<" "<< toString(prod_type) << " " << h_n->GetBinContent(i,j) << endl;
  h_n->SetBinContent( i, j, n );
  if(n!=0){
    h_stat->SetBinContent( i, j, stat/n );
    h_syst->SetBinContent( i, j, syst/n );
  }
  else{
    h_stat->SetBinContent( i, j, 0 );
    h_syst->SetBinContent( i, j, 0 );
  }

return;
}

void getBackgroundEstimationHM( EventType event_type, ProdTypeHM prod_type, float &n, float &stat, float &syst )
{
  int i = static_cast<int>(prod_type) +1;
  int j = static_cast<int>(event_type) +1;
  n = h_n->GetBinContent( i, j);
  stat = h_stat->GetBinContent( i, j);
  syst = h_syst->GetBinContent( i, j);
}
