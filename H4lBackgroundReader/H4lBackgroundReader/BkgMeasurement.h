#ifndef BKGMEASUREMENT_H
#define BKGMEASUREMENT_H

#include "H4lBackgroundReader/H4lBkgEnums.h"

#include "TH1F.h"
#include "TString.h"

#include <vector>

class Measurement
{
 public:
  Measurement(float N, float statErr, float systErr, float luminosity ) { n = N, stat = statErr, syst = systErr, lumi=luminosity; }  
  Measurement(){n=1,stat=0,syst=0,lumi=1;}
  float n; //normalized to 1/fb
  float stat; //relative err
  float syst; //relative err
  float lumi; //bkg measurement has to know its lumi
  inline TString getString() { return Form( "%g +- %g (stat) +- %g (syst)", n, stat, syst ); }
  inline TString getStringTex() { return Form( "%g \\pm %g \\pm %g", n, stat, syst ); }
};

class BkgMeasurement;

BkgMeasurement* Add(std::vector<BkgMeasurement*> bs, H4lBkg::Category::ProdType ptype, H4lBkg::Channel::EventType evtype, bool sameYear);
BkgMeasurement* Add(BkgMeasurement* b1,BkgMeasurement* b2, H4lBkg::Category::ProdType ptype, H4lBkg::Channel::EventType evtype, bool sameYear);
BkgMeasurement* Add(std::vector<BkgMeasurement*> bs, H4lBkg::Category::ProdTypeHM ptype, H4lBkg::Channel::EventType evtype, bool sameYear);
BkgMeasurement* Add(BkgMeasurement* b1,BkgMeasurement* b2, H4lBkg::Category::ProdTypeHM ptype, H4lBkg::Channel::EventType evtype, bool sameYear);

class BkgMeasurement
{
 public:
  BkgMeasurement() {}
  ~BkgMeasurement();
  
  BkgMeasurement( Measurement m, TH1F* n, TH1F* relstat, TH1F* syst_up, TH1F* syst_dn, TString obs, H4lBkg::Category::ProdType ptype, H4lBkg::Channel::EventType evtype);
  BkgMeasurement( Measurement m, TH1F* n, TH1F* relstat, TH1F* syst_up, TH1F* syst_dn, TString obs, H4lBkg::Category::ProdTypeHM ptype, H4lBkg::Channel::EventType evtype);

  inline TH1F* getHistShape() { return m_h_shape; }
  inline TH1F* getHistRelStatErr() { return m_h_relstat; }
  inline TH1F* getHistSystErrUp() { return m_h_relsyst_up; }
  inline TH1F* getHistSystErrDn() { return m_h_relsyst_dn; }

  /*  
  inline TH1F* getHistYieldsWithStatErrors() { return getHistYieldsWithErrors(true); }
  inline TH1F* getHistYieldsWithSystErrors() { return getHistYieldsWithErrors(false); }
  TH1F* getHistYieldsWithErrors(bool isStat);
  TH1F* getHistYieldsWithTotalErrors();
  */
  inline H4lBkg::Channel::EventType getEventType() { return m_event_type; }
  inline H4lBkg::Category::ProdType getProdType() { return m_prod_type; }
  inline H4lBkg::Category::ProdTypeHM getProdTypeHM() { return m_prod_type_hm; }
  inline TString getObservable() { return m_observable; }

  inline void setNorm(Measurement m) { norm=Measurement(m.n,m.stat,m.syst,m.lumi); }
  inline Measurement getNorm() { return Measurement(norm.n,norm.stat,norm.syst,norm.lumi); }
  inline void setNorm(float n,float stat,float syst, float lumi) { Measurement newNorm(n,stat,syst,lumi); norm=newNorm; }
  
  void print(); //print all numbers
  
 private:
  H4lBkg::Channel::EventType m_event_type;
  H4lBkg::Category::ProdType m_prod_type;
  H4lBkg::Category::ProdTypeHM m_prod_type_hm;
  TString m_observable;
  int m_nbins;

  //input histograms
  TH1F* m_h_shape; //distribution of observable; normalized to unit area, no errors
  TH1F* m_h_relstat; //shape relative stat variation
  TH1F* m_h_relsyst_up; //up variation of shape syst (defined as increasing HF fraction)
  TH1F* m_h_relsyst_dn; //dn variation of shape syst (defined as decreasing HF fraction)

  //histograms derived from input
  TH1F* m_h_n_wStatError;
  TH1F* m_h_n_wSystError;
  TH1F* m_h_n_wTotError;  

  Measurement norm;

};

#endif
