#ifndef H4LBKGENUMS_H
#define H4LBKGENUMS_H

#include "TString.h"
#include <vector>

namespace H4lBkg
{
  namespace Channel {
    enum class EventType{ _4mu=0, _4e, _2mu2e, _2e2mu, _2l2mu, _2l2e, _4l, Max};
    
    inline TString toString(Channel::EventType evt){
      switch ( evt ) {
      case Channel::EventType::_4mu: return "4mu";
      case Channel::EventType::_4e: return "4e";
      case Channel::EventType::_2e2mu: return "2e2mu";
      case Channel::EventType::_2mu2e: return "2mu2e";
      case Channel::EventType::_2l2mu: return "2l2mu";
      case Channel::EventType::_2l2e: return "2l2e";
      case Channel::EventType::_4l: return "4l";
      case Channel::EventType::Max: return "ReachedMax";
      }
      return "ERROR";	
    }
  }
  namespace Category {
    enum class ProdType{ _0Jet=0, _1Jet, _2Jet, _ge1Jet, _ge2Jet, _ge3Jet, _le1Jet,
	_y4l0, _y4l1, _y4l2, _y4l3,
	_jet1pt0, _jet1pt1, _jet1pt2,
	_0JetPt4l10, _0JetPt4l100, _0JetPt4lgt100,
	_1JetPt4l60_b1, _1JetPt4l60_b2, _1JetPt4l60120_b1, _1JetPt4l60120_b2, _1JetPt4l120200, _1JetPt4lgt200,
	_2JetVBFPtgt200,
	_2JetVHVBFPt200_b1, _2JetVHVBFPt200_b2,
	_VHlep, _ttHLep, _ttHHad_b1, _ttHHad_b2,
	_SB_0jet, _SB_1jet, _SB_2jet, _SB_ttH, _SB_VHLep,
	_BDT0, _BDT1, _BDT2, _BDT3,
	prodInclusive, Max};
    
    inline TString toString(Category::ProdType prt){
      switch ( prt ) {
      case Category::ProdType::_0Jet: return "0Jet";
      case Category::ProdType::_1Jet: return "1Jet";
      case Category::ProdType::_2Jet: return "2Jet";
      case Category::ProdType::_ge1Jet: return "ge1Jet";
      case Category::ProdType::_ge2Jet: return "ge2Jet";
      case Category::ProdType::_ge3Jet: return "ge3Jet";
      case Category::ProdType::_le1Jet: return "le1Jet";
      case Category::ProdType::_y4l0: return "y4l0";
      case Category::ProdType::_y4l1: return "y4l1";
      case Category::ProdType::_y4l2: return "y4l2";
      case Category::ProdType::_y4l3: return "y4l3";
      case Category::ProdType::_jet1pt0: return "jet1pt0";
      case Category::ProdType::_jet1pt1: return "jet1pt1";
      case Category::ProdType::_jet1pt2: return "jet1pt2";
      case Category::ProdType::_0JetPt4l10: return "0Jet_Pt4l_0_10";
      case Category::ProdType::_0JetPt4l100: return "0Jet_Pt4l_10_100";
      case Category::ProdType::_0JetPt4lgt100: return "0Jet_Pt4l_GE100";
      case Category::ProdType::_1JetPt4l60_b1: return "1Jet_Pt4l_0_60_b1";
      case Category::ProdType::_1JetPt4l60_b2: return "1Jet_Pt4l_0_60_b2";
      case Category::ProdType::_1JetPt4l60120_b1: return "1Jet_Pt4l_60_120_b1";
      case Category::ProdType::_1JetPt4l60120_b2: return "1Jet_Pt4l_60_120_b2";
      case Category::ProdType::_1JetPt4l120200: return "1Jet_Pt4l_120_200";
      case Category::ProdType::_1JetPt4lgt200: return "1Jet_Pt4l_GE200";
      case Category::ProdType::_2JetVBFPtgt200: return "2Jet_VBF_ptGE200";
      case Category::ProdType::_2JetVHVBFPt200_b1: return "2Jet_b1";
      case Category::ProdType::_2JetVHVBFPt200_b2: return "2Jet_b2";
      case Category::ProdType::_VHlep: return "VHLep";
      case Category::ProdType::_ttHLep: return "ttH_leptonic";
      case Category::ProdType::_ttHHad_b1: return "ttH_hadronic_b1";
      case Category::ProdType::_ttHHad_b2: return "ttH_hadronic_b2";
      case Category::ProdType::_SB_0jet: return "SB_0jet";
      case Category::ProdType::_SB_1jet: return "SB_1jet";
      case Category::ProdType::_SB_2jet: return "SB_2jet";
      case Category::ProdType::_SB_VHLep: return "SB_VHLep";
      case Category::ProdType::_SB_ttH: return "SB_ttH";
      case Category::ProdType::_BDT0: return "BDT0";
      case Category::ProdType::_BDT1: return "BDT1";
      case Category::ProdType::_BDT2: return "BDT2";
      case Category::ProdType::_BDT3: return "BDT3";
      case Category::ProdType::prodInclusive: return "incl";
      case Category::ProdType::Max : return "ReachedMax";
      }
      return "ERROR";
    }
    inline TString toStringLatex(Category::ProdType prt){
      switch ( prt ) {
      case Category::ProdType::_0Jet: return "0Jet";
      case Category::ProdType::_1Jet: return "1Jet";
      case Category::ProdType::_2Jet: return "2Jet";
      case Category::ProdType::_ge1Jet: return "ge1Jet";
      case Category::ProdType::_ge2Jet: return "ge2Jet";
      case Category::ProdType::_ge3Jet: return "ge3Jet";
      case Category::ProdType::_le1Jet: return "le1Jet";
      case Category::ProdType::_y4l0: return "|y_{4l}|<0.5";
      case Category::ProdType::_y4l1: return "0.5<|y_{4l}|<1.0";
      case Category::ProdType::_y4l2: return "1.0<|y_{4l}|<1.5";
      case Category::ProdType::_y4l3: return "1.5<|y_{4l}|<2.5";
      case Category::ProdType::_jet1pt0: return "0-jet or p_{T}^{j1}<60 GeV";
      case Category::ProdType::_jet1pt1: return "60< p_{T}^{j1}<120 GeV";
      case Category::ProdType::_jet1pt2: return "120< p_{T}^{j1}<350 GeV";
      case Category::ProdType::_0JetPt4l10: return "0-jet, 0<p_{T}^{4l}<10 GeV";
      case Category::ProdType::_0JetPt4l100: return "0-jet, 10<p_{T}^{4l}<100 GeV";
      case Category::ProdType::_0JetPt4lgt100: return "0-jet, p_{T}^{4l}>100 GeV";
      case Category::ProdType::_1JetPt4l60_b1: return "1-jet, 0<p_{T}^{4l}<60 GeV, bin 1";
      case Category::ProdType::_1JetPt4l60_b2: return "1-jet, 0<p_{T}^{4l}<60 GeV, bin 2";
      case Category::ProdType::_1JetPt4l60120_b1: return "1-jet, 60<p_{T}^{4l}<120 GeV, bin 1";
      case Category::ProdType::_1JetPt4l60120_b2: return "1-jet, 60<p_{T}^{4l}<120 GeV, bin 2";
      case Category::ProdType::_1JetPt4l120200: return "1-jet, 120<p_{T}^{4l}<200 GeV";
      case Category::ProdType::_1JetPt4lgt200: return "1-jet, p_{T}^{4l}>200 GeV";
      case Category::ProdType::_2JetVHVBFPt200_b1: return "2-jet bin 1";
      case Category::ProdType::_2JetVHVBFPt200_b2: return "2-jet bin 2";
      case Category::ProdType::_2JetVBFPtgt200: return "2-jet VBF-enriched, p_{T}^{j1}>200 GeV";
      case Category::ProdType::_VHlep: return "VH-leptonic";
      case Category::ProdType::_ttHLep: return "t\\bar{t}H-leptonic";
      case Category::ProdType::_ttHHad_b1: return "t\\bar{t}H-hadronic, bin 1";
      case Category::ProdType::_ttHHad_b2: return "t\\bar{t}H-hadronic, bin 2";
      case Category::ProdType::_SB_0jet: return "0-jet sideband";
      case Category::ProdType::_SB_1jet: return "1-jet sideband";
      case Category::ProdType::_SB_2jet: return "2-jet sideband";
      case Category::ProdType::_SB_VHLep: return "VH-leptonic sideband";
      case Category::ProdType::_SB_ttH: return "t\\bar{t}H-enriched sideband";
      case Category::ProdType::_BDT0: return "Mass BDT bin 0";
      case Category::ProdType::_BDT1: return "Mass BDT bin 1";
      case Category::ProdType::_BDT2: return "Mass BDT bin 2";
      case Category::ProdType::_BDT3: return "Mass BDT bin 3";
      case Category::ProdType::prodInclusive: return "incl";
      case Category::ProdType::Max : return "ReachedMax";
      }
      return "ERROR";
    }

    enum class ProdTypeHM{ _ggF=0, _VBF, Max};
    inline TString toString(Category::ProdTypeHM prt){
      switch ( prt ) {
      case Category::ProdTypeHM::_ggF: return "ggF";
      case Category::ProdTypeHM::_VBF: return "VBF";
      case Category::ProdTypeHM::Max : return "ReachedMax";
      }
      return "ERROR";	
    }
  }
  namespace Observable {
    enum class Variable{pt4l=0, m4lSR, m4l, m4lHM, m12, m34, m12m34, cosThetaStar, cth1, cth2, phi, phi1, y4l, njets, nbjets, jet1pt, mjj, detajj, dphijj, bdt, NN, dummy, 
	//BDT_discriminant, BDT_1Jet_pt4l_60, BDT_1Jet_pt4l_60_120, BDT_VH_noptHjj_discriminant, BDT_TwoJet_discriminant,
	m4lHFRelaxIsoD0unconstrained,	m4lfakesRelaxIsoD0unconstrained, m4lZjetsRelaxIsoD0unconstrained, m4lttbarRelaxIsoD0unconstrained,
	mZ1HFRelaxIsoD0unconstrained,	mZ1fakesRelaxIsoD0unconstrained, mZ1ZjetsRelaxIsoD0unconstrained, mZ1ttbarRelaxIsoD0unconstrained,
	mZ2HFRelaxIsoD0unconstrained,	mZ2fakesRelaxIsoD0unconstrained, mZ2ZjetsRelaxIsoD0unconstrained, mZ2ttbarRelaxIsoD0unconstrained,
	m4lHFRelaxIsoD0fsr,	m4lfakesRelaxIsoD0fsr, m4lZjetsRelaxIsoD0fsr, m4lttbarRelaxIsoD0fsr,
	mZ1HFRelaxIsoD0fsr,	mZ1fakesRelaxIsoD0fsr, mZ1ZjetsRelaxIsoD0fsr, mZ1ttbarRelaxIsoD0fsr,
	mZ2HFRelaxIsoD0fsr,	mZ2fakesRelaxIsoD0fsr, mZ2ZjetsRelaxIsoD0fsr, mZ2ttbarRelaxIsoD0fsr,
	Max};
    
    inline TString toString(Observable::Variable obs){
      switch ( obs ) {
      case Observable::Variable::pt4l: return "pt4l";
      case Observable::Variable::m4lSR: return "m4ldiffxs";
      case Observable::Variable::m4l: return "m4l";
      case Observable::Variable::m4lHM: return "m4lHM";
      case Observable::Variable::m12: return "m12";
      case Observable::Variable::m34: return "m34";
      case Observable::Variable::m12m34: return "m12m34";
      case Observable::Variable::cosThetaStar: return "cts";
      case Observable::Variable::cth1: return "cth1";
      case Observable::Variable::cth2: return "cth2";
      case Observable::Variable::phi1: return "phi1";
      case Observable::Variable::phi: return "phi";
      case Observable::Variable::y4l: return "y4l";
      case Observable::Variable::njets: return "njets";
      case Observable::Variable::nbjets: return "nbjets";
      case Observable::Variable::jet1pt: return "jet1pt";
      case Observable::Variable::mjj: return "mjj";
      case Observable::Variable::detajj: return "detajj";
      case Observable::Variable::dphijj: return "dphijj";
      case Observable::Variable::bdt: return "bdt";
      case Observable::Variable::NN: return "NN";
      case Observable::Variable::dummy: return "dummy";

	//case Observable::Variable::BDT_discriminant: return "BDT_discriminant";
	//case Observable::Variable::BDT_1Jet_pt4l_60: return "BDT_1Jet_pt4l_60";
	//case Observable::Variable::BDT_1Jet_pt4l_60_120: return "BDT_1Jet_pt4l_60_120";
	//case Observable::Variable::BDT_VH_noptHjj_discriminant: return "BDT_VH_noptHjj_discriminant";
	//case Observable::Variable::BDT_TwoJet_discriminant: return "BDT_TwoJet_discriminant";

      case Observable::Variable::m4lHFRelaxIsoD0unconstrained: return "m4lHFRelaxIsoD0unconstrained";
      case Observable::Variable::m4lfakesRelaxIsoD0unconstrained: return "m4lfakesRelaxIsoD0unconstrained";
      case Observable::Variable::m4lZjetsRelaxIsoD0unconstrained: return "m4lZjetsRelaxIsoD0unconstrained";
      case Observable::Variable::m4lttbarRelaxIsoD0unconstrained: return "m4lttbarRelaxIsoD0unconstrained";

      case Observable::Variable::mZ1HFRelaxIsoD0unconstrained: return "mZ1HFRelaxIsoD0unconstrained";
      case Observable::Variable::mZ1fakesRelaxIsoD0unconstrained: return "mZ1fakesRelaxIsoD0unconstrained";
      case Observable::Variable::mZ1ZjetsRelaxIsoD0unconstrained: return "mZ1ZjetsRelaxIsoD0unconstrained";
      case Observable::Variable::mZ1ttbarRelaxIsoD0unconstrained: return "mZ1ttbarRelaxIsoD0unconstrained";

      case Observable::Variable::mZ2HFRelaxIsoD0unconstrained: return "mZ2HFRelaxIsoD0unconstrained";
      case Observable::Variable::mZ2fakesRelaxIsoD0unconstrained: return "mZ2fakesRelaxIsoD0unconstrained";
      case Observable::Variable::mZ2ZjetsRelaxIsoD0unconstrained: return "mZ2ZjetsRelaxIsoD0unconstrained";
      case Observable::Variable::mZ2ttbarRelaxIsoD0unconstrained: return "mZ2ttbarRelaxIsoD0unconstrained";

      case Observable::Variable::m4lHFRelaxIsoD0fsr: return "m4lHFRelaxIsoD0fsr";
      case Observable::Variable::m4lfakesRelaxIsoD0fsr: return "m4lfakesRelaxIsoD0fsr";
      case Observable::Variable::m4lZjetsRelaxIsoD0fsr: return "m4lZjetsRelaxIsoD0fsr";
      case Observable::Variable::m4lttbarRelaxIsoD0fsr: return "m4lttbarRelaxIsoD0fsr";

      case Observable::Variable::mZ1HFRelaxIsoD0fsr: return "mZ1HFRelaxIsoD0fsr";
      case Observable::Variable::mZ1fakesRelaxIsoD0fsr: return "mZ1fakesRelaxIsoD0fsr";
      case Observable::Variable::mZ1ZjetsRelaxIsoD0fsr: return "mZ1ZjetsRelaxIsoD0fsr";
      case Observable::Variable::mZ1ttbarRelaxIsoD0fsr: return "mZ1ttbarRelaxIsoD0fsr";

      case Observable::Variable::mZ2HFRelaxIsoD0fsr: return "mZ2HFRelaxIsoD0fsr";
      case Observable::Variable::mZ2fakesRelaxIsoD0fsr: return "mZ2fakesRelaxIsoD0fsr";
      case Observable::Variable::mZ2ZjetsRelaxIsoD0fsr: return "mZ2ZjetsRelaxIsoD0fsr";
      case Observable::Variable::mZ2ttbarRelaxIsoD0fsr: return "mZ2ttbarRelaxIsoD0fsr";
	
      case Observable::Variable::Max : return "ReachedMax";
      }
      return "ERROR";	
    }
  }
  namespace BkgSample {
    enum class BkgComponent{ttbar=0,Zjets,fakes,gamma,HF,Max};
    inline TString toString(BkgSample::BkgComponent bkg){
      switch (bkg) {
      case BkgSample::BkgComponent::ttbar: return "ttbar";
      case BkgSample::BkgComponent::Zjets: return "Zjets";
      case BkgSample::BkgComponent::fakes: return "light";
      case BkgSample::BkgComponent::gamma: return "gamma";
      case BkgSample::BkgComponent::HF: return "HF";
      case BkgSample::BkgComponent::Max: return "ReachedMax";
      }
      return "ERROR";
    }
  }
  
  inline int toInt(Channel::EventType evt) { return static_cast<int>(evt); }
  inline int toInt(Category::ProdType prt) { return static_cast<int>(prt); }
  inline int toInt(Category::ProdTypeHM prt) { return static_cast<int>(prt); }
  inline int toInt(Observable::Variable obs) { return static_cast<int>(obs); }
  inline int toInt(BkgSample::BkgComponent bkg) { return static_cast<int>(bkg); }
  

  inline std::vector<TString> getEventTypesStr() {
    std::vector<TString> v;
    for (int i=0; i<static_cast<int>(Channel::EventType::Max); i++) v.push_back(toString(static_cast<Channel::EventType>(i)).Data());
    return v;
  }
  inline std::vector<TString> getProdTypesStr() {
    std::vector<TString> v;
    for (int i=0; i<static_cast<int>(Category::ProdType::Max); i++) v.push_back(toString(static_cast<Category::ProdType>(i)).Data());
    return v;
  }
  inline std::vector<TString> getProdTypesHMStr() {
    std::vector<TString> v;
    for (int i=0; i<static_cast<int>(Category::ProdTypeHM::Max); i++) v.push_back(toString(static_cast<Category::ProdTypeHM>(i)).Data());
    return v;
  }
  inline std::vector<TString> getObservablesStr() {
    std::vector<TString> v;
    for (int i=0; i<static_cast<int>(Observable::Variable::Max); i++){
      //FIXME using only these variables for first 2017 analysis
      //if(static_cast<Observable::Variable>(i)!=Observable::Variable::pt4l && static_cast<Observable::Variable>(i)!=Observable::Variable::njets && static_cast<Observable::Variable>(i)!=Observable::Variable::m4l && static_cast<Observable::Variable>(i)!=Observable::Variable::bdt && static_cast<Observable::Variable>(i)!=Observable::Variable::m4lHM) continue;
      v.push_back(toString(static_cast<Observable::Variable>(i)).Data());
    }
    return v;
  }

  inline std::vector<Channel::EventType> getEventTypes() {
    std::vector<Channel::EventType> v;
    for (int i=0; i<static_cast<int>(Channel::EventType::Max); i++) v.push_back(static_cast<Channel::EventType>(i));
    return v;
  }

  inline std::vector<Category::ProdType> getProdTypes() {
    std::vector<Category::ProdType> v;
    for (int i=0; i<static_cast<int>(Category::ProdType::Max); i++) v.push_back(static_cast<Category::ProdType>(i));
    return v;
  }

  inline std::vector<Category::ProdTypeHM> getProdTypesHM() {
    std::vector<Category::ProdTypeHM> v;
    for (int i=0; i<static_cast<int>(Category::ProdTypeHM::Max); i++) v.push_back(static_cast<Category::ProdTypeHM>(i));
    return v;
  }

  inline std::vector<Observable::Variable> getObservables() {
    std::vector<Observable::Variable> v;
    for (int i=0; i<static_cast<int>(Observable::Variable::Max); i++) v.push_back(static_cast<Observable::Variable>(i));
    return v;
  }

  inline std::vector<BkgSample::BkgComponent> getComponents() {
    std::vector<BkgSample::BkgComponent> v;
    for (int i=0; i<static_cast<int>(BkgSample::BkgComponent::Max); i++) v.push_back(static_cast<BkgSample::BkgComponent>(i));
    return v;
  }

}

#endif
