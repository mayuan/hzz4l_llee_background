#ifndef H4LBACKGROUNDREADER_H
#define H4LBACKGROUNDREADER_H

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include <vector>
#include <map>

#include "H4lBackgroundReader/H4lBkgEnums.h"

class BkgMeasurement;

class H4lBackgroundReader
{
 public:
  H4lBackgroundReader(std::map<TString,float> lumi);
  ~H4lBackgroundReader();

  void loadShapes( TString pathToFile );
  void loadNorms( TString pathToFile );
  void loadNormsHM( TString pathToFile );
  void loadNormsRelax( TString pathToFile );

  //inline void setLumiInvFb( float L, TString year ) { m_lumi[year] = L; }

  BkgMeasurement* getBkgMeasurement( TString obs, H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, float m4lmin=0, float m4lmax=0, 
				     TString year="all", int shapeWindow=0, std::vector<H4lBkg::Category::ProdType>* m4lfracs=nullptr );
  BkgMeasurement* getBkgMeasurement( TString obs, H4lBkg::Category::ProdTypeHM prt, H4lBkg::Channel::EventType evt, float m4lmin=0, float m4lmax=0, TString year="all" );
  BkgMeasurement* getBkgMeasurementRelax( H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, H4lBkg::BkgSample::BkgComponent comp, TString year="all" );

  //to redefine the prefix of the histogram names
  inline void setNormHistNamePrefix(TString str) {m_sNormHistNamePrefix = str;}
  inline void setShapeHistNameN(TString str) {m_sHistName_n = str;}
  inline void setShapeHistNameStatErr(TString str) {m_sHistName_staterr = str;}
  inline void setShapeHistNameSystErr(TString str) {m_sHistName_systerr = str;}
  
 private:
  //internal methods
  BkgMeasurement* findShapeFromFile(TString obs, H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, 
				    TString year, int shapeWindow,float m4lmin=0, float m4lmax=0, std::vector<H4lBkg::Category::ProdType>* m4lfracs=nullptr);
  BkgMeasurement* findShapeFromFile(TString obs, H4lBkg::Category::ProdTypeHM prt, H4lBkg::Channel::EventType evt, TString year);
  void findNormFromHist(float&n, float& stat,float&syst, H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, TString year);
  void findNormFromHist(float&n, float& stat,float&syst, H4lBkg::Category::ProdTypeHM prt, H4lBkg::Channel::EventType evt, TString year);
  void findRelaxNormFromHist(float&n,float& stat,H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, H4lBkg::BkgSample::BkgComponent comp, TString year);
  BkgMeasurement* addShapesFromChannels(H4lBkg::Channel::EventType chan1, H4lBkg::Channel::EventType chan2, TString obs, H4lBkg::Category::ProdType prt,TString year, int shapeWindow, float m4lmin=0,float m4lmax=0);
  BkgMeasurement* addShapesFromChannels(H4lBkg::Channel::EventType chan1, H4lBkg::Channel::EventType chan2, TString obs, H4lBkg::Category::ProdTypeHM prt, TString year, float m4lmin=0, float m4lmax=0);
  float getFractionInM4lRegion(H4lBkg::Channel::EventType channel, H4lBkg::Category::ProdType category, float xmin, float xmax, TString year, int shapeWindow);
  float getFractionInM4lRegion(H4lBkg::Channel::EventType channel, H4lBkg::Category::ProdTypeHM category, float xmin, float xmax, TString year);
  BkgMeasurement* getDummyMeasurement(TString obs, H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt);
  BkgMeasurement* getDummyMeasurement(TString obs, H4lBkg::Category::ProdTypeHM prt, H4lBkg::Channel::EventType evt);
  BkgMeasurement* getDummyRelaxMeasurement(H4lBkg::Category::ProdType prt, H4lBkg::Channel::EventType evt, H4lBkg::BkgSample::BkgComponent comp);
  
  //members
  std::map<TString,float> m_lumi; //in fb^-1, maps to string for the year (or "all" for inclusive)
  
  std::vector<std::map<TString,TFile*> > m_file; //contains the hists for shapes, organized by mass windows, then years
  std::map<TString,TH2F*> m_h_norms; //the normalizations
  std::map<TString,TH2F*> m_h_norms_stat;
  std::map<TString,TH2F*> m_h_norms_syst;
  std::map<TString,TH2F*> m_h_norms_hm; //the HM normalizations
  std::map<TString,TH2F*> m_h_norms_stat_hm;
  std::map<TString,TH2F*> m_h_norms_syst_hm;
  std::map<TString,std::vector<TH2F*> > m_h_norms_relax; //Normalizations in relaxIsoD0
  std::map<TString,std::vector<TH2F*> > m_h_norms_stat_relax;

  //name prefix for norm histograms
  TString m_sNormHistNamePrefix;
  
  //name prefix for shape histograms
  TString m_sHistName_n;
  TString m_sHistName_staterr;
  TString m_sHistName_systerr;

  std::vector<TString> m_allProdTypes;
  std::vector<TString> m_allEventTypes;
  std::vector<TString> m_allObservables;

  int dummyCount;
  bool debug;

};

////////////////////////////////////////////////////////////////////


#endif
