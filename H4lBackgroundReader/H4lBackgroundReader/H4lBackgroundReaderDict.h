#ifndef H4LBACKGROUNDREADER_H4LBACKGROUNDREADERDICT_H
#define H4LBACKGROUNDREADER_H4LBACKGROUNDREADERDICT_H

// Reflex dictionary generation
// Following instructions on the CP tools twiki:
// https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/DevelopingCPToolsForxAOD

// Special handling for Eigen vectorization
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif

// classes
#include "H4lBackgroundReader/H4lBackgroundReader.h"
#include "H4lBackgroundReader/BkgMeasurement.h"
#include "H4lBackgroundReader/H4lBkgEnums.h"


#endif
