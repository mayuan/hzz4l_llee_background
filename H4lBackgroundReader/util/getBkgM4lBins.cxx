#include "H4lBackgroundReader/H4lBackgroundReader.h"
#include "H4lBackgroundReader/BkgMeasurement.h"
#include "H4lBackgroundReader/H4lBkgEnums.h"
using namespace H4lBkg; //for enums
using namespace Observable;
using namespace Category;
using namespace Channel;

#include <iostream>
using namespace std;


int main()
{
  H4lBackgroundReader bSR;
  bSR.loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_SignalRegion.root");
  bSR.loadNorms("$ROOTCOREBIN/../H4lBackgroundReader/data/normalizations_m4lFullRange_1ifb.root");
  bSR.setLumiInvFb(36.5,0);

  BkgMeasurement* m4l;
  m4l = bSR.getBkgMeasurement( "m4l", ProdType::prodInclusive,  EventType::_4l, 115, 130);
  //float n,stat,syst;
  Measurement meas=m4l->getNorm();
  cout << "Background, signal m4l range: \t\t " << meas.n << " +- " << meas.stat << " (stat.) +- " << meas.syst << " (syst.)" << endl;

  TH1F* h_m4l = m4l->getHistShape();
  h_m4l->Rebin(2); //rebin to 1 GeV
  h_m4l->GetXaxis()->SetRange(116,130.5); // zoom in 115-130
  h_m4l->Scale(meas.n/h_m4l->Integral());
  h_m4l->SaveAs("m4l_signalRegion.C"); //normalized to unit area

  // TH1F* h_relativeStatVariation_m4l = m4l->getHistRelStatErr();
  // TH1F* h_relativeSystVariation_m4l = m4l->getHistRelSystErr();

  delete m4l;
  return 0;
}
 
