#include "H4lBackgroundReader/H4lBackgroundReader.h"
#include "H4lBackgroundReader/BkgMeasurement.h"
#include "H4lBackgroundReader/H4lBkgEnums.h"
using namespace H4lBkg; //for enums
using namespace Observable;
using namespace Category;
using namespace Channel;

#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
using namespace std;

#include "TCanvas.h"

int main(int argc, char* argv[])
{

  TString dir;

  if (argc==1){
    cout<<"\ngive a directory:"<<endl;
    cout<<"--dir /path/to/data/ (probably from your git clone)"<<endl;
    return -1;
  }
  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--dir")){
      dir=argv[++a];
    }
  }

  std::map<TString, float> lumiMap;
  //lumiMap["2016"]=36.2;
  //lumiMap["2017"]=44.3;
  //lumiMap["2018"]=59.9;
  lumiMap["all"]=139;

  H4lBackgroundReader b(lumiMap);
  b.loadShapes(dir+"shapes_SignalRegion");
  b.loadNorms(dir+"normalizations_SR");
  b.loadNormsHM(dir+"normalizations_HM");
  b.loadNormsRelax(dir+"normalizations_RelaxIsoD0");

  /////////////////////////////////////////////////////////////////
  //print full mass range yields
  cout << endl << "=> Reducible background, full mass range" << endl;
  BkgMeasurement* bgmInclusive;
  //float n,stat,syst;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_4l);
  Measurement measInc=bgmInclusive->getNorm();
  cout << "4l " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_2l2e);
  measInc=bgmInclusive->getNorm();
  cout << "2l2e "  << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << " (syst.)" << endl;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_2l2mu);
  measInc=bgmInclusive->getNorm();
  cout << "2l2mu " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  delete bgmInclusive;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_4mu);
  measInc=bgmInclusive->getNorm();
  cout << "4mu " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  delete bgmInclusive;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_2e2mu);
  measInc=bgmInclusive->getNorm();
  cout << "2e2mu " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  delete bgmInclusive;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_2mu2e);
  measInc=bgmInclusive->getNorm();
  cout << "2mu2e " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  delete bgmInclusive;
  bgmInclusive = b.getBkgMeasurement( "dummy", ProdType::prodInclusive,  EventType::_4e);
  measInc=bgmInclusive->getNorm();
  cout << "4e " << "\t\t " << measInc.n*measInc.lumi << " +- " << measInc.stat*100 << "% (stat.) +- " << measInc.syst*100 << "% (syst.)" << endl;
  delete bgmInclusive;

  cout<<endl<<"full mass range njet yields"<<endl;
  for(auto prodtype : getProdTypes()){
    int prec=3;
    if(prodtype>ProdType::_ge3Jet) break;
    BkgMeasurement* bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_4l);
    Measurement measProd4l=bkgMeas->getNorm();
    bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2e);
    Measurement measProd2l2e=bkgMeas->getNorm();
    bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2mu);
    Measurement measProd2l2mu=bkgMeas->getNorm();
    cout<<setprecision(prec)<<toStringLatex(prodtype)<<" | "<<measProd2l2mu.n*measProd2l2mu.lumi << " \\pm " << measProd2l2mu.stat*100 << "\\% \\pm " << measProd2l2mu.syst*100 << "\\% | " <<measProd2l2e.n*measProd2l2e.lumi << " \\pm " << measProd2l2e.stat*100 << "\\% \\pm " << measProd2l2e.syst*100 << "\\% | " <<measProd4l.n*measProd4l.lumi << " \\pm " << measProd4l.stat*100 << "\\% \\pm "<< measProd4l.syst*100 << "\\% \\\\ " <<endl;
  }

  cout<<endl<<"reducible background in final states, 115<m4l<130"<<endl;

  BkgMeasurement* bkgChannel;
  Measurement measChannel;
  for(auto ctype : getEventTypes()){
    if(ctype==EventType::_2l2mu || ctype==EventType::_2l2e) continue;
    bkgChannel=b.getBkgMeasurement( "dummy", ProdType::prodInclusive, ctype, 115, 129.5);
    measChannel=bkgChannel->getNorm();
    cout<<toString(ctype)<<": "<<measChannel.n*measChannel.lumi << " +- " << measChannel.stat*100 << "% (stat.) +- " << measChannel.syst*100 << "% (syst.)" << endl;
  }

  cout<<endl<<"reducible background in final states, 0<m4l<115"<<endl;

  for(auto ctype : getEventTypes()){
    if(ctype==EventType::_2l2mu || ctype==EventType::_2l2e) continue;
    bkgChannel=b.getBkgMeasurement( "dummy", ProdType::prodInclusive, ctype, 0, 114.5);
    measChannel=bkgChannel->getNorm();
    cout<<toString(ctype)<<": "<<measChannel.n*measChannel.lumi << " +- " << measChannel.stat*100 << "% (stat.) +- " << measChannel.syst*100 << "% (syst.)" << endl;
  }

  cout<<endl<<"reducible background in final states, 105<m4l<160"<<endl;

  for(auto ctype : getEventTypes()){
    if(ctype==EventType::_2l2mu) break;
    bkgChannel=b.getBkgMeasurement( "dummy", ProdType::prodInclusive, ctype, 105, 159.5);
    measChannel=bkgChannel->getNorm();
    cout<<toString(ctype)<<": "<<measChannel.n*measChannel.lumi << " +- " << measChannel.stat*100 << "% (stat.) +- " << measChannel.syst*100 << "% (syst.)" << endl;
  }

  vector<ProdType> sidebandProds;
  sidebandProds.push_back(ProdType::prodInclusive);
  sidebandProds.push_back(ProdType::_0Jet);
  sidebandProds.push_back(ProdType::_1Jet);
  sidebandProds.push_back(ProdType::_ge2Jet);
  for(auto& prod : sidebandProds){
    bkgChannel=b.getBkgMeasurement( "dummy", prod, EventType::_2l2e, 105, 114.5);
    measChannel=bkgChannel->getNorm();
    float sidebandllee=measChannel.n*measChannel.lumi;
    bkgChannel=b.getBkgMeasurement( "dummy", prod, EventType::_2l2e, 130, 159.5);
    measChannel=bkgChannel->getNorm();
    sidebandllee+=measChannel.n*measChannel.lumi;
    cout<<"4l sideband normalization for "<<toString(prod)<<"="<<sidebandllee<<endl;
  }

  BkgMeasurement* bkgMeas;

  vector<EventType> shapeChannels;
  shapeChannels.push_back(EventType::_2l2mu);
  shapeChannels.push_back(EventType::_2l2e);
  multimap<TString,ProdType> varMap;
  for(auto& prodtype : getProdTypes()){
    if(prodtype==ProdType::_0Jet || prodtype==ProdType::_1Jet|| prodtype==ProdType::_2Jet || prodtype==ProdType::_ge3Jet || prodtype==ProdType::prodInclusive){
      varMap.insert(pair<TString,ProdType>("pt4l",prodtype));
    }
  }
  varMap.insert(pair<TString,ProdType>("njets",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4l",ProdType::_y4l0));
  varMap.insert(pair<TString,ProdType>("pt4l",ProdType::_y4l1));
  varMap.insert(pair<TString,ProdType>("pt4l",ProdType::_y4l2));
  varMap.insert(pair<TString,ProdType>("pt4l",ProdType::_y4l3));
  varMap.insert(pair<TString,ProdType>("cts",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("cth1",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("cth2",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("phi1",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("phi",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("y4l",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m12",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m34",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m12m34",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("nbjets",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("mjj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("detajj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("dphijj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("jet2pt",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("jet1pt",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m4lj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m4ljj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4lj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4ljj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("jet1pt_jet2pt",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("jet1pt_jet1y",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4l_pt4lj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4lj_m4lj",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("pt4l_jet1pt",ProdType::prodInclusive));
  varMap.insert(pair<TString,ProdType>("m4l",ProdType::prodInclusive));
  TFile* fout=new TFile("allShapes.root","RECREATE");
  TH1F* hn;
  TH1F* hstat;
  TH1F* hup;
  TH1F* hdn;
  cout<<endl<<"category normalizations for 115<m4l<130 GeV"<<endl;
  for(auto prodtype : getProdTypes()){
    int prec=3;
    if(prodtype==ProdType::_2JetVBFPtgt200 || toString(prodtype).Contains("ttH")) prec=4;
    //if(prodtype!=ProdType::_0JetPt4l10 && prodtype!=ProdType::prodInclusive) continue;
    if(!toString(prodtype).Contains("SB")){
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_4l, 115, 129.5);
      Measurement measProd4l=bkgMeas->getNorm();
      //cout<<toString(prodtype)<<" "<<measProd.n*measProd.lumi << " \pm " << measProd.stat*100 << "\% (stat.) \pm " << measProd.syst*100 << "\% (syst.)" << endl;
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2e, 115, 129.5);
      Measurement measProd2l2e=bkgMeas->getNorm();
      //cout<<"2l2e "<<toString(prodtype)<<" "<<measProd.n*measProd.lumi << " \pm " << measProd.stat*100 << "\% (stat.) \pm " << measProd.syst*100 << "\% (syst.)" << endl;
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2mu, 115, 129.5);
      Measurement measProd2l2mu=bkgMeas->getNorm();
      //cout<<"2l2mu "<<toString(prodtype)<<" "<<measProd.n*measProd.lumi << " \pm " << measProd.stat*100 << "\% (stat.) \pm " << measProd.syst*100 << "\% (syst.)" << endl;
      cout<<setprecision(prec)<<toStringLatex(prodtype)<<" | "<<measProd2l2mu.n*measProd2l2mu.lumi << " \\pm " << measProd2l2mu.stat*100 << "\\% \\pm " << measProd2l2mu.syst*100 << "\\% | " <<measProd2l2e.n*measProd2l2e.lumi << " \\pm " << measProd2l2e.stat*100 << "\\% \\pm " << measProd2l2e.syst*100 << "\\% | " <<measProd4l.n*measProd4l.lumi << " \\pm " << measProd4l.stat*100 << "\\% \\pm " << measProd4l.syst*100 << "\\% \\\\ " <<endl;
    }
    else{
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2e, 105, 114.5);
      Measurement measProd2l2e_1=bkgMeas->getNorm();
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_2l2e, 130, 159.5);
      Measurement measProd2l2e_2=bkgMeas->getNorm();
      cout<<setprecision(prec)<<toStringLatex(prodtype)<<" | "<<(measProd2l2e_1.n+measProd2l2e_2.n)*measProd2l2e_1.lumi<<endl;
    }
    /*
    bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_4l, 0, 114.5);
    Measurement measProd=bkgMeas->getNorm();
    float sidebandYield=measProd.n*measProd.lumi;
    bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_4l, 130, 169.5);
    measProd=bkgMeas->getNorm();
    sidebandYield+=measProd.n*measProd.lumi;
    */
    //cout<<"total sideband yield: "<<sidebandYield<<endl;
    /*
    if(prodtype==ProdType::_ttHHad_b2){
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, EventType::_4l, 180, 799.5);
      measProd=bkgMeas->getNorm();
      cout<<"HM yield: "<<measProd.n*measProd.lumi<<endl;
    }
    */
  }
    /*
    if(prodtype==ProdType::_ttHHad || prodtype==ProdType::_ttHLep){
      for(auto evtype : shapeChannels){
	bkgMeas=b.getBkgMeasurement("dummy", prodtype, evtype, 115, 130);
	Measurement measProd=bkgMeas->getNorm();
	cout<<toString(prodtype)<<","<<toString(evtype)<<" "<<measProd.n*measProd.lumi << " +- " << measProd.stat*100 << "% (stat.) +- " << measProd.syst*100 << "% (syst.)" << endl;
	bkgMeas=b.getBkgMeasurement("dummy", prodtype, evtype, 1, 114);
	measProd=bkgMeas->getNorm();
	float sidebandYield=measProd.n*measProd.lumi;
	bkgMeas=b.getBkgMeasurement("dummy", prodtype, evtype, 131, 170);
	measProd=bkgMeas->getNorm();
	sidebandYield+=measProd.n*measProd.lumi;
	cout<<"total sideband yield "<<toString(evtype)<<": "<<sidebandYield<<endl;
      }
    }
    */
    /*
    if(prodtype==ProdType::_0JetPt4l100 || prodtype==ProdType::_1JetPt4l60 || prodtype==ProdType::_1JetPt4l60120 || prodtype==ProdType::_2JetVH || prodtype==ProdType::_2JetVBFPtJ200 || prodtype==ProdType::_ttHHad){
      bkgMeas=b.getBkgMeasurement(Variable::bdt, prodtype, EventType::_4l, 115,130);
      hn=(TH1F*)bkgMeas->getHistShape()->Clone(Form("h_%s_%s",toString(Variable::bdt).Data(),toString(prodtype).Data()));
      hstat=(TH1F*)bkgMeas->getHistRelStatErr()->Clone(Form("h_stat_%s_%s",toString(Variable::bdt).Data(),toString(prodtype).Data()));
      hup=(TH1F*)bkgMeas->getHistSystErrUp()->Clone(Form("h_up_%s_%s",toString(Variable::bdt).Data(),toString(prodtype).Data()));
      hdn=(TH1F*)bkgMeas->getHistSystErrDn()->Clone(Form("h_dn_%s_%s",toString(Variable::bdt).Data(),toString(prodtype).Data()));
      fout->cd();
      hn->Write();
      hstat->Write();
      hup->Write();
      hdn->Write();
    }
    */
  TString window[2]={"","_wide"};
  float njet0[2][2];
  float njetstat0[2][2];
  float njetup0[2][2];
  float njetdn0[2][2];
  vector<ProdType>* ptypes=nullptr;
  for(auto& entry : varMap){
    //std::cout<<"var "<<entry.first<<std::endl;
    for(auto evtype : shapeChannels){
      //std::cout<<toString(evtype)<<std::endl;
      for(int iwin=0;iwin<2;iwin++){ //m4l windows
	//std::cout<<"m4l window "<<iwin<<std::endl;
	float m4lmax=129.5,m4lmin=115;
	if(iwin==1){ m4lmax=159.5; m4lmin=105;}
	//to get the correct m4l fractions
	if(ptypes) ptypes->clear();
	if(entry.first=="njets"){
	  if(!ptypes) ptypes=new vector<ProdType>();
	  ptypes->push_back(ProdType::_0Jet);
	  ptypes->push_back(ProdType::_1Jet);
	  ptypes->push_back(ProdType::_2Jet);
	  ptypes->push_back(ProdType::_ge3Jet);
	}
	else if(entry.first=="jet1pt" || entry.first=="nbjet" || entry.first=="pt4l_jet1pt" || entry.first=="m4lj"
		|| entry.first=="pt4lj" || entry.first=="pt4l_pt4lj" || entry.first=="pt4lj_m4lj" || entry.first=="jet1pt_jet1y"){
	  if(!ptypes) ptypes=new vector<ProdType>();
	  ptypes->push_back(ProdType::_0Jet);
	  int nextrabins=0;
	  if(entry.first=="jet1pt" || entry.first=="pt4lj") nextrabins=3;
	  else if(entry.first=="nbjet") nextrabins=2;
	  else if(entry.first=="pt4l_jet1pt" || entry.first=="m4lj") nextrabins=6;
	  else if(entry.first=="pt4l_pt4lj" || entry.first=="pt4lj_m4lj") nextrabins=4;
	  else if(entry.first=="jet1pt_jet1y") nextrabins=5;
	  for(int i=0;i<nextrabins;i++) ptypes->push_back(ProdType::_ge1Jet);
	}
	else if(entry.first=="jet1pt_jet2pt"){
	  if(!ptypes) ptypes=new vector<ProdType>();
	  ptypes->push_back(ProdType::_0Jet);
	  ptypes->push_back(ProdType::_1Jet);
	  ptypes->push_back(ProdType::_1Jet);
	  for(int i=0;i<3;i++) ptypes->push_back(ProdType::_ge2Jet);
	}
	else if(entry.first=="jet2pt" || entry.first.Contains("jj")){
	  if(entry.first=="mjj") cout<<"create vector of types for mjj"<<endl;
	  if(!ptypes) ptypes=new vector<ProdType>();
	  ptypes->push_back(ProdType::_le1Jet);
	  int nextrabins=0;
	  if(entry.first=="jet2pt" || entry.first=="m4ljj") nextrabins=5;
	  else if(entry.first=="mjj" || entry.first=="detajj" || entry.first=="pt4ljj") nextrabins=3;
	  else if(entry.first=="dphijj") nextrabins=4;
	  for(int i=0;i<nextrabins;i++) ptypes->push_back(ProdType::_ge2Jet);
	}
	else{
	  if(ptypes) delete ptypes;
	  ptypes=nullptr;
	}
 	if(entry.first.CompareTo("m4l")!=0) bkgMeas=b.getBkgMeasurement(entry.first, entry.second, evtype, m4lmin, m4lmax, "all", iwin, ptypes);
	else bkgMeas=b.getBkgMeasurement(entry.first,entry.second,evtype);
	TString histname=Form("%s_%s%s",entry.first.Data(),toString(evtype).Data(),window[iwin].Data());
	if(entry.first.CompareTo("pt4l")==0){
	  if(entry.second!=ProdType::prodInclusive) histname.ReplaceAll("pt4l",Form("pt4l_%s",toString(entry.second).Data()));
	  else histname.ReplaceAll("pt4l","pt4l_incl");
	}
	hn=(TH1F*)bkgMeas->getHistShape()->Clone(Form("h_%s",histname.Data()));
	Measurement varMeas=bkgMeas->getNorm();
	//if(evtype==EventType::_2l2e) 
	hn->Scale(varMeas.n);
	if(((entry.first=="pt4l" && entry.second==ProdType::_1Jet) || entry.first=="njets") && iwin==0){
	  cout<<"norm is "<<varMeas.n<<"*"<<varMeas.lumi<<"="<<varMeas.n*varMeas.lumi<<endl;
	  for(int i=0;i<hn->GetNbinsX();i++){
	    std::cout<<entry.first<<","<<toString(evtype)<<" in bin "<<i+1<<" is "<<hn->GetBinContent(i+1)*varMeas.lumi<<endl;
	  }
	}
	/*
	if(!entry.first.Contains("jj") && !entry.first.Contains("jet")){
	  for(int i=0;i<hn->GetNbinsX();i++){
	    hn->SetBinContent(i+1,hn->GetBinContent(i+1)/hn->GetXaxis()->GetBinWidth(i+1));
	  }
	}
	*/
	/*
	if(iwin==0 && entry.first.CompareTo("njets")==0){
	  std::cout<<"per-fb norm for njets: "<<varMeas.n<<std::endl;
	  for(int i=0;i<hn->GetNbinsX();i++) std::cout<<toString(evtype)<<" njets in bin "<<i+1<<"="<<hn->GetBinContent(i+1)<<std::endl;
	}
	else if(iwin==0 && entry.first.CompareTo("pt4l")==0 && entry.second==ProdType::_2Jet){
	  std::cout<<toString(evtype)<<" total pt4l_2jet="<<hn->Integral()<<std::endl;
	  std::cout<<"per-fb norm for njets=2: "<<varMeas.n<<std::endl;
	}
	else if(iwin==0 && entry.first.Contains("pt4l_y4l")) std::cout<<"per-fb norm for "<<entry.first<<" is "<<varMeas.n<<std::endl;
	*/
	hstat=(TH1F*)bkgMeas->getHistRelStatErr()->Clone(Form("h_stat_%s",histname.Data()));
	if(entry.first.CompareTo("njets")==0){
	  njet0[(int)evtype-4][iwin]=hn->GetBinContent(1);
	  njetstat0[(int)evtype-4][iwin]=hstat->GetBinContent(1);
	}
	else if(entry.first.CompareTo("pt4l")==0 && entry.second==ProdType::_jet1pt0){
	  hn->SetBinContent(1,njet0[(int)evtype-4][iwin]);
	  hstat->SetBinContent(1,njetstat0[(int)evtype-4][iwin]);
	}
	fout->cd();
	hn->Write();
	hstat->Write();
	if(entry.first.CompareTo("m4l")!=0){
	  //if(evtype==EventType::_2l2e){
	  hup=(TH1F*)bkgMeas->getHistSystErrUp()->Clone(Form("h_up_%s",histname.Data()));
	  hdn=(TH1F*)bkgMeas->getHistSystErrDn()->Clone(Form("h_dn_%s",histname.Data()));
	  if(entry.first.CompareTo("njets")==0){
	    njetup0[(int)evtype-4][iwin]=hup->GetBinContent(1);
	    njetdn0[(int)evtype-4][iwin]=hdn->GetBinContent(1);
	  }
	  else if(entry.first.CompareTo("pt4l")==0 && entry.second==ProdType::_jet1pt0){
	    hup->SetBinContent(1,njetup0[(int)evtype-4][iwin]);
	    hdn->SetBinContent(1,njetdn0[(int)evtype-4][iwin]);
	  }
	  hup->Write();
	  hdn->Write();
	  //}
	  /*
	  else{
	    hsyst=(TH1F*)bkgMeas->getHistSystErrUp()->Clone(Form("h_syst_%s",histname.Data()));
	    hsyst->Write();
	  }
	  */
	}
      }
    }
  }
  
  cout<<"retrieve NN shapes"<<endl;
  for(auto cat : getProdTypes()){
    if(cat<ProdType::_0JetPt4l10 || cat>ProdType::_SB_ttH) continue;
    if(cat==ProdType::_0JetPt4lgt100 || cat==ProdType::_ttHLep) continue;
    bkgMeas=b.getBkgMeasurement("NN",cat,EventType::_4l,115,129.5);//,"all");
    Measurement varMeas=bkgMeas->getNorm();
    cout<<setprecision(3)<<toStringLatex(cat)<<": "<<varMeas.n*varMeas.lumi << "+/-" << varMeas.stat*100 << "%+/-" << varMeas.syst*100<<"%"<<endl;
    hn=(TH1F*)bkgMeas->getHistShape()->Clone(Form("h_NN_%s",toString(cat).Data()));
    hn->Scale(varMeas.n);
    hstat=(TH1F*)bkgMeas->getHistRelStatErr()->Clone(Form("h_stat_NN_%s",toString(cat).Data()));
    hup=(TH1F*)bkgMeas->getHistSystErrUp()->Clone(Form("h_up_NN_%s",toString(cat).Data()));
    hdn=(TH1F*)bkgMeas->getHistSystErrDn()->Clone(Form("h_dn_NN_%s",toString(cat).Data()));
    fout->cd();
    hn->Write();
    hstat->Write();
    hup->Write();
    hdn->Write();
  }

  fout->Write();
  fout->Close();

  cout<<"retrieve m4l shapes for mass measurement"<<endl;
  vector<ProdType> bdtProds;
  bdtProds.push_back(ProdType::_BDT0); bdtProds.push_back(ProdType::_BDT1); bdtProds.push_back(ProdType::_BDT2); bdtProds.push_back(ProdType::_BDT3);
  for(auto etype : getEventTypes()){
    if(etype>EventType::_2mu2e) break;
    for(auto ptype : bdtProds){
      bkgMeas=b.getBkgMeasurement("m4l",ptype,etype,105,159.5,"all",1);
      Measurement varMeas=bkgMeas->getNorm();
      cout<<toString(ptype)<<", "<<toString(etype)<<": "<<varMeas.n*varMeas.lumi << "+/-" << varMeas.stat*100 << "%+/-" << varMeas.syst*100<<"%"<<endl;
      hn=(TH1F*)bkgMeas->getHistShape()->Clone(Form("h_%s_%s",toString(ptype).Data(),toString(etype).Data()));
      hn->Scale(varMeas.n*varMeas.lumi);
      hn->GetXaxis()->SetRange(210,320);
      TCanvas* c=new TCanvas("c","c",400,400);
      hn->SetStats(0);
      hn->Draw();
      c->SaveAs(Form("m4lPlots/%s_%s.pdf",toString(ptype).Data(),toString(etype).Data()));
      delete c;
    }
  }


  //HM prod type yields
  /*
  std::cout<<"normalizations for high mass categories"<<std::endl;
  vector<EventType> shapeChannelsHM;
  shapeChannelsHM.push_back(EventType::_2l2mu);
  shapeChannelsHM.push_back(EventType::_2l2e);
  shapeChannelsHM.push_back(EventType::_2mu2e);
  shapeChannelsHM.push_back(EventType::_4e);
  shapeChannelsHM.push_back(EventType::_4mu);
  shapeChannelsHM.push_back(EventType::_2e2mu);
  for(auto prodtype : getProdTypesHM()){
    for(auto evtype : shapeChannelsHM){
      bkgMeas=b.getBkgMeasurement("dummy", prodtype, evtype,200,1499.99);
      Measurement measProd=bkgMeas->getNorm();
      cout<<toString(evtype)<<" "<<toString(prodtype)<<" "<<measProd.n*measProd.lumi << " +- " << measProd.stat*100 << "% (stat.) +- " << measProd.syst*100 << "% (syst.)" << endl;
    }
  }

  TFile* foutHM=new TFile("HMShapes.root","RECREATE");
  bkgMeas=b.getBkgMeasurement("m4lHM",ProdType::prodInclusive,EventType::_4l);
  hn=(TH1F*)bkgMeas->getHistShape()->Clone("h_m4lHM_4l");
  hstat=(TH1F*)bkgMeas->getHistRelStatErr()->Clone("h_stat_m4lHM_4l");
  foutHM->cd();
  hn->Write();
  hstat->Write();
  for(auto prodtype : getProdTypesHM()){
    for(auto evtype : shapeChannelsHM){
      cout<<"get m4lHM measurement for "<<toString(prodtype)<<", "<<toString(evtype)<<endl;
      bkgMeas=b.getBkgMeasurement("m4lHM",prodtype,evtype);
      hn=(TH1F*)bkgMeas->getHistShape()->Clone(Form("h_m4lHM_%s_%s",toString(evtype).Data(),toString(prodtype).Data()));
      hstat=(TH1F*)bkgMeas->getHistRelStatErr()->Clone(Form("h_stat_m4lHM_%s_%s",toString(evtype).Data(),toString(prodtype).Data()));
      hup=(TH1F*)bkgMeas->getHistSystErrUp()->Clone(Form("h_up_m4lHM_%s_%s",toString(evtype).Data(),toString(prodtype).Data()));
      hdn=(TH1F*)bkgMeas->getHistSystErrDn()->Clone(Form("h_dn_m4lHM_%s_%s",toString(evtype).Data(),toString(prodtype).Data()));
      foutHM->cd();
      hn->Write();
      hstat->Write();
      hup->Write();
      hdn->Write();
    }
  }
  foutHM->Write();
  foutHM->Close();
  */
  /*
  //in relaxed region
  for(auto comp: getComponents()){
    for(auto evtype: getEventTypes()){
      if(toInt(comp)<2 && evtype!=EventType::_4mu && evtype!=EventType::_2e2mu && evtype!=EventType::_2l2mu) continue;
      if(toInt(comp)>1 && evtype!=EventType::_4e && evtype!=EventType::_2mu2e && evtype!=EventType::_2l2e) continue;
      for(auto prodtype : getProdTypes()){
	if(prodtype<ProdType::_0JetPt4l10) continue;
	bkgMeas=b.getBkgMeasurementRelax(prodtype,evtype,comp,"all");
	Measurement measProd=bkgMeas->getNorm();
	cout<<"relaxed "<<toString(comp)<<" "<<toString(evtype)<<" "<<toString(prodtype)<<" "<<measProd.n*measProd.lumi << " +- " << measProd.stat*100 << "% (stat.) " << endl;
      }
    }
  }
  */

  /////////////////////////////////////////////////////////////////  
  //print yields per channel for mass measurement
  /*
  cout << endl << "=> yields for mass measurement (110<m4l<135 GeV):" << endl;
  BkgMeasurement* bgmSR;
  EventType allChannels[] = { EventType::_4mu, EventType::_4e, EventType::_2e2mu, EventType::_2mu2e, EventType::_4l };
  for (auto channel : allChannels) {
    bgmSR = b.getBkgMeasurement( "dummy", ProdType::prodInclusive, channel, 110, 140 );
    Measurement meas=bgmSR->getNorm();
    cout << toString(channel) << "\t\t " << meas.n << " +- " << meas.stat << " (stat.) +- " << meas.syst << " (syst.)" << endl;
  }
  delete bgmSR;
  */

  /////////////////////////////////////////////////////////////////  
  //print 2l2e, 2l2mu and 4l yields per category
  /*
  cout << endl << "=> yields for fiducial xs categories (115<m4l<130 GeV):" << endl;
  ProdType fidxsCategories[] = { ProdType::prodInclusive, ProdType::_0Jet, ProdType::_1Jet, ProdType::_ge2Jet };

  for (auto categ : fidxsCategories)  {
    cout << "\t category:" << toString(categ) << endl;
    BkgMeasurement* bgm;
    //float n,stat,syst;

    bgm = b.getBkgMeasurement( Variable::pt4l, categ,  EventType::_4l, 115,130, -1);
    Measurement meas=bgm->getNorm();
    cout << toString(EventType::_4l) << "\t\t " << meas.n*meas.lumi << " +- " << meas.stat*meas.lumi*meas.n << " (stat.) +- " << meas.syst*meas.lumi*meas.n << " (syst.)" << endl;
    cout<<"rel stat unc.: "<<meas.stat<<"; rel syst unc.:"<<meas.syst<<endl;
    bgm = b.getBkgMeasurement( Variable::pt4l, categ,  EventType::_2l2e, 115,130, -1);
    meas=bgm->getNorm();
    cout << toString(EventType::_2l2e) << "\t\t " << meas.n*meas.lumi << " +- " << meas.stat*meas.lumi*meas.n << " (stat.) +- " << meas.syst*meas.lumi*meas.n << " (syst.)" << endl;
    cout<<"rel stat unc.: "<<meas.stat<<"; rel syst unc.:"<<meas.syst<<endl;
    bgm = b.getBkgMeasurement( Variable::pt4l, categ,  EventType::_2l2mu, 115,130, -1);
    meas=bgm->getNorm();
    cout << toString(EventType::_2l2mu) << "\t\t " << meas.n*meas.lumi << " +- " << meas.stat*meas.lumi*meas.n << " (stat.) +- " << meas.syst*meas.lumi*meas.n << " (syst.)" << endl;
    cout<<"rel stat unc.: "<<meas.stat<<"; rel syst unc.:"<<meas.syst<<endl;
    //check some shapes...
    //...............
    Variable testObs = Variable::m4l;
    if (categ==ProdType::_ge1Jet) testObs = Variable::jet1pt;
    else if (categ==ProdType::_ge2Jet) testObs = Variable::mjj;
    bgm = b.getBkgMeasurement( testObs, categ,  EventType::_2l2e, 115,130, -1);
    TH1F* h = bgm->getHistShape();
    if (h) {
      float testmean = 0;
      testmean = h->GetMean();
      if (testmean==0) cout << "WARNING can't find shape for category " << toString(categ) << endl;
    } else 

    delete bgm;
  }
  */



  /////////////////////////////////////////////////////////////////
  //print 4l yields per category
  /*
  cout << endl << "=> 4l yields for couplings/EFT categories (118<m4l<129 GeV):" << endl;
  ProdType allCategories[] = { ProdType::_0JetPt4l100, ProdType::_0JetPt4lgt100,
  			      ProdType::_1JetPt4l60, ProdType::_1JetPt4l60120, ProdType::_1JetPt4lgt120,
  			      ProdType::_2JetVH,
  			      ProdType::_2JetVBFPtJ200 , ProdType::_2JetVBFPtJgt200,
  			      ProdType::_VHlep, ProdType::_ttH  };
  vector<ProdType> missingBdtShapes;
  for (auto categ : allCategories)  {
    BkgMeasurement* bgm = b.getBkgMeasurement( Variable::bdt, categ,  EventType::_4l, 118,129);
    float n,stat,syst;
    Measurement meas=bgm->getNorm();
    TH1F* h_shape_bdt = bgm->getHistShape();
    // // TH1F* h_relativeStatVariation_bdt = bgm->getHistRelStatErr(); // no stat error, only syst for bdt
    TH1F* h_relativeSystVariation_bdt = bgm->getHistRelSystErr();
    if (h_shape_bdt && h_relativeSystVariation_bdt) {;}
    cout << toString(categ) << "\t\t " << meas.n << " +- " << meas.stat << " (stat.) +- " << meas.syst << " (syst.)" << endl;
    delete bgm;
  }
  */
  //  for (auto categ: missingBdtShapes)
  //    cout << "ERROR can't find shape for category " << toString(categ) << endl;
  return 0;
}
 
