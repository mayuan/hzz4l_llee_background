#include "H4lBackgroundReader/H4lBackgroundReader.h"
#include "H4lBackgroundReader/BkgMeasurement.h"
#include "H4lBackgroundReader/H4lBkgEnums.h"

#include <unordered_map>
#include <map>
#include <vector>
#include <TTree.h>
#include <TH1D.h>
using namespace H4lBkg; //for enums
using namespace Observable;
using namespace Category;
using namespace Channel;

#include <iostream>
#include <fstream>
#include <TObjArray.h>
#include <TObjString.h>
using namespace std;

vector<TString> splitString(TString string, TString delimiter);
TTree* makeVaribleTree(TString treeName, std::map<TString, float> currMap);
TTree* makeVaribleTree(TString treeName, std::map<TString, TString> currMap);

int main()
{

    std::map<TString, float> lumiMap;
    lumiMap["all"]=138.96519;

    H4lBackgroundReader b(lumiMap);

    TString dir = "../source/H4lBackgroundReader/data/";


    b.loadShapes(dir+"shapes_SignalRegion");
    b.loadNorms(dir+"normalizations_SR");
    // b.loadNormsHM(dir+"normalizations_HM");


    map<TString, EventType> eventTypes;

    eventTypes["4l"]        = EventType::_4l;
    eventTypes["llee"]      = EventType::_2l2e;
    eventTypes["llmumu"]    = EventType::_2l2mu;

    map<TString, vector<ProdType>> catDefList;
    catDefList["0jet_Pt4l_0_10_4l_13TeV"].push_back(ProdType::_0JetPt4l10);
    catDefList["0jet_Pt4l_10_100_4l_13TeV"].push_back(ProdType::_0JetPt4l100);
    catDefList["0jet_Pt4l_GE100_4l_13TeV"].push_back(ProdType::_0JetPt4lgt100);
    catDefList["1jet_Pt4l_0_60_4l_13TeV_b1"].push_back(ProdType::_1JetPt4l60_b1);
    catDefList["1jet_Pt4l_0_60_4l_13TeV_b2"].push_back(ProdType::_1JetPt4l60_b2);
    catDefList["1jet_Pt4l_60_120_4l_13TeV_b1"].push_back(ProdType::_1JetPt4l60120_b1);
    catDefList["1jet_Pt4l_60_120_4l_13TeV_b2"].push_back(ProdType::_1JetPt4l60120_b2);
    catDefList["1jet_Pt4l_120_200_4l_13TeV"].push_back(ProdType::_1JetPt4l120200);
    catDefList["1jet_Pt4l_GE200_4l_13TeV"].push_back(ProdType::_1JetPt4lgt200);
    catDefList["2jet_4l_13TeV_b1"].push_back(ProdType::_2JetVHVBFPt200_b1);
    catDefList["2jet_4l_13TeV_b2"].push_back(ProdType::_2JetVHVBFPt200_b2);
    catDefList["2jet_VBF_ptJ1GE200_4l_13TeV"].push_back(ProdType::_2JetVBFPtgt200);
    catDefList["VHLep_4l_13TeV"].push_back(ProdType::_VHlep);
    catDefList["ttH_leptonic_4l_13TeV"].push_back(ProdType::_ttHLep);
    catDefList["ttH_hadronic_4l_13TeV_b1"].push_back(ProdType::_ttHHad_b1);
    catDefList["ttH_hadronic_4l_13TeV_b2"].push_back(ProdType::_ttHHad_b2);
    catDefList["SB_0jet_4l_13TeV"].push_back(ProdType::_SB_0jet);
    catDefList["SB_1jet_4l_13TeV"].push_back(ProdType::_SB_1jet);
    catDefList["SB_2jet_4l_13TeV"].push_back(ProdType::_SB_2jet);
    catDefList["SB_VHLep_4l_13TeV"].push_back(ProdType::_SB_VHLep);
    catDefList["SB_ttH_4l_13TeV"].push_back(ProdType::_SB_ttH);


    map<TString, TString> recoObs;
    recoObs["0jet_Pt4l_0_10_4l_13TeV"]      = "NN_0Jet_pTjLow_ggF";
    recoObs["0jet_Pt4l_10_100_4l_13TeV"]    = "NN_0Jet_pTjLow_ggF";
    recoObs["0jet_Pt4l_GE100_4l_13TeV"]     = "prod_type";       // This is a counting category
    recoObs["1jet_Pt4l_0_60_4l_13TeV_b1"]   = "NN_1Jet_pTLow_VBF";
    recoObs["1jet_Pt4l_0_60_4l_13TeV_b2"]   = "NN_1Jet_pTLow_ZZ";
    recoObs["1jet_Pt4l_60_120_4l_13TeV_b1"] = "NN_1Jet_pTMed_VBF";
    recoObs["1jet_Pt4l_60_120_4l_13TeV_b2"] = "NN_1Jet_pTMed_ZZ";
    recoObs["1jet_Pt4l_120_200_4l_13TeV"]   = "NN_1Jet_pTHigh_VBF";
    recoObs["1jet_Pt4l_GE200_4l_13TeV"]     = "prod_type";       // This is a counting category
    recoObs["2jet_4l_13TeV_b1"]             = "NN_2Jet_pTjLow_VBF";
    recoObs["2jet_4l_13TeV_b2"]             = "NN_2Jet_pTjLow_VH";
    recoObs["2jet_VBF_ptJ1GE200_4l_13TeV"]  = "NN_2Jet_pTjHigh_VBF";
    recoObs["VHLep_4l_13TeV"]               = "NN_VHLep_ttH";
    recoObs["ttH_leptonic_4l_13TeV"]        = "event_type";      // This is a counting category
    recoObs["ttH_hadronic_4l_13TeV_b1"]     = "NN_ttHHad_ggFttV";
    recoObs["ttH_hadronic_4l_13TeV_b2"]     = "NN_ttHHad_ggFttV";
    recoObs["ttH_leptonic_4l_13TeV"]        = "event_type";      // This is a counting category
    recoObs["SB_0jet_4l_13TeV"]             = "event_type";      // This is a counting category
    recoObs["SB_1jet_4l_13TeV"]             = "event_type";      // This is a counting category
    recoObs["SB_2jet_4l_13TeV"]             = "event_type";      // This is a counting category
    recoObs["SB_VHLep_4l_13TeV"]            = "event_type";      // This is a counting category
    recoObs["SB_ttH_4l_13TeV"]              = "event_type";      // This is a counting category



    map<TString, vector<double>> recoBin;
    recoBin["0jet_Pt4l_0_10_4l_13TeV"]      = vector<double> {0, 0.17, 0.39, 0.56, 0.69, 0.81,1};
    recoBin["0jet_Pt4l_10_100_4l_13TeV"]    = vector<double> {0, 0.17, 0.39, 0.56, 0.69, 0.81,1};
    recoBin["0jet_Pt4l_GE100_4l_13TeV"]     = vector<double> {0, 10};       // This is a counting category
    recoBin["1jet_Pt4l_0_60_4l_13TeV_b1"]   = vector<double> {0, 0.30, 0.50, 0.65, 1};
    recoBin["1jet_Pt4l_0_60_4l_13TeV_b2"]   = vector<double> {0, 0.35, 0.55, 1};
    recoBin["1jet_Pt4l_60_120_4l_13TeV_b1"] = vector<double> {0, 0.50, 0.70, 1};
    recoBin["1jet_Pt4l_60_120_4l_13TeV_b2"] = vector<double> {0, 0.40, 0.6, 1};
    recoBin["1jet_Pt4l_120_200_4l_13TeV"]   = vector<double> {0, 0.65, 1};
    recoBin["1jet_Pt4l_GE200_4l_13TeV"]     = vector<double> {0, 10};      // This is a counting category 
    recoBin["2jet_4l_13TeV_b1"]             = vector<double> {0, 0.50, 0.75, 0.90, 0.95, 1};
    recoBin["2jet_4l_13TeV_b2"]             = vector<double> {0, 0.53, 0.72, 0.82, 0.90, 1};
    recoBin["2jet_VBF_ptJ1GE200_4l_13TeV"]  = vector<double> {0, 0.5, 1};
    recoBin["VHLep_4l_13TeV"]               = vector<double> {0, 0.5, 1};
    recoBin["ttH_leptonic_4l_13TeV"]        = vector<double> {0, 10};      // This is a counting category
    recoBin["ttH_hadronic_4l_13TeV_b1"]     = vector<double> {0, 0.7, 1};
    recoBin["ttH_hadronic_4l_13TeV_b2"]     = vector<double> {0, 1};
    recoBin["SB_0jet_4l_13TeV"]             = vector<double> {0, 1};
    recoBin["SB_1jet_4l_13TeV"]             = vector<double> {0, 1};
    recoBin["SB_2jet_4l_13TeV"]             = vector<double> {0, 1};
    recoBin["SB_VHLep_4l_13TeV"]            = vector<double> {0, 1};
    recoBin["SB_ttH_4l_13TeV"]              = vector<double> {0, 1};


    map<TString, vector<pair<double, double>>> recoMassRange;
    for(const auto& catInfo:catDefList)
    {
        if(catInfo.first.Contains("SB_ttH_4l_13TeV")) recoMassRange[catInfo.first] = {{105, 115-0.5},{130, 350-0.5}};
        else if(catInfo.first.Contains("SB"))         recoMassRange[catInfo.first] = {{105, 115-0.5},{130, 160-0.5}};
        else                                          recoMassRange[catInfo.first] = {{115, 130-0.5}};
    }


    // double totalIncYield = 0;
    // auto bkgChannelInc = b.getBkgMeasurement( "dummy", ProdType::prodInclusive, EventType::_4l, 105, 159.5);
    // auto measChannel = bkgChannelInc->getNorm();
    // totalIncYield += measChannel.n * measChannel.lumi;

    // cout <<"Inclusive" << "\t\t " << totalIncYield  << endl;

    // bkgChannelInc = b.getBkgMeasurement( "dummy", ProdType::_SB_ttH, EventType::_4l, 160, 350.5);
    // measChannel = bkgChannelInc->getNorm();
    // totalIncYield += measChannel.n * measChannel.lumi;

    // cout <<"Inclusive with ttV"  << "\t\t " << totalIncYield  << endl;



    for(const auto& cEventTypeElement: eventTypes)
    {
        auto cEventType = cEventTypeElement.second;
        for(auto catDef: catDefList)
        {
            float tN = 0;
            float tStat = 0;
            float tSyst = 0;
            for(auto prodType : catDef.second)
            {
                for (const auto& massInfo: recoMassRange[catDef.first])
                {
                    BkgMeasurement* bgm = b.getBkgMeasurement("dummy", prodType,  cEventType, massInfo.first, massInfo.second);
                    //float n,stat,syst;
                    const Measurement meas=bgm->getNorm();
                    tN += meas.n * meas.lumi;
                    tStat += meas.stat;
                    tSyst += meas.syst;
                    delete bgm;
                }
            }
            cout <<catDef.first << "\t\t " << tN << " +- " << tStat << " (stat.) +- " << tSyst << " (syst.)" << endl;
        }


        // get the Nominal PDF
        TFile *fout = TFile::Open("redBkg_"+cEventTypeElement.first+"_all.root","recreate");
        // maps needed this - needs to follow the pattern for 
        map<TString, float> nom_yield;
        map<TString, float> nom_acceptance;
        map<TString, float> nom_stats;
        
        map<TString, map<TString, float>> sys_yield;

        fout->mkdir("Nominal");
        for(auto catDef: catDefList)
        {
            cout<<"Doing: "<<catDef.first<<endl;
            TH1F* hist = 0;
            TH1F* histUp = 0;
            TH1F* histDown = 0;
            double totalYield = 0;
            float tStat = 0;
            float tSyst = 0;

            for(auto prodType : catDef.second)
            {
                for (const auto& massInfo: recoMassRange[catDef.first])
                {
                    BkgMeasurement* bgm = b.getBkgMeasurement( "NN", prodType,  cEventType, massInfo.first, massInfo.second);
                    // do the shape first 
                    if(!bgm) continue; 
                    TH1F* temp = bgm->getHistShape();
                    TH1F* hstat = (TH1F*) bgm->getHistRelStatErr() ;
                    TH1F* hup   = (TH1F*) bgm->getHistSystErrUp()  ;
                    TH1F* hdn   = (TH1F*) bgm->getHistSystErrDn()  ;


                    // Do the norm
                    const Measurement meas=bgm->getNorm();
                    totalYield += meas.n * meas.lumi;
                    tStat += meas.stat;
                    tSyst += meas.syst;


                    cout<<"Norm: "<<totalYield<<" "<<massInfo.first<<" "<<massInfo.second<<" prodType "<<toString(prodType)<<" cEventType: "<<toString(cEventType)<<endl;


                    for(int i = 0; i < temp->GetXaxis()->GetNbins(); i++)
                    {
                        double content = temp->GetBinContent(i+1);
                        cout<<"Bin "<<i <<" GetBinContent: "<<content<<" center: "<<temp->GetBinCenter(i+1)<<endl;

                    }

                    if(!temp) continue;
                    if(!hist) 
                    {
                        hist = (TH1F*) temp->Clone(recoObs.at(catDef.first) + "_" + catDef.first+"temp");
                        hist->SetName(recoObs.at(catDef.first) + "_" + catDef.first+"temp");
                        hist->SetTitle(recoObs.at(catDef.first) + "_" + catDef.first+"temp");

                        histUp = (TH1F*) hup->Clone(recoObs.at(catDef.first) + "_" + catDef.first+"SystempUp");
                        histUp->SetName(recoObs.at(catDef.first) + "_" + catDef.first+"SystempUp");
                        histUp->SetTitle(recoObs.at(catDef.first) + "_" + catDef.first+"SystempUp");

                        histDown = (TH1F*) hdn->Clone(recoObs.at(catDef.first) + "_" + catDef.first+"SystempDown");
                        histDown->SetName(recoObs.at(catDef.first) + "_" + catDef.first+"SystempDown");
                        histDown->SetTitle(recoObs.at(catDef.first) + "_" + catDef.first+"SystempDown");

                    }
                    else
                    { 
                        hist->Add(temp);
                        histUp->Add(hup);
                        histDown->Add(hdn);
                    }

                    delete bgm;
                }
            }
            if(totalYield < 0) totalYield = 0; 
            //  correlated systematics: from inclusive estimate
            float totalSysRatio = tSyst; 
            float totalStatRatio = tStat;     

            sys_yield["Nominal"][catDef.first] = totalYield;

            if(fabs(totalSysRatio) > 1) totalSysRatio = 1;
            if(fabs(totalStatRatio) > 1) totalStatRatio = 1;


            sys_yield["ATLAS_norm_SF_H4l_redbkg_"+cEventTypeElement.first+"__1up"]  [catDef.first] = totalYield * (1+totalSysRatio);
            sys_yield["ATLAS_norm_SF_H4l_redbkg_"+cEventTypeElement.first+"__1down"][catDef.first] = totalYield * (1-totalSysRatio);

            if(sys_yield["ATLAS_norm_SF_H4l_redbkg_"+cEventTypeElement.first+"__1down"][catDef.first] < 0)   sys_yield["ATLAS_norm_SF_H4l_redbkg_"+cEventTypeElement.first+"__1down"][catDef.first] = totalYield / (1+totalSysRatio);




            TString catName = catDef.first;
            catName.ReplaceAll("_13TeV","");
            sys_yield["ATLAS_norm_MCStat_"+catName+"_H4l_redbkg__1up"]  [catDef.first] = totalYield * (1+totalStatRatio);
            sys_yield["ATLAS_norm_MCStat_"+catName+"_H4l_redbkg__1down"][catDef.first] = totalYield * (1-totalStatRatio);

            if(sys_yield["ATLAS_norm_MCStat_"+catName+"_H4l_redbkg__1down"][catDef.first] < 0)   sys_yield["ATLAS_norm_MCStat_"+catName+"_H4l_redbkg__1down"][catDef.first] = totalYield / (1+totalStatRatio);

            // Store the info in the map
            nom_yield[catDef.first] = totalYield;
            nom_acceptance[catDef.first] = -1; // dummy value
            nom_stats[catDef.first] = 10000; // dummy value

            cout<<recoObs.at(catDef.first)<<endl;

            TString obVarName = recoObs.at(catDef.first);
            TString histName = catDef.first;

            TString sysNameUp      = "ATLAS_shape_SF_H4l_redbkg_"+cEventTypeElement.first+"__1up";        
            TString sysNameDown    = "ATLAS_shape_SF_H4l_redbkg_"+cEventTypeElement.first+"__1down";    

            TH1D* hist_temp = 0;
            TH1D* hist_tempup   = 0;
            TH1D* hist_tempdown = 0;


            std::vector<double> histBinning = recoBin.at(catDef.first);
            
            if(histBinning.size() > 2)
            {    

                hist_temp     = new TH1D(histName ,           histName +      ";" , histBinning.size()-1, 0, histBinning.size()-1);
                hist_tempup   = new TH1D(histName + "__1up",  histName + "__1up;" , histBinning.size()-1, 0, histBinning.size()-1);
                hist_tempdown = new TH1D(histName + "__down", histName + "__down;", histBinning.size()-1, 0, histBinning.size()-1);
                for(int i = 0; i < hist->GetXaxis()->GetNbins(); i++)
                {
                    double content = hist->GetBinContent(i+1);
                    cout<<"Bin "<<i <<" GetBinContent: "<<content<<" center: "<<hist->GetBinCenter(i+1)<<endl;
                    if(content < 0.00001) content = 0.0001;

                    hist_temp->SetBinContent(i+1, content);
                    hist_temp->SetBinError(i+1, hist->GetBinError(i+1));

                    content = histUp->GetBinContent(i+1);
                    if(content < 0.00001) content = 0.0001;
                    hist_tempup->SetBinContent(i+1, content);
                    hist_tempup->SetBinError(i+1, histUp->GetBinError(i+1));
                    
                    content = histDown->GetBinContent(i+1);
                    if(content < 0.00001) content = 0.0001;
                    hist_tempdown->SetBinContent(i+1, content);
                    hist_tempdown->SetBinError(i+1, histDown->GetBinError(i+1));
                }
            }
            else
            {
                cout<<"Making new for: "<<histName<<endl;            
                hist_temp = new TH1D(histName, histName+";"+obVarName+";event", 1, 0, 1);
                hist_temp->Fill(0.5, 1);
            
                hist_tempup     = (TH1D*) hist_temp->Clone(histName + "__1up");            
                hist_tempdown   = (TH1D*) hist_temp->Clone(histName + "__down");    

            }
            

            cout<<"Integral: "<<hist_temp->Integral()<<endl;

            hist_temp->Scale(1.0/hist_temp->Integral());
            
            fout->cd();
            fout->mkdir("Nominal");
            fout->cd("Nominal");
            hist_temp->Write();


            fout->cd();
            fout->mkdir(sysNameUp);
            fout->cd(sysNameUp);
            hist_tempup->SetNameTitle(histName, histName);
            hist_tempup->Write();


            fout->cd();
            fout->mkdir(sysNameDown);
            fout->cd(sysNameDown);
            hist_tempdown->SetNameTitle(histName, histName);
            hist_tempdown->Write();

        }

        auto acceptanceTree = makeVaribleTree("acceptance", nom_acceptance);
        auto xsTree         = makeVaribleTree("XS",         map<TString, float>{{"all", 1}});
        auto RBRTree        = makeVaribleTree("BRr_4l_ZZ",  map<TString, float>{{"BRr_4l_ZZ", 1}});
        auto statTree       = makeVaribleTree("stats",      nom_stats);
        auto sumWeightTree  = makeVaribleTree("sumWeight",  map<TString, float>{{"all", 1}});
        auto fracTree       = makeVaribleTree("STXSFrac",   map<TString, float>{{"all", 1}});


        fout->cd();
        fout->cd("Nominal");
        acceptanceTree->Write();
        xsTree->Write();
        statTree->Write();
        RBRTree->Write();
        sumWeightTree->Write();
        fracTree->Write();

        fout->cd();

        for(auto& sysInfo: sys_yield)
        {
            fout->cd();
            fout->mkdir(sysInfo.first);
            fout->cd(sysInfo.first);
            auto yieldsTree = makeVaribleTree("yield", sysInfo.second);
            yieldsTree->Write();
        }


        fout->cd();
        auto metaDataTree         = makeVaribleTree("metaData",         map<TString, TString>{{"sampleName", "redBkg_"+cEventTypeElement.first}});
        metaDataTree->Write();


        fout->Close();
    }
    return 0;
}

vector<TString> splitString(TString string, TString delimiter)
{
    string.ReplaceAll(" ", "");
    TObjArray *Varparts = string.Tokenize(delimiter);
    vector<TString> varNameVec;
    if(Varparts->GetEntriesFast()) {
        TIter iString(Varparts);
        TObjString* os=0;
        while ((os=(TObjString*)iString())) {
            varNameVec.push_back(os->GetString());
            //cout<<os->GetString()<<endl;
        }
    }

    return varNameVec;
}

TTree* makeVaribleTree(TString treeName, std::map<TString, float> currMap)
{
    TTree* tree = new TTree(treeName, treeName);
    TString key = "";
    double val = -999;

    tree->Branch("key", &key);
    tree->Branch("val", &val);

    for(const auto& cKey:currMap)
    {
        key = cKey.first;
        val = cKey.second;
        tree->Fill();
    }

    return tree;
}
TTree* makeVaribleTree(TString treeName, std::map<TString, TString> currMap)
{
    TTree* tree = new TTree(treeName, treeName);
    TString key = "";
    TString val = "";

    for(auto& cKey:currMap)
    {
        tree->Branch(cKey.first, &cKey.second);
    }
    tree->Fill();

    return tree;
}
