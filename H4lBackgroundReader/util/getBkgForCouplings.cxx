#include "H4lBackgroundReader/H4lBackgroundReader.h"
#include "H4lBackgroundReader/BkgMeasurement.h"
#include "H4lBackgroundReader/H4lBkgEnums.h"
using namespace H4lBkg; //for enums
using namespace Observable;
using namespace Category;
using namespace Channel;

#include <iostream>
using namespace std;

int main()
{
  H4lBackgroundReader b;
  b.loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_SignalRegion.root");
  b.loadNorms("$ROOTCOREBIN/../H4lBackgroundReader/data/normalizations_m4lFullRange_1ifb.root");

  b.setLumiInvFb(36.5,0);


  /////////////////////////////////////////////////////////////////
  //print 4l yields per category
  cout << endl << "=> 4l yields for couplings/EFT categories (118<m4l<129 GeV):" << endl;

  ProdType allCategories[] = { ProdType::_0JetPt4l100, ProdType::_0JetPt4lgt100,
  			      ProdType::_1JetPt4l60, ProdType::_1JetPt4l60120, ProdType::_1JetPt4lgt120,
  			      ProdType::_2JetVH,
  			      ProdType::_2JetVBFPtJ200 , ProdType::_2JetVBFPtJgt200,
			      ProdType::_VHlep, ProdType::_ttHHad, ProdType::_ttHLep  };

  EventType allChannels[] = {EventType::_2l2mu, EventType::_2l2e, EventType::_4l};

  cout << "category    &   " << toString(EventType::_2l2mu) <<   "  &  " << toString(EventType::_2l2e) << " & " << toString(EventType::_4l) << " \\\\" << endl;

  BkgMeasurement* bgm;
  
  for (auto categ : allCategories)  {
    vector<Measurement> m;
    for (auto channel : allChannels)  {
      bgm = b.getBkgMeasurement( "dummy", categ,  channel, 118,129);
      m.push_back( bgm->getNorm() );
    }
    cout << toString(categ) << "\t"
	 << " $ " << m[0].getStringTex() << " $ &"
	 << " $ " << m[1].getStringTex() << " $ &"
      	 << " $ " << m[2].getStringTex() << " $ \\\\" << endl;
  }

  delete bgm;
  return 0;
}
 
