# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

# $Id: __init__.py 653633 2015-03-12 13:44:48Z krasznaa $
#
# File to make it possible to refer to xAODRootAccess as a module in
# Python (PyROOT) code.
#

