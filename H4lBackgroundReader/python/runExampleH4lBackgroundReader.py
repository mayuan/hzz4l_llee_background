#!/usr/bin/env python

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

from ROOT import H4lBackgroundReader

b = ROOT.H4lBackgroundReader()
b.loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_SignalRegion.root");
b.loadNorms("$ROOTCOREBIN/../H4lBackgroundReader/data/normalizations_m4lFullRange_1ifb.root");
b.setLumiInvFb(36.1);

#
#
#
print '\n => Reducible background, full mass range'

#channels = [ ROOT.H4lBkg.Channel._4l, ROOT.H4lBkg.Channel._2l2e, ROOT.H4lBkg.Channel._2l2mu ]
channels = [ ROOT.H4lBkg.Channel._4l, ROOT.H4lBkg.Channel._4e, ROOT.H4lBkg.Channel._2mu2e, ROOT.H4lBkg.Channel._2l2e, ROOT.H4lBkg.Channel._4mu, ROOT.H4lBkg.Channel._2e2mu, ROOT.H4lBkg.Channel._2l2mu ]

for channel in channels:
    bgm = b.getBkgMeasurement( ROOT.H4lBkg.Observable.dummy, ROOT.H4lBkg.Category.prodInclusive,  channel);
    m = bgm.getNorm()
    print ROOT.H4lBkg.Channel.toString( channel ), '\t', m.getString()
    
#
#
#
print '\n => yields for mass measurement (110<m4l<135 GeV):'

allChannels = [ ROOT.H4lBkg.Channel._4mu, ROOT.H4lBkg.Channel._4e, ROOT.H4lBkg.Channel._2e2mu, ROOT.H4lBkg.Channel._2mu2e, ROOT.H4lBkg.Channel._4l ]

#for channel in allChannels:
for channel in channels:
    bgm = b.getBkgMeasurement( ROOT.H4lBkg.Observable.dummy, ROOT.H4lBkg.Category.prodInclusive, channel, 110, 135 );
    m = bgm.getNorm();
    print ROOT.H4lBkg.Channel.toString( channel ), '\t', m.getString()

#
#
#
print '\n => yields for fiducial xs categories (115<m4l<130 GeV):'
categories= [ ROOT.H4lBkg.Category.prodInclusive, ROOT.H4lBkg.Category._0Jet, ROOT.H4lBkg.Category._1Jet, ROOT.H4lBkg.Category._ge1Jet, ROOT.H4lBkg.Category._ge2Jet ]

for categ in categories:
    print '\t', 'category ', ROOT.H4lBkg.Category.toString(categ)
    for channel in channels:
        bgm = b.getBkgMeasurement( ROOT.H4lBkg.Observable.pt4l, categ,  channel, 115,130 );
        bgmFullRange = b.getBkgMeasurement( ROOT.H4lBkg.Observable.pt4l, categ,  channel );
        m = bgm.getNorm();
        print ROOT.H4lBkg.Channel.toString( channel ), '\t', m.getString()

        # check some shapes...
        testObs = ROOT.H4lBkg.Observable.m4l;
        if categ==ROOT.H4lBkg.Category._ge1Jet:
            testObs = ROOT.H4lBkg.Observable.jet1pt;
        elif categ==ROOT.H4lBkg.Category._ge2Jet:
            testObs = ROOT.H4lBkg.Observable.mjj;
        bgm = b.getBkgMeasurement( testObs, categ, ROOT.H4lBkg.Channel._2l2e, 115,130 );
        h = bgm.getHistShape();
        if h!=0:        
            if h.GetMean()==0:
                print 'WARNING cannot find shape for category', categ  

#
#
#
print '\n => 4l yields for couplings/EFT categories (118<m4l<129 GeV):'
allCategories = [
    ROOT.H4lBkg.Category._0Jet,
    ROOT.H4lBkg.Category._1JetPt4l60, ROOT.H4lBkg.Category._1JetPt4l60120, ROOT.H4lBkg.Category._1JetPt4lgt120,
    ROOT.H4lBkg.Category._2JetVHPt4l150, ROOT.H4lBkg.Category._2JetVHPt4lgt150,
    ROOT.H4lBkg.Category._2JetVBFPtJ200 , ROOT.H4lBkg.Category._2JetVBFPtJgt200,
    ROOT.H4lBkg.Category._VHlep, ROOT.H4lBkg.Category._ttH ]
    
for categ in allCategories:
    bgm = b.getBkgMeasurement( ROOT.H4lBkg.Observable.bdt, categ,  ROOT.H4lBkg.Channel._4l, 118,129);
    m = bgm.getNorm();
    h_shape_bdt = bgm.getHistShape();
    #   // // TH1F* h_relativeStatVariation_bdt = bgm->getHistRelStatErr(); // no stat error, only syst for bdt
    h_relativeSystVariation_bdt = bgm.getHistRelSystErr();
    # if h_shape_bdt and h_relativeSystVariation_bdt:
    #     ;
    print ROOT.H4lBkg.Category.toString(categ), '\t', m.getString();

print 'done'
