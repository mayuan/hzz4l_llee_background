#!/usr/bin/env python

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

from ROOT import H4lBackgroundReader

bShapes = ROOT.H4lBackgroundReader()
bShapes.loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_RelaxIsoD0.root");

bNormReader = [ ROOT.H4lBackgroundReader(), ROOT.H4lBackgroundReader() ]
for i in range(0,2):
    bNormReader[i].loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_RelaxIsoD0.root");
    bNormReader[i].setNormHistNamePrefix('h_redBkg_{0}_'.format(i))
    bNormReader[i].loadNorms("$ROOTCOREBIN/../H4lBackgroundReader/data/normalizations_RelaxIsoD0_1ifb.root");
    bNormReader[i].setLumiInvFb(36.1);

bNorms = { 'ttbar' : bNormReader[0], 'fakes' : bNormReader[0], 'Zjets' : bNormReader[1], 'HF' : bNormReader[1] }

print '\n => Reducible background, relaxed Iso/D0 region'

llmumuChannels = [ ROOT.H4lBkg.Channel._4mu, ROOT.H4lBkg.Channel._2e2mu, ROOT.H4lBkg.Channel._2l2mu ]
lleeChannels = [ ROOT.H4lBkg.Channel._4e, ROOT.H4lBkg.Channel._2mu2e, ROOT.H4lBkg.Channel._2l2e ]

bkgChannels = { 'ttbar' : llmumuChannels, 'Zjets' : llmumuChannels, 'fakes' : lleeChannels, 'HF' : lleeChannels }

m4lshapes = { 'ttbar' : ROOT.H4lBkg.Observable.m4lttbarRelaxIsoD0unconstrained, 'Zjets': ROOT.H4lBkg.Observable.m4lZjetsRelaxIsoD0unconstrained, 'HF' : ROOT.H4lBkg.Observable.m4lHFRelaxIsoD0unconstrained, 'fakes' : ROOT.H4lBkg.Observable.m4lfakesRelaxIsoD0unconstrained }

for bkg in bkgChannels:
    print bkg
    for channel in bkgChannels[bkg]:
        bgm = bNorms[bkg].getBkgMeasurement( ROOT.H4lBkg.Observable.dummy, ROOT.H4lBkg.Category.prodInclusive, channel );
        bgmShape = bShapes.getBkgMeasurement( m4lshapes[bkg], ROOT.H4lBkg.Category.prodInclusive, channel );
        print ROOT.H4lBkg.Channel.toString(channel), bgm.getNorm().getString(), bgmShape        
        
        
print 'done'
