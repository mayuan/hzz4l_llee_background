#!/usr/bin/env python

import ROOT
ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

from ROOT import H4lBackgroundReader

b = ROOT.H4lBackgroundReader()
b.loadShapes("$ROOTCOREBIN/../H4lBackgroundReader/data/shapes_SignalRegion.root");
b.loadNorms("$ROOTCOREBIN/../H4lBackgroundReader/data/normalizations_m4lFullRange_CategoriesHM_1ifb.root");
b.setLumiInvFb(36.1);

print '\n => Reducible background, high-mass'

categories = [ ROOT.H4lBkg.Category._HMggF, ROOT.H4lBkg.Category._HMVBF ]
for category in categories:
    print '-----------',ROOT.H4lBkg.Category.toString(category),'------------'
    bgm_4mu = b.getBkgMeasurement( ROOT.H4lBkg.Observable.m4lHM, category,  ROOT.H4lBkg.Channel._4mu , 130, 1500);
    bgm_4e = b.getBkgMeasurement( ROOT.H4lBkg.Observable.m4lHM, category,  ROOT.H4lBkg.Channel._4e , 130, 1500);
    bgm_2e2mu = b.getBkgMeasurement( ROOT.H4lBkg.Observable.m4lHM, category,  ROOT.H4lBkg.Channel._2e2mu , 130, 1500);
    bgm_2mu2e = b.getBkgMeasurement( ROOT.H4lBkg.Observable.m4lHM, category,  ROOT.H4lBkg.Channel._2mu2e , 130, 1500);
    bgm_mixed = ROOT.Add(bgm_2mu2e,bgm_2e2mu, category, ROOT.H4lBkg.Channel._4l);

    print ROOT.H4lBkg.Channel.toString( ROOT.H4lBkg.Channel._4mu ), '\t', bgm_4mu.getNorm().getString()
    print ROOT.H4lBkg.Channel.toString( ROOT.H4lBkg.Channel._4e ), '\t', bgm_4e.getNorm().getString()
    print '2e2mu', '\t', bgm_mixed.getNorm().getString()

    ## example of getting shape
    h_m4mu_shape = bgm_4mu.getHistShape();
    
print 'done'
