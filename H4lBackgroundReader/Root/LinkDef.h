#ifndef H4LBACKGROUNDREADER_LINKDEF_H
#define H4LBACKGROUNDREADER_LINKDEF_H

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#endif

#endif
