#include "H4lBackgroundReader/H4lBackgroundReader.h"

#include "H4lBackgroundReader/BkgMeasurement.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TClass.h"
#include "TKey.h"
#include "TDirectory.h"

#include <iostream>
using namespace std;

using namespace H4lBkg;
using namespace Observable;
using namespace Category;
using namespace Channel;
using namespace BkgSample;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
H4lBackgroundReader::H4lBackgroundReader(std::map<TString,float> lumi) :
  m_sNormHistNamePrefix("h_"),
  m_sHistName_n("h_shape_"),
  m_sHistName_staterr("h_stat_"),
  m_sHistName_systerr("h_syst_")
{
  m_lumi=lumi;
  //FIXME hard-coding the number of mass windows
  int nMW=2;
  m_file.resize(nMW);
  dummyCount=0;
  debug=false;
}

H4lBackgroundReader::~H4lBackgroundReader() {;}

void H4lBackgroundReader::loadShapes( TString pathToFile )
{
  //FIXME more hardcoding of what files to expect
  vector<TString> mwin={"","_wide"};
  cout << "Retrieving shapes from file: " << endl;
  for(unsigned int j=0;j<mwin.size();j++){
    for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){
      this->m_file[j][(*mapit).first] = TFile::Open(pathToFile+"_"+(*mapit).first+mwin[j]+".root");
      this->m_file[j][(*mapit).first]->cd();
    }
  }

  m_allEventTypes.clear();
  m_allProdTypes.clear();
  m_allObservables.clear();

  //loop over observables
  TKey *key;
  TFile* tmp;
  if(m_file[0].find("2016")!=m_file[0].end()) tmp=m_file[0]["2016"];
  else if(m_file[0].find("2017")!=m_file[0].end()) tmp=m_file[0]["2017"];
  else if(m_file[0].find("2018")!=m_file[0].end()) tmp=m_file[0]["2018"];
  else if(m_file[0].find("all")!=m_file[0].end()) tmp=m_file[0]["all"];
  else{
    cout<<"Error! No file found"<<endl;
    return;
  }
  TIter nextKey(tmp->GetListOfKeys());
  while ((key = (TKey*)nextKey())) {
    if (! gROOT->GetClass(key->GetClassName())->InheritsFrom("TDirectory")) continue;
    TDirectory* dir_obs = (TDirectory*)key->ReadObj();
    m_allObservables.push_back((TString)dir_obs->GetName());

    if (m_allProdTypes.size()>0) continue; ///////////

    //loop over prod type
    TKey *key2;
    TIter nextKey2(dir_obs->GetListOfKeys());  
    while ((key2 = (TKey*)nextKey2())) {
      if (! gROOT->GetClass(key2->GetClassName())->InheritsFrom("TDirectory")) continue;
      TDirectory* dir_prt = (TDirectory*)key2->ReadObj();
      m_allProdTypes.push_back((TString)dir_prt->GetName());

      if (m_allEventTypes.size()>0) continue; ///////////

      //loop over channel type
      TKey *key3;
      TIter nextKey3(dir_prt->GetListOfKeys());      
      while ((key3 = (TKey*)nextKey3())) {
  	if (! gROOT->GetClass(key3->GetClassName())->InheritsFrom("TDirectory")) continue;
	TDirectory* dir_evt = (TDirectory*)key3->ReadObj();
	m_allEventTypes.push_back((TString)dir_evt->GetName());
      }
    }
  }
  cout << "Found shapes for..." << endl;
  cout << "Event type: "; 
  for (auto s : m_allEventTypes) cout << s <<" ";
  cout << endl;
  cout << "Prod type: ";
  for (auto s : m_allProdTypes) cout << s <<" ";
  cout << endl;
  cout << "Observable: ";
  for (auto s : m_allObservables) cout << s <<" ";
  cout << endl;
  
  return;
}

void H4lBackgroundReader::loadNorms( TString pathToFile )
{
  cout << "Retrieving normalizations from file: " << endl;
  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){
    TFile *f_tmp = TFile::Open(pathToFile+"_"+(*mapit).first+".root");
    m_h_norms[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"n")->Clone("m_h_norms_"+(*mapit).first);
    m_h_norms_stat[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"stat")->Clone("m_h_norms_"+(*mapit).first+"_stat");
    m_h_norms_syst[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"syst")->Clone("m_h_norms_"+(*mapit).first+"_syst");
    m_h_norms[(*mapit).first]->SetDirectory(0);
    m_h_norms_stat[(*mapit).first]->SetDirectory(0);
    m_h_norms_syst[(*mapit).first]->SetDirectory(0);
    f_tmp->Close();
  }
  
  cout << "Found normalizations for..." <<   endl;
  cout << "Event type: ";
  TString ayear;
  if(m_h_norms.find("2016")!=m_h_norms.end()) ayear="2016";
  else if(m_h_norms.find("2017")!=m_h_norms.end()) ayear="2017";
  else if(m_h_norms.find("2018")!=m_h_norms.end()) ayear="2018";
  else if(m_h_norms.find("all")!=m_h_norms.end()) ayear="all";
  else{
    cout<<"Error, no year?!?"<<endl;
    return;
  }

  for (int i=1; i<=m_h_norms[ayear]->GetYaxis()->GetNbins(); i++) cout << m_h_norms[ayear]->GetYaxis()->GetBinLabel(i) <<" ";
  cout << endl;
  cout << "Prod type: ";
  for (int i=1; i<=m_h_norms[ayear]->GetXaxis()->GetNbins(); i++) cout << m_h_norms[ayear]->GetXaxis()->GetBinLabel(i) <<" ";
  cout << endl;
}

void H4lBackgroundReader::loadNormsHM( TString pathToFile )
{
  cout << "Retrieving HM normalizations from file: " << endl;
  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){
    TFile *f_tmp = TFile::Open(pathToFile+"_"+(*mapit).first+".root");
    m_h_norms_hm[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"n_hm")->Clone("m_h_norms_hm_"+(*mapit).first);
    m_h_norms_stat_hm[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"hm_stat")->Clone("m_h_norms_hm_"+(*mapit).first+"_stat");
    m_h_norms_syst_hm[(*mapit).first] = (TH2F*)f_tmp->Get(m_sNormHistNamePrefix+"hm_syst")->Clone("m_h_norms_hm_"+(*mapit).first+"_syst");
    m_h_norms_hm[(*mapit).first]->SetDirectory(0);
    m_h_norms_stat_hm[(*mapit).first]->SetDirectory(0);
    m_h_norms_syst_hm[(*mapit).first]->SetDirectory(0);
    f_tmp->Close();
  }

  cout << "Found HM normalizations for..." <<   endl;
  cout << "Event type: ";
  TString ayear;
  if(m_h_norms_hm.find("2016")!=m_h_norms_hm.end()) ayear="2016";
  else if(m_h_norms_hm.find("2017")!=m_h_norms_hm.end()) ayear="2017";
  else if(m_h_norms_hm.find("2018")!=m_h_norms_hm.end()) ayear="2018";
  else if(m_h_norms_hm.find("all")!=m_h_norms_hm.end()) ayear="all";
  else{
    cout<<"Error, no year?!?"<<endl;
    return;
  }
  for (int i=1; i<=m_h_norms_hm[ayear]->GetYaxis()->GetNbins(); i++) cout << m_h_norms_hm[ayear]->GetYaxis()->GetBinLabel(i) <<" ";
  cout << endl;
  cout << "Prod type: ";
  for (int i=1; i<=m_h_norms_hm[ayear]->GetXaxis()->GetNbins(); i++) cout << m_h_norms_hm[ayear]->GetXaxis()->GetBinLabel(i) <<" ";
  cout << endl;

}

void H4lBackgroundReader::loadNormsRelax( TString pathToFile )
{
  cout << "Retrieving relaxIsoD0 normalizations from file: " << endl;
  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){
    TFile *f_tmp = TFile::Open(pathToFile+"_"+(*mapit).first+".root");
    for(int i=0;i<3;i++){
      m_h_norms_relax[(*mapit).first].push_back((TH2F*)f_tmp->Get(Form("%sredBkg_%i_n",m_sNormHistNamePrefix.Data(),i))->Clone(Form("m_h_norms_relax_%s_%i",(*mapit).first.Data(),i)));
      m_h_norms_stat_relax[(*mapit).first].push_back((TH2F*)f_tmp->Get(Form("%sredBkg_%i_stat",m_sNormHistNamePrefix.Data(),i))->Clone(Form("m_h_norms_relax_%s_stat_%i",(*mapit).first.Data(),i)));
      m_h_norms_relax[(*mapit).first].at(i)->SetDirectory(0);
      m_h_norms_stat_relax[(*mapit).first].at(i)->SetDirectory(0);
    }
    f_tmp->Close();
  }
}

BkgMeasurement* H4lBackgroundReader::getBkgMeasurement( TString obs, ProdType prt, EventType evt, float m4lmin, float m4lmax, TString year, int shapeWindow, vector<ProdType>* m4lfracs )
{
  debug=false;
  //if(prt==ProdType::prodInclusive) debug=true;
  //if(obs.CompareTo("NN")==0 && prt==ProdType::_0JetPt4l100) debug=true;
  //if(prt==ProdType::_0JetPt4l10 || (prt==ProdType::prodInclusive && evt==EventType::_4l && m4lmin==115)) debug=true;
  //if(obs.CompareTo("cth1")==0) debug=true;
  //if(prt==ProdType::_2JetVHVBFPt200_b1) debug=true;
  //if(obs.CompareTo("NN")==0 && prt==ProdType::_SB_0jet) debug=true;
  //if(prt==ProdType::_jet1pt0 || obs.CompareTo("jet1pt")==0 || obs.CompareTo("njets")==0) debug=true;
  //if(obs.CompareTo("bdt")==0 && shapeWindow) debug=true;
  //if(obs.CompareTo("mjj")==0) debug=true;
  //if(prt==ProdType::_0Jet || obs=="njets") debug=true;
  //if(prt==ProdType::_1Jet) debug=true;
  bool getM4lFrac=false;
  if(!m4lfracs && obs!="m4l") getM4lFrac=true;

  if(m_lumi.find(year)==m_lumi.end() && year!="all"){
    cout<<"year "<<year<<" not recognized!  Please input 2016, 2017, 2018, or inc"<<endl;
    return 0;
  }
  else if(debug){
    if(year=="all") cout<<"get background measurement for 2015-18"<<endl;
    else cout<<"get background measurement for "<<year<<endl;
  }

  if(shapeWindow!=0 && shapeWindow!=1){
    cout<<"shape window "<<shapeWindow<<" not recognized, please input 0 for 115-130 or 1 for 105-160"<<endl;
    return 0;
  }
  else if(debug){
    if(shapeWindow==0) cout<<"get shape for 115<m4l<130"<<endl;
    else cout<<"get shape for 105<m4l<160"<<endl;
  }

  if (debug) cout << "in getBkgMeasurement, " << obs << " " << toString(prt) << " " << toString(evt)  << endl;

  vector<BkgMeasurement*> bVec;
  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){

    if(year!=(*mapit).first && year!="all") continue; //in this case, we only do one year
    
    BkgMeasurement* b;
    if(getM4lFrac) b = this->findShapeFromFile( obs, prt, evt, (*mapit).first, shapeWindow );
    else b=this->findShapeFromFile( obs, prt, evt, (*mapit).first, shapeWindow, m4lmin, m4lmax, m4lfracs );
    bool addShapes=false;
  
    if (!b) {
      if (debug) cout << "  shape not found" << endl;
      
      if (evt==EventType::_4l) {
	if (debug) cout << "  asking for 4l, adding up 2l2e and 2l2mu" << endl;
	b = addShapesFromChannels( EventType::_2l2e, EventType::_2l2mu, obs, prt, (*mapit).first, shapeWindow, m4lmin, m4lmax );
	addShapes=true;
	if (!b) {
	  if (debug) cout << "ERROR Can't find shape for " << obs << " " << toString(prt) << endl;
	  b = getDummyMeasurement(obs,prt,evt);
	}
      }
      else {
	ProdType tmpProdType = prt;
	EventType tmpChannel = evt;
	if (prt!=ProdType::prodInclusive) {
	  tmpProdType = ProdType::prodInclusive;
	}    
	if (evt==EventType::_4e || evt==EventType::_2mu2e) {
	  tmpChannel = EventType::_2l2e;
	}
	else if (evt==EventType::_4mu || evt==EventType::_2e2mu) {
	  tmpChannel = EventType::_2l2mu;
	}
	
	if (debug) cout << "  trying to retrieve shape for " << toString(tmpProdType) << " " << toString(tmpChannel) << endl;
	if(getM4lFrac) b = this->findShapeFromFile( obs, tmpProdType, tmpChannel, (*mapit).first, shapeWindow );
	else b = this->findShapeFromFile( obs, tmpProdType, tmpChannel, (*mapit).first, shapeWindow, m4lmin, m4lmax, m4lfracs );
      }
      if (!b) {
	cout << "ERROR Couldn't find shape " << obs << " " << toString(prt) << " " << toString(evt)  << " for year " << (*mapit).first << endl;
	return 0;
      }
    }

    if(debug) cout<<"got shapes"<<endl;
    if(!addShapes){
      float m4lFrac;
      if(getM4lFrac) m4lFrac = (m4lmin || m4lmax) ? getFractionInM4lRegion( evt, prt, m4lmin, m4lmax, (*mapit).first, shapeWindow ) : 1;
      else m4lFrac=1; //if observable is m4l, doesn't make sense to get the fraction; njets is handled separately so each bin gets the correct fraction
      if (debug) cout << " frac in m4l region " << m4lFrac << endl;
      float n=0,stat=0,syst=0;
      this->findNormFromHist(n,stat,syst, prt, evt, (*mapit).first);
      if (!n) cout << "WARNING Did not find normalization" << endl;
      else if(debug) cout<<"got normalization "<<n<<"+/-"<<stat<<"(rel)+/-"<<syst<<"(rel)"<<endl;
      b->setNorm( n*m4lFrac, stat, syst, (*mapit).second );
    }
    else{
      if(debug) cout<<"normalizations were accounted for when adding shapes"<<endl;
    }
  
    if(year!="all" || m_lumi.size()==1) return b; //for just the requested year
    bVec.push_back(b);
  }
  if(debug) cout<<"now add "<<bVec.size()<<" measurements"<<endl;

  BkgMeasurement* bTot=Add(bVec, prt, evt, false);
  
  return bTot;
}

BkgMeasurement* H4lBackgroundReader::getBkgMeasurement( TString obs, ProdTypeHM prt, EventType evt, float m4lmin, float m4lmax, TString year )
{
  //note: for now this can only be called with obs=dummy or m4lHM
  debug=false;

  if(m_lumi.find(year)==m_lumi.end() && year!="all"){
    cout<<"year "<<year<<" not recognized!  Please input 2015, 2017, 2018, or all"<<endl;
    return 0;
  }
  else if(debug){
    if(year=="all") cout<<"get background measurement for 2015-18"<<endl;
    else cout<<"get background measurement for "<<year<<endl;
  }

  if (debug) cout << "in getBkgMeasurement, " << obs << " " << toString(prt) << " " << toString(evt)  << endl;

  float m4lFrac = 0;

  vector<BkgMeasurement*> bVec;

  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){

    bool addShapes=false;
    if(year!=(*mapit).first && year!="all") continue; //in this case, we only do one year
    BkgMeasurement* b = this->findShapeFromFile( obs, prt, evt, (*mapit).first );
    if (!b) {
      if (debug) cout << "  shape not found" << endl;

      if (evt==EventType::_4l) {
        if (debug) cout << "  asking for 4l, adding up 2l2e and 2l2mu" << endl;
        b = addShapesFromChannels( EventType::_2l2e, EventType::_2l2mu, obs, prt, (*mapit).first, m4lmin, m4lmax );
	addShapes=true;
        if (!b) {
          if (debug) cout << "ERROR Can't find shape for " << obs << " " << toString(prt) << endl;
          b = getDummyMeasurement(obs,prt,evt);
        }
      }
    }
    if(debug) cout<<"got shapes"<<endl;
    if(!addShapes){
      if (!m4lFrac) m4lFrac = (m4lmin || m4lmax) ? getFractionInM4lRegion( evt, prt, m4lmin, m4lmax, (*mapit).first ) : 1;
      if (debug) cout << " frac in m4l region " << m4lFrac << endl;
      float n=0,stat=0,syst=0;
      this->findNormFromHist(n,stat,syst, prt, evt, (*mapit).first);
      if (!n) cout << "WARNING Did not find normalization" << endl;
      else if(debug) cout<<"got normalization "<<n<<"+/-"<<stat<<"%+/-"<<syst<<"%"<<endl;

      b->setNorm( n*m4lFrac, stat, syst, (*mapit).second );
    }

    if(year!="all" || m_lumi.size()==1) return b; //for just the requested year
    bVec.push_back(b);
  }

  BkgMeasurement* bTot=Add(bVec, prt, evt, false);

  return bTot;
}

void H4lBackgroundReader::findNormFromHist(float&n, float& stat,float&syst, ProdType prt, EventType evt, TString year)
{
  if(debug) cout<<"in findNormFromHist, "<<toString(prt)<<", "<<toString(evt)<<", "<<year<<endl;
  if (m_h_norms.find(year)==m_h_norms.end()) {
    cout << "WARNING Normalizations not set!" << endl;
    n = 1; stat = 0; syst = 0;
    return;
  }
  int i = m_h_norms[year]->GetXaxis()->FindBin( toString(prt) );
  int j = m_h_norms[year]->GetYaxis()->FindBin( toString(evt) );

  n = m_h_norms[year]->GetBinContent(i,j);
  stat = m_h_norms_stat[year]->GetBinContent(i,j);
  syst = m_h_norms_syst[year]->GetBinContent(i,j);
}

void H4lBackgroundReader::findNormFromHist(float&n, float& stat,float&syst, ProdTypeHM prt, EventType evt, TString year)
{
  if(debug) cout<<"in findNormFromHist"<<endl;
  if (m_h_norms_hm.find(year)==m_h_norms_hm.end()) {
    cout << "WARNING HM Normalizations not set!" << endl;
    n = 1; stat = 0; syst = 0;
    return;
  }
  int i = m_h_norms_hm[year]->GetXaxis()->FindBin( toString(prt) );
  int j = m_h_norms_hm[year]->GetYaxis()->FindBin( toString(evt) );

  n = m_h_norms_hm[year]->GetBinContent(i,j);
  stat = m_h_norms_stat_hm[year]->GetBinContent(i,j);
  syst = m_h_norms_syst_hm[year]->GetBinContent(i,j);
}

BkgMeasurement* H4lBackgroundReader::findShapeFromFile(TString obs, ProdType prt, EventType evt, TString year, int shapeWindow, float m4lmin, float m4lmax, vector<ProdType>* m4lfracs )
{  
  if(debug) cout << "in findShapeFromFile" << endl;

  if (obs.CompareTo("dummy")==0) {
    if(debug) cout<<"want a dummy, don't look for a shape after all"<<endl;
    return getDummyMeasurement(obs,prt,evt);
  }

  TString s_prt = toString(prt);
  TString s_evt = toString(evt);
    
  if(debug) cout<<"find shape for "<<obs<<", "<<s_prt<<", "<<s_evt<<", "<<year<<endl;

  TString hpath = obs+"/"+s_prt+"/"+s_evt;
  if(debug) cout << hpath << endl;
  TString hname = hpath;
  hname.ReplaceAll("/","_");

  TH1F* hn = 0;
  TH1F* hstat = 0;
  TH1F* hsyst_shape_up = 0;
  TH1F* hsyst_shape_dn = 0;

  this->m_file[shapeWindow][year]->cd();
  if(debug) cout<<"get shape from file "<<m_file[shapeWindow][year]->GetName()<<endl;

  hn = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_n+hname).Data() );
  if(debug) cout<<"get shape from path "<<hpath+"/"+m_sHistName_n+hname<<endl;
  if (hn) hn = (TH1F*)hn->Clone();
  hstat = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_staterr+hname).Data() );
  if (hstat) hstat = (TH1F*)hstat->Clone();
  /*
  if((evt==EventType::_4mu || evt==EventType::_2e2mu || evt==EventType::_2l2mu) && !obs.Contains("m4l") && !obs.Contains("NN") && !obs.Contains("bdt")){ //llmumu diffxs shapes
    hsyst_shape_up = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+hname).Data());
    if (hsyst_shape_up) hsyst_shape_up = (TH1F*)hsyst_shape_up->Clone();
    hsyst_shape_dn=new TH1F(Form("h_syst_dn_%s",hname.Data()),"",1,0,1);
  }
  */
  if(!obs.Contains("m4l") && /*!obs.Contains("NN") &&*/ /*!obs.Contains("bdt") &&*/ !s_prt.Contains("SB")){
    if(debug) cout<<"retrieve up syst with name "<<hpath+"/"+m_sHistName_systerr+"shape_up_"+hname<<endl;
    hsyst_shape_up = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"shape_up_"+hname).Data() );
    if (hsyst_shape_up) hsyst_shape_up = (TH1F*)hsyst_shape_up->Clone();
    hsyst_shape_dn = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"shape_dn_"+hname).Data() );
    if (hsyst_shape_dn) hsyst_shape_dn = (TH1F*)hsyst_shape_dn->Clone();
  }
  else if(obs.Contains("SB")){
    if(debug) cout<<"retrieve up syst with name "<<hpath+"/"+m_sHistName_systerr+"shape_up_"+hname<<endl;
    hsyst_shape_up = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"up_"+hname).Data() );
    if (hsyst_shape_up) hsyst_shape_up = (TH1F*)hsyst_shape_up->Clone();
    hsyst_shape_dn = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"dn_"+hname).Data() );
    if (hsyst_shape_dn) hsyst_shape_dn = (TH1F*)hsyst_shape_dn->Clone();
  }
  else if(obs.Contains("m4l")/* || obs.Contains("NN")*/){
    hsyst_shape_up=new TH1F(Form("h_syst_up_%s",hname.Data()),"",1,0,1);
    hsyst_shape_dn=new TH1F(Form("h_syst_dn_%s",hname.Data()),"",1,0,1);
  }
  else{
    hsyst_shape_up = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"up_"+hname).Data() );
    if (hsyst_shape_up) hsyst_shape_up = (TH1F*)hsyst_shape_up->Clone();
    hsyst_shape_dn = (TH1F*) this->m_file[shapeWindow][year]->Get( (hpath+"/"+m_sHistName_systerr+"dn_"+hname).Data() );
    if (hsyst_shape_dn) hsyst_shape_dn = (TH1F*)hsyst_shape_dn->Clone();
  }
  
  if (!hn || !hstat || !hsyst_shape_up || !hsyst_shape_dn){
    if(debug){
      if(!hn) cout<<"no shape hist"<<endl;
      if(!hstat) cout<<"no stat hist"<<endl;
      if(!hsyst_shape_up) cout<<"no up syst"<<endl;
      if(!hsyst_shape_dn) cout<<"no down syst"<<endl;
    }
    /*
    cout<<"failed to find the following histograms for : "<<obs<<", "<<s_prt<<", "<<s_evt<<", "<<year;
    if(!hn) cout<<" norm ";
    if(!hstat) cout<<" stat ";
    if(!hsyst_shape_up) cout<<" syst up ";
    if(!hsyst_shape_dn) cout<<" syst dn ";
    cout<<endl;
    */
// cout << "WARNING: Can't find histograms for measurement " << toString(evt) << ", " << toString(prt) << endl;
//  cout << (hpath+"/"+m_sHistName_n+hname) << endl;
//  cout << hn << " " << hstat << " " << hsyst << endl;
    if(hsyst_shape_up) delete hsyst_shape_up;
    if(hsyst_shape_dn) delete hsyst_shape_dn;
    return 0;
  }
  //cout << hn->GetName() << " " << hstat->GetName() << " " << hsyst->GetName() << endl;
  Measurement emptyNorm(1,0,0,1);
  if(m4lfracs!=nullptr){ //special: apply the appropriate m4l fraction to each bin
    for(int i=0;i<hn->GetNbinsX();i++){
      ProdType njettype=m4lfracs->at(i);
      float m4lfrac=getFractionInM4lRegion( evt, njettype, m4lmin, m4lmax, year, shapeWindow );
      if(debug) cout<<"m4l fraction for "<<toString(njettype)<<"="<<m4lfrac<<endl;
      hn->SetBinContent(i+1,m4lfrac*hn->GetBinContent(i+1));
      hsyst_shape_up->SetBinContent(i+1,m4lfrac*hsyst_shape_up->GetBinContent(i+1));
      hsyst_shape_dn->SetBinContent(i+1,m4lfrac*hsyst_shape_dn->GetBinContent(i+1));
    }
  }
  return new BkgMeasurement( emptyNorm, hn, hstat, hsyst_shape_up, hsyst_shape_dn, obs, prt, evt);
}

BkgMeasurement* H4lBackgroundReader::findShapeFromFile(TString obs, ProdTypeHM prt, EventType evt, TString year)
{
  if(debug) cout << "in HM findShapeFromFile" << endl;

  if (obs.CompareTo("dummy")==0) {
    if(debug) cout<<"want a dummy, don't look for a shape after all"<<endl;
    return getDummyMeasurement(obs,prt,evt);
  }
  else if(obs.CompareTo("m4lHM")==0){
    TString s_prt = toString(prt);
    TString s_evt = toString(evt);

    if(debug) cout<<"find shape for "<<obs<<", "<<s_prt<<", "<<s_evt<<", "<<year<<endl;

    TString hpath = obs+"/"+s_prt+"/"+s_evt;
    if(debug) cout << hpath << endl;
    TString hname = hpath;
    hname.ReplaceAll("/","_");

    TH1F* hn = 0;
    TH1F* hstat = 0;
    TH1F* hsyst_shape_up = 0;
    TH1F* hsyst_shape_dn = 0;

    this->m_file[0][year]->cd();
    if(debug) cout<<"get shape from file "<<m_file[0][year]->GetName()<<endl;

    hn = (TH1F*) this->m_file[0][year]->Get( (hpath+"/"+m_sHistName_n+hname).Data() );
    if(debug) cout<<"get shape from path "<<hpath+"/"+m_sHistName_n+hname<<endl;
    if (hn) hn = (TH1F*)hn->Clone();
    hstat = (TH1F*) this->m_file[0][year]->Get( (hpath+"/"+m_sHistName_staterr+hname).Data() );
    if (hstat) hstat = (TH1F*)hstat->Clone();
    hsyst_shape_up = (TH1F*) this->m_file[0][year]->Get( (hpath+"/"+m_sHistName_systerr+"up_"+hname).Data() );
    if (hsyst_shape_up) hsyst_shape_up = (TH1F*)hsyst_shape_up->Clone();
    hsyst_shape_dn = (TH1F*) this->m_file[0][year]->Get( (hpath+"/"+m_sHistName_systerr+"dn_"+hname).Data() );
    if (hsyst_shape_dn) hsyst_shape_dn = (TH1F*)hsyst_shape_dn->Clone();
    if (!hn || !hstat || !hsyst_shape_up || !hsyst_shape_dn){
      if(debug){
	if(!hn) cout<<"no shape hist"<<endl;
	if(!hstat) cout<<"no stat hist"<<endl;
	if(!hsyst_shape_up) cout<<"no up syst"<<endl;
	if(!hsyst_shape_dn) cout<<"no down syst"<<endl;
      }
      if(hn) delete hn;
      if(hstat) delete hstat;
      if(hsyst_shape_up) delete hsyst_shape_up;
      if(hsyst_shape_dn) delete hsyst_shape_dn;
      return 0;
    }
    Measurement emptyNorm(1,0,0,1);
    return new BkgMeasurement( emptyNorm, hn, hstat, hsyst_shape_up, hsyst_shape_dn, obs, prt, evt);
  }
  else cout<<"WARNING cannot use HM findShapeFromFile for "<<obs<<endl;
  return 0;
}

BkgMeasurement* H4lBackgroundReader::getDummyMeasurement(TString obs, ProdType prt, EventType evt)
{
  TString hname = obs+"_"+toString(prt)+"_"+toString(evt);  
  TH1F* hn = new TH1F(Form("h_shape_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hstat = new TH1F(Form("h_stat_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_up = new TH1F(Form("h_syst_up_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_dn = new TH1F(Form("h_syst_dn_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  Measurement emptyNorm(1,0,0,1);
  dummyCount++;
  return new BkgMeasurement( emptyNorm, hn, hstat, hsyst_up, hsyst_dn, obs, prt, evt);
}

BkgMeasurement* H4lBackgroundReader::getDummyMeasurement(TString obs, ProdTypeHM prt, EventType evt)
{
  TString hname = obs+"_"+toString(prt)+"_"+toString(evt);
  TH1F* hn = new TH1F(Form("h_shape_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hstat = new TH1F(Form("h_stat_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_up = new TH1F(Form("h_syst_up_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_dn = new TH1F(Form("h_syst_dn_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  Measurement emptyNorm(1,0,0,1);
  dummyCount++;
  return new BkgMeasurement( emptyNorm, hn, hstat, hsyst_up, hsyst_dn, obs, prt, evt);
}

float H4lBackgroundReader::getFractionInM4lRegion(EventType channel, ProdType category, float xmin, float xmax, TString year, int shapeWindow)
{
  //debug=true;
  if(debug) cout<<"get m4l fraction"<<endl;
  BkgMeasurement* m4l = 0;
  ProdType useCategory=ProdType::prodInclusive;
  if(toString(category).Contains("0Jet")) useCategory=ProdType::_0Jet;
  else if(toString(category).CompareTo("ge1Jet")==0/* || toString(category).Contains("jet1pt")*/) useCategory=ProdType::_ge1Jet;
  else if(toString(category)=="le1Jet") useCategory=ProdType::_le1Jet;
  else if(toString(category).Contains("1Jet")) useCategory=ProdType::_1Jet;
  else if(toString(category).CompareTo("2Jet")==0) useCategory=ProdType::_2Jet;
  else if(toString(category).Contains("2Jet") || toString(category).Contains("VHlep") || toString(category).Contains("ttH")) useCategory=ProdType::_ge2Jet;
  else if(toString(category).Contains("Jet")) useCategory=ProdType::_ge3Jet;
  else useCategory=category;
  if(debug) cout<<"get m4l fraction using category "<<toString(useCategory)<<endl;
  m4l = this->findShapeFromFile("m4l", useCategory, channel, year, shapeWindow);

  if (!m4l) {
    if (channel==EventType::_4l)  {
      if(debug) cout<<"no 4l m4l shape, try adding 2l2e and 2l2mu"<<endl;
      m4l = addShapesFromChannels(EventType::_2l2e, EventType::_2l2mu, "m4l", category, year, shapeWindow);
    }
    else {
      EventType tmpChannel = channel;
      if (channel==EventType::_4e || channel==EventType::_2mu2e)  tmpChannel = EventType::_2l2e;
      else if (channel==EventType::_4mu || channel==EventType::_2e2mu) tmpChannel = EventType::_2l2mu;
      if(debug) cout<<"no m4l shape, try channel "<<toString(tmpChannel)<<endl;
      m4l = findShapeFromFile("m4l", useCategory, tmpChannel, year, shapeWindow);
    }
    //still not finding m4l? use fraction for inclusive production
    if (!m4l) {
      if(debug) cout<<"fall back on inclusive"<<endl;
      if (category!=ProdType::prodInclusive) return getFractionInM4lRegion(channel, ProdType::prodInclusive, xmin, xmax, year, shapeWindow);
      else {
	cout << "ERROR, can't find inclusive m4l shape for channel " << toString(channel) << " year " << year << endl;
	return 0;
      }
    }
  }
  TH1F* h_m4l = (TH1F*)m4l->getHistShape()->Clone("h_m4l");
  int m1 = h_m4l->FindBin(xmin);
  int m2 = h_m4l->FindBin(xmax);
  float fraction= h_m4l->Integral(m1,m2)/h_m4l->Integral();
  delete h_m4l;
  delete m4l;
  return fraction;
}

float H4lBackgroundReader::getFractionInM4lRegion(EventType channel, ProdTypeHM category, float xmin, float xmax, TString year)
{
  if(debug) cout<<"get m4l fraction"<<endl;
  BkgMeasurement* m4lHM = 0;
  if(debug) cout<<"get m4l fraction using category "<<toString(category)<<endl;
  m4lHM = this->findShapeFromFile("m4lHM", category, channel, year);

  if (!m4lHM) {
    if (channel==EventType::_4l)  {
      if(debug) cout<<"no 4l m4l shape, try adding 2l2e and 2l2mu"<<endl;
      m4lHM = addShapesFromChannels(EventType::_2l2e, EventType::_2l2mu, "m4lHM", category, year);
    }
    else {
      EventType tmpChannel = channel;
      if (channel==EventType::_4e || channel==EventType::_2mu2e)  tmpChannel = EventType::_2l2e;
      else if (channel==EventType::_4mu || channel==EventType::_2e2mu) tmpChannel = EventType::_2l2mu;
      if(debug) cout<<"no m4l shape, try channel "<<toString(tmpChannel)<<endl;
      m4lHM = findShapeFromFile("m4lHM", category, tmpChannel, year);
    }
  }
  TH1F* h_m4l_hm = (TH1F*)m4lHM->getHistShape()->Clone("h_m4l_hm");
  int m1 = h_m4l_hm->FindBin(xmin);
  int m2 = h_m4l_hm->FindBin(xmax);
  float fraction= h_m4l_hm->Integral(m1,m2)/h_m4l_hm->Integral();
  delete h_m4l_hm;
  delete m4lHM;
  return fraction;
}

BkgMeasurement* H4lBackgroundReader::addShapesFromChannels(EventType chan1, EventType chan2, TString obs, ProdType prt, TString year, int shapeWindow, float m4lmin, float m4lmax)
{

  if(debug) cout<<"adding "<<obs<<" shapes for "<<toString(prt)<<" from "<<toString(chan1)<<" and "<<toString(chan2)<<" from "<<year<<endl;

  ProdType shapeCategory=prt;
  if(obs=="m4l"){
    if(toString(prt).Contains("0Jet")) shapeCategory=ProdType::_0Jet;
    else if(toString(prt).CompareTo("ge1Jet")==0) shapeCategory=ProdType::_ge1Jet;
    else if(toString(prt).Contains("1Jet")) shapeCategory=ProdType::_1Jet;
    else if(toString(prt).CompareTo("2Jet")==0) shapeCategory=ProdType::_2Jet;
    else if(toString(prt).Contains("2Jet") || toString(prt).Contains("VHlep") || toString(prt).Contains("ttH")) shapeCategory=ProdType::_ge2Jet;
    else if(toString(prt).Contains("Jet")) shapeCategory=ProdType::_ge3Jet;
  }

  BkgMeasurement *b1 = this->findShapeFromFile( obs, shapeCategory, chan1, year, shapeWindow );
  BkgMeasurement *b2 = this->findShapeFromFile( obs, shapeCategory, chan2, year, shapeWindow );

  if (!b1 || !b2) {
    if(obs.CompareTo("m4l")!=0){
      if(!b1) cout<<"ERROR failed to find shape for "<<toString(chan1)<<","<<obs<<","<<toString(prt)<<endl;
      if(!b2) cout<<"ERROR failed to find shape for "<<toString(chan2)<<","<<obs<<","<<toString(prt)<<endl;
    }
    //cout << " ERROR: Can't find shape for " << obs << ", " << toString(prt) << endl;
    return 0;
  }

  EventType chanComb;
  if ((chan1 == EventType::_2l2e && chan2 == EventType::_2l2mu) ||
      (chan2 == EventType::_2l2e && chan1 == EventType::_2l2mu) ) chanComb = EventType::_4l;
  else if ((chan1 == EventType::_4e && chan2 == EventType::_2mu2e) ||
	   (chan2 == EventType::_4e && chan1 == EventType::_2mu2e) ) chanComb = EventType::_2l2e;
  else if ((chan1 == EventType::_4mu && chan2 == EventType::_2e2mu) ||
	   (chan2 == EventType::_4mu && chan1 == EventType::_2e2mu) ) chanComb = EventType::_2l2mu;
  else {
    cout << "ERROR trying to merge channels " << toString(chan1) << " " << toString(chan2) << endl;
    return 0;
  }

  float n1=0,stat1=0,syst1=0,n2=0,stat2=0,syst2=0;
  this->findNormFromHist(n1,stat1,syst1, prt, chan1, year);
  if (!n1) cout << "WARNING Did not find normalization" << endl;
    
  this->findNormFromHist(n2,stat2,syst2, prt, chan2, year);
  if (!n2) cout << "WARNING Did not find normalization" << endl;

  float m4lFrac1=1,m4lFrac2=1;
  if(!obs.Contains("m4l") && (m4lmin>0 || m4lmax>0)){
    m4lFrac1 = getFractionInM4lRegion( chan1, prt, m4lmin, m4lmax, year, shapeWindow );
    m4lFrac2 = getFractionInM4lRegion( chan2, prt, m4lmin, m4lmax, year, shapeWindow );
  }

  b1->setNorm(n1*m4lFrac1,stat1,syst1,m_lumi[year]);
  b2->setNorm(n2*m4lFrac2,stat2,syst2,m_lumi[year]);
  
  BkgMeasurement *b = Add(b1,b2,prt,chanComb,true);
  delete b1; delete b2;
  
  return b;
}

BkgMeasurement* H4lBackgroundReader::addShapesFromChannels(EventType chan1, EventType chan2, TString obs, ProdTypeHM prt, TString year, float m4lmin, float m4lmax)
{
  BkgMeasurement *b1 = this->findShapeFromFile( obs, prt, chan1, year );
  BkgMeasurement *b2 = this->findShapeFromFile( obs, prt, chan2, year );

  if (!b1 || !b2) {
    if(obs.CompareTo("m4l")!=0){
      if(!b1) cout<<"ERROR failed to find shape for "<<toString(chan1)<<","<<obs<<","<<toString(prt)<<endl;
      if(!b2) cout<<"ERROR failed to find shape for "<<toString(chan2)<<","<<obs<<","<<toString(prt)<<endl;
    }
    return 0;
  }

  EventType chanComb;
  if ((chan1 == EventType::_2l2e && chan2 == EventType::_2l2mu) ||
      (chan2 == EventType::_2l2e && chan1 == EventType::_2l2mu) ) chanComb = EventType::_4l;
  else if ((chan1 == EventType::_4e && chan2 == EventType::_2mu2e) ||
           (chan2 == EventType::_4e && chan1 == EventType::_2mu2e) ) chanComb = EventType::_2l2e;
  else if ((chan1 == EventType::_4mu && chan2 == EventType::_2e2mu) ||
           (chan2 == EventType::_4mu && chan1 == EventType::_2e2mu) ) chanComb = EventType::_2l2mu;
  else {
    cout << "ERROR trying to merge channels " << toString(chan1) << " " << toString(chan2) << endl;
    return 0;
  }

  float m4lFrac1=1,m4lFrac2=1;
  if(m4lmin>0 || m4lmax>0){
    m4lFrac1 = getFractionInM4lRegion( chan1, prt, m4lmin, m4lmax, year);
    m4lFrac2 = getFractionInM4lRegion( chan2, prt, m4lmin, m4lmax, year);
  }

  float n1=0,stat1=0,syst1=0;
  this->findNormFromHist(n1,stat1,syst1, prt, chan1, year);
  if (!n1) cout << "WARNING Did not find normalization" << endl;
  float n2=0,stat2=0,syst2=0;
  this->findNormFromHist(n2,stat2,syst2, prt, chan2, year);
  if (!n2) cout << "WARNING Did not find normalization" << endl;

  b1->setNorm(n1*m4lFrac1,stat1,syst1,m_lumi[year]);
  b2->setNorm(n2*m4lFrac2,stat2,syst2,m_lumi[year]);

  BkgMeasurement *b = Add(b1,b2,prt,chanComb,true);
  delete b1; delete b2;

  return b;
}

BkgMeasurement* H4lBackgroundReader::getBkgMeasurementRelax( ProdType prt, EventType evt, BkgComponent comp, TString year )
{
  debug=false;

  if(m_lumi.find(year)==m_lumi.end() && year!="all"){
    cout<<"year "<<year<<" not recognized!  Please input 2015, 2017, 2018, or all"<<endl;
    return 0;
  }
  else if(debug){
    if(year=="all") cout<<"get background measurement for 2015-18"<<endl;
    else cout<<"get background measurement for "<<year<<endl;
  }

  if (debug) cout << "in getBkgMeasurementRelax, " << toString(prt) << " " << toString(evt) << toString(comp) << endl;

  vector<BkgMeasurement*> bVec;

  for(map<TString,float>::iterator mapit=m_lumi.begin();mapit!=m_lumi.end();++mapit){

    if(year!=(*mapit).first && year!="all") continue; //in this case, we only do one year
    BkgMeasurement* b = this->getDummyRelaxMeasurement(prt,evt,comp);

    float n=0,stat=0;
    this->findRelaxNormFromHist(n,stat, prt, evt, comp, (*mapit).first);
    if (!n) cout << "WARNING Did not find normalization" << endl;
    else if(debug) cout<<"got normalization "<<n<<"+/-"<<stat<<"%"<<endl;

    b->setNorm( n, stat, 0, (*mapit).second );

    if(year!="all" || m_lumi.size()==1) return b; //for just the requested year
    bVec.push_back(b);
  }

  BkgMeasurement* bTot=Add(bVec, prt, evt, false);


  return bTot;
}

BkgMeasurement* H4lBackgroundReader::getDummyRelaxMeasurement(ProdType prt, EventType evt, BkgComponent comp)
{
  TString hname = "relax_"+toString(prt)+"_"+toString(evt)+"_"+toString(comp);
  TH1F* hn = new TH1F(Form("h_shape_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hstat = new TH1F(Form("h_stat_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_up = new TH1F(Form("h_syst_up_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  TH1F* hsyst_dn = new TH1F(Form("h_syst_dn_%s_%i",hname.Data(),dummyCount),"",1,0,1);
  Measurement emptyNorm(1,0,0,1);
  dummyCount++;
  return new BkgMeasurement( emptyNorm, hn, hstat, hsyst_up, hsyst_dn, "dummy", prt, evt);
}

void H4lBackgroundReader::findRelaxNormFromHist(float&n, float& stat,ProdType prt, EventType evt, BkgComponent comp, TString year)
{
  if(debug) cout<<"in findRelaxNormFromHist"<<endl;
  if (m_h_norms_relax.find(year)==m_h_norms_relax.end()) {
    cout << "WARNING relaxIsoD0 normalizations not set!" << endl;
    n = 1; stat = 0;
    return;
  }
  int i = m_h_norms_relax[year].at(0)->GetXaxis()->FindBin( toString(prt) );
  int j = m_h_norms_relax[year].at(0)->GetYaxis()->FindBin( toString(evt) );
  int k=toInt(comp);
  if(k>1) k-=2;

  n = m_h_norms_relax[year].at(k)->GetBinContent(i,j);
  stat = m_h_norms_stat_relax[year].at(k)->GetBinContent(i,j);
}
