#include "H4lBackgroundReader/BkgMeasurement.h"

#include <iostream>
using namespace std;

using namespace H4lBkg;
using namespace Observable;
using namespace Category;
using namespace Channel;

 
BkgMeasurement::BkgMeasurement(Measurement m, TH1F* shape, TH1F* relstat, TH1F* relsyst_shape_up, TH1F* relsyst_shape_dn, TString obs, ProdType ptype, EventType evtype):
  m_h_n_wStatError(0), m_h_n_wSystError(0), m_h_n_wTotError(0)
{
  m_observable = obs;
  m_event_type = evtype;
  m_prod_type = ptype;
  m_prod_type_hm=ProdTypeHM::Max;
  
  m_h_shape = shape;
  m_h_relstat = relstat;
  m_h_relsyst_up = relsyst_shape_up;
  m_h_relsyst_dn = relsyst_shape_dn;

  norm=m;  
}

BkgMeasurement::BkgMeasurement(Measurement m, TH1F* shape, TH1F* relstat, TH1F* relsyst_shape_up, TH1F* relsyst_shape_dn, TString obs, ProdTypeHM ptype, EventType evtype):
  m_h_n_wStatError(0), m_h_n_wSystError(0), m_h_n_wTotError(0)
{
  m_observable = obs;
  m_event_type = evtype;
  m_prod_type_hm = ptype;
  m_prod_type=ProdType::Max;

  m_h_shape = shape;
  m_h_relstat = relstat;
  m_h_relsyst_up = relsyst_shape_up;
  m_h_relsyst_dn = relsyst_shape_dn;

  norm=m;
}

BkgMeasurement::~BkgMeasurement(){
  if(m_observable.CompareTo("m4l")==0){
    delete m_h_relsyst_up;
    delete m_h_relsyst_dn;
  }
}

/*
TH1F* BkgMeasurement::getHistYieldsWithErrors(bool isStat)
{
  Measurement m = this->getNorm();
  float err=0;
  TH1F *h_n_wError = 0;
  TH1F *h_relerr = 0; 
  if (isStat) {
    err = m.stat;
    h_n_wError = m_h_n_wStatError;
    h_relerr = m_h_relstat;
  } else {
    err = m.syst;
    h_n_wError = m_h_n_wSystError;
    h_relerr = m_h_relsyst;
  }
			
  h_n_wError = (TH1F*)m_h_shape->Clone();
  h_n_wError->Scale( m.n /h_n_wError->Integral());
  for (int i=1; i<=this->m_h_shape->GetNbinsX(); i++){
    float relerr =  h_relerr->GetBinContent(i)*h_n_wError->GetBinContent(i);
    if (relerr==0) relerr = err*h_n_wError->GetBinContent(i) / m.n;
    h_n_wError->SetBinError( i, relerr );
  }
  return h_n_wError;
}

TH1F* BkgMeasurement::getHistYieldsWithTotalErrors()
{
  if (this->m_h_n_wTotError) return this->m_h_n_wTotError;
  
  TString hname = this->m_h_n_wStatError->GetName();
  hname.ReplaceAll("stat","tot");
  this->m_h_n_wTotError = (TH1F*)this->m_h_n_wStatError->Clone(hname.Data());      
  for (int i=1; i<=this->m_h_n_wStatError->GetNbinsX(); i++){
    float stat = this->m_h_n_wStatError->GetBinError(i);
    float syst = this->m_h_n_wSystError->GetBinError(i);
    float tot = sqrt(stat*stat+syst*syst);
    this->m_h_n_wTotError->SetBinError(i,tot);
  }
  return this->m_h_n_wTotError;
}
*/
void BkgMeasurement::print()
{
  cout << "========================== INFO ==========================" <<endl;
  if(this->getProdType()!=ProdType::Max){
    cout << "Event type: " << toString(this->getEventType()) << " , Prod type: " << toString(this->getProdType()) << " , Observable: " << this->getObservable() << endl;
  }
  else{
    cout << "Event type: " << toString(this->getEventType()) << " , Prod type: " << toString(this->getProdTypeHM()) << " , Observable: " << this->getObservable() << endl;
  }
  cout << "----------------------------------------------------------" <<endl;

  Measurement m = this->getNorm();
  
  cout << "Normalization: " << m.getString() << endl;
  cout << "----------------------------------------------------------" <<endl;

  /*  
  TH1F* hshape = this->m_h_shape;
  TH1F* hstat = this->m_h_relstat;
  TH1F* hsyst = this->m_h_relsyst;
  cout << "In bins:" <<endl;
  for (int i=1; i<=hshape->GetXaxis()->GetNbins(); i++) {
    cout << Form("[%g,%g]",hshape->GetBinLowEdge(i),hshape->GetBinLowEdge(i)+hshape->GetBinWidth(i))<< "   \t";
    float nbin = m.n*hshape->GetBinContent(i);
    cout << nbin << " +- " << nbin*hstat->GetBinContent(i) << " (stat) +- " << nbin*hsyst->GetBinContent(i) << " (syst)" << endl;
  }
  */
  cout << "==========================================================" <<endl;
}

/////////////////////////////////////

//to add measurements
BkgMeasurement* Add(vector<BkgMeasurement*> bs, ProdType ptype, EventType evtype, bool sameYear=true)
{
  if (bs.size()<2) {
    cout << "ERROR: not enough measurements to add"  << endl;
    return 0;
  }
  /*
  cout<<"add measurements with "<<toString(ptype)<<", "<<toString(evtype)<<endl;
  if(sameYear) cout<<"adding measurements from the same year"<<endl;
  else cout<<"adding measurements from different years"<<endl;
  */
  float nSum=0,statSum=0,systSum=0,lumiSum=0;
  TH1F* hn = 0;
  TH1F* hsyst_up = 0;
  TH1F* hsyst_dn = 0;
  TH1F* hstat = 0; 
  for (auto b: bs) {
    Measurement m = b->getNorm();
    //if(ptype==ProdType::_0JetPt4l10 || ptype==ProdType::prodInclusive) cout<<"measurement: "<<m.n<<"+/-"<<m.stat<<"+/-"<<m.syst<<" for lumi "<<m.lumi<<endl;
    lumiSum+=m.lumi;
    if(m.n==0) continue;
    nSum+=m.n*m.lumi;
    statSum+=pow(m.stat*m.n*m.lumi,2);
    systSum+=pow(m.syst*m.n*m.lumi,2);
    //if(ptype==ProdType::_0JetPt4l10 || ptype==ProdType::prodInclusive) cout<<"nSum now "<<nSum<<", systSum now "<<systSum<<endl;
      
    if (!hsyst_up) {
      hsyst_up = (TH1F*)b->getHistSystErrUp()->Clone();
      hsyst_up->Scale(m.lumi*m.n);
      hsyst_dn = (TH1F*)b->getHistSystErrDn()->Clone();
      hsyst_dn->Scale(m.lumi*m.n);
      hstat = (TH1F*)b->getHistRelStatErr()->Clone();
      hstat->Scale(m.n*m.lumi);
      hn = (TH1F*)b->getHistShape()->Clone();
      hn->Scale(m.n*m.lumi);
    }
    else {
      hsyst_up->Add(b->getHistSystErrUp(),m.lumi*m.n);
      hsyst_dn->Add(b->getHistSystErrDn(),m.lumi*m.n);
      for(int i=0;i<hstat->GetNbinsX();i++){
	hstat->SetBinContent(i+1,sqrt(pow(hstat->GetBinContent(i+1),2)+pow(b->getHistRelStatErr()->GetBinContent(i+1)*m.n*m.lumi,2)));
      }
      /*
      cout<<"bin limits: ";
      for(int i=0;i<hn->GetNbinsX()+1;i++) cout<<hn->GetXaxis()->GetBinLowEdge(i+1)<<" ";
      cout<<endl<<" bin limits: ";
      for(int i=0;i<b->getHistShape()->GetNbinsX()+1;i++) cout<<b->getHistShape()->GetXaxis()->GetBinLowEdge(i+1)<<" ";
      cout<<endl;
      */
      hn->Add(b->getHistShape(),m.n*m.lumi);
    }
    if (!hsyst_up || !hsyst_dn || !hstat) cout << "ERROR: did not get histograms" << endl;
  }

  if(sameYear) lumiSum=lumiSum/bs.size();

  //cout<<"new measurement: "<<nSum<<"+-"<<sqrt(statSum)<<"+-"<<sqrt(systSum)<<", lumi="<<lumiSum<<endl;
  if(nSum>0){
    hsyst_up->Scale(1/nSum);
    hsyst_dn->Scale(1/nSum);
    hn->Scale(1/nSum);
    hstat->Scale(1/nSum);

    Measurement addNorm(nSum/lumiSum, sqrt(statSum)/nSum, sqrt(systSum)/nSum, lumiSum);
    //cout<<"new measurement: "<<addNorm.n<<"+-"<<addNorm.stat<<"%+-"<<addNorm.syst<<"%, lumi "<<lumiSum<<endl;
  
    return new BkgMeasurement( addNorm, hn, hstat,hsyst_up,hsyst_dn,bs.back()->getObservable(),ptype,evtype);
  }
  else{
    Measurement addNorm(0,0,0,lumiSum);
    return new BkgMeasurement( addNorm, hn, hstat,hsyst_up,hsyst_dn,bs.back()->getObservable(),ptype,evtype);
  }
}

BkgMeasurement* Add(BkgMeasurement* b1,BkgMeasurement* b2, ProdType ptype, EventType evtype, bool sameYear=true)
{
  vector<BkgMeasurement*> bs;
  bs.push_back(b1);
  bs.push_back(b2);
  return Add(bs,ptype,evtype,sameYear);
}

BkgMeasurement* Add(vector<BkgMeasurement*> bs, ProdTypeHM ptype, EventType evtype, bool sameYear=true)
{
  if (bs.size()<2) {
    cout << "ERROR: not enough measurements to add"  << endl;
    return 0;
  }
  float nSum=0,statSum=0,systSum=0,lumiSum=0;
  TH1F* hn = 0;
  TH1F* hsyst_up = 0;
  TH1F* hsyst_dn = 0;
  TH1F* hstat = 0;
  for (auto b: bs) {
    Measurement m = b->getNorm();
    lumiSum+=m.lumi;
    if(m.n==0) continue;
    nSum+=m.n*m.lumi;
    statSum+=pow(m.stat*m.n*m.lumi,2);
    systSum+=pow(m.syst*m.n*m.lumi,2);

    if (!hsyst_up) {
      hsyst_up = (TH1F*)b->getHistSystErrUp()->Clone();
      hsyst_up->Scale(m.lumi*m.n);
      hsyst_dn = (TH1F*)b->getHistSystErrDn()->Clone();
      hsyst_dn->Scale(m.lumi*m.n);
      hstat = (TH1F*)b->getHistRelStatErr()->Clone();
      hstat->Scale(m.n*m.lumi);
      hn = (TH1F*)b->getHistShape()->Clone();
      hn->Scale(m.n*m.lumi);
    }
    else {
      hsyst_up->Add(b->getHistSystErrUp(),m.lumi*m.n);
      hsyst_dn->Add(b->getHistSystErrDn(),m.lumi*m.n);
      for(int i=0;i<hstat->GetNbinsX();i++){
	hstat->SetBinContent(i+1,sqrt(pow(hstat->GetBinContent(i+1),2)+pow(b->getHistRelStatErr()->GetBinContent(i+1)*m.n*m.lumi,2)));
      }
      hn->Add(b->getHistShape(),m.n*m.lumi);
    }
    if (!hsyst_up || !hsyst_dn || !hstat) cout << "ERROR: did not get histograms" << endl;
  }

  if(sameYear) lumiSum=lumiSum/bs.size();
  if(nSum>0){
    hsyst_up->Scale(1/nSum);
    hsyst_dn->Scale(1/nSum);
    hn->Scale(1/nSum);
    hstat->Scale(1/nSum);

    Measurement addNorm(nSum/lumiSum, sqrt(statSum)/nSum, sqrt(systSum)/nSum, lumiSum);
    return new BkgMeasurement( addNorm, hn, hstat,hsyst_up,hsyst_dn,bs.back()->getObservable(),ptype,evtype);
  }
  else{
    Measurement addNorm(0,0,0,lumiSum);
    return new BkgMeasurement( addNorm, hn, hstat,hsyst_up,hsyst_dn,bs.back()->getObservable(),ptype,evtype);
  }
}

BkgMeasurement* Add(BkgMeasurement* b1,BkgMeasurement* b2, ProdTypeHM ptype, EventType evtype, bool sameYear=true)
{
  vector<BkgMeasurement*> bs;
  bs.push_back(b1);
  bs.push_back(b2);
  return Add(bs,ptype,evtype,sameYear);
}
