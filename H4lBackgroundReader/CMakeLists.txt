# Declare the package name:
atlas_subdir( H4lBackgroundReader )

# External dependencies:
find_package( Eigen )
find_package( ROOT COMPONENTS Core MathCore Boost Root RooFit HistFactory RooFitCore RooStats Hist Tree RIO HistPainter Physics pthread Graf Graf3d Gpad Html Postscript Gui GX11TTF GX11 )

# Component(s) in the package:
atlas_add_library( H4lBackgroundReaderLib
  		   H4lBackgroundReader/*.h Root/*.cxx
  		   PUBLIC_HEADERS H4lBackgroundReader
  		   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
  		   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES})

       #atlas_add_root_dictionary( H4lBackgroundReaderLib H4lBackgroundReaderDictSource
       #    ROOT_HEADERS H4lBackgroundReader/H4lBackgroundReader.h
       #    H4lBackgroundReader/BkgMeasurement.h
       #    Root/LinkDef.h
       #    EXTERNAL_PACKAGES ROOT Eigen)
  
atlas_add_dictionary( H4lBackgroundReaderDict
    H4lBackgroundReader/H4lBackgroundReaderDict.h
    H4lBackgroundReader/selection.xml
    LINK_LIBRARIES H4lBackgroundReaderLib )

#atlas_add_executable( getBkgForCouplings util/getBkgForCouplings.cxx
#		      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
#		      LINK_LIBRARIES H4lBackgroundReaderLib ${ROOT_LIBRARIES})

#atlas_add_executable( getBkgM4lBins util/getBkgM4lBins.cxx
#		      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
#		      LINK_LIBRARIES H4lBackgroundReaderLib ${ROOT_LIBRARIES})

atlas_add_executable( getInputForCoupWS util/getInputForCoupWS.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}	
                      LINK_LIBRARIES H4lBackgroundReaderLib ${ROOT_LIBRARIES})

atlas_add_executable( runExampleH4lBackgroundReader util/runExampleH4lBackgroundReader.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}	
                      LINK_LIBRARIES H4lBackgroundReaderLib ${ROOT_LIBRARIES})

atlas_install_python_modules( python/*.py )
atlas_install_data( data/* )


