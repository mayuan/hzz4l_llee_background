#ifndef PROCESS_H
#define PROCESS_H



class Process{
  public:

TString process_name, Title;
int Color;
bool IsForFit;
double Weight;

vector <TString> Files;
TChain* ch[5];

double CRevents[5], CRfraction[5], CRfractionError[5], CRfractionRelError[5], CREventsError[5], SRevents[5], SReventsError[5], TF[5], TFerror_u[5], TFerror_d[5];
double m4lSREvents=0, m4lInvIsoEvents=0;

TTreeReaderValue<Double_t> *w, *l_ef;
TTreeReaderArray<Float_t> *IDpt, *MSpt, *l_pt, *l_phi, *l_eta, *jet_pt, *jet_eta, *jet_phi, *jet_m;
TTreeReaderValue<Float_t> *m4lj, *m4ljj, *mZ1, *met, *mZ2, *m4l, *pt4l, *y4l, *cthstr, *cth1, *cth2, *pt4l_fsr, *met_et, *phi, *phi1, *mjj, *Etajj, *Phijj, *phi4l, *eta4l;
TTreeReaderValue<Int_t> *e_t, *p_vtx, *n_bjets, *n_bjets_70, *n_jets, *n_exl, *prod_type, *prod_type_fine, *prod_type_SB;
TTreeReaderArray<Int_t> *p_iso, *p_d0, *l_id;
TH1F * MZ1[5];

TH1F * njets, * l_pt3, * l_pt4;

Process(TString a, TString b, vector<TString> c, bool d, int e, double w);
Process(TString a, TString b, TString c, bool d, int e, double w);
void BuildCRsDatasets();
//bool ApplyIsoCuts(TString D);
bool Apply_ttH_Leptonic_Sel();
bool Apply_VH_Leptonic_Sel();
bool Couplings(int I);
};

Process::Process(TString a, TString b, vector<TString> c, bool d, int e, double w){
process_name=a;
Title=b;
Files=c;
IsForFit=d;
Color=e;
Weight=w;
cout<<"Object for "<<process_name<<" process created"<<endl;}

Process::Process(TString a, TString b, TString c, bool d, int e, double w){
process_name=a;
Title=b;
Files.push_back(c);
IsForFit=d;
Color=e;
Weight=w;
cout<<"Object for "<<process_name<<" process created"<<endl;}

void Process::BuildCRsDatasets(){

cout<<endl<<process_name<<" creating datasets..." <<endl;

double tmp_Weight;
tmp_Weight=Weight;

double I=0;

for (int j=0; j<5; j++){
    ch[j] = new TChain("tree_"+ModifiedCR[j]);
    for (int i=0; i<Files.size(); i++) ch[j]->Add(Files[i]);

//    MZ1[j]=new TH1F(Title,Title,56,50,106);
//    MZ1[j]->SetLineColor(Color);
//    MZ1[j]->GetYaxis()->SetTitle("Events / GeV");
//    MZ1[j]->GetXaxis()->SetTitle("m_{12} [GeV]");
//    MZ1[j]->SetMarkerStyle(kDot);

    TTreeReader R(ch[j]);

    w = new TTreeReaderValue<Double_t>(R,"weight");
    met = new TTreeReaderValue<Float_t>(R,"met_et");
    //if (process_name!="Data") l_ef = new TTreeReaderValue<Double_t>(R,"w_lepEff");
    mZ1 = new TTreeReaderValue<Float_t>(R,"mZ1_fsr");
    mZ2 = new TTreeReaderValue<Float_t>(R,"mZ2_unconstrained");
    m4lj = new TTreeReaderValue<Float_t>(R,"m4lj_fsr");
    m4ljj = new TTreeReaderValue<Float_t>(R,"m4ljj_fsr");
    m4l = new TTreeReaderValue<Float_t>(R,"m4l_constrained");
    y4l = new TTreeReaderValue<Float_t>(R,"y4l_fsr");
    mjj = new TTreeReaderValue<Float_t>(R,"dijet_invmass");
    Etajj = new TTreeReaderValue<Float_t>(R,"dijet_deltaeta");
    Phijj = new TTreeReaderValue<Float_t>(R,"dijet_deltaphi");
    phi = new TTreeReaderValue<Float_t>(R,"phi_fsr");
    phi1 = new TTreeReaderValue<Float_t>(R,"phi1_fsr");
    pt4l_fsr = new TTreeReaderValue<Float_t>(R,"pt4l_fsr");
    eta4l = new TTreeReaderValue<Float_t>(R,"eta4l_unconstrained");
    phi4l = new TTreeReaderValue<Float_t>(R,"phi4l_unconstrained");
    met_et = new TTreeReaderValue<Float_t>(R,"met_et");
    jet_pt = new TTreeReaderArray<Float_t>(R,"jet_pt");
    jet_m = new TTreeReaderArray<Float_t>(R,"jet_m");
    jet_phi = new TTreeReaderArray<Float_t>(R,"jet_phi");
    jet_eta = new TTreeReaderArray<Float_t>(R,"jet_eta");
    e_t = new TTreeReaderValue<Int_t>(R,"event_type");
    ////prod_type = new TTreeReaderValue<Int_t>(R,"prod_type");
    prod_type_fine = new TTreeReaderValue<Int_t>(R,"prod_type_fine");
    prod_type_SB = new TTreeReaderValue<Int_t>(R,"prod_type_SB");

    cthstr = new TTreeReaderValue<Float_t>(R,"cthstr_fsr");
    cth1 = new TTreeReaderValue<Float_t>(R,"cth1_fsr");
    cth2 = new TTreeReaderValue<Float_t>(R,"cth2_fsr");

    p_vtx = new TTreeReaderValue<Int_t>(R,"pass_vtx4lCut");
    p_iso = new TTreeReaderArray<Int_t>(R,"lepton_passIsoCut");
    p_d0 = new TTreeReaderArray<Int_t>(R,"lepton_passD0sig");

    l_pt = new TTreeReaderArray<Float_t> (R, "lepton_pt");
    l_phi = new TTreeReaderArray<Float_t> (R, "lepton_phi");
    l_eta = new TTreeReaderArray<Float_t> (R, "lepton_eta");

    n_bjets_70 = new TTreeReaderValue<Int_t>(R,"n_jets_btag70");
    n_jets  = new TTreeReaderValue<Int_t>(R,"n_jets");
    n_exl   = new TTreeReaderValue<Int_t>(R,"n_extraLep");

    l_id = new TTreeReaderArray<Int_t>(R,"lepton_id");

    IDpt = new TTreeReaderArray<Float_t>(R,"mu_IDpt");
    MSpt = new TTreeReaderArray<Float_t>(R,"mu_MSpt");

    Double_t W=0, WW=0, imb1=0, imb2=0, SRWW=0, DB_W[5]={0,0,0,0,0}, tmp_PT2, tmp_PT;
    int LJ, sLJ;
    bool passSR, passSRm4l, Overlap;

    TEfficiency TEff("tmp_effobj", "", 1, 0, 1);
    TEff.SetUseWeightedEvents();
    TEff.SetPosteriorMode(1);
    TEff.SetStatisticOption(TEfficiency::kFNormal);

    TLorentzVector leps, j0, j1, lj;

    while (R.Next()) { //Overlap=0;
 ///------------------------------------------------Reweighting------------------------------------------------------------------------------///

 ///------------------------------------------------Preselection-----------------------------------------------------------------------------///
          if (**e_t !=0 &&  **e_t !=3 &&  **e_t!=9) continue;
          if (p_iso->At(0)==0 || p_iso->At(1)==0  || p_d0->At(0)==0 || p_d0->At(1)==0) continue;
///------------------------------------------------Differential-----------------------------------------------------------------------///
           //if (**pt4l_fsr<MIN || **pt4l_fsr>=MAX) continue;
 ///-------------------------------------------------Split L-H------------------------------------------------------------------------------///
          imb1 = (IDpt->At(2) - MSpt->At(2))/IDpt->At(2);
		  imb2 = (IDpt->At(3) - MSpt->At(3))/IDpt->At(3);
          if(imb1 < ImbCut[CR[j]]  && imb2 < ImbCut[CR[j]]) continue;
 ///----------------------------------------------------Fill CRs------------------------------------------------------------------------------///
          if (ActiveRegion["ss"]==0 && j==2 && **p_vtx==0) continue;
          if (process_name=="Diboson" && **mZ1<75) DB_W[j]+=**w*Weight;
          W+=**w*Weight; WW+=(**w*Weight)*(**w*Weight);
          m12->setVal(**mZ1);
         // MZ1[j]->Fill(**mZ1, **w*Weight);

		  if (process_name.Contains("Data")) RDS_Data[j]->add(*m12, **w*Weight);
          else                               RDS_MC[j]->add(*m12, **w*Weight);

 ///----------------------------------------------------Fill SR-------------------------------------------------------------------------///
          passSR=(**p_vtx==1 && p_iso->At(2)==1 && p_iso->At(3)==1 && p_d0->At(2)==1 && p_d0->At(3)==1);
          if(passSR) SRWW+=(**w*Weight)*(**w*Weight);
          if (passSR && j==0 && **m4l>115 && **m4l<130) m4lSREvents+=(**w*Weight);
          TEff.FillWeighted(passSR, **w*Weight, 0.5);
     }

       CRevents[j]=W;
       CREventsError[j]=Sqrt(WW);
       CRfraction[j]=CRevents[j]/CRevents[0];
       CRfractionError[j]=0;
       CRfractionRelError[j]=0;
       TF[j]=TEff.GetEfficiency(1);
       SRevents[j]= TEff.GetEfficiency(1)*CRevents[j];
       SReventsError[j]=sqrt(SRWW);
       TFerror_u[j]=TEff.GetEfficiencyErrorUp(1);
       TFerror_d[j]=TEff.GetEfficiencyErrorLow(1);
       if (process_name=="Diboson") {DB_fraction[j]= DB_W[j]/CRevents[j]; cout<<" ------- "<<DB_fraction[j]<<endl;}
       cout<<setw(15)<<CR[j]<<" ok   ---   events :  "<<CRevents[j]<<endl;}//"   "<<SRevents[j]<<"   "<<TEff.GetEfficiency(1)<<endl;}
    cout<<setw(15)<<"Signal Region"<<" ok   ---   events :  "<<SRevents[0]<<endl;
}








//bool Process::ApplyIsoCuts(TString D){
//int I,J;
//if (D=="Primary") {I=0; J=2;}
//else              {I=2; J=4;}
//
//int Set = IsoSet;
//
//for (int i=I; i<J; i++ ){
///// --------------------- Roger --------------------------------------------------------///
//if(Set==2) if ( (pt30_500->At(i)+0.4*flow20->At(i))/l_pt->At(i)>0.3 ) return 0;
//if(Set==1) if ( (pt30_500->At(i)+0.4*flow20->At(i))/l_pt->At(i)>0.16 ) return 0;
//if(Set==5) if (pt30_1000->At(i)/l_pt->At(i)>0.2)                      return 0;
/////--------------------------------------------------------------------------------------///
//
/////-------------------------------hi mu----------------------------------------------------///
//if (Set==8){
//if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//if(et20->At(i)>0.30)                  return 0;}
/////-------------------------------------------------------------------------------------///
//
//
/////-------------------------------STD----------------------------------------------------///
//if (Set==0){
//        if (l_id->At(i)<5){
//            if(pt30->At(i)>0.15) return 0;
//            if(et20->At(i)>0.30) return 0; }
//else {
//            if(pt20->At(i)>0.15) return 0;
//            if(et20->At(i)>0.20) return 0; }   }
/////---------------------------------------------------------------------------------------///
//
/////---------------------------------FCLoose-----------------------------------------------///
//if (Set==6){
//        if (l_id->At(i)<5){
//            if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.30)                  return 0; }
//else {
//            if(pt20_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.20)                  return 0; }   }
/////----------------------------------------------------------------------------------------///
//
/////---------------------------------FCLoose modified -----------------------------------------------///
//if (Set==7){
//        if (l_id->At(i)<5){
//            if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.30)                  return 0; }
//else {
//            if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.20)                  return 0; }   }
/////----------------------------------------------------------------------------------------///
//
/////-------------------------------Rongkun-------------------------------------------------///
//if(Set==4){if (pt30_1000->At(i)/l_pt->At(i)>0.001 && et20->At(i)/0.38 + pt30_1000->At(i)/(l_pt->At(i)*0.15)>1) return 0;
//           if (pt30_1000->At(i)/l_pt->At(i)<0.001 && et20->At(i)>0.5)                                          return 0;}
/////---------------------------------------------------------------------------------------///
//
/////-------------------------------Rongkun-------------------------------------------------///
//if(Set==3){if (pt30_1000->At(i)/l_pt->At(i)>0.001 &&  (flow20->At(i)/l_pt->At(i))/0.45 + (pt30_1000->At(i)/l_pt->At(i))/0.13>1)  return 0 ;
//           if (pt30_1000->At(i)/l_pt->At(i)<0.001 &&   flow20->At(i)/l_pt->At(i)>0.3)                                            return 0;}
/////---------------------------------------------------------------------------------------///
//}
//
//return 1; }
//
//
//bool Process::Apply_ttH_Leptonic_Sel(){
//if (**n_bjets>=1 && **n_jets>=2 && **n_exl>=1) return true;
//else return false;}
//
//bool Process::Apply_VH_Leptonic_Sel(){
//if (Apply_ttH_Leptonic_Sel()==true) return false;
//else if (**n_exl>=1) return true;
//else return false;}
//
//
//bool Process::Couplings(int I){
//
//    bool ttHSB=0;
//
//    if (**n_bjets_60>= 1 && **n_jets>= 2 && (**met_et>100)) ttHSB=1;
//
//     if (I==0 && **prod_type == 0 && **pt4l_fsr < 10)                     return true;
//else if (I==1 && **prod_type == 0 && **pt4l_fsr > 10 && **pt4l_fsr < 100) return true;
//else if (I==2 && **prod_type == 0 && **pt4l_fsr > 100)                    return true;
//
//else if (I==3 && **prod_type == 1 && **pt4l_fsr < 60)                     return true;
//else if (I==4 && **prod_type == 1 && **pt4l_fsr > 60  && **pt4l_fsr<120)  return true;
//else if (I==5 && **prod_type == 1 && **pt4l_fsr > 120 && **pt4l_fsr<200)  return true;
//else if (I==6 && **prod_type == 1 && **pt4l_fsr > 200)                    return true;
//
//else if (I==7 && (**prod_type == 2 || (**prod_type == 3 && **pt4l_fsr < 200))) return true;
//
//else if (I==8 && **prod_type == 3 && **pt4l_fsr > 200)  return true;
//else if (I==9 && **prod_type == 4)                         return true;
//else if (I==10 && **prod_type == 5)                        return true;
//else if (I==11 && **prod_type == 6)                        return true;
//
//else if (I==12  && ttHSB==0 && **prod_type == 0 )                      return true;
//else if (I==13  && ttHSB==0 && **prod_type == 1 )                      return true;
//else if (I==14  && ttHSB==0 && (**prod_type == 2 || **prod_type == 3)) return true;
//else if (I==15  && ttHSB==0 && **prod_type == 4 )                      return true;
//else if (I==16  && ttHSB==1)                                           return true;
//
//return false;
//}

#endif



