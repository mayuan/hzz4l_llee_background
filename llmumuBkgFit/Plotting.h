
//__________________________________________
void DoPlot(TString fitTo)
{
	//if (!fInitOK) return;
	RooRealVar* m12 = fWorkspace->var("mZ1_fsr");

	//~~~~~~~~~~~~~~~~~
	// Draw fit results
	//~~~~~~~~~~~~~~~~~


	TLegend *leg(0);
	Bool_t legOK(0);

	TPaveText* label = new TPaveText(0.18, 0.72, 0.52, 0.95, "ndc");
	label->SetBorderSize(0);
	label->SetFillStyle(0);
	label->SetTextAlign(13);

	TText* text = label->AddText("#bf{#it{ATLAS}} Internal");
	text = label->AddText("data 2015-18 + mc16a,d,e");
    text = label->AddText("#sqrt{s}=13 TeV, 138.97 fb^{-1}");    //<---- it should be 36.2 fb-1 for 2015-16 <-----
	text->SetTextSize(0.04);
    text = label->AddText(" ");

	// Loop on all regions
	for (UInt_t iregion(0); iregion < 5; iregion++) {

		//if (!ControlRegionStatus[icregion]) continue;

		RooDataSet *ds = dataset[iregion];
		if (!ds) continue;

		TString region = CR[iregion];

		if (iregion == 0) text->SetText(text->GetX(), text->GetY(), "Relaxed Region");
		if (iregion == 1) text->SetText(text->GetX(), text->GetY(), "Inverted d_{0} CR");
		if (iregion == 2) text->SetText(text->GetX(), text->GetY(), "Inverted isolation CR");
		if (iregion == 3) text->SetText(text->GetX(), text->GetY(), "Same Sign CR");
		if (iregion == 4) text->SetText(text->GetX(), text->GetY(), "e#mu+#mu#mu CR");
		text->SetTextSize(0.04);

		RooPlot* frame = m12->frame(Bins(28), Title("Region: " + region));
		frame->SetXTitle("");
		frame->SetYTitle("Events / 2GeV");

		TString     tmp = "Region_" + region;
		RooAbsPdf  *pdf = fWorkspace->pdf("pdf_" + region);
		if (!pdf) cout<<"Pdf "<<"pdf_" + region<<" doesn't exist!!!!!!!!!!\n";

		//~~~~~~~~~~~~~~~~~~~~~~~
		// Draw Data and Total BG
		//~~~~~~~~~~~~~~~~~~~~~~~

		RooCmdArg   ErrArg = ds->isNonPoissonWeighted() ? DataError(RooAbsData::SumW2) : DataError(RooAbsData::Poisson);
		//CombinedData.plotOn(frame, Name("Data"), Cut("cat_regions==cat_regions::"+tmp));
		ds->plotOn(frame, Name("Data"),ErrArg);
		pdf->plotOn(frame, LineColor(kRed+1), Name("BG"), Normalization(1.0,RooAbsReal::RelativeExpected));


		if (!legOK) {
			leg = new TLegend(0.56,0.65,0.88,0.92, "", "brNDC");
			leg->SetFillStyle(0);
			leg->SetLineColor(0);
			leg->AddEntry(frame->findObject("Data"),fitTo,"lep");
			leg->AddEntry(frame->findObject("BG"),"Total BG","l");
		}

		//~~~~~~~~~~~~~~~~~~~
		// Draw BG components
		//~~~~~~~~~~~~~~~~~~~

		for (UInt_t isample(0); isample < P.size(); isample++) {
			//sample* samp   = GetSample(isample);
			if (P[isample]->process_name.Contains("Data")) continue;
			TString samp_name = P[isample]->process_name;
			TString samp_title = P[isample]->Title;

			TString samp_pdf_name = "pdf_" + samp_name + "_" + region;
			if (fWorkspace->pdf(samp_pdf_name) == 0) {
				samp_pdf_name = "pdf_" + samp_name;
				if (fWorkspace->pdf(samp_pdf_name) == 0) {
					continue;
				}
			}

			pdf->plotOn(frame, Components(samp_pdf_name), Name(samp_name), LineStyle(kDashed), LineColor(P[isample]->Color), Normalization(1.0,RooAbsReal::RelativeExpected));

			if (!legOK) leg->AddEntry(frame->findObject(samp_name), samp_title,"l");
		}

		legOK = kTRUE;

		TString str_name = (fitTo == "Data") ? "Data_" : "MC_";
		str_name.Append(region);

		TCanvas *m_Canvas   = new TCanvas(str_name, "Region: " + region, 600, 600);
		TPad* m_UpperPad  = new TPad("m_UpperPad", "m_UpperPad", .005, .250, .995, .995);
		TPad* m_LowerPad  = new TPad("m_LowerPad", "m_LowerPad", .005, .005, .995, .250);
		m_UpperPad->SetBottomMargin(0.02);
		m_UpperPad->SetRightMargin(0.1);
		m_LowerPad->SetTopMargin(0.06);
		m_LowerPad->SetBottomMargin(0.4);
		m_LowerPad->SetRightMargin(0.1);
		m_Canvas->cd();
		m_LowerPad->Draw();
		m_Canvas->cd();
		m_UpperPad->Draw();


		m_UpperPad->cd();
		Double_t fr_max = frame->GetMaximum();
		frame->SetMaximum(fr_max*1.5);

		TAxis* xup = frame->GetXaxis();
		TAxis* yup = frame->GetYaxis();
		xup->SetLabelFont(43);
		yup->SetLabelFont(43);
		xup->SetTitleFont(43);
		yup->SetTitleFont(43);
		xup->SetLabelSize(0);
		yup->SetLabelSize(16.);
		xup->SetTitleSize(0.);
		yup->SetTitleSize(18.);

		frame->Draw();
		label->Draw();
		leg->Draw();

		m_LowerPad->cd();
		RooPlot* frame2 = m12->frame(Title("Pulls for Region: " + region));
		frame2->SetXTitle("m_{12} [GeV]");
		frame2->SetYTitle("Pull");
		RooHist *hpull = frame->pullHist("Data","BG");
		frame2->addPlotable(hpull,"P"); // must include RooHist.h or it crashes!

		frame2->SetMinimum(-4.);
		frame2->SetMaximum(4.);

		TAxis* xdo = frame2->GetXaxis();
		TAxis* ydo = frame2->GetYaxis();
		xdo->SetLabelFont(43);
		ydo->SetLabelFont(43);
		xdo->SetTitleFont(43);
		ydo->SetTitleFont(43);
		xdo->SetLabelSize(16.);
		ydo->SetLabelSize(16.);
		xdo->SetTitleSize(18.);
		ydo->SetTitleSize(18.);
		xdo->SetTitleOffset(4.2);
		ydo->SetNdivisions(4, 4, 0);

		frame2->Draw();

		TGraph* l = new TGraph(2);
		l->SetPoint(0, xdo->GetBinLowEdge(xdo->GetFirst()), 0);
		l->SetPoint(1, xdo->GetBinLowEdge(xdo->GetLast()+1), 0);
		l->SetBit(kCanDelete);
		l->SetLineStyle(2);
		l->SetLineWidth(2);
		l->Draw("Lsame");

		m_LowerPad->SetGridy(1);
		//m_LowerPad->SetGridx(1);

      	 m_Canvas->SaveAs("Results/" + Options + str_name +".eps");   //save as .eps or .png files!!
	}
}

//______________________________________________________________________________
void SetStyle()
{
   static TStyle* myStyle = 0;
   if ( myStyle!=0 ) return;

	myStyle = new TStyle("ATLAS","Atlas style");

	Int_t icol = 0; // WHITE
	myStyle->SetFrameBorderMode(icol);
	myStyle->SetFrameFillColor(icol);

	myStyle->SetCanvasDefH(600);
	myStyle->SetCanvasDefW(600);

	myStyle->SetCanvasBorderMode(icol);
	myStyle->SetCanvasColor(icol);
	myStyle->SetPadBorderMode(icol);
	myStyle->SetPadColor(icol);
	myStyle->SetStatColor(icol);

	// set the paper & margin sizes
	myStyle->SetPaperSize(20,26);

	// set margin sizes
	myStyle->SetPadTopMargin(0.05);
	myStyle->SetPadRightMargin(0.05);
	myStyle->SetPadBottomMargin(0.16);
	myStyle->SetPadLeftMargin(0.16);

	// set title offsets (for axis label)
	myStyle->SetTitleXOffset(1.6);
	myStyle->SetTitleYOffset(2.1);
	myStyle->SetLabelOffset(0.015,"X");

	Int_t font(42);
	//Float_t tsize=16;
	//Float_t tsize_big=18;
	Float_t tsize(0.03);
	//Float_t tsize_big=0.04;

	myStyle->SetTextFont(font);
	myStyle->SetLegendFont(font);
    myStyle->SetLegendFillColor(kWhite);
	myStyle->SetLegendBorderSize(1);
	myStyle->SetTextSize(tsize);
	myStyle->SetLabelFont(font,"xyz");
	myStyle->SetTitleFont(font,"xyz");
	myStyle->SetLabelSize(tsize,"xyz");
	myStyle->SetTitleSize(tsize,"xyz");

	// use bold lines and markers
	myStyle->SetMarkerStyle(5);
	myStyle->SetMarkerSize(1.2);
	myStyle->SetHistLineWidth(2);
	myStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

	// Get rid of X error bars
	// myStyle->SetErrorX(0.001);

	// Get rid of error bar caps
	myStyle->SetEndErrorSize(0.);

	// Do not display any of the standard histogram decorations
	myStyle->SetOptTitle(0);
	myStyle->SetOptStat(0);
	myStyle->SetOptFit(0);

	// Put tick marks on top and RHS of plots
	myStyle->SetPadTickX(1);
	myStyle->SetPadTickY(1);

	myStyle->SetPalette(1);

	gROOT->SetStyle("ATLAS");
	gROOT->ForceStyle();
}
