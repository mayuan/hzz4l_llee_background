
  RooFitResult: minimized FCN value: -37756.1, estimated distance to minimum: 7.18561e-06
                covariance matrix quality: Full, accurate covariance matrix
                Status : MINIMIZE=0 

    Constant Parameter    Value     
  --------------------  ------------
           DBfraction1    1.1955e-01
           DBfraction2    2.0284e-01
           DBfraction3    1.0978e-01
                DBmean    5.8146e+01
               DBsigma    1.0710e+01
     alpha_Diboson_emu    0.0000e+00
   alpha_Diboson_invD0    0.0000e+00
  alpha_Diboson_invIso    0.0000e+00
      alpha_Diboson_ss    0.0000e+00
               bw_mean    9.1188e+01
              bw_width    2.4952e+00
                 cb_a0    9.5786e-01
               cb_eta0    1.8892e+01
                cb_mu0    4.3214e-01
             cb_sigma0    7.9849e-01
      frac_Diboson_emu    4.6658e-02
    frac_Diboson_invD0    6.6862e-02
   frac_Diboson_invIso    1.0620e-01
       frac_Diboson_ss    6.6910e-02
          frac_Top_emu    1.7033e+00
        frac_Top_invD0    8.5143e-01
       frac_Top_invIso    1.4575e-01
           frac_Top_ss    7.8900e-01
            frac_Z_emu    5.6885e-02
          frac_Z_invD0    7.5966e-01
         frac_Z_invIso    2.3533e-01
             frac_Z_ss    5.3531e-01
  frac_rerr_Diboson_emu    7.9800e-03
  frac_rerr_Diboson_invD0    5.4640e-03
  frac_rerr_Diboson_invIso    4.2430e-03
  frac_rerr_Diboson_ss    6.9820e-03
     frac_rerr_Top_emu    3.1280e-03
   frac_rerr_Top_invD0    1.0310e-03
  frac_rerr_Top_invIso    5.9730e-03
      frac_rerr_Top_ss    3.7730e-03
       frac_rerr_Z_emu    5.2506e-02
     frac_rerr_Z_invD0    4.1530e-03
    frac_rerr_Z_invIso    1.3309e-02
        frac_rerr_Z_ss    1.5710e-02
    mean_alpha_Top_emu    0.0000e+00
  mean_alpha_Top_invD0    0.0000e+00
  mean_alpha_Top_invIso    0.0000e+00
     mean_alpha_Top_ss    0.0000e+00
      mean_alpha_Z_emu    0.0000e+00
    mean_alpha_Z_invD0    0.0000e+00
   mean_alpha_Z_invIso    0.0000e+00
       mean_alpha_Z_ss    0.0000e+00
          norm_Diboson    3.2354e+03

    Floating Parameter  InitialValue    FinalValue +/-  Error     GblCorr.
  --------------------  ------------  --------------------------  --------
         alpha_Top_emu    1.6997e-01    1.7767e-01 +/-  9.89e-01  <none>
       alpha_Top_invD0   -9.7511e-02   -9.7831e-02 +/-  9.94e-01  <none>
      alpha_Top_invIso    2.0607e-01    2.0545e-01 +/-  9.92e-01  <none>
          alpha_Top_ss    1.5425e-02    1.3877e-02 +/-  9.86e-01  <none>
           alpha_Z_emu    8.3871e-02    8.3727e-02 +/-  9.92e-01  <none>
         alpha_Z_invD0   -6.8841e-01   -6.8618e-01 +/-  1.01e+00  <none>
        alpha_Z_invIso    8.9717e-01    8.9842e-01 +/-  9.89e-01  <none>
            alpha_Z_ss    1.5458e+00    1.5589e+00 +/-  9.99e-01  <none>
                  cb_a    1.1305e+00    1.1300e+00 +/-  1.59e-01  <none>
                cb_eta    4.6316e+00    4.6488e+00 +/-  2.04e+00  <none>
                 cb_mu   -7.2692e-02   -7.1805e-02 +/-  9.64e-02  <none>
              cb_sigma    1.9405e+00    1.9398e+00 +/-  1.13e-01  <none>
                 ch_c0   -1.3494e-01   -1.3460e-01 +/-  2.65e-02  <none>
                 ch_c1   -2.1263e-01   -2.1272e-01 +/-  1.97e-02  <none>
              norm_Top    3.0778e+03    3.0777e+03 +/-  3.87e+01  <none>
                norm_Z    3.2872e+03    3.2867e+03 +/-  7.24e+01  <none>
               poly_c1   -7.3345e-03   -7.4075e-03 +/-  3.23e-03  <none>

