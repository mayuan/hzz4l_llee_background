
Z
relaxIsoD0   Events:   3286.93 +/-  24.2674    Fraction:         1 +/-          0%    -----    TF: 0.00433038 +/- 0.000117541   Signal Region Events: 14.2337 +/- 0.373405
     invD0   Events:   2496.95 +/-  18.8051    Fraction:   0.75966 +/- 0.00415275%
    invIso   Events:   701.873 +/-  14.9382    Fraction:  0.213534 +/-   0.014169%
        ss   Events:   1759.51 +/-  24.4002    Fraction:  0.535305 +/-  0.0157104%
       emu   Events:   186.978 +/-  9.71993    Fraction: 0.0568852 +/-   0.052506%

Top
relaxIsoD0   Events:   2605.34 +/-  6.42812    Fraction:         1 +/-          0%    -----    TF: 0.00240102 +/- 0.000108881   Signal Region Events: 6.2555 +/- 0.283936
     invD0   Events:   2218.27 +/-  5.81092    Fraction:  0.851432 +/- 0.00103064%
    invIso   Events:   327.788 +/-  2.55739    Fraction:  0.125814 +/- 0.00650364%
        ss   Events:   2055.62 +/-  5.86688    Fraction:  0.789002 +/- 0.00377269%
       emu   Events:   4437.64 +/-  8.53232    Fraction:   1.70328 +/- 0.00312799%

Diboson
relaxIsoD0   Events:   3235.35 +/-  4.73164    Fraction:         1 +/-          0%    -----    TF: 0.823624 +/- 0.000595441   Signal Region Events: 2664.71 +/- 4.21077
     invD0   Events:   216.322 +/-  1.28606    Fraction:  0.066862 +/- 0.00546353%
    invIso   Events:   337.722 +/-  1.69705    Fraction:  0.104385 +/- 0.00428383%
        ss   Events:   216.476 +/-  1.47784    Fraction: 0.0669097 +/-  0.0069817%
       emu   Events:   150.956 +/-  1.18428    Fraction: 0.0466584 +/- 0.00798031%

Data
relaxIsoD0   Events:      9612 +/-  98.0408    Fraction:         1 +/-          0%    -----    TF: 0.294112 +/- 0   Signal Region Events: 2827 +/- 53.1695
     invD0   Events:      5064 +/-  71.1618    Fraction:  0.526841 +/- 0.00966622%
    invIso   Events:      1573 +/-  39.6611    Fraction:   0.16365 +/-  0.0230584%
        ss   Events:      4553 +/-  67.4759    Fraction:  0.473679 +/-  0.0179909%
       emu   Events:      5642 +/-  75.1132    Fraction:  0.586975 +/-  0.0167714%

Total MCE ---- RelaxIsoD0 : 9127.63   InvD0 : 4931.55   InvIso : 1367.38   SS : 4031.61   emu : 4775.57

Z
relaxIsoD0  Events:    2923.15 +/-    86.8362  fr: 1   -----    Signal Region Events:    12.6583 +/-   0.376034 +/-   0.343591 ------   m4l 115-130 :3.25594
     invD0  Events:    2220.58 +/-    65.9655  fr: 0.759654
    invIso  Events:    624.192 +/-    18.5425  fr: 0.213534
        ss  Events:    1564.78 +/-    46.4839  fr: 0.535305
       emu  Events:    166.491 +/-    4.94585  fr: 0.0569561

Top
relaxIsoD0  Events:    3111.86 +/-     41.062  fr: 1   -----    Signal Region Events:    7.47166 +/-  0.0985909 +/-   0.338824 ------   m4l 115-130 :1.23356
     invD0  Events:    2649.48 +/-    34.9607  fr: 0.851411
    invIso  Events:    391.516 +/-    5.16617  fr: 0.125814
        ss  Events:    2455.27 +/-     32.398  fr: 0.789002
       emu  Events:    5301.56 +/-    69.9557  fr: 1.70366

Diboson
relaxIsoD0  Events:    3235.35 +/-    4.73164  fr: 1   -----    Signal Region Events:    2664.71 +/-    4.21077 +/-          0 ------   m4l 115-130 :193.291
     invD0  Events:    216.322 +/-    1.28606  fr: 0.066862
    invIso  Events:    337.722 +/-    1.69705  fr: 0.104385
        ss  Events:    216.478 +/-    1.47784  fr: 0.06691
       emu  Events:    150.955 +/-    1.18428  fr: 0.046658

Total DDE ---- RelaxIsoD0 : 9270.37   InvD0 : 5086.38   InvIso : 1353.43   SS : 4236.52   emu : 5619.01
