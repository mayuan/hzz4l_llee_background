const int BINS=5;
double Z_R[BINS], Z_Re[BINS],Z_RD[BINS], Z_RDe[BINS], Z_TF[BINS], Z_TFe[BINS], Z_MC[BINS], Z_MCe[BINS], Z_D[BINS], Z_DeSys[BINS], Z_DeSt[BINS] ;
double tt_R[BINS], tt_Re[BINS],tt_RD[BINS], tt_RDe[BINS], tt_TF[BINS], tt_TFe[BINS], tt_MC[BINS], tt_MCe[BINS], tt_D[BINS], tt_DeSys[BINS],tt_DeSt[BINS];

void CorrectTFs(TString component,int I,double value);
void Compare(TH1 *h1, TH1 *h2, TString XTitle, TString LegTitle);

void Norm(){
TH1F* TFZ = new TH1F("Z+jets","Z+jets",BINS,0,BINS);  TFZ->Sumw2();
TH1F* TFT = new TH1F("t#bar{t}","t#bar{t}",BINS,0,BINS); TFT->Sumw2();
TH1F* DZ = new TH1F("DDE","",BINS,0,BINS);  DZ->Sumw2();
TH1F* DT = new TH1F("DDE","",BINS,0,BINS);  DT->Sumw2();
TH1F* DZSys = new TH1F("DDE","",BINS,0,BINS);  DZ->Sumw2();
TH1F* DTSys = new TH1F("DDE","",BINS,0,BINS);  DT->Sumw2();
TH1F* DZSt = new TH1F("DDE","",BINS,0,BINS);  DZ->Sumw2();
TH1F* DTSt = new TH1F("DDE","",BINS,0,BINS);  DT->Sumw2();
TH1F* MCZ = new TH1F("MC ","MC ",BINS,0,BINS);  MCZ->Sumw2();
TH1F* MCZe = new TH1F("MC ","MC ",BINS,0,BINS);  MCZ->Sumw2();
TH1F* MCT = new TH1F("MC ","MC",BINS,0,BINS);  MCT->Sumw2();
TH1F* MCTe = new TH1F("MC ","MC",BINS,0,BINS);  MCT->Sumw2();
TH1F* RDZ = new TH1F("DDE","",BINS,0,BINS);  DZ->Sumw2();
TH1F* RDT = new TH1F("DDE","",BINS,0,BINS);  DT->Sumw2();
TH1F* RMCZ = new TH1F("MC","MC",BINS,0,BINS);  MCZ->Sumw2();
TH1F* RMCT = new TH1F("MC","MC",BINS,0,BINS);  MCT->Sumw2();

int num = 0;
TString Category = "Pt4l";
ifstream file (Category + ".txt");

while(!file.eof()) { file>>Z_R[num]>>Z_Re[num]>>Z_RD[num]>>Z_RDe[num]>>Z_TF[num]>>Z_TFe[num]>>Z_MC[num]>>Z_MCe[num]>>Z_D[num]>>Z_DeSt[num]>>tt_R[num]>>tt_Re[num]>>tt_RD[num]>>tt_RDe[num]>>tt_TF[num]>>tt_TFe[num]>>tt_MC[num]>>tt_MCe[num]>>tt_D[num]>>tt_DeSt[num]; ++num;}
file.close();

//CorrectTFs("Zjets",6,0.000807586);   //for couplings

///--------------------------------------------------------------------------------------------------------------------------------------------------
double HF_TF_un = 0.078; //v22
for(int i=0; i<BINS; i++){ Z_MCe[i]=sqrt(Z_MCe[i]*Z_MCe[i]+Z_MC[i]*Z_MC[i]*HF_TF_un*HF_TF_un);
                           tt_MCe[i]=sqrt(tt_MCe[i]*tt_MCe[i]+tt_MC[i]*tt_MC[i]*HF_TF_un*HF_TF_un);
                           Z_DeSys[i] =Z_D[i] *sqrt(HF_TF_un*HF_TF_un+pow(Z_TFe[i]/Z_TF[i],2));
                           tt_DeSys[i]=tt_D[i]*sqrt(HF_TF_un*HF_TF_un+pow(tt_TFe[i]/tt_TF[i],2));}
///----------------------------------------------------------------------------------------------------------------------------------------------------

///-------------------------------------------Normalization---------------------------------------------------------------------------------------------------------
double ZMC=0,ZD=0,tMC=0,tD=0;
double ComResZ=(12.42+2.89), ComResT=7.35;         //v22

for (int i=0; i<BINS; i++){  ZMC+=Z_MC[i]; ZD+=Z_D[i]; tMC+=tt_MC[i]; tD+=tt_D[i];}
for (int i=0; i<BINS; i++){ Z_MC[i]*=ComResZ/ZMC; Z_MCe[i]*=ComResZ/ZMC; Z_D[i]*=ComResZ/ZD; Z_DeSt[i]*=ComResZ/ZD; Z_DeSys[i]*=ComResZ/ZD; tt_MC[i]*=ComResT/tMC; tt_MCe[i]*=ComResT/tMC; tt_D[i]*=ComResT/tD; tt_DeSt[i]*=ComResT/tD;tt_DeSys[i]*=ComResT/tD;}
///---------------------------------------------------------------------------------------------------------------------------------------------------///


for (int i=1; i<BINS+1; i++){

TFZ->SetBinContent(i,Z_TF[i-1]);  TFZ->SetBinError(i,Z_TFe[i-1]);
TFT->SetBinContent(i,tt_TF[i-1]);  TFT->SetBinError(i,tt_TFe[i-1]);

DZ->SetBinContent(i,Z_D[i-1]);         DZ->SetBinError(i,sqrt(Z_DeSys[i-1]*Z_DeSys[i-1]+Z_DeSt[i-1]*Z_DeSt[i-1]));
DZSys->SetBinContent(i,Z_DeSys[i-1]);
DZSt->SetBinContent(i,Z_DeSt[i-1]);

MCZ->SetBinContent(i,Z_MC[i-1]);  MCZ->SetBinError(i,Z_MCe[i-1]);
MCZe->SetBinContent(i,Z_MCe[i-1]);

RDZ->SetBinContent(i,Z_RD[i-1]);  RDZ->SetBinError(i,Z_RDe[i-1]);
RMCZ->SetBinContent(i,Z_R[i-1]);  RMCZ->SetBinError(i,Z_Re[i-1]);

DT->SetBinContent(i,tt_D[i-1]);          DT->SetBinError(i,sqrt(tt_DeSys[i-1]*tt_DeSys[i-1]+tt_DeSt[i-1]*tt_DeSt[i-1]));
DTSys->SetBinContent(i,tt_DeSys[i-1]);
DTSt->SetBinContent(i,tt_DeSt[i-1]);

MCT->SetBinContent(i,tt_MC[i-1]);  MCT->SetBinError(i,tt_MCe[i-1]);
MCTe->SetBinContent(i,tt_MCe[i-1]);

RDT->SetBinContent(i,tt_RD[i-1]);  RDT->SetBinError(i,tt_RDe[i-1]);
RMCT->SetBinContent(i,tt_R[i-1]);  RMCT->SetBinError(i,tt_Re[i-1]);
}

///------------------------------Histos for BkgReader---------------------------------------------------------------------------------------------///
TFile *f1=new TFile("BkgReader_"+Category+".root","recreate");
DZ->Write("ZEvs");  DZSt->Write("ZSt"); DZSys->Write("ZSys");
DT->Write("tEvs");  DTSt->Write("tSt"); DTSys->Write("tSys");
f1->Close();

///----------------------------------------------Printing Results-------------------------------------------------------------------------------------///
double Ztot=0, Ttot=0;
cout<<endl<<"-------------------------------------------------------------"<<Category<<"--------------------------------------------------------------"<<endl;
cout<<"------------Zjets--------------------"<<endl;
for (int num=0; num<BINS; num++) {Ztot+=Z_D[num]; cout<<setw(14)<<Z_R[num]<<setw(14)<<Z_Re[num]<<setw(14)<<Z_RD[num]<<setw(14)<<Z_RDe[num]<<setw(14)<<Z_TF[num]<<setw(14)<<Z_TFe[num]<<setw(14)<<Z_MC[num]<<setw(14)<<Z_MCe[num]<<setw(14)<<Z_D[num]<<setw(14)<<Z_DeSt[num]<<setw(14)<<Z_DeSys[num]<<endl;}
cout<<"--- Total Zjets : "<<Ztot<<endl;
cout<<endl<<"------------ttbar--------------------"<<endl;
for (int num=0; num<BINS; num++) {Ttot+=tt_D[num]; cout<<setw(14)<<tt_R[num]<<setw(14)<<tt_Re[num]<<setw(14)<<tt_RD[num]<<setw(14)<<tt_RDe[num]<<setw(14)<<tt_TF[num]<<setw(14)<<tt_TFe[num]<<setw(14)<<tt_MC[num]<<setw(14)<<tt_MCe[num]<<setw(14)<<tt_D[num]<<setw(14)<<tt_DeSt[num]<<setw(14)<<tt_DeSys[num]<<endl;}
cout<<"--- Total ttbar : "<<Ttot<<endl;
cout<<"-------------------------------------------------------------------------------------------------------------------------------------"<<endl;




///----------------------------Drawing------------------------------------------------------------------------------------------------
TFile *f=new TFile("Plots"+Category+".root","recreate");
TString X = "Categories";

Compare(DZ,MCZ,X,"ZEvs");
Compare(DT,MCT,X,"tEvs");

TCanvas *c1=new TCanvas();
TFZ->Draw("EPsame");
TFZ->SetLineColor(kRed);
TFZ->SetMarkerColor(kRed);
TFZ->SetMarkerStyle(20);
TFZ->SetStats(0);
TFZ->GetYaxis()->SetTitle("Tranfer Factor (Iso+d0+vtx)");
TFZ->GetYaxis()->SetTitleOffset(1.3);
TFZ->GetXaxis()->SetTitle(X);
TFT->Draw("EPsame");
TFT->SetLineColor(kBlue);
TFT->SetMarkerColor(kBlue);
TFT->SetMarkerStyle(20);
TFT->SetStats(0);
TLegend *L = c1->BuildLegend();
L ->SetBorderSize(0);
c1->Write();
f->Close();
}

void Compare(TH1 *h1, TH1 *h2, TString XTitle, TString LegTitle) {

  h1->Sumw2();
  h2->Sumw2();

  h1->SetLineColor(kBlack);
  h1->SetMarkerColor(kBlack);
  h1->SetMarkerStyle(20);
  h2->SetLineColor(kRed);
  h2->SetMarkerColor(kRed);
  h2->SetMarkerStyle(20);

  TH1F *H1=(TH1F*)h1->Clone();
  TH1F *H2=(TH1F*)h2->Clone();

  TLegend* leg1 = new TLegend(0.62,0.62,0.88,0.88, "", "brNDC");
  leg1->SetBorderSize(0);
  leg1->SetHeader("");

  TCanvas *c1 = new TCanvas(LegTitle,LegTitle,600,700);

  TPad *pad1 = new TPad("pad1","pad1",0,0.30,1,1);
  pad1->Draw();
  pad1->cd();
  H1->SetStats(0);
  H2->SetStats(0);

  H2->SetMarkerStyle(20);
  H2->Draw("EP");
  H2->SetMarkerColor(kRed);
  H2->GetXaxis()->SetLabelSize(0);
  H1->SetMarkerStyle(20);
  H1->Draw("EP same");
  H2->GetYaxis()->SetTitle("#scale[1.2]{Events}");
  H2->GetYaxis()->SetTitleOffset(1.2);

  leg1->AddEntry(h2,h2->GetName());
  leg1->AddEntry(h1,h1->GetName());
  leg1->Draw();
  leg1->SetHeader(LegTitle);

  c1->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0.0,1,0.30);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0.12);
  pad2->Draw();
  pad2->cd();

  h1->SetStats(0);
  h1->GetYaxis()->SetRangeUser(0.666,1.5);
  h1->GetYaxis()->SetTitle("ratio");
  h1->GetYaxis()->SetLabelSize(0.05);
  h1->GetYaxis()->SetTitleOffset(0.4);
  h1->GetYaxis()->SetTitleSize(0.1);
  h1->GetXaxis()->SetTitle(XTitle);
  h1->GetXaxis()->SetTitleOffset(0.6);
  h1->GetXaxis()->SetTitleSize(0.08);
  h1->Divide(H1);
  h1->Draw("EP");
  h1->GetXaxis()->SetLabelSize(0.06);
  h1->GetYaxis()->SetLabelSize(0.06);
  h1->SetMarkerStyle(20);
  h2->Divide(H1);
  h2->Draw("EPsame");
  h2->SetMarkerStyle(20);
  h2->SetMarkerColor(kRed);

  c1->cd();

  c1->Write("aa");
}

void CorrectTFs(TString component,int I,double value){
if (component=="Zjets") Z_TF[I]  = value ; Z_TFe[I]=Z_TF[I]/2;   Z_MC[I]=Z_R[I]*Z_TF[I];    Z_MCe[I]= Z_R[I]*Z_TFe[I];    Z_D[I]=Z_RD[I]*Z_TF[I];    Z_DeSt[I]=Z_RDe[I]*Z_TF[I];
if (component=="ttbar") tt_TF[I] = value;  tt_TFe[I]=tt_TF[I]/2; tt_MC[I]=tt_R[I]*tt_TF[I]; tt_MCe[I]= tt_R[I]*tt_TFe[I]; tt_D[I]=tt_RD[I]*tt_TF[I]; tt_DeSt[I]=tt_RDe[I]*tt_TF[I];
}
