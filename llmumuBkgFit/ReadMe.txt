-----Fitting-------------------------------------



--------Differential Fitting---------------------

0. Method settings already set 
1. Set the vector with the variable limits -- BkgFit.cpp line 75
2. Set the name of the txt of variable results -- BkgFit.cpp line 76
3. Set the name of the variable for the subcategories txts -- BkgFit.cpp line 112
4. Set the variable  -- Process.h line 143
These have to be changed for every variable. For step 4 some more complex cuts may be needed. Example with pT is provided.

run the code
1. source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.06.06/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh
2. root
3. .L BkgFit.cpp
4. dif_Fit()

----------------Normalization--------------------------------------------
1. Set the number of bins for this variable -- Norm.cpp line 1
2. Set the variable name (naming must follow Differential Fitting step 2 convention) -- Norm.cpp line 27 
3. Call a function to correct a specific TF -- Norm.cpp line 33
4. Set the normalization of each components -- Norm.cpp line 45
5. In case that the first bin must be excluded from the normalization set i=1 -- Norm.cpp line 47 and 48

run the code 
root
.x Norm.cpp
