//my functions
#include "TMath.h"
using namespace TMath;


double DR(double eta1, double eta2, double phi1, double phi2){
double Deta=eta1-eta2;
double Dphi=phi1-phi2;
if(Dphi>3.14159) Dphi = 2*3.14159 - Dphi;
if(Dphi<-3.14159) Dphi = 2*3.14159 + Dphi;
return sqrt(Deta*Deta+Dphi*Dphi);}

void SumWeights(Double_t& Events, Double_t& SqEr, Bool_t isLF, Double_t weight, Bool_t cut, Bool_t LFcut) // pass 2 arguments by reference!
{
	if(isLF){
		if (cut&&LFcut) { Events += weight; SqEr += Power(weight, 2); }
	}
	else{
		if (cut) { Events += weight; SqEr += Power(weight, 2); }
	}
}

void mc_comp(vector<sample*> list, Int_t j, Bool_t isHF, Double_t& nom, Double_t& denom, Double_t& n_er, Double_t& d_er)
    //mc efficiency components
	//counter j = 1, 2, 3, 4 ONLY!!! --> Iso, d0, Iso+d0, Iso after d0 : cut case
{
	if(j>4||j<0) {cout<<"mc_comp 2nd argument out of scope\n"; return;}

	nom=0; denom=0; n_er=0; d_er=0;
	vector <sample*>::const_iterator i;
	i=list.begin();
	if(isHF){                        //use HF_enr "region"-->Evt[1]
		if(j!=1){
			cout<<"Caution: zero events passing d0 in HF enriched area\n";
			//would print inf+/-nan (divide with 0 and 0/0)
			return;
		}
		else{
			nom=  (*i)->Evt[1][j];
		    denom=(*i)->Evt[1][0];
		    n_er= sqrt((*i)->Sw2[1][j]);
		    d_er= sqrt((*i)->Sw2[1][0]);
	    }
	}
	else{                           //use LF_enr "region"-->Evt[2]
		if(j==4){
			nom=  (*(i+1))->Evt[2][3];
		    denom=(*(i+1))->Evt[2][2];
		    n_er= sqrt((*(i+1))->Sw2[2][3]);
		    d_er= sqrt((*(i+1))->Sw2[2][2]);
		}
		else{
			nom=  (*(i+1))->Evt[2][j];
		    denom=(*(i+1))->Evt[2][0];
		    n_er= sqrt((*(i+1))->Sw2[2][j]);
		    d_er= sqrt((*(i+1))->Sw2[2][0]);
		}
	}
}

void data_comp(vector<sample*> list, Int_t j, Bool_t isHF, Double_t& nom, Double_t& denom, Double_t& n_er, Double_t& n_mc_er, Double_t& d_er)
	//data efficiency components
	//counter j = 1, 2, 3, 4 ONLY!!! --> Iso, d0, Iso+d0, Iso after d0 : cut case
{
	if(j>4||j<0) {cout<<"data_comp 2nd argument out of scope\n"; return;}

	nom=0; denom=0; n_er=0; n_mc_er=0; d_er=0;
	vector <sample*>::const_iterator i;
	i=list.end()-1;      //data sample
	if(isHF && j==1)					//use HF_enr "region"-->Evt[1]
	{
	    nom=    (*i)->Evt[1][j];
		denom=  (*i)->Evt[1][0];
		n_er=   (*i)->Sw2[1][j];  	//first squared
		d_er=   (*i)->Sw2[1][0];
	}
	else if(isHF && j!=1){
		cout<<"Caution: zero events passing d0 in HF enriched area\n"; return;
	}
	else{								//use LF_enr "region"-->Evt[2]
		if(j==4){
			nom=  (*i)->Evt[2][3];
		    denom=(*i)->Evt[2][2];
			n_er= (*i)->Sw2[2][3];
		    d_er= (*i)->Sw2[2][2];
		}
		else{
			nom=  (*i)->Evt[2][j];
		    denom=(*i)->Evt[2][0];
			n_er= (*i)->Sw2[2][j];
		    d_er= (*i)->Sw2[2][0];
		}
	}
	for (i=list.begin(); i!=list.end()-1; ++i)
	{
		if(isHF){
			if (i==list.begin()) continue;  //skip HF component
			nom -=   (*i)->Evt[1][j];
			denom -= (*i)->Evt[1][0];
			n_er +=  (*i)->Sw2[1][j];
			n_mc_er+=(*i)->Sw2[1][j];  //only mc components
			d_er +=  (*i)->Sw2[1][0];
		}
		else{
			if(j==4){
				if(i==list.begin()+1) continue;  //skip LF comp.
				nom -=   (*i)->Evt[2][3];
				denom -= (*i)->Evt[2][2];
				n_er +=  (*i)->Sw2[2][3];
				n_mc_er+=(*i)->Sw2[2][3];
				d_er +=  (*i)->Sw2[2][2];
			}
			else{
				if(i==list.begin()+1) continue;  //skip LF comp.
				nom -=   (*i)->Evt[2][j];
				denom -= (*i)->Evt[2][0];
				n_er +=  (*i)->Sw2[2][j];
				n_mc_er+=(*i)->Sw2[2][j];
				d_er +=  (*i)->Sw2[2][0];
			}
		}
	}
	n_er  = sqrt(n_er);	//may put "S" capital (of sqrt)
	n_mc_er=sqrt(n_mc_er);
	d_er  = sqrt(d_er);
}

Double_t Eff(Double_t nom, Double_t denom)
{
	return(nom/denom);
}

Double_t EfErBi (Double_t nom, Double_t denom, Double_t denomEr)
{
	Double_t ef = nom/denom;
	return(sqrt(ef*(1-ef))*denomEr/denom);
}

Double_t EfErPoi (Double_t nom, Double_t denom, Double_t nomEr, Double_t denomEr)
{
	Double_t ef = nom/denom;
	return(ef*(sqrt(Power(nomEr/nom, 2) + Power(denomEr/denom, 2))));
}

Double_t ErBiPlPoi (Double_t erBi, Double_t nom_mc_er, Double_t denom)
{
	return(sqrt(Power(erBi, 2) + Power(nom_mc_er/denom ,2)));
}

