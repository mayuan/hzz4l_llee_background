int IsoSet;

#include "Zpl_ef_header.h"
#include "Zpl_paths.h"
#include "TTreeReader.h"



void Zpl_Ef(){

vector<sample*> sampleList;
vector<sample*>::const_iterator i;

TString region[3]={"Full", "HFenr", "LFenr"};
TString criteria[4]={"No",  "Iso",  "d0", "Iso+d0"};

//soSet=7;

TString txt="23.txt";

double Lumi_w=1;

vector<TString> ZL_Total{Zlf_a,Zlf_d,Zlf_e};
vector<TString> ZH_Total{Zhf_a,Zhf_d,Zhf_e};
vector<TString> TT_Total{ttbar_a,ttbar_d,ttbar_e};
vector<TString> WZ_Total{WZ_a,WZ_d,WZ_e};
vector<TString> DB_Total{ggZZ_a,qqZZ_a,ggZZEW_a,ttVa_a,ttVb_a,WZ_a,VVV_a,ggH_a,WH_a,ZH_a,ttH_a,vbfH_a,
                         ggZZ_d,qqZZ_d,ggZZEW_d,ttVa_d,ttVb_d,WZ_d,VVV_d,ggH_d,WH_d,ZH_d,ttH_d,vbfH_d,
                         ggZZ_e,qqZZ_e,ggZZEW_e,ttVa_e,ttVb_e,WZ_e,VVV_e,ggH_e,WH_e,ZH_e,ttH_e,vbfH_e};


sampleList.push_back(new sample("ZH","Z+HFjets",ZH_Total,1,kGreen-1, Lumi_w));
sampleList.push_back(new sample("Zl","Z+LFjets",ZL_Total,1,kGreen, Lumi_w));
sampleList.push_back(new sample("Top","t#bar{t}",TT_Total,1,kOrange-2, Lumi_w));
sampleList.push_back(new sample("2,3bos,ttv","Diboson",DB_Total,1,kGray, Lumi_w));
//
sampleList.push_back(new sample("Data","Data",Data_p23, 1, kBlack, 1.00));

cout<<"HI, the number of samples is "<<sampleList.size()<<"\n";

for (i=sampleList.begin(); i!=sampleList.end(); ++i){

	(*i)->InitSamples((*i)->name, (*i)->title, 0);

}


cout<<"=================================================================================================="<<endl;

//remove("EventCounter.txt");
ofstream f1("EventCounter"+txt, ofstream::app);//let's count them

for(Int_t j=0;j<1;j++){
	f1<<"\n-------- "<<region[j]<<" --------\n";
	for(Int_t k=0;k<4;k++){
		for (i=sampleList.begin(); i!=sampleList.end(); ++i){

			f1<<setw(10)<<criteria[k]+" cut"<<setw(10)<<(*i)->name<<setw(10)<<(*i)->Evt[j][k]<<" +/- "<<sqrt((*i)->Sw2[j][k])<<endl;

		}
	}
}

//-------calculate MC, data efficiencies, all Z+mu sample inclusive-----//
//2,3bos,ttV: not included in mc total, subtracted from data
Double_t tot1(0), tot2(0), tot3(0), tot4(0);
Double_t toter1(0), toter2(0), toter3(0), toter4(0);
Double_t ef1, ef2, ef3;       //(iso, d0, iso+d0)  mc

for (i=sampleList.begin(); i!=sampleList.end()-2; i++){  //loop over mc samples, not dibosons
	tot1+=(*i)->Evt[0][1];  toter1+=(*i)->Sw2[0][1];
	tot2+=(*i)->Evt[0][2];  toter2+=(*i)->Sw2[0][2];
	tot3+=(*i)->Evt[0][3];  toter3+=(*i)->Sw2[0][3];
	tot4+=(*i)->Evt[0][0];  toter4+=(*i)->Sw2[0][0];  //relaxed
}
toter4=sqrt(toter4);

ef1=tot1/tot4;  ef2=tot2/tot4;  ef3=tot3/tot4;
f1<<"\nIso ef. MCtot   = "<<ef1<<" +- "<<EfErBi(tot1, tot4, toter4)<<endl;
f1<<  "d0 ef MCtot     = "<<ef2<<" +- "<<EfErBi(tot2, tot4, toter4)<<endl;
f1<<  "Iso+d0 ef. MCtot= "<<ef3<<" +- "<<EfErBi(tot3, tot4, toter4)<<endl;

for (i=sampleList.begin();i!=sampleList.end()-2;i++){
f1<<"\n"<<setw(3)<<(*i)->name<<
          " Iso ef = "   <<Eff((*i)->Evt[0][1],(*i)->Evt[0][0])<<" +- "<<EfErBi((*i)->Evt[0][1],(*i)->Evt[0][0], sqrt((*i)->Sw2[0][0]))<<
          " d0 ef = "    <<Eff((*i)->Evt[0][2],(*i)->Evt[0][0])<<" +- "<<EfErBi((*i)->Evt[0][2],(*i)->Evt[0][0], sqrt((*i)->Sw2[0][0]))<<
		  " iso+d0 ef = "<<Eff((*i)->Evt[0][3],(*i)->Evt[0][0])<<" +- "<<EfErBi((*i)->Evt[0][3],(*i)->Evt[0][0], sqrt((*i)->Sw2[0][0]));
}
f1<<"\n";
i++;                                         //data now
Double_t comp1, comp2, comp3, comp4;         //eff. components, diboson-ttV-VVV subtracted
Double_t comper1, comper2, comper3, comper4; //errors

comp1  =(*i)->Evt[0][1] - (*(i-1))->Evt[0][1]; comp2=(*i)->Evt[0][2] - (*(i-1))->Evt[0][2]; comp3=(*i)->Evt[0][3] - (*(i-1))->Evt[0][3]; comp4=(*i)->Evt[0][0] - (*(i-1))->Evt[0][0];
comper1=sqrt((*(i-1))->Sw2[0][1]); //mc impurity (diboson-ttV-VVV), iso cut
comper2=sqrt((*(i-1))->Sw2[0][2]); //mc impurity (-"-)  , d0 cut
comper3=sqrt((*(i-1))->Sw2[0][3]); //mc impurity (-"-)  , iso+d0 cut
comper4=sqrt((*i)->Sw2[0][0] + (*(i-1))->Sw2[0][0]);  //relaxed

Double_t binom[3]; binom[0]=EfErBi(comp1, comp4, comper4); binom[1]=EfErBi(comp2, comp4, comper4); binom[2]=EfErBi(comp3, comp4, comper4);

f1<<"\nIso ef. data    = "<<Eff(comp1, comp4)<<" +- "<<ErBiPlPoi(binom[0], comper1, comp4) <<" or (bi) "<<binom[0]<<endl;
f1<<  "d0 ef. data     = "<<Eff(comp2, comp4)<<" +- "<<ErBiPlPoi(binom[1], comper2, comp4) <<" or (bi) "<<binom[1]<<endl;
f1<<  "Iso+d0 ef. data = "<<Eff(comp3, comp4)<<" +- "<<ErBiPlPoi(binom[2], comper3, comp4) <<" or (bi) "<<binom[2]<<endl;
f1.close();

//--------LF Efficiencies------------//

//remove("EfficienciesFCLoose.txt");

Double_t Ef[2][4], ErPoi[2][4], ErBi[2][4], ErBi_plPoi[2][4];  // LF efficiencies and errors:(mc, data)*(Iso, d0, Iso+d0, Iso after d0)
Double_t Nom, Denom, NomEr, DenomEr, NomMCEr;                 // efficiencies' nominator and denominator And errors
TString Cut[4]={"Iso", "d0", "Iso+d0", "Iso after d0"};

ofstream f(txt, ofstream::app);
f<<"	Efficiencies calculated by simple subtraction of impurities\n";

for(Int_t j=1; j<5; j++){

	mc_comp(sampleList, j, 0, Nom, Denom, NomEr, DenomEr);
	Ef[0][j-1]= Eff(Nom, Denom);
	ErPoi[0][j-1]= EfErPoi(Nom, Denom, NomEr, DenomEr);
	ErBi[0][j-1]=  EfErBi (Nom, Denom, DenomEr);
	f<<setw(8)<<"MC LF "<<setw(12)<<Cut[j-1]<<setw(10)<<Ef[0][j-1]<<" +/- "<<setw(10)<<ErBi[0][j-1]
	<<" (bi) or "<<setw(10)<<ErPoi[0][j-1]<<" (Poi)\n";
}
f<<"\n";

for(Int_t j=1; j<5; j++){

	data_comp(sampleList, j, 0, Nom, Denom, NomEr, NomMCEr, DenomEr);
	Ef[1][j-1]=Eff(Nom, Denom);
    ErPoi[1][j-1]=EfErPoi(Nom, Denom, NomEr, DenomEr);
    ErBi[1][j-1]= EfErBi (Nom, Denom, DenomEr);
	ErBi_plPoi[1][j-1]=ErBiPlPoi(ErBi[1][j-1], NomMCEr, Denom);
    f<<"Data LF "<<setw(12)<<Cut[j-1]<<setw(10)<<Ef[1][j-1]<<" +/- "<<setw(10)<<ErBi[1][j-1]
	<<" (bi) or "<<setw(10)<<ErPoi[1][j-1]<<" (Poi) or "<<setw(10)<<ErBi_plPoi[1][j-1]<<" (Bi+Poi)\n";
}
f<<"\n";

//--------HF Iso Efficiency--------//
Double_t hf[2], hfPoi[2], hfBi[2], hfBiPoi[2]; //(mc, data)
TString type[2]={"MC", "Data"};
for(Int_t j=0;j<2;j++){
	if(j==0)
	{ mc_comp(sampleList, 1, 1, Nom, Denom, NomEr, DenomEr); }
	else
	{ data_comp(sampleList, 1, 1, Nom, Denom, NomEr, NomMCEr, DenomEr); }

	hf[j]=Eff(Nom, Denom);
	hfPoi[j]=EfErPoi(Nom, Denom, NomEr, DenomEr);
	hfBi[j]=EfErBi(Nom, Denom, DenomEr);
	f<<setw(8)<<type[j]+" HF "<<setw(12)<<" Iso "<<setw(10)<<hf[j]<<" +/- "<<setw(10)<<hfBi[j]
	<<" (bi) or "<<setw(10)<<hfPoi[j]<<" (Poi)\n";
}

//------ Efficiencies Squared ----- //
f<<"\n	LF Efficiencies for 2mu \n";
f<<setw(21)<<"2mu d0"<<setw(21)<<"2mu Iso after d0\n";
f<<setw(5)<<"MC"<<setw(16)<<Power(Ef[0][1], 2)<<setw(16)<<Power(Ef[0][3], 2)<<"\n";
f<<setw(5)<<"Data"<<setw(16)<<Power(Ef[1][1], 2)<<setw(16)<<Power(Ef[1][3], 2)<<"\n";

f<<"\n"<<setw(21)<<"Inverted Iso CR"<<setw(21)<<"Inverted d0 CR"<<endl;  //fractions' systematics (Data-MC)/MC
f<<setw(21)<<((Power(Ef[1][1], 2))*(1-Power(Ef[1][3], 2))-(Power(Ef[0][1], 2))*(1-Power(Ef[0][3], 2)))/((Power(Ef[0][1], 2))*(1-Power(Ef[0][3], 2)));
f<<setw(16)<<((1-Power(Ef[1][1], 2))-(1-Power(Ef[0][1], 2)))/(1-Power(Ef[0][1], 2))<<"\n";

f<<"\n"<<setw(21)<<"	TF(...) uncertainty for Z+LF \n";
f<<setw(21)<<(Power(Ef[1][2], 2)-Power(Ef[0][2], 2))/Power(Ef[0][2], 2)<<endl;

f<<"\n"<<setw(21)<<"	TF[Iso] uncertainty for Z+HF \n";
f<<setw(21)<<(Power(hf[1], 2)-Power(hf[0], 2))/Power(hf[0], 2)<<endl<<endl;
f<<" ------- oooooo ------- \n\n";

f.close();

cout<<"done\n";
}



