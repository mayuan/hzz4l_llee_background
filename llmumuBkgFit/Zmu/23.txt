	Efficiencies calculated by simple subtraction of impurities
  MC LF          Iso 0.0801333 +/- 0.00268103 (bi) or 0.00297728 (Poi)
  MC LF           d0  0.924859 +/-  0.0026032 (bi) or  0.0132762 (Poi)
  MC LF       Iso+d0 0.0749313 +/- 0.00259986 (bi) or 0.00290424 (Poi)
  MC LF Iso after d0 0.0810192 +/- 0.00284288 (bi) or 0.00315171 (Poi)

Data LF          Iso  0.119263 +/- 0.000886274 (bi) or 0.00102629 (Poi) or 0.000959835 (Bi+Poi)
Data LF           d0   0.91557 +/- 0.000760304 (bi) or 0.00354823 (Poi) or  0.0010486 (Bi+Poi)
Data LF       Iso+d0  0.111679 +/- 0.000861317 (bi) or 0.000940702 (Poi) or 0.00090333 (Bi+Poi)
Data LF Iso after d0  0.121977 +/- 0.000898682 (bi) or 0.00102791 (Poi) or 0.00094661 (Bi+Poi)

  MC HF         Iso   0.171188 +/- 0.000697823 (bi) or 0.000847035 (Poi)
Data HF         Iso   0.163752 +/- 0.000918268 (bi) or  0.0010627 (Poi)

	LF Efficiencies for 2mu 
               2mu d0    2mu Iso after d0
   MC        0.855364      0.00656411
 Data        0.838268       0.0148785

      Inverted Iso CR       Inverted d0 CR
           -0.0281894        0.118204

	TF(...) uncertainty for Z+LF 
              1.22134

	TF[Iso] uncertainty for Z+HF 
           -0.0849925

 ------- oooooo ------- 

