#ifndef Zpl_ef_header_h
#define Zpl_ef_header_h


typedef vector<Double_t> Dvec;

class sample{
	public:
	sample(TString n, TString t, TString p, UInt_t c, Bool_t mc, double W);
	sample(TString n, TString t, vector<TString> p, UInt_t c, Bool_t mc, double W);

	TString name;
	TString path;
	TString title;
	UInt_t color;
	Bool_t isMC;
	double Weight;

	TTreeReaderArray<Float_t> *pt20, *pt30, *et20, *pt20_500, *pt20_1000, *pt30_500, *pt30_1000, *flow20, *flow30, *l_pt, *l_phi, *l_eta;
	TTreeReaderArray<Int_t> *l_id, *origin;

	TChain* chain[1];
	vector < vector<Double_t> > Evt;    //3*4 vector: (Full, HFenr, LFenr) * (No, Iso, d0, Iso+d0)_Cuts
	vector < vector<Double_t> > Sw2;    //list of Sumw2 --> errors^2
	void InitEventList();
	void InitSamples(TString, TString, Int_t);
	bool ApplyIsoCuts(TString D);
};

#include "functions.h"

sample::sample(TString n, TString t, TString p, UInt_t c, Bool_t mc, double W){
	name=n;
	title=t;
	path=p;
	color=c;
	isMC=mc;
	chain[0]=new TChain("tree_ZplusMu");
	//chain[1]=new TChain("tree_ZplusEl");
	chain[0]->Add(p);
	//chain[1]->Add(p);
	Weight=W;
}

sample::sample(TString n, TString t, vector<TString> p, UInt_t c, Bool_t mc, double W){
	name=n;
	title=t;
	path=p[0];
	color=c;
	isMC=mc;
	chain[0]=new TChain("tree_ZplusMu");
	//chain[1]=new TChain("tree_ZplusEl");
	for (int i=0; i<p.size(); i++) chain[0]->Add(p[i]);
	//chain[1]->Add(p);
	Weight=W;
}

void sample::InitEventList(){
	for(Int_t i=0;i<3;i++){
		Dvec temp;
		for(Int_t j=0;j<4;j++){
			temp.push_back(0.0);
		}
		Evt.push_back(temp);
		Sw2.push_back(temp);
	}
}

void sample::InitSamples(TString s_n, TString s_t, Int_t tr){  //s_n=sample_name, s_t=sample_title

	Bool_t isLF=(s_n=="ZL");
	Bool_t isData=(s_n=="Top");

	InitEventList();

	TTreeReader R(chain[tr]);


	//l_id = new TTreeReaderArray<Int_t>(R,"lepton_id");

//    pt30 = new TTreeReaderArray<Float_t>(R, "lepton_ptvarcone30");
//    pt20 = new TTreeReaderArray<Float_t>(R, "lepton_ptvarcone20");
//    et20 = new TTreeReaderArray<Float_t> (R, "lepton_topoetcone20");

    l_pt = new TTreeReaderArray<Float_t> (R, "lepton_pt");
//
//    flow20 = new TTreeReaderArray<float> (R, "lepton_neflowisol20");
//    flow30 = new TTreeReaderArray<float> (R, "lepton_neflowisol30");
//
//    pt20_500 = new TTreeReaderArray<float>(R, "lepton_ptvarcone20_TightTTVA_pt500");
//    pt20_1000 = new TTreeReaderArray<Float_t>(R, "lepton_ptvarcone20_TightTTVA_pt1000");
//    pt30_500 = new TTreeReaderArray<float>(R, "lepton_ptvarcone30_TightTTVA_pt500");
//    pt30_1000 = new TTreeReaderArray<Float_t>(R, "lepton_ptvarcone30_TightTTVA_pt1000");

    l_phi = new TTreeReaderArray<Float_t> (R, "lepton_phi");
    l_eta = new TTreeReaderArray<Float_t> (R, "lepton_eta");

	TTreeReaderArray <Float_t>  d0 (R, "lepton_d0sig");
	TTreeReaderArray <Float_t>  IDpt (R, "mu_IDpt");
	TTreeReaderArray <Float_t>  MSpt (R, "mu_MSpt");

     if (name!="Data"){
 	   origin = new TTreeReaderArray  <Int_t> (R, "mu_MCOriginType");}


    TTreeReaderArray  <Int_t>   Pass1 (R, "lepton_passIsoCut");
	TTreeReaderArray  <Int_t>   id (R, "lepton_id");
	TTreeReaderArray  <Int_t>   Pass2 (R, "lepton_passD0sig");
	TTreeReaderValue <Double_t> w (R, "weight");

    bool Overlap, HF_types;
    double Events=0;

    double tmp_Weight;
    tmp_Weight=Weight;

	while(R.Next())
	{


//          TString FN(chain[0]->GetFile()->GetName());
//          Weight=tmp_Weight;
//          if (FN.Contains("34570")) Weight*=1.62;   //k factor for ggZZ

		//Long64_t q=R.GetCurrentEntry(); if(q<10){cout<<"HIIIIIIII\n";}
	   if (name!="Data"){	HF_types=(origin->At(2)==9||origin->At(2)==25||origin->At(2)==26||origin->At(2)==27||origin->At(2)==29||origin->At(2)==32||origin->At(2)==33); }
	   else HF_types=1;

		Bool_t l12_passSR(Pass1[0]==1 && Pass1[1]==1 && Pass2[0] && Pass2[1]);
		Double_t imbal=(IDpt[2]-MSpt[2])/IDpt[2];

///---------------------------------------------------------------------------------------------------------------------------------------
//		Overlap=0;
//		for (int ii=0; ii<3; ii++){
//            for (int jj=ii+1; jj<3; jj++){
//                if (DR(l_eta->At(ii), l_eta->At(jj), l_phi->At(ii), l_phi->At(jj))<0.3) Overlap=1;}}
//        if (Overlap==1) continue;
///--------------------------------------------------------------------------------------------------------------------------------------------
        if (!l12_passSR) continue;

        Events+=*w*Weight;

		SumWeights(Evt[0][0], Sw2[0][0], isLF, *w*Weight, 1, !HF_types);
		SumWeights(Evt[0][1], Sw2[0][1], isLF, *w*Weight, Pass1[2]==1, !HF_types);              //Iso cut
		SumWeights(Evt[0][2], Sw2[0][2], isLF, *w*Weight, Pass2[2]==1, !HF_types);              //d0 cut
		SumWeights(Evt[0][3], Sw2[0][3], isLF, *w*Weight, Pass1[2]==1&&Pass2[2]==1, !HF_types); //Iso+d0 cut


		if(d0[2]>3.0)                                                     //--->HF enriched control sample
		{
			SumWeights(Evt[1][0], Sw2[1][0], isLF, *w*Weight, 1, !HF_types);
		    SumWeights(Evt[1][1], Sw2[1][1], isLF, *w*Weight, Pass1[2]==1, !HF_types);              //Iso cut
		    SumWeights(Evt[1][2], Sw2[1][2], isLF, *w*Weight, Pass2[2]==1, !HF_types);              //d0 cut
		    SumWeights(Evt[1][3], Sw2[1][3], isLF, *w*Weight, Pass1[2]==1&&Pass2[2]==1, !HF_types); //Iso+d0 cut
		}

		if(imbal>0.2 && id[2]==0)                                              //--->LF enriched control sample, only CB muons
		{
			SumWeights(Evt[2][0], Sw2[2][0], isLF, *w*Weight, 1, !HF_types);
		    SumWeights(Evt[2][1], Sw2[2][1], isLF, *w*Weight, Pass1[2]==1, !HF_types);              //Iso cut
		    SumWeights(Evt[2][2], Sw2[2][2], isLF, *w*Weight, Pass2[2]==1, !HF_types);              //d0 cut
		    SumWeights(Evt[2][3], Sw2[2][3], isLF, *w*Weight, Pass1[2]==1&&Pass2[2]==1, !HF_types); //Iso+d0 cut
		}
	}

	cout<<endl<<"------------------"<<endl<<name<<"  "<<Events<<endl<<"------------------"<<endl;
}
//
//bool sample::ApplyIsoCuts(TString D){
//int I,J;
//if (D=="Primary") {I=0; J=2;}
//else              {I=2; J=3;}
//
//int Set = IsoSet;
//
//for (i=I; i<J; i++ ){
///// --------------------- Roger --------------------------------------------------------///
//if(Set==2) if ( (pt30_500->At(i)+0.4*flow20->At(i))/l_pt->At(i)>0.3 ) return 0;
//if(Set==1) if ( (pt30_500->At(i)+0.4*flow20->At(i))/l_pt->At(i)>0.16 ) return 0;
//if(Set==5) if (pt30_1000->At(i)/l_pt->At(i)>0.2)                      return 0;
/////--------------------------------------------------------------------------------------///
//
/////-------------------------------STD----------------------------------------------------///
//if (Set==0){
//        if (l_id->At(i)<5){
//            if(pt30->At(i)>0.15) return 0;
//            if(et20->At(i)>0.30) return 0; }
//else {
//            if(pt20->At(i)>0.15) return 0;
//            if(et20->At(i)>0.20) return 0; }   }
/////---------------------------------------------------------------------------------------///
//
/////---------------------------------FCLoose modified-----------------------------------------------///
//if (Set==7){
//        if (l_id->At(i)<5){
//            if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.30)                  return 0; }
//else {
//            if(pt30_1000->At(i)/l_pt->At(i)>0.15) return 0;
//            if(et20->At(i)>0.20)                  return 0; }   }
/////----------------------------------------------------------------------------------------///
//
/////-------------------------------Rongkun-------------------------------------------------///
//if(Set==4){if (pt30_1000->At(i)/l_pt->At(i)>0.001 && et20->At(i)/0.38 + pt30_1000->At(i)/(l_pt->At(i)*0.15)>1) return 0;
//           if (pt30_1000->At(i)/l_pt->At(i)<0.001 && et20->At(i)>0.5)                                          return 0;}
/////---------------------------------------------------------------------------------------///
//
/////-------------------------------Rongkun-------------------------------------------------///
//if(Set==3){if (pt30_1000->At(i)/l_pt->At(i)>0.001 &&  (flow20->At(i)/l_pt->At(i))/0.45 + (pt30_1000->At(i)/l_pt->At(i))/0.13>1)  return 0 ;
//           if (pt30_1000->At(i)/l_pt->At(i)<0.001 &&   flow20->At(i)/l_pt->At(i)>0.3)                                            return 0;}
/////---------------------------------------------------------------------------------------///
//}
//
//return 1; }
//

#endif
