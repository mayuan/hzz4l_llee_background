#include "paths2.h"
#include "Process.h"
vector<Process*> P;
#include "Plotting.h"



RooRealVar* GetVar(const char* name, Double_t val=0., Double_t min=0., Double_t max=0.);
void BuildPDFs();
void BuildCRModels();
void Fit(TString Mode);
void ConstrainShapeParameters(Double_t nsigma, Bool_t set_mean = kFALSE, RooAbsPdf* pdf = 0);
RooArgSet* GetShapeParameters(RooAbsPdf* pdf);
const RooArgSet* GetShapeParameters();
bool ConstrainParameters;
void BkgFit(double pt_min, double pt_max, int i);

vector <double> Z_R, Z_Re, Z_RD, Z_RDe, Z_TF, Z_TFe, Z_MC, Z_MCe, Z_D, Z_De;
vector <double> tt_R, tt_Re, tt_RD, tt_RDe, tt_TF, tt_TFe, tt_MC, tt_MCe, tt_D, tt_De;
map <TString, vector <TH1F*>> CRH;


void dif_Fit(){
vector<double> jets{0,1,2,3,100};

/* masses */
vector<double> M12{50.0, 64.0, 73.0, 85.0, 106.0};         //--------//
vector<double> M34{12.0, 20.0, 24.0, 28.0, 32.0, 40.0, 55.0, 65.0};
vector<double>  m4lj{120,180,220,300,400,600,1600};        //--------//
vector<double>  m4ljj{180,320,450,600,1000,2500};          //--------//

/* pts */
vector<double> Pt4l{0.0,10.0,20.0,30.0,45.0,60.0,80.0,120.0,200.0,350.0,1000.0};
vector<double> LjPt{30.0,  60.0,   120.0,  350.0};  //+0jets        //--------//
vector<double> SjPt{30.0,  60.0,   120.0,  350.0};  //+0-1jets      //--------//
vector<double> Pt4lj{0,60,120,350};
vector<double> Pt4ljj{0,60,180,600};

/* nbjet, y, cts, ct1, ct2, phi and phi1 */
double pi=3.14159;
///vector<double>  Nbjet{0j, 1j0bj, 1bj};       //--------//
vector<double>  Rap{0.0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.2, 1.6,2.0, 2.5};
vector<double>  Cts{0.0, 0.125, 0.250, 0.375, 0.500, 0.625, 0.750, 0.875, 1.0};
vector<double>  Ct1{-1.0, -0.750, -0.500, -0.250, 0.0, 0.250, 0.500, 0.750, 1.0};
vector<double>  Ct2{-1.0, -0.750, -0.500, -0.250, 0.0, 0.250, 0.500, 0.750, 1.0};
vector<double>  Phi{-pi, -6*pi/8, -4*pi/8, -2*pi/8, 0.0, 2*pi/8, 4*pi/8, 6*pi/8, pi};
vector<double>  Phi1{-pi, -6*pi/8, -4*pi/8, -2*pi/8, 0.0, 2*pi/8, 4*pi/8, 6*pi/8, pi};

/* jj */
vector<double> Mjj{0.0, 120.0, 450.0, 3000.0};
vector<double> Etajj{0.0, 1.0, 2.5, 9.0};
vector<double> Phijj{0.0, 0.5*pi, pi, 1.5*pi, 2*pi};

/* pT vs njet */
vector<double>  Pt0j{0.0,  15.0,  30.0,  120.0, 350.0};
vector<double>  Pt1j{0,60,80,120,350};
vector<double>  Pt2j{0.0, 120.0, 350.0};
vector<double>  Pt3j{0.0, 120.0, 350.0};

/* pT vs y */
vector <double>  Y{0.0, 0.5, 1.0, 1.5, 2.5};
vector<double>  Pt0y{0.0, 45.0, 120.0, 350.0};
vector<double>  Pt1y{0.0, 45.0, 120.0, 350.0};
vector<double>  Pt2y{0.0, 45.0, 120.0, 350.0};
vector<double>  Pt3y{0.0, 45.0, 120.0, 350.0};

/*pt4l vs. jet1pt */
/// LjPt{30.0,  60.0,   120.0,  350.0};
vector<double> Pt0LjptRange { 1.0, 81.0, 351.0 };   //--------//
vector<double> Pt1LjptRange {0,120,350};            //--------//
vector<double> Pt2LjptRange {0,120,350};            //--------//



vector <double> FitVar = Pt4l;
ofstream ffile1("Results/Pt4l.txt", ofstream::trunc);

for (int i=0; i<FitVar.size()-1; i++){   //FitVar.size()-1
        BkgFit(FitVar[i],FitVar[i+1], i); P.erase(P.begin(),P.end()); //FitVar[i],FitVar[i+1]
        ffile1<<Z_R[i]<<setw(12)<<Z_Re[i]<<setw(12)<<Z_RD[i]<<setw(12)<<Z_RDe[i]<<setw(12)<<Z_TF[i]<<setw(12)<<Z_TFe[i]<<setw(12)<<Z_MC[i]<<setw(12)<<Z_MCe[i]<<setw(12)<<Z_D[i]<<setw(12)<<Z_De[i]<<setw(12)<<tt_R[i]<<setw(12)<<tt_Re[i]<<setw(12)<<tt_RD[i]<<setw(12)<<tt_RDe[i]<<setw(12)<<tt_TF[i]<<setw(12)<<tt_TFe[i]<<setw(12)<<tt_MC[i]<<setw(12)<<tt_MCe[i]<<setw(12)<<tt_D[i]<<setw(12)<<tt_De[i]<<endl;
                     }
ffile1.close();
}


void BkgFit(double bin_min=0, double bin_max=0, int II=0){
SetStyle();
fWorkspace = new RooWorkspace();
m12 = new RooRealVar("mZ1_fsr","m_{12} [GeV]", 50, 106);
for(int j=0; j<5; j++ ) {RDS_MC[j] = new RooDataSet("DataSet",  "", RooArgSet(*m12,weight), "weight");
                         RDS_Data[j] = new RooDataSet("DataSet",  "", RooArgSet(*m12,weight), "weight");}

///---------------------------------------Initial Configuration-----------------------------------------------------------------------//
vector<TString> ZL_Total{Zlf_a,Zlf_d,Zlf_e};
vector<TString> ZH_Total{Zhf_a,Zhf_d,Zhf_e};
vector<TString> TT_Total{ttbar_a,ttbar_d,ttbar_e};
vector<TString> WZ_Total{WZ_a,WZ_d,WZ_e};
vector<TString> DB_Total{ggZZ_a,qqZZ_a,ggZZEW_a,ttVa_a,ttVb_a,WZ_a,VVV_a,ggH_a,WH_a,ttH_a,vbfH_a,      //ZH_a
                         ggZZ_d,qqZZ_d,ggZZEW_d,ttVa_d,ttVb_d,WZ_d,VVV_d,ggH_d,WH_d,ttH_d,vbfH_d,      //ZH_d
                         ggZZ_e,qqZZ_e,ggZZEW_e,ttVa_e,ttVb_e,WZ_e,VVV_e,ggH_e,WH_e,ttH_e,vbfH_e};     //ZH_e

//vector<TString> DATA_Total{data15_16_p21,data17_p21,data18_p21};

double Lumi_w=1;

ImbCut["relaxIsoD0"]=-10e10;  ImbCut["invD0"]=-10e10;   ImbCut["invIso"]=-10e10;/*0.2*/   ImbCut["ss"]=-10e10;/*0.2*/  ImbCut["emu"]=-10e10;

ActiveRegion["relaxIsoD0"]=1; ActiveRegion["invD0"]=1;  ActiveRegion["invIso"]=1;  ActiveRegion["ss"]=1;  ActiveRegion["emu"]=1;

//ModifiedCR[2]="relaxIsoD0";

Options = "M2fix_Pt4l_"+String(II,false);
ofstream ffile("Results/"+Options+".txt", ofstream::trunc);

ConstrainParameters=1;
MIN=bin_min; MAX=bin_max;
//Cps=II;

//P.push_back(new Process("Zl","Z+LFjets",ZL_Total,1,kGreen, Lumi_w));
P.push_back(new Process("Z","Z+HFjets",ZH_Total,1,kGreen-1, Lumi_w));
P.push_back(new Process("Top","t#bar{t}",TT_Total,1,kOrange-2, Lumi_w));
P.push_back(new Process("Diboson","Diboson",DB_Total,0,kBlue, Lumi_w));
//
P.push_back(new Process("Data","Data",Data_p23, 1, kBlack, 1.00));

for (int i=0; i<P.size(); i++){P[i]->BuildCRsDatasets();}
//      CRH["MCE"].push_back(new TH1F(P[i]->process_name,P[i]->Title,5,0,5));
//      CRH["DDE"].push_back(new TH1F(P[i]->process_name,P[i]->Title,5,0,5)); }
/////----------------------------------------------------------------------------------------------------------------------------------//
//
//TFile *Fl = new TFile("Results/"+Options+"_histos.root", "recreate");


//for (int i=0; i<5; i++){
//
//    TCanvas *C = new TCanvas();
//    TH1F *Tot = new TH1F("Total MC","Total MC",56,50,106);
//    Tot->SetLineColor(kRed);
//    Tot->SetMarkerStyle(kDot);
//
//    for (int j=0; j<P.size()-1; j++){
//        Tot->Add(P[j]->MZ1[i]);
//        P[j]->MZ1[i]->Draw("hist same");
//    }
//    Tot->Draw("hist same");
//    P[4]->MZ1[i]->Draw("EP same");
//
//    TLegend* L=C->BuildLegend();
//    L->SetHeader(CR[i]);
//    C->Write();
//}
//
//Fl->Close();

//-------------------------- Define Errors --------------------------------------------------------------------///
cout<<"Defining Errors"<<endl;
for (int i=0; i<P.size(); i++)if (P[i]->Title=="Z+LFjets"){P[i]->CRfractionRelError[1] = 0.5 ;  P[i]->CRfractionRelError[2] = 0.1 ;}

double def_err, stat_err;

for(int i=0; i<P.size(); i++){
        for(int iregion=1; iregion<5; iregion++){

def_err  = P[i]->CRfractionRelError[iregion]*P[i]->CRfraction[iregion];

stat_err = (iregion == 1 || iregion == 2)
						     ? sqrt(P[i]->CRfraction[iregion]*(1.-P[i]->CRfraction[iregion]))*P[i]->CREventsError[0]/P[i]->CRevents[0]
						     : Abs(P[i]->CRfraction[iregion]*sqrt(Power(P[i]->CREventsError[0]/P[i]->CRevents[0],2) + Power(P[i]->CREventsError[iregion]/P[i]->CRevents[iregion],2)));


P[i]->CRfractionRelError[iregion] = sqrt(def_err*def_err + stat_err*stat_err)/P[i]->CRfraction[iregion];}}//}


////-------------------------------------------------------------------------------------------------------------------///
for(int i=0; i<P.size(); i++){ ffile<<endl<<P[i]->process_name<<endl;
        for(int j=0; j<5; j++){ ffile<<setw(10)<<CR[j]<<setw(10)<<"  Events:"<<setw(10)<<P[i]->CRevents[j]<<" +/- "<<setw(8)<< P[i]->CREventsError[j]<<"    Fraction:" <<setw(10)<< P[i]->CRfraction[j]<<" +/- "<<setw(10)<<P[i]->CRfractionRelError[j]<<"%";
        if(j==0) ffile<<"    -----    TF: "<<P[i]->TF[0]<<" +/- "<<P[i]->TFerror_u[0]<<"   Signal Region Events: "<<P[i]->SRevents[0]<<" +/- "<<P[i]->SReventsError[0]<<endl; else ffile<<endl;}}

double TMC[5]={0,0,0,0,0};
for(int j=0; j<5; j++) { for(int i=0; i<P.size()-1; i++) TMC[j]+=P[i]->CRevents[j];} ffile<<endl<<"Total MCE ---- RelaxIsoD0 : "<<TMC[0]<<"   InvD0 : "<<TMC[1]<<"   InvIso : "<<TMC[2]<<"   SS : "<<TMC[3]<<"   emu : "<<TMC[4]<<endl;

//for(int i=0; i<P.size(); i++) for(int j=0; j<5; j++) {CRH["MCE"][i]->SetBinContent(j+1,P[i]->CRevents[j]);}

cout<<"Errors defined"<<endl;

Z_TF.push_back(P[0]->TF[0]); Z_TFe.push_back(P[0]->TFerror_u[0]); tt_TF.push_back(P[1]->TF[0]); tt_TFe.push_back(P[1]->TFerror_u[0]);
Z_MC.push_back(P[0]->SRevents[0]); Z_MCe.push_back(P[0]->SReventsError[0]); tt_MC.push_back(P[1]->SRevents[0]); tt_MCe.push_back(P[1]->SReventsError[0]);
Z_R.push_back(P[0]->CRevents[0]); Z_Re.push_back(P[0]->CREventsError[0]); tt_R.push_back(P[1]->CRevents[0]); tt_Re.push_back(P[1]->CREventsError[0]);

///////-------------------------------------------------------------------------------------------------------------------///
BuildPDFs();
BuildCRModels();

Fit("MC Data");
Fit("Data");
Fit("Data");
 ////-----------------------------------------------------------------------------------------------------------------------------///


double ExpCRevents, ExpCReventsError, ExpSRevents, ExpSReventsErrorSys, ExpSReventsErrorStat;

////------------------------------------------------Define Errors-----------------------------------------------------------///
double TBKG[5]={0,0,0,0,0};
 for(int i=0; i<P.size()-1; i++){ ffile<<endl<<P[i]->process_name<<endl;
        for(int j=0; j<5; j++){

                TString region = CR[j];
			    TString str_suffix = "_" + P[i]->process_name + "_" + region;

                RooRealVar* normRel  = fWorkspace->var("norm_" + P[i]->process_name);
			    RooAbsReal* frac     = fWorkspace->function("frac_term" + str_suffix);

                ExpCRevents = normRel->getVal()*frac->getVal();
                ExpCReventsError = (P[i]->IsForFit) ? normRel->getError()*frac->getVal() : P[i]->CREventsError[j];
                ExpSRevents = ExpCRevents*P[i]->TF[j];
                ExpSReventsErrorSys = 0;

                if (P[i]->IsForFit==0)  ExpSReventsErrorStat = P[i]->SReventsError[j];
			    else                   {ExpSReventsErrorStat = ExpCReventsError*P[i]->TF[j];
				                        ExpSReventsErrorSys =  ExpCRevents*Max(P[i]->TFerror_u[j], P[i]->TFerror_d[j]);}

				                    ffile<<setw(10)<<CR[j]<<"  Events: "<<setw(10)<<ExpCRevents<<" +/- "<<setw(10)<<ExpCReventsError<<"  fr: "<<frac->getVal() ;
              if (j==0) ffile<<"   -----    Signal Region Events: "<<setw(10)<<ExpSRevents<<" +/- "<<setw(10)<<ExpSReventsErrorStat<<" +/- "<<setw(10)<<ExpSReventsErrorSys<<" ------   m4l 115-130 :" << ExpSRevents*P[i]->m4lSREvents/P[i]->SRevents[0]<<endl;
              else      ffile<<endl;

              TBKG[j]+=ExpCRevents; //CRH["DDE"][i]->SetBinContent(j+1,ExpCRevents);

              if (i==0 && j==0) {Z_D.push_back(ExpSRevents); Z_De.push_back(ExpSReventsErrorStat); Z_RD.push_back(ExpCRevents); Z_RDe.push_back(ExpCReventsError);}
              if (i==1 && j==0) {tt_D.push_back(ExpSRevents); tt_De.push_back(ExpSReventsErrorStat); tt_RD.push_back(ExpCRevents); tt_RDe.push_back(ExpCReventsError);}

              }}

//CRH["DDE"][P.size()-1]=CRH["MCE"][P.size()-1];
ffile<<endl<<"Total DDE ---- RelaxIsoD0 : "<<TBKG[0]<<"   InvD0 : "<<TBKG[1]<<"   InvIso : "<<TBKG[2]<<"   SS : "<<TBKG[3]<<"   emu : "<<TBKG[4]<<endl;

 ///----------------------------------------------------------------------------------------------------------------------------------------------------------------------///


//TFile *F=new TFile("Results/"+Options+"CRs.root","recreate");
//
//THStack *hs[2];
//TCanvas *C[2];
//int I=-1;
//TString cr[5]={"RelaxIsoD0","Inverted d0", "Inverted Iso","Same Sign", "e#mu"};
//
//for (std::map<TString,vector<TH1F*>>::iterator i=CRH.begin(); i!=CRH.end(); ++i){
//  I++;
//  hs[I] = new THStack("hs","");
//  for (int j=P.size()-2; j>-1; j--) {
//        for(int k=0; k<5; k++)  i->second[j]->GetXaxis()->SetBinLabel(k+1,cr[k]);
//        i->second[j]->SetFillColor(P[j]->Color);
//        i->second[j]->SetLineColor(P[j]->Color);
//        i->second[j]->SetMarkerColor(P[j]->Color);
//        hs[I]->Add(i->second[j],"hist");}
//  C[I] = new TCanvas();
//  C[I]->SetCanvasSize(700, 700);
//  C[I]->BuildLegend();
//  hs[I]->Draw("h");
//  gStyle->SetErrorX(0.0001);
//  i->second[P.size()-1]->SetMarkerStyle(20);
//  i->second[P.size()-1]->Draw("same PE");
//  C[I]->BuildLegend();
//  C[I]->Write();}
//
//F->Close();

ffile.close();

}




/////------------------------------------------Build Model--------------------------------------------------------///
void BuildPDFs()
{
    cout<<"building pdfs"<<endl;

	RooRealVar m12("mZ1_fsr","m_{12} [GeV]", 50, 106);  // the fitting variable

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// P.D.F.s for the contributing background components
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// Chebychev for ttbar (WARNING: Convergence is very sensitive to c0 and c1 initial values)
	RooRealVar c0("ch_c0", "Chebyshev c0", -1.3460e-01);    c0.setConstant(ConstrainParameters);
	RooRealVar c1("ch_c1", "Chebyshev c1", -2.1272e-01);    c1.setConstant(ConstrainParameters);

	RooChebychev* Chebychev = new RooChebychev("pdf_Top","Chebychev", m12, RooArgList(c0, c1)); Chebychev->getVal(); fWorkspace->import(*Chebychev);

	fWorkspace->extendSet("Set_ShapeParams", "ch_c0,ch_c1");

	// Breit Wigner + Crystal Ball for ZZ and Z+jets
	RooRealVar mean("bw_mean", "Breit Wigner mean [theory]", 91.1876);    mean.setConstant(1);                   // constant (theory)
	RooRealVar width("bw_width","Breit Wigner width [theory]", 2.4952);   width.setConstant(1);                   // constant (theory)
	RooRealVar mu("cb_mu", "Crystal Ball mean (GeV)", -7.1805e-02);        mu.setConstant(ConstrainParameters);
	RooRealVar sigma("cb_sigma", "Crystal Ball sigma (pos)", 1.9398e+00);	      sigma.setConstant(ConstrainParameters); sigma.setMin(0.);   // (pos) to ensure it stays positive after TestFit   //<--- this should never go negative!
	RooRealVar eta("cb_eta", "Crystal Ball exponent (pos)" , 4.6488e+00);     eta.setConstant(ConstrainParameters);   eta.setMin(0.);     // (pos) to ensure it stays positive after TestFit
	RooRealVar a("cb_a", "Crystal Ball cutoff parameter", 1.1300e+00);    a.setConstant(ConstrainParameters);


    RooRealVar mu0("cb_mu0", "Crystal Ball mean (GeV)",4.32138e-01);          mu0.setConstant(1);
	RooRealVar sigma0("cb_sigma0", "Crystal Ball sigma (pos)",7.98491e-01);   sigma0.setConstant(1);
	RooRealVar eta0("cb_eta0", "Crystal Ball exponent (pos)" , 1.88919e+01);  eta0.setConstant(1);
	RooRealVar a0("cb_a0", "Crystal Ball cutoff parameter", 9.57858e-01);      a0.setConstant(1);


	RooRealVar DBmean("DBmean", "DBmean", 5.81462e+01);                            DBmean.setConstant(1);
	RooRealVar DBsigma("DBsigma", "DBsigma", 1.07103e+01);                         DBsigma.setConstant(1);
	RooRealVar DBfraction0("DBfraction0", "DBfraction0", DB_fraction[0]);    DBfraction0.setConstant(1);
	RooRealVar DBfraction1("DBfraction1", "DBfraction1", DB_fraction[1]);    DBfraction1.setConstant(1);
	RooRealVar DBfraction2("DBfraction2", "DBfraction2", DB_fraction[2]);    DBfraction2.setConstant(1);
	RooRealVar DBfraction3("DBfraction3", "DBfraction3", DB_fraction[3]);    DBfraction3.setConstant(1);

	RooBreitWigner bw("BreitWigner", "BreitWigner", m12, mean, width);
	RooCBShape gauss("gauss", "gauss", m12, mu, sigma, a, eta);
	RooCBShape gauss0("gauss0", "gauss0", m12, mu0, sigma0, a0, eta0);
	RooGaussian DBG ("DB", "DB", m12, DBmean, DBsigma);


	// Crystal Ball convoluted with Breit Wigner
	RooFFTConvPdf* BWCB  = new RooFFTConvPdf("pdf_Z", "BWCB", m12, bw, gauss);        BWCB->getVal();   fWorkspace->import(*BWCB);
	RooFFTConvPdf* BWCB1 = new RooFFTConvPdf("pdf_Zl", "BWCB1", m12, bw, gauss);      BWCB1->getVal();  fWorkspace->import(*BWCB1, RecycleConflictNodes());
	//RooFFTConvPdf* BWCB2 = new RooFFTConvPdf("pdf_Diboson", "BWCB2", m12, bw, gauss); BWCB2->getVal();  fWorkspace->import(*BWCB2, RecycleConflictNodes());
	RooFFTConvPdf* BWCB0 = new RooFFTConvPdf("pdf_Diboson", "BWCB0", m12, bw, gauss0);     BWCB0->getVal();  fWorkspace->import(*BWCB0, RecycleConflictNodes());

	fWorkspace->extendSet("Set_ShapeParams", "bw_mean,bw_width,cb_mu,cb_sigma,cb_eta,cb_a,cb_mu0,cb_sigma0,cb_eta0,cb_a0"); //mean,width

	RooAddPdf * DB_GBWCB3 = new RooAddPdf("pdf_Diboson_ss","DB", RooArgList(DBG,*BWCB0), DBfraction3);     DB_GBWCB3->getVal(); fWorkspace->import(*DB_GBWCB3, RecycleConflictNodes());
	RooAddPdf * DB_GBWCB2 = new RooAddPdf("pdf_Diboson_invIso","DB", RooArgList(DBG,*BWCB0), DBfraction2);     DB_GBWCB2->getVal(); fWorkspace->import(*DB_GBWCB2, RecycleConflictNodes());
	RooAddPdf * DB_GBWCB1 = new RooAddPdf("pdf_Diboson_invD0","DB", RooArgList(DBG,*BWCB0), DBfraction1);     DB_GBWCB1->getVal(); fWorkspace->import(*DB_GBWCB1, RecycleConflictNodes());
	RooAddPdf * DB_GBWCB0  = new RooAddPdf("pdf_Diboson_relaxIsoD0","DB", RooArgList(DBG,*BWCB0), DBfraction0);  DB_GBWCB0->getVal();  fWorkspace->import(*DB_GBWCB0, RecycleConflictNodes());

    fWorkspace->extendSet("Set_ShapeParams","DBmean,DBsigma,DBfraction0,DBfraction1,DBfraction2,DBfraction3");

	// Polynomial for Z+jets and Diboson in the emu CR
	RooRealVar poly_c1("poly_c1", "poly_c1[fix]", -7.4075e-03);  poly_c1.setConstant(ConstrainParameters); //poly_c1.setMin(-2e-3); poly_c1.setMax(2e-3);

	TString form = "(1+poly_c1*mZ1_fsr > 0)*(1+poly_c1*mZ1_fsr)+1e-12";
	RooGenericPdf* Poly  = new RooGenericPdf("pdf_Z_emu","genpdf", form, RooArgSet(m12,poly_c1)) ;        Poly->getVal();  fWorkspace->import(*Poly);
	RooGenericPdf* Poly1 = new RooGenericPdf("pdf_Zl_emu","genpdf", form, RooArgSet(m12,poly_c1)) ;       Poly1->getVal(); fWorkspace->import(*Poly1,RecycleConflictNodes());
	RooGenericPdf* Poly2 = new RooGenericPdf("pdf_Diboson_emu","genpdf", form, RooArgSet(m12,poly_c1)) ;  Poly2->getVal(); fWorkspace->import(*Poly2, RecycleConflictNodes());

	fWorkspace->extendSet("Set_ShapeParams", "poly_c1");


}


void BuildCRModels() {

    cout<<"Building CR Models"<<endl;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Background model for each region
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	RooCategory cat_regions("cat_regions","cat_regions");

	TString term_name, region;
	TString param_name, param_title, param_mean, param_sigma, param_constr;

	for (int icregion=0; icregion < 5; icregion++) {

		region = CR[icregion];

		if (icregion > 0) cat_regions.defineType("CR_"+region);
        RooArgList list_norm;

		for (int isample=0; isample < P.size(); isample++) {

			//samp = GetSample(isample);
			if (P[isample]->process_name.Contains("Data")) continue; //jump the iteration
			TString str_suffix = P[isample]->process_name + "_" + region;

			// ~ Add this sample's shape to the list of pdf terms of this region

			if (fWorkspace->pdf("pdf_" + str_suffix)) {fWorkspace->extendSet("Set_Pdf_" + region, "pdf_" + str_suffix);  }
			else {fWorkspace->extendSet("Set_Pdf_" + region, "pdf_" + P[isample]->process_name); }

			// ~ Add the corresponding normalization term

			// [1] Relaxed region term (unconstrained)
			RooRealVar*  norm_SR = GetVar("norm_" + P[isample]->process_name, P[isample]->CRevents[0]);
			if (P[isample]->IsForFit==0) norm_SR->setConstant();
			norm_SR->setMin(0.);

			// [2] [Fraction] mc-expectation in this CR over the relaxed region (constrained)
			RooRealVar*  frac  = GetVar("frac_" + str_suffix, P[isample]->CRfraction[icregion]);                   frac->setConstant();
			RooRealVar*  frac_rerr = GetVar("frac_rerr_" + str_suffix, P[isample]->CRfractionRelError[icregion]);  frac_rerr->setConstant();
			RooRealVar*  alpha = GetVar("alpha_" + str_suffix, 0, -3.0, 3.0); // how many sigmas away... +/- 5 sigma!!!

			fWorkspace->extendSet("Set_Alphas", alpha->GetName());

			TString expr;
			term_name = "frac_term_" + str_suffix;
			const char* expr_gaus = "expr::%s('@0*(1. + @1*@2)', %s,%s,%s)";
			const char* expr_logn = "expr::%s('@0*pow(1. + @1, @2)', %s,%s,%s)";
			Bool_t use_gaus(P[isample]->CRfractionRelError[icregion] < 0.25); // safe since alpha is in [-3,3]

			expr.Form((use_gaus ? expr_gaus : expr_logn), term_name.Data(), frac->GetName(), frac_rerr->GetName(), alpha->GetName());

			fWorkspace->factory(expr);
			RooAbsReal* frac_term = fWorkspace->function(term_name);

			// [3] Final normalization term ([1]*[2])
			RooProduct*  norm_CR = new RooProduct("norm_" + str_suffix, "", RooArgSet(*frac_term, *norm_SR));
			list_norm.addOwned(*norm_CR);

			// ~ Constraint for the alpha variable
			param_name   = "alpha_"   + str_suffix;
			param_constr = "constr_"  + param_name;
			param_mean   = "mean_"    + param_name;

			if (icregion == 0 || P[isample]->IsForFit==0) {    //|| !samp->isForFit
				alpha->setConstant();
			} else if (!fWorkspace->pdf(param_constr) && P[isample]->CRfractionRelError[icregion] > 0.) {
				fWorkspace->factory(TString::Format("Gaussian::%s(%s, %s[0., -3., 3.], 1.)", param_constr.Data(), param_name.Data(), param_mean.Data()));
				fWorkspace->var(param_mean)->setConstant();

				fWorkspace->extendSet("Set_AllConstraints_" + region, param_constr);
				fWorkspace->extendSet("Set_AllMeans", param_mean);
				fWorkspace->extendSet("Set_AlphaConstraints_" + region, param_constr);
				fWorkspace->extendSet("Set_AlphaMeans", param_mean);
			}
		}

		 ///~ Unconstrained model for this region

		const RooArgSet* list_pdf =fWorkspace->set("Set_Pdf_"+region);
		cout<<"Info | PDF components defined for region "<<region<<": ";
		list_pdf->Print();

		term_name = "pdf_" + region;
		RooAddPdf* pdf = new RooAddPdf(term_name, term_name, *list_pdf, list_norm);
		pdf->getVal();
		cout<<"Info | Unconstrained model ready: ";
		pdf->Print();
		fWorkspace->import(*pdf);
		delete pdf;
		pdf = (RooAddPdf*)fWorkspace->pdf(term_name);

		/// ~ Constrains for the shape parameters... in case we want to constrain them!!!

    //  if (ConstrainParameters==1){
//		RooRealVar* tmp_var(0);
//		RooArgSet* list_shapepars_tmp = GetShapeParameters(pdf);
//		RooLinkedListIter iter = list_shapepars_tmp->iterator();
//		while ((tmp_var = (RooRealVar*)iter.Next())) {
//
//			param_name  = tmp_var->GetName();
//			param_title = tmp_var->GetTitle();
//
//			if (param_title.Contains("[theory]")) continue; // parameters that should always be kept constant
//			if (param_title.Contains("[fix]")) continue;    // parameters that should be fixed in the data fit
//
//			param_constr = "constr_"  + param_name;
//			param_mean   = "mean_"    + param_name;
//			param_sigma  = "sigma_"   + param_name;
//
//			if (!fWorkspace->pdf(param_constr)) {
//				fWorkspace->factory(TString::Format("Gaussian::%s(%s, %s[0., -5., 5.], %s[1])", param_constr.Data(), param_name.Data(), param_mean.Data(), param_sigma.Data()));
//				fWorkspace->var(param_mean)->setConstant();
//			}
//
//			fWorkspace->extendSet("Set_ShapeConstraints_" + region, param_constr);
//			fWorkspace->extendSet("Set_AllConstraints_" + region, param_constr);
//			fWorkspace->extendSet("Set_ShapeMeans", param_mean); // Check that it drops double entries...
//			fWorkspace->extendSet("Set_AllMeans", param_mean);

	//	}

		 ///Cleanup
//		delete list_shapepars_tmp;
//		list_norm.Clear();}

		// ~ Constrained model for this region

		RooAbsPdf* model(0);

		 ///[Model-1] with fraction constraints only
		term_name = "model_"+ region;
		const RooArgSet* list_constrterms = fWorkspace->set("Set_AlphaConstraints_" + region);
		if (list_constrterms && list_constrterms->getSize() > 0) {
			model = new RooProdPdf(term_name, term_name, RooArgSet(*pdf, *list_constrterms));
		} else {
			model = (RooAbsPdf*)pdf->Clone(term_name);
		}

		model->getVal();
		fWorkspace->import(*model);
		delete model; model = 0;
		cout<<"Info | Constrained model with fraction constraints ready for "<<region<<endl;

		/// [Model-2] with fraction + shape constraints
		term_name = "model_shconstr_"+region;
		list_constrterms = fWorkspace->set("Set_AllConstraints_" + region);
		if (list_constrterms && list_constrterms->getSize() > 0) {
			model = new RooProdPdf(term_name, term_name, RooArgSet(*pdf, *list_constrterms));
		} else {
			model = (RooAbsPdf*)pdf->Clone(term_name);
		}

		model->getVal();
		fWorkspace->import(*model);
		delete model; model = 0;
		cout<<"Info | Constrained model with fraction+shape constraints ready for "<<region<<endl;
		pdf = 0;
     }

	fWorkspace->import(cat_regions);

	//fModelOK = kTRUE;
	cout<<"Build Done!"<<endl;
	//return kTRUE;
}
/////------------------------------------------Build Model--------------------------------------------------------///


RooRealVar* GetVar(const char* name, Double_t val=0., Double_t min=0., Double_t max=0.)
{
	RooRealVar* rvar = fWorkspace->var(name);

	if (!rvar) {
		fWorkspace->factory(TString::Format("%s[%lf]", name, val));
		rvar = fWorkspace->var(name);
		rvar->setConstant(kFALSE);
	}

	if (max > min) {
		rvar->setMin(min);
		rvar->setMax(max);
	}

	return rvar;
}


///-------------------------------------------Fit---------------------------------------------------------------///

void Fit(TString Mode)
{
    cout<<"Fitting"<<endl;

	TString region;
	UInt_t icregion;
	Int_t ncregions = ActiveRegion["relaxIsoD0"]+ActiveRegion["invIso"]+ActiveRegion["invD0"]+ActiveRegion["emu"]+ActiveRegion["ss"]-1;// GetActiveControlRegions();

	map<string, RooDataSet*> dataMap;
	RooCategory* cat_regions = fWorkspace->cat("cat_regions");
	//cat_regions->Print();
	//RooRealVar*  m12 = fWorkspace->var("mZ1_fsr");

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Build the data and the simultaneous p.d.f.
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//Clear("D"); // clear previous datasets
	RooAbsPdf* thePdf(0);

	if (ncregions > 1) {
		cout<<"Info: There are "<<ncregions<<" active control regions. We are going to use a RooSimultaneous.\n";
		thePdf = new RooSimultaneous("simPdf", "simultaneous pdf", *cat_regions);
	} else if (ncregions == 1) {
		cout<<"Info: There is 1 active control region. We are going to use a simple pdf.\n";
	} else {
		cout<<"Warning | There are no active control regions. Exiting.\n";
		return;
	}

    if (Mode=="MC Data")dataset[0] = RDS_MC[0];
    else                dataset[0] = RDS_Data[0];

	//dataset[0]->Print("v");

	for (icregion = 1; icregion <5; icregion++) {

		if (ActiveRegion[CR[icregion]]==0) continue;

		//region = CR[icregion];

		TString tmp = "CR_" + CR[icregion];
		if (Mode=="MC Data")  dataset[icregion] = RDS_MC[icregion];
		else                  dataset[icregion] = RDS_Data[icregion];
		//dataset[icregion]->Print("v");
		dataMap[tmp.Data()] = dataset[icregion];

		TString strpdf = /*(ConstrainParameters) ? "model_shconstr_" + region :*/ "model_" + CR[icregion];

		if (ncregions > 1) {
			((RooSimultaneous*)thePdf)->addPdf(*fWorkspace->pdf(strpdf), "CR_"+CR[icregion]);
		} else {
			thePdf = fWorkspace->pdf(strpdf);
		}
	}

	//thePdf->Print();
	cout<<"Info: Pdf acquired. Ready to build the dataset\n";

	RooDataSet CombinedData("CombinedData","CombinedData", RooArgSet(*m12, weight, *cat_regions), Index(*cat_regions), Import(dataMap), WeightVar("weight"));

	//~~~~~~~~~~~~~~~~
	// Fit to the Data
	//~~~~~~~~~~~~~~~~

	//if (do_fit) {
		RooArgSet ConstrVars(*fWorkspace->set("Set_Alphas"));
		RooArgSet GlobalObs(*fWorkspace->set("Set_AlphaMeans"));

//		if (ConstrainParameters) {
//			ConstrVars.add(*fWorkspace->set("Set_ShapeParams"));
//			GlobalObs.add(*fWorkspace->set("Set_ShapeMeans"));
//		}

		RooStats::RemoveConstantParameters(&ConstrVars);
		// also remove it from global observables ?

		Bool_t sumw2 = kFALSE; // False for fit to unbinned data (even weighted)
		Bool_t hesse = 0;
		Int_t  strat = 2;
		Int_t  ncpu  = ncregions;
		Int_t  stratcpu = (ncregions > 1) ? 3 : 0;

		RooLinkedList l;
		//l.Add(new RooCmdArg(Extended(kTRUE)));
		l.Add(new RooCmdArg(Constrain(ConstrVars)));
		l.Add(new RooCmdArg(GlobalObservables(GlobalObs)));
		l.Add(new RooCmdArg(NumCPU(ncpu,stratcpu)));
		l.Add(new RooCmdArg(SumW2Error(sumw2)));
		l.Add(new RooCmdArg(Save()));
		l.Add(new RooCmdArg(Strategy(strat)));
		l.Add(new RooCmdArg(Hesse(hesse)));
		l.Add(new RooCmdArg(Minimizer("Minuit2","MIGRAD")));
		l.Add(new RooCmdArg(PrintLevel(0)));
		//l.Add(new RooCmdArg(Offset(kTRUE)));

         RooFitResult* rr = thePdf->fitTo(CombinedData, l);

		cout<<"Fit done!!\n";
		if (ncregions > 1) delete thePdf;

		//~~~~~~~~~~~~~~~~~
		// Show the Results
		//~~~~~~~~~~~~~~~~~

		cout<<"----------------------Results----------------"<<endl;

		ofstream ffile2("Results/"+Options+"_Pars.txt", ofstream::trunc);

		rr->printMultiline(ffile2, 1, kTRUE);

		ffile2.close();

       // Construct 2D color plot of correlation matrix (?)
       /*gStyle->SetOptStat(0) ;
       gStyle->SetPalette(1) ;
       TH2* hcorr = rr->correlationHist() ;

	   TFile myf("rf_fitresult.root","RECREATE") ;
       TCanvas* c = new TCanvas("rf_fitresult","rf_fitresult",800,400) ;
       gPad->SetLeftMargin(0.15) ; hcorr->GetYaxis()->SetTitleOffset(1.4) ;
	   hcorr->Draw("colz");
	   c->Write();
	   myf.Close() ;*/

	   // Access correlation matrix elements
	   //if(do_splitZlight){
//		HF_LF_crl=rr->correlation("norm_Z", "norm_Zl");
//		cout << "\n\n!!!!----> correlation between Z+HF and Z+LF is  " << rr->correlation("norm_Z", "norm_Zl") << "!!!!! <-----"<<endl<<endl;
		//}
        //cout << "correlation between bkgfrac and mean is " << rr->correlation("bkgfrac","mean") << "!!!!! <-----\n"<<endl ;

       l.Delete(); // delete all elements
	   delete rr;
	   cout<<endl;
	//}

  //if (do_plotdata) DoPlot("Data");
  //if (do_plotmc)
  DoPlot(Mode);
}
