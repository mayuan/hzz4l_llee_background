using namespace RooFit;
using namespace TMath;

TString mc16e   = "root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v23/AntiKt4EMPFlow/mc16e/Background/BkgCR/";
TString mc16d   = "root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v23/AntiKt4EMPFlow/mc16d/Background/BkgCR/";
TString mc16a   = "root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v23/AntiKt4EMPFlow/mc16a/Background/BkgCR/";

TString data_p23 = "root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v23/data/Background/BkgCR/";

TString Zlf_e    = mc16e + "mc16_13TeV.3641*.Sherpa_221_NNPDF30NNLO_Z*_MAXHTPTV*_bkgCR.root";
TString Zlf_d    = mc16d + "mc16_13TeV.3641*.Sherpa_221_NNPDF30NNLO_Z*_MAXHTPTV*_bkgCR.root";
TString Zlf_a    = mc16a + "mc16_13TeV.3641*.Sherpa_221_NNPDF30NNLO_Z*_MAXHTPTV*_bkgCR.root";

TString Zhf_e    = mc16e + "mc16_13TeV.3442*.Sherpa_NNPDF30NNLO_Z*_bkgCR.root";
TString Zhf_d    = mc16d + "mc16_13TeV.3442*.Sherpa_NNPDF30NNLO_Z*_bkgCR.root";
TString Zhf_a    = mc16a + "mc16_13TeV.3442*.Sherpa_NNPDF30NNLO_Z*_bkgCR.root";

TString ttbar_e  = mc16e + "mc16_13TeV.410*.PhPy8EG_A14_ttbar_hdamp258p75_*_bkgCR.root";
TString ttbar_d  = mc16d + "mc16_13TeV.410*.PhPy8EG_A14_ttbar_hdamp258p75_*_bkgCR.root";
TString ttbar_a  = mc16a + "mc16_13TeV.410*.PhPy8EG_A14_ttbar_hdamp258p75_*_bkgCR.root";

TString qqZZ_e   = mc16e + "mc16_13TeV.36425*.Sherpa_222_NNPDF30NNLO_llll_*bkgCR.root";
TString qqZZ_d   = mc16d + "mc16_13TeV.36425*.Sherpa_222_NNPDF30NNLO_llll_*bkgCR.root";
TString qqZZ_a   = mc16a + "mc16_13TeV.36425*.Sherpa_222_NNPDF30NNLO_llll_*bkgCR.root";

TString ggZZ_e  = mc16e + "mc16_13TeV.34570*.Sherpa_222_NNPDF30NNLO_ggllllNoHiggs_*_bkgCR.root";
TString ggZZ_d  = mc16d + "mc16_13TeV.34570*.Sherpa_222_NNPDF30NNLO_ggllllNoHiggs_*_bkgCR.root";
TString ggZZ_a  = mc16a + "mc16_13TeV.34570*.Sherpa_222_NNPDF30NNLO_ggllllNoHiggs_*_bkgCR.root";

TString ggZZEW_e  = mc16e + "mc16_13TeV.364364.Sherpa_222_NNPDF30NNLO_lllljj_EW6_noHiggs_AFii__bkgCR.root";
TString ggZZEW_d  = mc16d + "mc16_13TeV.364364.Sherpa_222_NNPDF30NNLO_lllljj_EW6_noHiggs_AFii__bkgCR.root";
TString ggZZEW_a  = mc16a + "mc16_13TeV.364364.Sherpa_222_NNPDF30NNLO_lllljj_EW6_noHiggs_AFii__bkgCR.root";

TString WZ_e     = mc16e + "mc16_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4_bkgCR.root";
TString WZ_d     = mc16d + "mc16_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4_bkgCR.root";
TString WZ_a     = mc16a + "mc16_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4_bkgCR.root";

TString ttVb_e   = mc16e + "mc16_13TeV.34593*.aMcAtNloPy8EG_MEN30NLO_A14N23LO_tt*_m4l100_150_mll_3l_bkgCR.root";
TString ttVb_d   = mc16d + "mc16_13TeV.34593*.aMcAtNloPy8EG_MEN30NLO_A14N23LO_tt*_m4l100_150_mll_3l_bkgCR.root";
TString ttVb_a   = mc16a + "mc16_13TeV.34593*.aMcAtNloPy8EG_MEN30NLO_A14N23LO_tt*_m4l100_150_mll_3l_bkgCR.root";

TString ttVa_e    = mc16e + "mc16_13TeV.410*.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tt*_bkgCR.root";
TString ttVa_d    = mc16d + "mc16_13TeV.410*.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tt*_bkgCR.root";
TString ttVa_a    = mc16a + "mc16_13TeV.410*.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tt*_bkgCR.root";

TString VVV_e    = mc16e + "mc16_13TeV.36424*.Sherpa_222_*_bkgCR.root";
TString VVV_d    = mc16d + "mc16_13TeV.36424*.Sherpa_222_*_bkgCR.root";
TString VVV_a    = mc16a + "mc16_13TeV.36424*.Sherpa_222_*_bkgCR.root";

TString ggH_e    = mc16e + "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l_bkgCR.root";
TString ggH_d    = mc16d + "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l_bkgCR.root";
TString ggH_a    = mc16a + "mc16_13TeV.345060.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4l_bkgCR.root";

TString vbfH_e   = mc16e + "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau_bkgCR.root";
TString vbfH_d   = mc16d + "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau_bkgCR.root";
TString vbfH_a   = mc16a + "mc16_13TeV.346228.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau_bkgCR.root";

TString WH_e     = mc16e + "mc16_13TeV.34664*.PowhegPythia8EvtGen_NNPDF30_AZNLO_W*H125J_Wincl_MINLO_shw_bkgCR.root";
TString WH_d     = mc16d + "mc16_13TeV.34664*.PowhegPythia8EvtGen_NNPDF30_AZNLO_W*H125J_Wincl_MINLO_shw_bkgCR.root";
TString WH_a     = mc16a + "mc16_13TeV.34664*.PowhegPythia8EvtGen_NNPDF30_AZNLO_W*H125J_Wincl_MINLO_shw_bkgCR.root";

TString ZH_e     = mc16e + "mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw_bkgCR.root";
TString ZH_d     = mc16d + "mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw_bkgCR.root";
TString ZH_a     = mc16a + "mc16_13TeV.346645.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO_shw_bkgCR.root";

TString ttH_e    = mc16e + "mc16_13TeV.346*.PowhegPy*_ttH125_ZZ4l_*_bkgCR.root";
TString ttH_d    = mc16d + "mc16_13TeV.346*.PowhegPy*_ttH125_ZZ4l_*_bkgCR.root";
TString ttH_a    = mc16a + "mc16_13TeV.346*.PowhegPy*_ttH125_ZZ4l_*_bkgCR.root";


TString Data_p23 = data_p23 + "data1*_bkgCR.root";





RooWorkspace* fWorkspace(0);
RooRealVar   weight("weight", "", 1.);

TString CR[5]={"relaxIsoD0","invD0","invIso","ss","emu"};
TString ModifiedCR[5]={"relaxIsoD0","invD0","invIso","ss","emu"};
RooRealVar*  m12;
RooDataSet* RDS_MC[5];
RooDataSet* RDS_Data[5];
RooDataSet*  dataset[5];
map <TString, double> ImbCut;
map <TString, double> D0Cut;
map <TString, double> PVtxCut;
map <TString, bool> ActiveRegion;
TString Options;
TString Category;
int IsoSet;
double MIN, MAX, DB_fraction[5];
int Cps;

double DR(double eta1, double eta2, double phi1, double phi2){
double Deta=eta1-eta2;
double Dphi=phi1-phi2;
if(Dphi>3.14159) Dphi = 2*3.14159 - Dphi;
if(Dphi<-3.14159) Dphi = 2*3.14159 + Dphi;
return sqrt(Deta*Deta+Dphi*Dphi);}


TString String (int number, bool zero){

string Result;

ostringstream convert;
convert << number;
Result = convert.str();

if(zero==true && number<10) return "0"+Result;
else                        return Result;
}
