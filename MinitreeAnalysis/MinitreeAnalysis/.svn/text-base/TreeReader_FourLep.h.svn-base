#ifndef FourLep_h
#define FourLep_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <string>
using namespace std;

// Header file for the classes stored in the TTree if any.

class FourLep {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       event;   //!
   Int_t           event_type;   //!
   Int_t           fsr_associated_lepton;   //!
   Int_t           fsr_type;   //!
   Int_t           lbn;   //!
   Int_t           n_jets;   //!
   Int_t           n_jets_truth_bare;   //!
   Int_t           n_jets_truth_fid;   //!
   Int_t           prod_type;   //!
   Int_t           run;   //!
   Float_t         BDT_discriminant;   //!
   Float_t         KD_discriminant;   //!
   Float_t         eta4l_fsr;   //!
   Float_t         eta4l_truth_born;   //!
   Float_t         eta4l_truth_matched_bare;   //!
   Float_t         eta4l_unconstrained;   //!
   Float_t         fsr_eta;   //!
   Float_t         fsr_phi;   //!
   Float_t         fsr_pt;   //!
   Float_t         fsr_pt_constrained;   //!
   Float_t         m4l_constrained;   //!
   Float_t         m4l_fsr;   //!
   Float_t         m4l_truth_born;   //!
   Float_t         m4l_truth_matched_bare;   //!
   Float_t         m4l_unconstrained;   //!
   Float_t         m4lerr_fsr;   //!
   Float_t         m4lerr_unconstrained;   //!
   Float_t         mZ1_constrained;   //!
   Float_t         mZ1_fsr;   //!
   Float_t         mZ1_truth_born;   //!
   Float_t         mZ1_truth_matched_bare;   //!
   Float_t         mZ1_unconstrained;   //!
   Float_t         mZ2_constrained;   //!
   Float_t         mZ2_fsr;   //!
   Float_t         mZ2_truth_born;   //!
   Float_t         mZ2_truth_matched_bare;   //!
   Float_t         mZ2_unconstrained;   //!
   Float_t         outBDT_gauss;   //!
   Float_t         pt4l_fsr;   //!
   Float_t         pt4l_truth_born;   //!
   Float_t         pt4l_truth_matched_bare;   //!
   Float_t         pt4l_unconstrained;   //!
   Float_t         weight;   //!
   Float_t         weight_corr;   //!
   Float_t         weight_lumi;   //!
   Float_t         weight_ptDown;   //!
   Float_t         weight_ptUp;   //!
   Float_t         weight_sampleoverlap;   //!
   Float_t         y4l_fsr;   //!
   Float_t         y4l_truth_born;   //!
   Float_t         y4l_truth_matched_bare;   //!
   Float_t         y4l_unconstrained;   //!
   Float_t         dijet_invmass; //!
   Float_t         dijet_deltaeta; //!
   Float_t         leading_jet_pt; //!
   Float_t         subleading_jet_pt; //!
   Int_t           pass_vtx4lCut; //!

   int             el_nInnerExpPix[4]; //!
   float           lepton_pt[4]; //!

   // List of branches
   TBranch        *b_event;   //!
   TBranch        *b_event_type;   //!
   TBranch        *b_fsr_associated_lepton;   //!
   TBranch        *b_fsr_type;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_n_jets;   //!
   TBranch        *b_n_jets_truth_bare;   //!
   TBranch        *b_n_jets_truth_fid;   //!
   TBranch        *b_prod_type;   //!
   TBranch        *b_run;   //!
   TBranch        *b_BDT_discriminant;   //!
   TBranch        *b_KD_discriminant;   //!
   TBranch        *b_eta4l_fsr;   //!
   TBranch        *b_eta4l_truth_born;   //!
   TBranch        *b_eta4l_truth_matched_bare;   //!
   TBranch        *b_eta4l_unconstrained;   //!
   TBranch        *b_fsr_eta;   //!
   TBranch        *b_fsr_phi;   //!
   TBranch        *b_fsr_pt;   //!
   TBranch        *b_fsr_pt_constrained;   //!
   TBranch        *b_m4l_constrained;   //!
   TBranch        *b_m4l_fsr;   //!
   TBranch        *b_m4l_truth_born;   //!
   TBranch        *b_m4l_truth_matched_bare;   //!
   TBranch        *b_m4l_unconstrained;   //!
   TBranch        *b_m4lerr_fsr;   //!
   TBranch        *b_m4lerr_unconstrained;   //!
   TBranch        *b_mZ1_constrained;   //!
   TBranch        *b_mZ1_fsr;   //!
   TBranch        *b_mZ1_truth_born;   //!
   TBranch        *b_mZ1_truth_matched_bare;   //!
   TBranch        *b_mZ1_unconstrained;   //!
   TBranch        *b_mZ2_constrained;   //!
   TBranch        *b_mZ2_fsr;   //!
   TBranch        *b_mZ2_truth_born;   //!
   TBranch        *b_mZ2_truth_matched_bare;   //!
   TBranch        *b_mZ2_unconstrained;   //!
   TBranch        *b_outBDT_gauss;   //!
   TBranch        *b_pt4l_fsr;   //!
   TBranch        *b_pt4l_truth_born;   //!
   TBranch        *b_pt4l_truth_matched_bare;   //!
   TBranch        *b_pt4l_unconstrained;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_weight_corr;   //!
   TBranch        *b_weight_lumi;   //!
   TBranch        *b_weight_ptDown;   //!
   TBranch        *b_weight_ptUp;   //!
   TBranch        *b_weight_sampleoverlap;   //!
   TBranch        *b_y4l_fsr;   //!
   TBranch        *b_y4l_truth_born;   //!
   TBranch        *b_y4l_truth_matched_bare;   //!
   TBranch        *b_y4l_unconstrained;   //!
   TBranch        *b_dijet_invmass; //!
   TBranch        *b_dijet_deltaeta; //!
   TBranch        *b_leading_jet_pt; //!
   TBranch        *b_subleading_jet_pt; //!
   TBranch        *b_pass_vtx4lCut; //!

   TBranch        *b_el_nInnerExpPix; //!
   TBranch        *b_lepton_pt; //!

   FourLep(string fname, string tname="tree_incl_all", TTree *tree=0);
   FourLep(TTree* tree){Init(tree);}
   FourLep(){;}
   virtual ~FourLep();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
};

#endif


