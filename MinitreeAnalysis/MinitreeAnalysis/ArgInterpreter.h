#ifndef MinitreeAnalysis_ArgInterpreter_H
#define MinitreeAnalysis_ArgInterpreter_H

#include "TString.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>

class ArgInterpreter{
  
  public:
    ArgInterpreter();

    std::string input;
    std::string output;
    bool verbose;
    bool doAna;
    int proofNw;
    bool atCarleton;
    std::vector<bool> doRegion;
    float lumi;
    bool doSys;
    bool saveJets;
    int maxEvt;
    std::string sample;
    bool isTxt;
    bool triplets;
    bool isOld;
    bool useVertex;
    bool isData;
    
    void PrintOptions(int input=0, int output=0, int region=0, int proofNw=0, int doAna=0, int verbose=0, int regions=0, int lumi=0, int sys=0, int ev=0,int sample=0, int triplets=0, int old=0);
    void Interpret(int argc, char* argv[]);
    bool Check(int input=0, int output=0, int region=0, int proofNw=0);
    void PrintConfig(int input=0, int output=0, int region=0, int proofNw=0, int doAna=0, int verbose=0, int atCarleton=0, int lumi=0, int sys=0, int ev=0,int sample=0, int triplets=0, int old=0);};


ArgInterpreter::ArgInterpreter(){
  input="";
  output="";
  verbose=false;
  atCarleton=false;
  doAna=true;
  proofNw=0;
  doRegion=std::vector<bool>(0,false);
  lumi=1.;
  doSys=false;
  saveJets=false;
  maxEvt=-1;
  sample="";
  isTxt=false;
  triplets=false;
  isOld=false;
  useVertex=true;
  isData=false;
}

void ArgInterpreter::Interpret(int argc, char* argv[]){
  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--input")){
      input=argv[++a];
      if (input.find("data/data")!=std::string::npos) atCarleton=true;
      if (input.find(".txt")!=std::string::npos) isTxt=true;
    }
    else if (!strcmp(argv[a],"--output")){
      output=argv[++a];
    }
    else if (!strcmp(argv[a],"--region")){
    }
    else if (!strcmp(argv[a],"--verbose")){
      verbose=true;
    }
    else if (!strcmp(argv[a],"--doAna")){
      std::string s=argv[++a];
      if (s=="true" || s=="t" || s=="1")
        doAna=true;
      else if (s=="false" || s=="f" || s=="0")
        doAna=false;
    }
    else if (!strcmp(argv[a],"--proofNw")){
      proofNw=std::atoi(argv[++a]);
    }
    else if (!strcmp(argv[a],"--lumi")){
      lumi=std::atof(argv[++a]);
    }
    else if (!strcmp(argv[a],"--doSys")){
      doSys=true;
    }
    else if (!strcmp(argv[a],"--saveJets")){
      saveJets=true;
    }
    else if (!strcmp(argv[a],"--maxEvt")){
      maxEvt=std::atoi(argv[++a]);
    }
    else if (!strcmp(argv[a],"--sample")){
      sample=argv[++a];
    }
    else if(!strcmp(argv[a],"--triplets")){
      triplets=true;
    }
    else if(!strcmp(argv[a],"--isOld")){
      isOld=true;
    }
    else if(!strcmp(argv[a],"--noVertex")){
      useVertex=false;
    }
    else if(!strcmp(argv[a],"--isData")){
      isData=true;
    }
    /* else { */
    /*   std::cout<<"invalid argument received: "<<argv[a]<<std::endl; */
    /*   std::cout<<"run command with arguments to view valid options"<<std::endl; */
    /*   exit(-1); */
    /* } */
  }
}

void ArgInterpreter::PrintOptions(int p_input, int p_output, int p_region, int p_proofNw, int p_doAna, int p_verbose, int p_regions, int lumi, int sys, int ev, int p_sample, int p_triplets, int p_old) {
  std::cout<<"\nMust provide arguments!\n"<<std::endl;
  if (p_input==1)
    std::cout<<"--input [e.g. /eos/atlas/blabla.... or /afs/cern.ch/work/blabla]"<<std::endl;
  if (p_input==2)
    std::cout<<"--input [e.g. mc15_13TeV.*ggH*HIGG2D2*p1851*/ ]"<<std::endl;
  if (p_output==1)
    std::cout<<"--output [e.g. /afs/cern.ch/work/blabla]"<<std::endl;
  if (p_output==2)
    std::cout<<"--output [e.g. test_v0] your job will be named user.blablabla.prod_test_v0"<<std::endl;
  if (p_region)
    std::cout<<"--region [\"all\" or specific region*]"<<std::endl;
  if (p_proofNw)
    std::cout<<"--proofNw {0},1...8"<<std::endl;
  if (p_doAna)
    std::cout<<"--doAna {t}/f"<<std::endl;
  if (p_verbose)
    std::cout<<"--verbose"<<std::endl;
  if (lumi)
    std::cout<<"--lumi [multiplier]"<<std::endl;
  if (sys)
    std::cout<<"--doSys"<<std::endl;
  if (ev)
    std::cout<<"--maxEvt"<<std::endl;
  if(p_sample)
    std::cout<<"--sample"<<std::endl;
  if (p_regions){
    std::cout<<"*regions available:"<<std::endl;
    for (int i(0);i<0;i++){
      std::cout<<"    "<< i<<" : "<<std::endl;
    }    
  }
  if(p_triplets)
    std::cout<<"--triplets"<<std::endl;
  if(p_old)
    std::cout<<"--isOld"<<std::endl;
}

void ArgInterpreter::PrintConfig(int p_input, int p_output, int p_region, int p_proofNw, int p_doAna, int p_verbose, int p_atCarleton, int p_lumi, int p_sys, int p_ev, int p_sample, int p_triplets, int p_old) {
  std::cout<<"\njob configuration\n----------------\n"<<std::endl;
  if (p_input)
    std::cout<<"input:    "<<input<<std::endl;
  if (p_output)
    std::cout<<"output:   "<<output<<std::endl;
  if (p_region)
  {std::cout<<"regions:  "; for (int i(0);i<0;i++) std::cout<<(doRegion[i]? " ":""); std::cout<<std::endl;}
  if (p_doAna)
    std::cout<<"doAna     "<<(doAna?"on":"OFF")<<std::endl;
  if (p_verbose)
    std::cout<<"verbose   "<<(verbose?"ON":"off")<<std::endl;
  if (p_proofNw)
    std::cout<<"proofNw  "<<proofNw<<std::endl;
  if (p_atCarleton){
    if (atCarleton) std::cout<<"running at CARLETON"<<std::endl;
    else std::cout<<"running at CERN"<<std::endl;
  }
  if (p_lumi)
    std::cout<<"luminosity multiplier: "<<lumi<<std::endl;
  if (p_sys)
    std::cout<<"systematics: "<<(doSys?"yes":"no")<<std::endl;
  if (p_ev)
    std::cout<<"maxEvent: "<<maxEvt<<std::endl;
  if(p_sample)
    std::cout<<"sample: "<<sample<<std::endl;
  if(p_triplets)
    std::cout<<"running on a triplets-style file"<<std::endl;
  if(p_old)
    std::cout<<"running on an old-style file"<<std::endl;
  if (!verbose){
    /* std::cout<<"\npress enter to continue\n"<<std::endl; */
    /* std::cin.ignore(); */
  }
}

bool ArgInterpreter::Check(int p_input, int p_output, int p_region, int p_proofNw){
  std::cout<<"\n CHECKING INPUT GIVEN \n"<<std::endl;
  //if (p_region)
    //if (doRegion==std::vector<bool>(0,false)){std::cout<<"no regions given to run on!"<<std::endl;return false;}
  /* if (p_region==2){ */
  /*   int n=0; for (auto b:doRegion) if (b)++n; if (n!=1) {std::cout<<"provide exactly 1 region!"<<std::endl; return false;}} */
  /* if (p_region==3 && doAna==false){ */
  /*   std::cout<<"doAna=false, defaulting to region 0 only!"<<std::endl; doRegion=std::vector<bool>(0,false);doRegion[0]=true;} */
  if (p_input)
    if (input==""){std::cout<<"no input given!"<<std::endl;return false;}
  if (p_output){
    if (p_output==2 && output=="") output="outputDir";
    else if (output==""){std::cout<<"no output given!"<<std::endl;return false;}
  }
  if (p_proofNw)
    if (proofNw<0||proofNw>8) {std::cout<<"invalid cut level!"<<std::endl; return false; }
  return true;
}

#endif
