#ifndef MinitreeAnalysis_FourLep_analysis_H
#define MinitreeAnalysis_FourLep_analysis_H

#include "TH1F.h"
#include "TH2F.h"
#include "TreeReader_FourLep.h"
//#include "TreeReader_ZplusX.h"
#include "MinitreeAnalysis/CommonDefs.h"

class FourLep_analysis: public FourLep
{
 public:
  //FourLep_analysis (string inFilename, string outFilename, string treename);
  FourLep_analysis(TChain* chain);
  
  FourLep *treeReader; //!
  //ZplusX *treeReaderBkg;
  
  TFile *fout; //!
    
  //hists
  TH1F* h_mZ1[6]; //!
  TH1F* h_mZ2[6]; //!

  TH1F* h_lepton_pt; //!
  TH1F* h_lepton_eta; //!
  TH1F* h_pt_all_tot; //!
  TH1F* h_eta_all_tot; //!
  TH1F* h_m4l[6]; //!
  TH1F* h_m4l_fsr[6]; //!
  TH1F* h_m4l_const[6]; //!
  TH1F* h_pt[6]; //!
  TH1F* h_y[6]; //!
  TH1F* h_bdt[6]; //!
  TH1F* h_m4l_const_all; //!
  TH1F* h_m4l_fsr_all[2]; //!
  TH1F* h_m4l_fsr_full_all[2]; //!
  TH1F* h_m4l_fsr_full; //!
  TH1F* h_m4l_fsr_pileup_all[2]; //!
  TH1F* h_pt_all[2]; //!
  TH1F* h_pt_all_binned[2]; //!
  TH1F* h_pt_all_binned_tot; //!
  TH1F* h_eta_all[2]; //!
  TH1F* h_y_all[2]; //!
  TH1F* h_mZ1_all[2]; //!
  TH1F* h_mZ2_all[2]; //!
  TH1F* h_cts_all[2]; //!
  TH1F* h_mjj_all[2]; //!
  TH1F* h_detajj_all[2]; //!
  TH1F* h_dphijj_all[2]; //!
  TH1F* h_nbjets_all[2]; //!
  TH1F* h_pt_0jet[2]; //!
  TH1F* h_pt_1jet[2]; //!
  TH1F* h_pt_2jet[2]; //!
  TH1F* h_Z1pT_all[2]; //!
  TH1F* h_HT_all[2]; //!
  TH2F* h_sample_pt4l_all[2]; //!
  TH1F* h_minDR_leading_lep_jet_all[2]; //!
  TH1F* h_minDR_subleading_lep_jet_all[2]; //!

  TH1F* h_njets[2]; //!
  TH1F* h_njets_binned[2]; //!
  TH1F* h_njets_binned_tot; //!
  TH1F* h_dijet_invmass; //!
  TH1F* h_dijet_deltaeta; //!
  TH1F* h_prod_type; //!
  TH1F* h_leading_jet_pt[2]; //!
  TH1F* h_subleading_jet_pt; //!
  TH1F* h_leading_jet_pt_all[2]; //!
  TH1F* h_prod_type_mu; //!
  TH1F* h_prod_type_e; //!

  TH1F* h_leading_jet_pt_var[2][2][2]; //!
  TH1F* h_m4l_fsr_var[2][2][2]; //!
  TH1F* h_pt_var[2][2][2]; //!
  TH1F* h_y_var[2][2][2]; //!
  TH1F* h_mZ1_var[2][2][2]; //!
  TH1F* h_mZ2_var[2][2][2]; //!
  TH1F* h_cts_var[2][2][2]; //!

  TH1F* h_mZ1_highpT[2]; //!
  TH1F* h_mZ2_highpT[2]; //!
  TH1F* h_nbtag70_highpT[2]; //!
  TH1F* h_njets_highpT[2]; //!
  TH1F* h_trackIso_leading_highpT[2]; //!
  TH1F* h_caloIso_leading_highpT[2]; //!
  TH1F* h_d0Sig_leading_highpT[2]; //!
  TH1F* h_trackIso_subleading_highpT[2]; //!
  TH1F* h_caloIso_subleading_highpT[2]; //!
  TH1F* h_d0Sig_subleading_highpT[2]; //!
  TH1F* h_minDR_leading_lep_jet_highpT[2]; //!
  TH1F* h_minDR_subleading_lep_jet_highpT[2]; //!
  //TH1F* h_el_nBL_perMCType[4];
  //TH1F* h_el_eProbHT_perMCType[4];
  //TH1F* h_el_rTRT_perMCType[4];
  //TH1F* h_el_f1_perMCType[4];
  //TH1F* h_el_Rhad_perMCType[4];
  //TH1F* h_mu_sub_d0sig;
  TH1F* h_pt_lep[2][2]; //!
  TH1F* h_eta_lep[2][2]; //!
  TH1F* h_caloIso_lep[2][2]; //!
  TH1F* h_trackIso_lep[2][2]; //!
  TH1F* h_caloIso_comb_lep[2]; //!
  TH1F* h_trackIso_comb_lep[2]; //!
  TH1F* h_d0Sig_lep[2][2]; //!
  TH1F* h_mLeadingZ[2]; //!
  TH1F* h_mLeadingZ_fullRange[2]; //!
  TH1F* h_dRZ1[2]; //!
  TH1F* h_met[2]; //!
  TH1F* h_vtxChi2[4]; //!
  TH1F* h_vtxChi2_FO[4]; //!
  TH1F* h_vtxChi2_IsoD0[4]; //!
  TH1F* h_vtxChi2All; //!
  TH1F* h_vtxChi2All_FO; //!
  TH1F* h_vtxChi2_IsoD0_FO[4]; //!

  void runAnalysis (string outFileName,bool isData,int cut);
  void bookHists(int cut, string outFilename);
  void setupTools();
};

#endif
