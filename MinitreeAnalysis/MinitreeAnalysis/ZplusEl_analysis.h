#ifndef MinitreeAnalysis_ZplusEl_analysis_H
#define MinitreeAnalysis_ZplusEl_analysis_H

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TChain.h"
#include "TreeReader_ZplusEl.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "MinitreeAnalysis/CommonDefs.h"
#include <string>
#include <map>
#include <PATCore/TAccept.h>

class ZplusEl_analysis  : public ZplusEl
{
 private:
  
  //#ifndef __MAKECINT__

  
 public:
  
  // this is a standard constructor
  ZplusEl_analysis(TChain* chain, bool vertex=false, bool Data = false);
    
  void initialize(std::string fname, std::string year);
  void execute(int jentry);
  void finalize();
  void setupTools();

  TH1F* h_ZX_m[2][3];  //!

  TH1F* typeDists_pt[2][3][2][4];  //!
  TH1F* typeTot_pt[2][3][2];  //!
  TH1F* typeDists_eta[2][3][2][4];  //!
  TH1F* typeTot_eta[2][3][2];  //!
  TH1F* typeDists_njet[2][3][2][4];  //!
  TH1F* typeTot_njet[2][3][2];  //!
  TH1F* typeDists_mu[2][3][2][4];  //!
  TH1F* typeTot_mu[2][3][2];  //!


  TH1F* h_Z_m[2][3];  //!
  TH1F* h_Z_m_cons[2];  //!
  TH1F* h_Z_m_cons_llpt[2][5];  //!
  TH1F* h_Z_m_cons_slpt[2][5];  //!
  TH1F* h_Z_m_cons_lleta[2][5];  //!
  TH1F* h_Z_m_cons_sleta[2][5];  //!
  TH1F* h_Z_m_mu[2][2];  //!
  TH1F* h_Z_m_med[2][3];  //!
  TH1F* h_Z_pt[2][3];  //!
  TH1F* h_Z_y[2][3];  //!
  TH1F* h_Z_dR[2][3];  //!
  
  TH1F* h_Z_l1_pt[2][3];  //!
  TH1F* h_Z_l2_pt[2][3];  //!
  TH1F* h_Z_l1_pt_eta[2][5];  //!
  TH1F* h_Z_l2_pt_eta[2][5];  //!
  TH1F* h_Z_l1_pt_slpt[2][5];  //!
  TH1F* h_Z_l1_eta[2][3];  //!
  TH1F* h_Z_l2_eta[2][3];  //!
  TH1F* h_Z_l1_phi[2][3];  //!
  TH1F* h_Z_l2_phi[2][3];  //!

  TH1F* h_Z_pt_hm[2];  //!
  TH1F* h_Z_y_hm[2];  //!
  TH1F* h_Z_dR_hm[2];  //!

  TH1F* h_njets[2]; //!
  TH1F* h_njets_Z[2]; //!
  TH1F* h_njets_Z_hm[2]; //!

  TH1F* h_Z_l1_pt_hm[2];  //!
  TH1F* h_Z_l2_pt_hm[2];  //!
  TH1F* h_Z_l1_eta_hm[2];  //!
  TH1F* h_Z_l2_eta_hm[2];  //!
  TH1F* h_Z_l1_phi_hm[2];  //!
  TH1F* h_Z_l2_phi_hm[2];  //!

  TH1F* h_Z_el_nInnerExpPix; //!
  
  TH1F* h_el_pt[3];  //!
  TH1F* h_el_eta[3];  //!
  TH1F* h_el_phi[3];  //!
  
  TH1F* h_el_passLooseLHnoBL;  //!
  TH1F* h_el_passLooseLH;  //!
  TH1F* h_el_MCClass;  //!

  TH1F* h_el_MCClass_perCR[3][7];  //!

  TH2F* h_el_nInnerExpPix_vs_MCType; //!
  
  TH1F* h_el_nInnerExpPix; //!
  TH1F* h_el_d0Sig[3];  //!
  TH1F* h_el_trackIso[3];  //!
  TH1F* h_el_totalIso[3];  //!
  TH1F* h_el_caloIso[3];  //!

  TH1F* h_el_pt_mu[2];  //!
  TH1F* h_el_eta_mu[2];  //!
  TH1F* h_el_phi_mu[2];  //!
  TH1F* h_el_d0Sig_mu[2];  //!
  TH1F* h_el_trackIso_mu[2];  //!
  TH1F* h_el_caloIso_mu[2];  //!
  TH1F* h_el_nInnerExpPix_mu[2]; //!

  TH1F* h_el_Pass_pt[2][4];  //!
  TH1F* h_el_Fail_pt[2][4];  //! 
  TH1F* h_el_Pass_eta[2][4];  //!
  TH1F* h_el_Fail_eta[2][4];  //!

  TH2F* h_el_Pass_pt_vs_MCType_CR[2][4];  //!
  TH2F* h_el_Fail_pt_vs_MCType_CR[2][4];  //! 
  TH2F* h_el_Pass_eta_vs_MCType_CR[2][4];  //!
  TH2F* h_el_Fail_eta_vs_MCType_CR[2][4];  //! 
  TH2F* h_el_Pass_njet_vs_MCType_CR[2][4];  //!
  TH2F* h_el_Fail_njet_vs_MCType_CR[2][4];  //!
  TH2F* h_el_PassID_pt_vs_MCType[2]; //!
  TH2F* h_el_FailID_pt_vs_MCType[2]; //!
  TH2F* h_el_PassIDFailIso_pt_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Pass_pt_eta_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Fail_pt_eta_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Pass_pt_njet_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Pass_pt_pileup_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Fail_pt_njet_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Fail_pt_pileup_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Pass_pt_njet2_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Fail_pt_njet2_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Pass_pt_cat_vs_MCType_CR[2][4]; //!
  TH3F* h_el_Fail_pt_cat_vs_MCType_CR[2][4]; //!

  TH2F* h_el_passID_passIso_pt_vs_MCType[2]; //!
  TH2F* h_el_passID_failIso_pt_vs_MCType[2]; //!
  TH2F* h_el_Pass_pt_Iso_vs_MCType; //!
  TH2F* h_el_Fail_pt_Iso_vs_MCType; //!
  TH2F* h_el_passIso_passID_pt_vs_MCType[2]; //!
  TH2F* h_el_passIso_failID_pt_vs_MCType[2]; //!

  TH2F* h_el_Pass_AccIBLTRT_pt_vs_MCType;  //!
  TH2F* h_el_Total_AccIBLTRT_pt_vs_MCType;  //!
  TH2F* h_el_Pass_AccIBLTRT_eta_vs_MCType;  //!
  TH2F* h_el_Total_AccIBLTRT_eta_vs_MCType;  //!
  
  TH1F* h_el_Pass_pt_All[2];  //!
  TH1F* h_el_Pass_eta_All[2];  //!
  TH1F* h_el_Fail_pt_All[2];  //!
  TH1F* h_el_Fail_eta_All[2];  //!

  TH1F* h_el_Pass_pt_ID[2]; //!
  TH1F* h_el_Pass_eta_ID[2]; //!
  TH1F* h_el_Pass_pt_Iso[2]; //!
  TH1F* h_el_Pass_eta_Iso[2]; //!
  TH1F* h_el_Pass_pt_D0[2]; //!
  TH1F* h_el_Pass_eta_D0[2]; //!

  TH1F* h_el_FailID_pt[2];  //!
  TH1F* h_el_FailIso_pt[2];  //!
  
  TH1F* h_el_FailD0_pt[2];  //!
  TH1F* h_el_FailIsoD0_pt[2];  //!

  TH1F* h_el_FailID_eta[2];  //!
  TH1F* h_el_FailIso_eta[2];  //!
  TH1F* h_el_FailD0_eta[2];  //!
  TH1F* h_el_FailIsoD0_eta[2];  //!

  TH1F* h_el_ptvc20_vs_njet[5][MCType::nMAX+1]; //!
  TH1F* h_el_ptvc30_vs_njet[5][MCType::nMAX+1]; //!
  TH1F* h_el_tec20_vs_njet[5][MCType::nMAX+1]; //!

  //TH1F* h_nBL_notExp_run; //!
  //TH1F* h_nBL_2_run; //!
  //TH1F* h_nBL_run; //!
  //TH2F* h_el_nBL_notExp_etaPhi; //!
  //TH2F* h_el_nBL_2_etaPhi; //!
  //TH2F* h_el_nBL_3_etaPhi; //!
  //TH2F* h_el_nBL_etaPhi; //!
  //TH1F* h_el_nBL_3_pT; //!
  //TH1F* h_el_nBL_3_p; //!
  //TH1F* h_nBL_notExp_run_lumi; //!
  //TH1F* h_nBL_2_run_lumi; //!

  //TH1F* h_gamma_pass_eProbHTCut; //!
  //TH1F* h_gamma_true_pass_eProbHTCut; //!

  TH1F* h_el_fake_weight; //!
  TH1F* h_el_run_pass; //!
  TH1F* h_lepton_id; //!
  TH1F* h_lepton_id_vs_njet[5][MCType::nMAX+1]; //!
  //TH1F* h_el_d0sig; //!
  TH1F* h_mu; //!

  TFile* outfile; //!
  std::map<int,float> lumiMap; //!
  bool useVertex; //!
  bool use2e2e; //!
  bool use2mu2e; //!
  bool relaxIso; //!
  bool isData; //!

  TString filename; //!
  string m_year; //!
  
  //tools
  //CP::IsolationSelectionTool*       m_elIsolationSelection;  //!
  
  void fillIsoLepHists(strObj leptonObj,float w,float mctype, Root::TAccept &acc, int njets); 

};

#endif
