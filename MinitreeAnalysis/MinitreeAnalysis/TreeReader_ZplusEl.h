#ifndef ZplusEl_h
#define ZplusEl_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <string>
using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class ZplusEl {
public :
   TChain          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   ULong64_t       event;   //!
   Int_t           event_type;   //!
   Int_t           lbn;   //!
   Int_t           run;   //!
   Float_t         mu;   //!
   Float_t         mZ_unconstrained;   //!
   Float_t         mZ_constrained;   //!
   Double_t        weight;   //!
   //Float_t         w_lumi; //!
   //Float_t         w_MCw; //!
   //Float_t         w_pileup; //!
   vector<int>*    el_MCClass;   //!
   vector<int>*    el_nInnerExpPix; //!
   vector<int>*    el_q;   //!
   int             n_jets; //!

   vector<int>*    mu_type;   //!
   vector<float>*  lepton_d0Sig;   //!
   vector<float>*  mu_IDpt;   //!
   vector<float>*  mu_MSpt;   //!
   vector<float>   *lepton_pt; //!
   vector<float>   *lepton_eta; //!
   vector<float>   *lepton_phi; //!
   vector<float>   *lepton_m; //!
   vector<int>*    lepton_charge; //!
   //float   lepton_ptvarcone30[4]; //!
   vector<float>*  lepton_ptvarcone30_tightTTVA; //!
   vector<float>*  lepton_ptcone20_tightTTVA; //!
   vector<float>*  lepton_neflowisol20; //!
   //float   lepton_ptvarcone20[4]; //!
   //float   lepton_topoetcone20[4]; //!
   vector<int>*    lepton_passD0Sig; //!
   vector<int>*    lepton_passIsoCut; //!
   vector<int>*    lepton_id; //!
   //float   lepton_isoWeight[4]; //!
   //double  w_xs; //!

   // List of branches
   TBranch        *b_event;   //!
   TBranch        *b_event_type;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_run;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mZ_unconstrained;   //!
   TBranch        *b_mZ_constrained;   //!
   TBranch        *b_weight;   //!
   //TBranch        *b_w_lumi; //!
   //TBranch        *b_w_MCw; //!
   //TBranch        *b_w_pileup; //!
   TBranch        *b_el_MCClass;   //!
   TBranch        *b_el_nInnerExpPix; //!
   TBranch        *b_el_q;   //!

   TBranch        *b_mu_type;   //!
   TBranch        *b_lepton_d0Sig;   //!
   TBranch        *b_mu_IDpt;   //!
   TBranch        *b_mu_MSpt;   //!
   TBranch        *b_lepton_pt; //!
   TBranch        *b_lepton_eta; //!
   TBranch        *b_lepton_phi; //!
   TBranch        *b_lepton_m; //!
   TBranch        *b_lepton_id; //!
   TBranch        *b_lepton_charge; //!
   //TBranch        *b_lepton_ptvarcone30; //!
   TBranch        *b_lepton_ptvarcone30_tightTTVA; //!
   TBranch        *b_lepton_ptcone20_tightTTVA; //!
   //TBranch        *b_lepton_ptvarcone20; //!
   //TBranch        *b_lepton_topoetcone20; //!
   TBranch        *b_lepton_neflowisol20; //!
   TBranch        *b_lepton_passD0Sig; //!
   TBranch        *b_lepton_passIsoCut; //!
   TBranch        *b_n_jets; //!
   //TBranch        *b_lepton_isoWeight; //!
   //TBranch        *b_w_xs; //!
   TBranch        *b_pileupMu; //!

   ZplusEl(string fname, string tname="tree_incl_all", TChain *tree=0);
   ZplusEl(TChain* tree){Init(tree);}
   ZplusEl(TChain* tree, bool isdata){isData = isdata;Init(tree);}
   ZplusEl(){;}
   virtual ~ZplusEl();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TChain *tree);
   bool isData; //!
};
#endif
