#include <MinitreeAnalysis/ZplusEl_analysis.h>

#include <iostream>
#include <fstream>
#include <iomanip>      // std::setprecision
#include <TGraph.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TLegend.h>
#include <TColor.h>
#include <TMath.h>
#include "TLorentzVector.h"

#define Info(x)     std::cout << "-- <INFO>  ZplusEl_analysis: " << x << std::endl;

namespace CREL{
  enum _CREL {ALL=0,GAMMA,FAKE,HVQ,nMAX};
  string strCREL[(const int)nMAX]={"All","Gamma","Fake","Heavy"};
}

enum ELLH{Loose=0,VeryLoose=1,nELLHMAX};
const int nElLHMax=nELLHMAX;
string strLH[nElLHMax]={"Loose","VeryLoose"};

ZplusEl_analysis::ZplusEl_analysis(TChain* chain, bool vertex, bool Data) : ZplusEl(chain,Data), useVertex(vertex), filename(""),isData(Data){
}

void ZplusEl_analysis::fillIsoLepHists(strObj leptonObj,float w,float mctype, Root::TAccept &acc, int njets)
{

  float pt = leptonObj.pt/1000.;
  float eta = leptonObj.eta;

  bool passD0 = acc.getCutResult("d0Sig");
  bool passIso = acc.getCutResult("Iso");
  
  int iCR=-1;
  bool passID_noBL[nElLHMax];
  bool passID[nElLHMax];

  //---- for efficiencies in (q,f,gamma,...) enriched CRs
    
  if (acc.getCutResult("BL"))     
    iCR=CREL::FAKE;
  else if (!acc.getCutResult("BL") && acc.getCutResult("BL_expected")) 
    iCR=CREL::GAMMA;

  int itype = -1;
  if (mctype==MCType::gamma) itype = 0;
  else if (mctype==MCType::f || mctype==MCType::other) itype = 1;
  else if (mctype==MCType::q)  itype = 2;
  else if (mctype==MCType::e) itype = 3;
  
  int iregion = -1;
  if (iCR==CREL::FAKE) iregion = 0;
  if (iCR==CREL::GAMMA) iregion = 1;
  for(int iLH=0;iLH<nElLHMax;iLH++){ //loop over electron id types
    passID_noBL[iLH] = acc.getCutResult(strLH[iLH]+"ID_noBL");
    if (iregion>=0) {
      typeDists_pt[iLH][0][iregion][itype]->Fill(pt,w);
      typeTot_pt[iLH][0][iregion]->Fill(pt,w);
      typeDists_mu[iLH][0][iregion][itype]->Fill(mu,w);
      typeTot_mu[iLH][0][iregion]->Fill(mu,w);
      typeDists_eta[iLH][0][iregion][itype]->Fill(fabs(eta),w);
      typeTot_eta[iLH][0][iregion]->Fill(fabs(eta),w);
      typeDists_njet[iLH][0][iregion][itype]->Fill(njets,w);
      typeTot_njet[iLH][0][iregion]->Fill(njets,w);
      
      if(passID_noBL[iLH]) {
	typeDists_pt[iLH][2][iregion][itype]->Fill(pt,w);
	typeTot_pt[iLH][2][iregion]->Fill(pt,w);
	typeDists_mu[iLH][2][iregion][itype]->Fill(mu,w);
	typeTot_mu[iLH][2][iregion]->Fill(mu,w);
	typeDists_eta[iLH][2][iregion][itype]->Fill(fabs(eta),w);
	typeTot_eta[iLH][2][iregion]->Fill(fabs(eta),w);
	typeDists_njet[iLH][2][iregion][itype]->Fill(njets,w);
	typeTot_njet[iLH][2][iregion]->Fill(njets,w);
      }
    }
    
    if (passD0 && passIso && passID_noBL[iLH]) {
      h_el_Pass_pt[iLH][CREL::ALL]->Fill(pt, w);
      h_el_Pass_eta[iLH][CREL::ALL]->Fill(eta, w);
	
      if (iCR>=0){
	h_el_Pass_pt[iLH][iCR]->Fill(pt, w);
	h_el_Pass_eta[iLH][iCR]->Fill(eta, w);
	h_el_Pass_pt_vs_MCType_CR[iLH][iCR]->Fill(mctype,pt,w); //needed for purity
	
	if (iregion>=0) {
	  typeDists_pt[iLH][1][iregion][itype]->Fill(pt,w);
	  typeTot_pt[iLH][1][iregion]->Fill(pt,w);
	  typeDists_mu[iLH][1][iregion][itype]->Fill(mu,w);
	  typeTot_mu[iLH][1][iregion]->Fill(mu,w);
	  typeDists_eta[iLH][1][iregion][itype]->Fill(fabs(eta),w);
          typeTot_eta[iLH][1][iregion]->Fill(fabs(eta),w);
	  typeDists_njet[iLH][1][iregion][itype]->Fill(njets,w);
	  typeTot_njet[iLH][1][iregion]->Fill(njets,w);
	}
      }      
    }
    
    if (!(passD0 && passIso && passID_noBL[iLH])) {
      h_el_Fail_pt[iLH][CREL::ALL]->Fill(pt, w);
      h_el_Fail_eta[iLH][CREL::ALL]->Fill(eta, w);
	
      if (iCR>=0){
	h_el_Fail_pt[iLH][iCR]->Fill(pt, w);
	h_el_Fail_eta[iLH][iCR]->Fill(eta, w);
	h_el_Fail_pt_vs_MCType_CR[iLH][iCR]->Fill(mctype,pt,w); //needed for purity		
      }
    }

    passID[iLH] = acc.getCutResult(strLH[iLH]+"ID"); //for electrons, this is the full LooseLH with the BL hit cut, if expected

    //---- for efficiencies of true MCType (q,f,gamma,...)  
    if (passD0 && passIso && passID[iLH]) {
      //if(w!=0 && pt>10 && pt<15 && mctype==MCType::f) std::cout<<"found X electron that passes cuts with weight "<<w<<", pileup weight "<<w_pileup<<" and pt "<<pt<<std::endl;
      h_el_Pass_pt_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,w);
      h_el_Pass_eta_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,fabs(eta),w);
      h_el_Pass_pt_eta_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,fabs(eta),w);
      h_el_Pass_njet_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,njets,w);
      if(iLH==0 && mctype==MCType::gamma && njets==2 && pt>10 && pt<15){
	//cout<<"gamma event passes in bin 2,3 with weight "<<w<<endl;
	if(w>-80) h_el_Pass_pt_njet_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,njets,w);
      }
      else h_el_Pass_pt_njet_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,njets,w);
      h_el_Pass_pt_pileup_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,mu,w);
      if(njets==0) h_el_Pass_pt_njet2_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,0.,w);
      else h_el_Pass_pt_njet2_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,1.,w);
      h_el_run_pass->Fill(run);
    } else {
      h_el_Fail_pt_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,w);
      h_el_Fail_eta_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,fabs(eta),w);
      h_el_Fail_pt_eta_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,fabs(eta),w);
      h_el_Fail_njet_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,njets,w);
      h_el_Fail_pt_pileup_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,mu,w);
      h_el_Fail_pt_njet_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,njets,w);
      if(njets==0) h_el_Fail_pt_njet2_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,0.,w);
      else h_el_Fail_pt_njet2_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,1.,w);
    }
    if(passID[iLH]){
      h_el_Pass_pt_ID[iLH]->Fill(pt, w);
      h_el_Pass_eta_ID[iLH]->Fill(eta, w);
      h_el_PassID_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
      if(!passIso) h_el_PassIDFailIso_pt_vs_MCType_CR[iLH][CREL::ALL]->Fill(mctype,pt,w);
      if(passIso) h_el_passID_passIso_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
      else h_el_passID_failIso_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
    }
    else h_el_FailID_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
    if(passIso){
      h_el_Pass_pt_Iso[iLH]->Fill(pt, w);
      if(iLH==0) h_el_Pass_pt_Iso_vs_MCType->Fill(mctype,pt, w);
      h_el_Pass_eta_Iso[iLH]->Fill(eta, w);
      if(passID[iLH]) h_el_passIso_passID_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
      else h_el_passIso_failID_pt_vs_MCType[iLH]->Fill(mctype,pt,w);
    }
    else{
      if(iLH==0) h_el_Fail_pt_Iso_vs_MCType->Fill(mctype,pt,w);
    }
    if(passD0){
      h_el_Pass_pt_D0[iLH]->Fill(pt, w);
      h_el_Pass_eta_D0[iLH]->Fill(eta, w);
    }
    if (passD0 && passIso && passID[iLH]) {      
      h_el_Pass_pt_All[iLH]->Fill(pt, w);
      h_el_Pass_eta_All[iLH]->Fill(eta, w);
    }
    else if (passD0 && passIso && !passID[iLH]) {
      h_el_FailID_pt[iLH]->Fill(pt, w);
      h_el_FailID_eta[iLH]->Fill(eta, w);
    }
    else if (passD0 && !passIso && passID[iLH]) {
      h_el_FailIso_pt[iLH]->Fill(pt, w);
      h_el_FailIso_eta[iLH]->Fill(eta, w);
    }
    else if (!passD0 && passIso && passID[iLH]) {
      h_el_FailD0_pt[iLH]->Fill(pt, w);
      h_el_FailD0_eta[iLH]->Fill(eta, w);
    }
    if(!passD0 || !passIso || !passID[iLH]){
      h_el_Fail_pt_All[iLH]->Fill(pt, w);
      h_el_Fail_eta_All[iLH]->Fill(eta, w);
    }
  }

}

void ZplusEl_analysis::finalize ()
{
  outfile->Write();
  outfile->Close();
}

void ZplusEl_analysis::execute(int jentry)
{
  //std::cout<<"execute "<<jentry<<std::endl;
  TString tempname(fChain->GetCurrentFile()->GetName());
  //std::cout<<tempname<<std::endl;
  GetEntry(jentry);
  if(filename.CompareTo(tempname)!=0){
    std::cout<<"old file:"<<filename<<"; new file "<<tempname<<std::endl;
    filename=tempname;
  }
  //std::cout<<"new entry, run/event="<<run<<"/"<<event<<std::endl;

  if(use2e2e && event_type!=1) return;
  if(use2mu2e && event_type!=2) return;
  //if(lepton_pt->at(2)>10 && lepton_pt->at(2)<70 && n_jets==3 && weight<-10) cout<<"negative weight "<<weight<<endl;
  //else return;

  float w=1;
  if(fChain->GetBranch("weight")) w = weight; //central production doesn't have weight branch for data
  //if(w<-80) cout<<"event with very low weight "<<w<<endl;
  /*
  if(run<364510 && run>364499 && w_xs==-1){
    if(run==364500) w*=-57619;
    else if(run==364501) w*=-34590;
    else if(run==364502) w*=-6285.6;
    else if(run==364503) w*=-491.86;
    else if(run==364504) w*=-62.987;
    else if(run==364505) w*=-57701;
    else if(run==364506) w*=-34588;
    else if(run==364507) w*=-6285.3;
    else if(run==364508) w*=-493.92;
    else if(run==364509) w*=-63.08;
  }
  if(run==410289 && m_year.compare("2018")==0){ //while mc16e 4l-filtered ttbar isn't ready yet
    w*=59.9/44.3;
  }
  */
  int nInnerCR=2;
  if(el_nInnerExpPix->at(2)==0) nInnerCR=1;
  int evType=-1;
  if(event_type==11 || event_type==10) evType=0;
  else if(event_type==13 || event_type==12) evType=1;
  //else std::cout<<"bad event type "<<event_type<<std::endl;
  int iZ = (evType==Zee)? Zee: Zmumu;

  TLorentzVector Zleptons[2];
  for (int i=0;i<2;i++) {
    Zleptons[i].SetPtEtaPhiM(lepton_pt->at(i),lepton_eta->at(i),lepton_phi->at(i),lepton_m->at(i));
  }

  //check order of leptons; first one is the leading-pt lepton
  //int leadingInd=0; //for other variables
  if (Zleptons[0].Pt()<Zleptons[1].Pt()) { 
    TLorentzVector tmp = Zleptons[0];
    Zleptons[0] = Zleptons[1];
    Zleptons[1] = tmp;
    //leadingInd=1;
  }

  float wEl = w ;//* el_weight[i]; //don't use electon SF

  if(!lepton_passD0Sig->at(1) || !lepton_passD0Sig->at(0)) return;
  if(!relaxIso){
    if(!lepton_passIsoCut->at(0) || !lepton_passIsoCut->at(1)) return;
    /*
    bool pass=true;
    for(int ilep=0;ilep<3;ilep++){
      if(ilep<2){
	if(lepton_ptvarcone30_tightTTVA[ilep]/lepton_pt->at(ilep)>0.15){ pass=false; break;}
	if(event_type==11 && lepton_topoetcone20[ilep]>0.3) {pass=false; break;}
	if(event_type==13 && lepton_topoetcone20[ilep]>0.2) {pass=false; break;}
      }
      for(int jlep=ilep+1;jlep<3;jlep++){
	if(sqrt((lepton_eta->at(ilep)-lepton_eta->at(jlep))*(lepton_eta->at(ilep)-lepton_eta->at(jlep))+(lepton_phi->at(ilep)-lepton_phi->at(jlep))*(lepton_phi->at(ilep)-lepton_phi->at(jlep)))<0.3) {pass=false; break;}
      }
      if(!pass) break;
    }
    if(!pass) return;
    */
  }
  //else{
  //  if(fChain->GetBranch("lepton_iso_weight")) w=w/(lepton_isoWeight[0]*lepton_isoWeight[1]);
  //}
  //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.30 || lepton_ptvarcone20[1] > 0.30 || lepton_topoetcone20[0] > 0.40 || lepton_topoetcone20[1] > 0.40)) return; //!)
  //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.30 || lepton_ptvarcone30[1] > 0.30 || lepton_topoetcone20[0] > 0.60 || lepton_topoetcone20[1] > 0.60)) return; //!)
  //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.30 || lepton_ptvarcone20[1] > 0.30 )) return; //!)
  //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.30 || lepton_ptvarcone30[1] > 0.30 )) return; //!) 

  h_el_d0Sig[0]->Fill(fabs(lepton_d0Sig->at(2)),wEl);
  h_el_d0Sig[nInnerCR]->Fill(fabs(lepton_d0Sig->at(2)),wEl);
  //hists for Z
  //std::cout<<"z of type "<<iZ<<"; event type is "<<evType<<std::endl;
  h_Z_m[iZ][0]->Fill(mZ_unconstrained, w);
  h_Z_m_cons[iZ]->Fill(mZ_constrained, w);
  int llptcut[6]={20,40,60,80,100,100000};
  int slptcut[6]={15,30,45,60,90,100000};
  float absetacut[6]={0,0.5,1,1.5,2,2.5};
  for(int ibin=0;ibin<5;ibin++){
    if(Zleptons[0].Pt()>llptcut[ibin] && Zleptons[0].Pt()<=llptcut[ibin+1]) h_Z_m_cons_llpt[iZ][ibin]->Fill(mZ_constrained, w);
    if(Zleptons[1].Pt()>slptcut[ibin] && Zleptons[1].Pt()<=slptcut[ibin+1]){
      h_Z_m_cons_slpt[iZ][ibin]->Fill(mZ_constrained, w);
      h_Z_l1_pt_slpt[iZ][ibin]->Fill(Zleptons[0].Pt(),w);
    }
    if(abs(Zleptons[0].Eta())>absetacut[ibin] && abs(Zleptons[0].Eta())<=absetacut[ibin+1]){
      h_Z_m_cons_lleta[iZ][ibin]->Fill(mZ_constrained, w);
      h_Z_l1_pt_eta[iZ][ibin]->Fill(Zleptons[0].Pt(),w);
    }
    if(abs(Zleptons[1].Eta())>absetacut[ibin] && abs(Zleptons[1].Eta())<=absetacut[ibin+1]){
      h_Z_m_cons_sleta[iZ][ibin]->Fill(mZ_constrained, w);
      h_Z_l2_pt_eta[iZ][ibin]->Fill(Zleptons[1].Pt(),w);
    }
  }
  if((lepton_id->at(0)==0 || /*lepton_id->at(0)==9 ||*/ lepton_id->at(0)==10) || (lepton_id->at(1)==0 || /*lepton_id->at(1)==9 ||*/ lepton_id->at(1)==10)) h_Z_m_med[iZ][0]->Fill(mZ_unconstrained, w);
  if(mu>40){
    h_Z_m_mu[iZ][1]->Fill(mZ_unconstrained, w);
    h_el_d0Sig_mu[1]->Fill(fabs(lepton_d0Sig->at(2)),wEl);
  }
  else{
    h_Z_m_mu[iZ][0]->Fill(mZ_unconstrained, w);
    h_el_d0Sig_mu[0]->Fill(fabs(lepton_d0Sig->at(2)),wEl);
  }
  //h_ZX_m[1]->Fill(mZ_unconstrained,w);

  //std::cout<<"filled z mass hist"<<std::endl;
  //std::cout<<"there are "<<mu_pt->size()<<" muons and "<<el_pt->size()<<" electrons"<<std::endl;

  //std::cout<<"fill z hists"<<std::endl; 
 
  TLorentzVector Z = Zleptons[0] + Zleptons[1];
  h_Z_pt[iZ][0]->Fill(Z.Pt(), w);
  h_Z_y[iZ][0]->Fill(Z.Rapidity(), w);
  h_Z_dR[iZ][0]->Fill(Zleptons[0].DeltaR(Zleptons[1]), w);

  h_Z_l1_pt[iZ][0]->Fill( Zleptons[0].Pt(), w );
  h_Z_l2_pt[iZ][0]->Fill( Zleptons[1].Pt(), w );
  h_Z_l1_eta[iZ][0]->Fill( Zleptons[0].Eta(), w );
  h_Z_l2_eta[iZ][0]->Fill( Zleptons[1].Eta(), w );
  h_Z_l1_phi[iZ][0]->Fill( Zleptons[0].Phi(), w );
  h_Z_l2_phi[iZ][0]->Fill( Zleptons[1].Phi(), w );

  h_Z_m[iZ][nInnerCR]->Fill(mZ_unconstrained, w);
  if((lepton_id->at(0)==0 || /*lepton_id->at(0)==9 ||*/ lepton_id->at(0)==10) && (lepton_id->at(1)==0 || /*lepton_id->at(1)==9 ||*/ lepton_id->at(1)==10)) h_Z_m_med[iZ][nInnerCR]->Fill(mZ_unconstrained, w);
  h_Z_pt[iZ][nInnerCR]->Fill(Z.Pt(), w);
  h_Z_y[iZ][nInnerCR]->Fill(Z.Rapidity(), w);
  h_Z_dR[iZ][nInnerCR]->Fill(Zleptons[0].DeltaR(Zleptons[1]), w);
  h_Z_l1_pt[iZ][nInnerCR]->Fill( Zleptons[0].Pt(), w );
  h_Z_l2_pt[iZ][nInnerCR]->Fill( Zleptons[1].Pt(), w );
  h_Z_l1_eta[iZ][nInnerCR]->Fill( Zleptons[0].Eta(), w );
  h_Z_l2_eta[iZ][nInnerCR]->Fill( Zleptons[1].Eta(), w );
  h_Z_l1_phi[iZ][nInnerCR]->Fill( Zleptons[0].Phi(), w );
  h_Z_l2_phi[iZ][nInnerCR]->Fill( Zleptons[1].Phi(), w );

  h_njets_Z[iZ]->Fill(n_jets,w);

  if(mZ_unconstrained>100){ //high-mass tail
    h_Z_pt_hm[iZ]->Fill(Z.Pt(), w);
    h_Z_y_hm[iZ]->Fill(Z.Rapidity(), w);
    h_Z_dR_hm[iZ]->Fill(Zleptons[0].DeltaR(Zleptons[1]), w);

    h_Z_l1_pt_hm[iZ]->Fill( Zleptons[0].Pt(), w );
    h_Z_l2_pt_hm[iZ]->Fill( Zleptons[1].Pt(), w );
    h_Z_l1_eta_hm[iZ]->Fill( Zleptons[0].Eta(), w );
    h_Z_l2_eta_hm[iZ]->Fill( Zleptons[1].Eta(), w );
    h_Z_l1_phi_hm[iZ]->Fill( Zleptons[0].Phi(), w );
    h_Z_l2_phi_hm[iZ]->Fill( Zleptons[1].Phi(), w );

    h_njets_Z_hm[iZ]->Fill(n_jets,w);

  }

  if(useVertex && !lepton_passD0Sig->at(2)){/*std::cout<<"reject for bad d0 sig"<<endl;*/ return;} //if we apply vertex cut, apply d0 sig to X electrons

  h_mu->Fill(mu,w);
  //if(fChain->GetBranch("trigDecisionOK")){
  //if(!trigDecisionOK) return;
  //}
  //if(fChain->GetBranch("random_run")){
    //if(random_run>=296939) return;
  //}
  int m_el_MCClass = isData ? -999: el_MCClass->at(2);
  if(m_el_MCClass==5) h_el_fake_weight->Fill(weight);
  h_lepton_id->Fill(lepton_id->at(2),w);
  //h_el_d0sig->Fill(lepton_d0Sig->at(2),w);
  //if(w_pileup>4) return; //exclude highly-weighted events that give nonsense numbers
  //if(w==1) w=3;
  //h_npv->Fill(npv,w);
  //h_avgmu->Fill(avgMu,w);

  //if(weight!=1) std::cout<<"weight from tree is "<<weight<<std::endl;
  //if(w!=1) std::cout<<"w="<<w<<std::endl;
  //if(wEl!=1) std::cout<<"wEl="<<w<<std::endl;
  
  //std::cout<<"; event type is "<<event_type<<"run: "<<run<<std::endl;//"; avgMu: "<<avgMu<<std::endl;
  float lepton_trackIso[3];
  float lepton_totalIso[3];
  for(int i=0;i<3;i++){
    if(lepton_ptcone20_tightTTVA->at(i)>lepton_ptvarcone30_tightTTVA->at(i)) lepton_trackIso[i]=lepton_ptcone20_tightTTVA->at(i);
    else lepton_trackIso[i]=lepton_ptvarcone30_tightTTVA->at(i);
    lepton_totalIso[i]=(lepton_trackIso[i]+0.4*lepton_neflowisol20->at(i));
  }

  int e_nInnerExpPix[3];
  for(int i=0;i<3;i++){
    if(el_nInnerExpPix->at(i)==-999) e_nInnerExpPix[i]=-1;
    else e_nInnerExpPix[i]=el_nInnerExpPix->at(i);
  }
  if (iZ==Zee) {
    h_Z_el_nInnerExpPix->Fill( e_nInnerExpPix[0], w );
    h_Z_el_nInnerExpPix->Fill( e_nInnerExpPix[1], w );
  }
		   
  h_el_pt[0]->Fill( lepton_pt->at(2), wEl );
  h_el_eta[0]->Fill( lepton_eta->at(2), wEl );
  h_el_phi[0]->Fill( lepton_phi->at(2), wEl );
  h_el_pt[nInnerCR]->Fill( lepton_pt->at(2), wEl );
  h_el_eta[nInnerCR]->Fill( lepton_eta->at(2), wEl );
  h_el_phi[nInnerCR]->Fill( lepton_phi->at(2), wEl );
  if(mu>40){
    h_el_pt_mu[1]->Fill( lepton_pt->at(2), wEl );
    h_el_eta_mu[1]->Fill( lepton_eta->at(2), wEl );
    h_el_phi_mu[1]->Fill( lepton_phi->at(2), wEl );
    //h_el_trackIso_mu[1]->Fill(lepton_ptvarcone20[2]/(lepton_pt->at(2)),wEl);
    h_el_trackIso_mu[1]->Fill(lepton_trackIso[2],wEl);
    //h_el_caloIso_mu[1]->Fill(lepton_topoetcone20[2]/(lepton_pt->at(2)),wEl);
    h_el_caloIso_mu[1]->Fill(lepton_neflowisol20->at(2),wEl);
    h_el_nInnerExpPix_mu[1]->Fill(e_nInnerExpPix[2],wEl);
  }
  else{
    h_el_pt_mu[0]->Fill( lepton_pt->at(2), wEl );
    h_el_eta_mu[0]->Fill( lepton_eta->at(2), wEl );
    h_el_phi_mu[0]->Fill( lepton_phi->at(2), wEl );
    //h_el_trackIso_mu[0]->Fill(lepton_ptvarcone20[2]/(lepton_pt->at(2)),wEl);
    //h_el_caloIso_mu[0]->Fill(lepton_topoetcone20[2]/(lepton_pt->at(2)),wEl);
    h_el_trackIso_mu[0]->Fill(lepton_trackIso[2],wEl);
    h_el_caloIso_mu[0]->Fill(lepton_neflowisol20->at(2),wEl);
    h_el_nInnerExpPix_mu[0]->Fill(e_nInnerExpPix[2],wEl);
  }
  /*
  h_el_trackIso[0]->Fill(lepton_ptvarcone20[2]/(lepton_pt->at(2)),wEl);
  h_el_caloIso[0]->Fill(lepton_topoetcone20[2]/(lepton_pt->at(2)),wEl);
  h_el_trackIso[nInnerCR]->Fill(lepton_ptvarcone20[2]/(lepton_pt->at(2)),wEl);
  h_el_caloIso[nInnerCR]->Fill(lepton_topoetcone20[2]/(lepton_pt->at(2)),wEl);
  */
  h_el_trackIso[0]->Fill(lepton_trackIso[2],wEl);
  h_el_caloIso[0]->Fill(lepton_neflowisol20->at(2),wEl);
  h_el_totalIso[0]->Fill(lepton_totalIso[2],wEl);
  h_el_trackIso[nInnerCR]->Fill(lepton_trackIso[2],wEl);
  h_el_caloIso[nInnerCR]->Fill(lepton_neflowisol20->at(2),wEl);
  h_el_totalIso[nInnerCR]->Fill(lepton_totalIso[2],wEl);
  h_el_nInnerExpPix->Fill(e_nInnerExpPix[2],wEl);
  h_el_MCClass->Fill(m_el_MCClass,wEl);
  int lepton_qual;
  if(lepton_id->at(2)>=7 && lepton_id->at(2)<11) lepton_qual=0; //7=loose_noBL, 8=loose, 9=medium, 10=tight
  else if(lepton_id->at(2)>=6 && lepton_id->at(2)<11) lepton_qual=1;
  else lepton_qual=2;
  h_el_passLooseLHnoBL->Fill(lepton_qual,wEl);
    
  if((e_nInnerExpPix[2]>0 || e_nInnerExpPix[2]==-1)) h_el_passLooseLH->Fill( lepton_qual, wEl );

  //std::cout<<"filled basic hists"<<std::endl;

  if (e_nInnerExpPix[2]>=0) {
    int npix=e_nInnerExpPix[2];
    int mctype=getMCType(m_el_MCClass);
    h_el_nInnerExpPix_vs_MCType->Fill(mctype,npix,wEl);
  }
     
  Root::TAccept elCutResults("elCutResults");
  elCutResults.addCut("Iso","track and calo isolation");
  elCutResults.addCut("d0Sig","d0 significance");
  elCutResults.addCut("LooseID","passes electron ID cut, loose LH");
  elCutResults.addCut("LooseID_noBL","passes loose LH electron ID cut, no B-layer hit cut");
  elCutResults.addCut("VeryLooseID","passes VeryLoose electron ID cut, loose LH");
  elCutResults.addCut("VeryLooseID_noBL","passes VeryLoose LH electron ID cut, no B-layer hit cut");
  elCutResults.addCut("BL","has B-layer hits");
  elCutResults.addCut("BL_expected","B-layer hit expected");
    
  //std::cout<<"get electron cut results"<<std::endl;

  float pt, eta;
  strObj electronObj;	
  electronObj.isolationValues.resize(xAOD::Iso::numIsolationTypes); 
  pt = lepton_pt->at(2)*1000;
  eta = lepton_eta->at(2);
  electronObj.pt = pt;
  electronObj.eta = eta;
      
  //electronObj.isolationValues[xAOD::Iso::ptvarcone20]=lepton_ptvarcone20[2];
  //electronObj.isolationValues[xAOD::Iso::topoetcone20]=lepton_topoetcone20[2];

  int njet=n_jets;
  TLorentzVector xvec;
  xvec.SetPtEtaPhiM(lepton_pt->at(2),lepton_eta->at(2),lepton_phi->at(2),lepton_m->at(2));
  TLorentzVector jetvec;
  if(njet>3) njet=3;
  //cout<<"fill histograms for njet="<<njet<<" and mctype="<<getMCType(el_MCClass[2])<<endl;
  h_el_ptvc20_vs_njet[njet][0]->Fill(lepton_ptcone20_tightTTVA->at(2),w);
  h_el_ptvc30_vs_njet[njet][0]->Fill(lepton_ptvarcone30_tightTTVA->at(2),w);
  h_el_tec20_vs_njet[njet][0]->Fill(lepton_neflowisol20->at(2),w);
  h_lepton_id_vs_njet[njet][0]->Fill(lepton_id->at(2),w);
  h_el_ptvc20_vs_njet[njet][getMCType(m_el_MCClass)+1]->Fill(lepton_ptcone20_tightTTVA->at(2),w);
  h_el_ptvc30_vs_njet[njet][getMCType(m_el_MCClass)+1]->Fill(lepton_ptvarcone30_tightTTVA->at(2),w);
  h_el_tec20_vs_njet[njet][getMCType(m_el_MCClass)+1]->Fill(lepton_neflowisol20->at(2),w);
  h_lepton_id_vs_njet[njet][getMCType(m_el_MCClass)+1]->Fill(lepton_id->at(2),w);
  //if(lepton_ptvarcone30_tightTTVA[2]>0.35 && lepton_ptvarcone30_tightTTVA[2]<0.4 && njet==3){
    //cout<<"ptvarcone30_TightTTVA="<<lepton_ptvarcone30_tightTTVA[2]<<", X pt="<<lepton_pt->at(2)<<", event weight="<<w<<", sample="<<run<<", event="<<event<<endl;
  //}

  elCutResults.clear();
  if(lepton_passIsoCut->at(2)) elCutResults.setCutResult("Iso",true);
  else elCutResults.setCutResult("Iso",false);
  if(lepton_passD0Sig->at(2)) elCutResults.setCutResult("d0Sig",true);
  else elCutResults.setCutResult("d0Sig",false); 

  elCutResults.setCutResult("LooseID_noBL", (lepton_qual==0));
  elCutResults.setCutResult("VeryLooseID_noBL", (lepton_qual<2)); 
  elCutResults.setCutResult("BL", (e_nInnerExpPix[2]>0));
  elCutResults.setCutResult("BL_expected", (e_nInnerExpPix[2]>=0));
  elCutResults.setCutResult("LooseID", (lepton_qual==0 && (e_nInnerExpPix[2]>0 || e_nInnerExpPix[2]<0))); 
  elCutResults.setCutResult("VeryLooseID", (lepton_qual<2 && (e_nInnerExpPix[2]>0 || e_nInnerExpPix[2]<0)));

  fillIsoLepHists(electronObj, wEl, getMCType(m_el_MCClass), elCutResults, njet);
}


 
void ZplusEl_analysis::setupTools ()
{}

void ZplusEl_analysis::initialize (string fname, string year)
{
  Info("ZplusEl_analysis::Initialize()");

  m_year=year;
  std::string line;
  std::ifstream lumifile ("lumi.txt");
  if (lumifile.is_open()){
    while(std::getline(lumifile,line)){
      std::string run=line.substr(0,6);
      std::string lumi=line.substr(6,line.length()-6);
      lumiMap.insert(std::pair<int,float>(std::stoi(run),std::stof(lumi)));
      //std::cout<<"map at "<<run<<" is "<<lumiMap[std::stoi(run)]<<std::endl;
    }
    lumifile.close();
  }
  use2e2e=false;
  if(fname.find("2e2e")!=string::npos) use2e2e=true;
  use2mu2e=false;
  if(fname.find("2mu2e")!=string::npos) use2mu2e=true;

  relaxIso=false;
  
  TH1::SetDefaultSumw2(true);
  double ptBins[4]={7,10,15,70};
  double etaBins[4]={0,0.7,1.4,2.4};

  outfile=new TFile(Form("%s/hists_%s.root",fname.c_str(),year.c_str()),"RECREATE");

  for(int m=0;m<nElLHMax;m++){ //Loose/VeryLoose
    for(int i=0;i<3;i++){ //no cuts/cuts/relaxed cuts
      for(int j=0;j<2;j++){ //light/gamma
	typeTot_pt[m][i][j]=new TH1F(Form("typeTot_pt_%i_%i_%i",m,i,j),"",3,ptBins);
	typeTot_eta[m][i][j]=new TH1F(Form("typeTot_eta_%i_%i_%i",m,i,j),"",3,etaBins);
	typeTot_njet[m][i][j]=new TH1F(Form("typeTot_njet_%i_%i_%i",m,i,j),"",4,0,4);
	typeTot_mu[m][i][j]=new TH1F(Form("typeTot_mu_%i_%i_%i",m,i,j),"",7,0,70);
	for(int k=0;k<4;k++){ //MC type
	  typeDists_pt[m][i][j][k]=new TH1F(Form("typeDists_pt_%i_%i_%i_%i",m,i,j,k),"",3,ptBins);
	  typeDists_eta[m][i][j][k]=new TH1F(Form("typeDists_eta_%i_%i_%i_%i",m,i,j,k),"",3,etaBins);
	  typeDists_njet[m][i][j][k]=new TH1F(Form("typeDists_njet_%i_%i_%i_%i",m,i,j,k),"",4,0,4);
	  typeDists_mu[m][i][j][k]=new TH1F(Form("typeDists_mu_%i_%i_%i_%i",m,i,j,k),"",7,0,70);
	}
      }
    }
  }

  string Zchan[2]= {"mumu","ee"};
  string lepFlav[2]= {"mu","el"};
  string crStr[3]={"","_nInner0","_nInner1"};
  string muStr[2]={"lomu","himu"};

  for(int i=0;i<3;i++){
    h_el_pt[i] = new TH1F(Form("h_el_pt%s",crStr[i].c_str()),";electron p_{T} [GeV];Entries", 200, 0, 200 );
    h_el_eta[i] = new TH1F(Form("h_el_eta%s",crStr[i].c_str()),";electron #eta;Entries", 54, -2.7, 2.7 );
    h_el_phi[i] = new TH1F(Form("h_el_phi%s",crStr[i].c_str()),";electron #phi;Entries", 63, -3.15, 3.15 );
    h_el_d0Sig[i] = new TH1F(Form("h_el_d0Sig%s",crStr[i].c_str()),";|d_{0}|/#sigma(d_{0});Entries",25,0,10);
    h_el_caloIso[i] = new TH1F(Form("h_el_caloIso%s",crStr[i].c_str()),";#sum E_{T}^{Calo}/p_{T};Entries",53,-0.6,10.);
    h_el_trackIso[i] = new TH1F(Form("h_el_trackIso%s",crStr[i].c_str()),";#sum p_{T}^{ID}/p_{T};Entries",70,0,10.5);
    h_el_totalIso[i] = new TH1F(Form("h_el_totalIso%s",crStr[i].c_str()),";#sum p_{T}^{ID}/p_{T};Entries",70,0,10.5);
  }
  for(int i=0;i<2;i++){
    h_el_pt_mu[i] = new TH1F(Form("h_el_pt_%s",muStr[i].c_str()),";electron p_{T} [GeV];Entries", 100, 0, 200 );
    h_el_eta_mu[i] = new TH1F(Form("h_el_eta_%s",muStr[i].c_str()),";electron #eta;Entries", 54, -2.7, 2.7 );
    h_el_phi_mu[i] = new TH1F(Form("h_el_phi_%s",muStr[i].c_str()),";electron #phi;Entries", 63, -3.15, 3.15 );
    h_el_d0Sig_mu[i] = new TH1F(Form("h_el_d0Sig_%s",muStr[i].c_str()),";|d_{0}|/#sigma(d_{0});Entries",25,0,10);
    h_el_caloIso_mu[i] = new TH1F(Form("h_el_caloIso_%s",muStr[i].c_str()),";#sum E_{T}^{Calo}/p_{T};Entries",53,-0.6,10.);
    h_el_trackIso_mu[i] = new TH1F(Form("h_el_trackIso_%s",muStr[i].c_str()),";#sum p_{T}^{ID}/p_{T};Entries",70,0,10.5);
    h_el_nInnerExpPix_mu[i] = new TH1F(Form("h_el_nInnerExpPix_%s",muStr[i].c_str()),";number of inner pixel hits;Entries",5,-1.5,3.5);
  }
  h_el_nInnerExpPix = new TH1F("h_el_nInnerExpPix",";number of inner pixel hits;Entries",4,-0.5,3.5);
  h_mu=new TH1F("h_mu",";#mu",7,0,70);

  for(int iLH=0;iLH<nElLHMax;iLH++){
    h_el_Pass_pt_All[iLH] = new TH1F(Form("h_el_%s_Pass_pt_All",strLH[iLH].c_str()),";lepton p_{T} [GeV];Entries",100, 0, 100);
    h_el_Pass_eta_All[iLH] = new TH1F(Form("h_el_%s_Pass_eta_All",strLH[iLH].c_str()),";lepton #eta;Entries",27, -2.7,2.7);

    h_el_Pass_pt_ID[iLH] = new TH1F(Form("h_el_%s_Pass_pt_ID",strLH[iLH].c_str()),";lepton p_{T} [GeV];Entries",100, 0, 100);
    h_el_Pass_eta_ID[iLH] = new TH1F(Form("h_el_%s_Pass_eta_ID",strLH[iLH].c_str()),";lepton #eta;Entries",27, -2.7,2.7);
    h_el_Pass_pt_Iso[iLH] = new TH1F(Form("h_el_%s_Pass_pt_Iso",strLH[iLH].c_str()),";lepton p_{T} [GeV];Entries",100, 0, 100);
    h_el_Pass_eta_Iso[iLH] = new TH1F(Form("h_el_%s_Pass_eta_Iso",strLH[iLH].c_str()),";lepton #eta;Entries",27, -2.7,2.7);
    h_el_Pass_pt_D0[iLH] = new TH1F(Form("h_el_%s_Pass_pt_D0",strLH[iLH].c_str()),";lepton p_{T} [GeV];Entries",100, 0, 100);
    h_el_Pass_eta_D0[iLH] = new TH1F(Form("h_el_%s_Pass_eta_D0",strLH[iLH].c_str()),";lepton #eta;Entries",27, -2.7,2.7);
    
    h_el_Fail_pt_All[iLH] = (TH1F*) h_el_Pass_pt_All[iLH]->Clone(Form("h_el_%s_Fail_pt",strLH[iLH].c_str()));
    h_el_FailID_pt[iLH] = (TH1F*) h_el_Pass_pt_All[iLH]->Clone(Form("h_el_%s_FailID_pt",strLH[iLH].c_str()));
    h_el_FailIso_pt[iLH] = (TH1F*) h_el_Pass_pt_All[iLH]->Clone(Form("h_el_%s_FailIso_pt",strLH[iLH].c_str()));
    h_el_FailD0_pt[iLH] = (TH1F*) h_el_Pass_pt_All[iLH]->Clone(Form("h_el_%s_FailD0_pt",strLH[iLH].c_str()));
    
    h_el_Fail_eta_All[iLH] = (TH1F*) h_el_Pass_eta_All[iLH]->Clone(Form("h_el_%s_Fail_eta",strLH[iLH].c_str()));
    h_el_FailID_eta[iLH] = (TH1F*) h_el_Pass_eta_All[iLH]->Clone(Form("h_el_%s_FailID_eta",strLH[iLH].c_str()));
    h_el_FailIso_eta[iLH] = (TH1F*) h_el_Pass_eta_All[iLH]->Clone(Form("h_el_%s_FailIso_eta",strLH[iLH].c_str()));
    h_el_FailD0_eta[iLH] = (TH1F*) h_el_Pass_eta_All[iLH]->Clone(Form("h_el_%s_FailD0_eta",strLH[iLH].c_str()));
  }

  for (int i=0;i<2;i++) {
    h_Z_pt_hm[i] = new TH1F( Form("h_Z%s_pt_hm",Zchan[i].c_str()),";Z p_{T} [GeV];Entries", 50, 0, 500 );
    h_Z_y_hm[i] = new TH1F( Form("h_Z%s_y_hm",Zchan[i].c_str()),";Z rapidity;Entries", 30, -3., 3. );
    h_Z_dR_hm[i] = new TH1F( Form("h_Z%s_dR_hm",Zchan[i].c_str()),";Z #Delta R(ll);Entries", 32, 0, 6.4 );

    h_Z_l1_pt_hm[i] = new TH1F( Form("h_Z%s_leadingLep_pt_hm",Zchan[i].c_str()),";Z leading lepton p_{T} [GeV];Entries", 100, 0, 200 );
    h_Z_l2_pt_hm[i] = new TH1F( Form("h_Z%s_subleadingLep_pt_hm",Zchan[i].c_str()),";Z subleading lepton p_{T} [GeV];Entries", 75, 0, 150 );

    h_Z_l1_eta_hm[i] = new TH1F( Form("h_Z%s_leadingLep_eta_hm",Zchan[i].c_str()),";Z leading lepton #eta;Entries", 54, -2.7, 2.7 );
    h_Z_l2_eta_hm[i] = new TH1F( Form("h_Z%s_subleadingLep_eta_hm",Zchan[i].c_str()),";Z subleading lepton #eta;Entries", 54, -2.7, 2.7 );

    h_Z_l1_phi_hm[i] = new TH1F( Form("h_Z%s_leadingLep_phi_hm",Zchan[i].c_str()),";Z leading lepton #phi;Entries", 63, -3.15, 3.15 );
    h_Z_l2_phi_hm[i] = new TH1F( Form("h_Z%s_subleadingLep_phi_hm",Zchan[i].c_str()),";Z subleading lepton #phi;Entries", 63, -3.15, 3.15 );

    h_njets[i]=new TH1F(Form("h_Z%s_njets",Zchan[i].c_str()),"; N_{jets}; Entries",10,0,10);
    h_njets_Z[i]=new TH1F(Form("h_Z%s_njets_Z",Zchan[i].c_str()),"; N_{jets}; Entries",10,0,10);
    h_njets_Z_hm[i]=new TH1F(Form("h_Z%s_njets_Z_hm",Zchan[i].c_str()),"; N_{jets}; Entries",10,0,10);

    h_Z_m_cons[i] = new TH1F( Form("h_Z%s_mass_cons",Zchan[i].c_str()),";Z mass [GeV];Entries", 40, 86, 96 );

    //slice by: leading lepton pt: 20,40,60,80,100,inf; subleading lepton pt: 15,30,45,60,90,inf; leading/subleading lepton |eta|: 0,0.5,1,1.5,2,2.5
    for(int j=0;j<5;j++){
      h_Z_m_cons_llpt[i][j]=new TH1F(Form("h_Z%s_mass_cons_llpt_%i",Zchan[i].c_str(),j),";Z mass [GeV];Entries", 40, 86, 96 );
      h_Z_m_cons_slpt[i][j]=new TH1F(Form("h_Z%s_mass_cons_slpt_%i",Zchan[i].c_str(),j),";Z mass [GeV];Entries", 40, 86, 96 );
      h_Z_m_cons_lleta[i][j]=new TH1F(Form("h_Z%s_mass_cons_lleta_%i",Zchan[i].c_str(),j),";Z mass [GeV];Entries", 40, 86, 96 );
      h_Z_m_cons_sleta[i][j]=new TH1F(Form("h_Z%s_mass_cons_sleta_%i",Zchan[i].c_str(),j),";Z mass [GeV];Entries", 40, 86, 96 );
      h_Z_l1_pt_eta[i][j] = new TH1F( Form("h_Z%s_leadingLep_pt_eta_%i",Zchan[i].c_str(),j),";Z leading lepton p_{T} [GeV];Entries", 100, 0, 200 );
      h_Z_l2_pt_eta[i][j] = new TH1F( Form("h_Z%s_subleadingLep_pt_eta_%i",Zchan[i].c_str(),j),";Z subleading lepton p_{T} [GeV];Entries", 75, 0, 150 );
      h_Z_l1_pt_slpt[i][j] = new TH1F( Form("h_Z%s_leadingLep_pt_slpt_%i",Zchan[i].c_str(),j),";Z leading lepton p_{T} [GeV];Entries", 100, 0, 200 );
    }

    for(int j=0;j<3;j++){
      
      h_Z_m[i][j] = new TH1F( Form("h_Z%s_mass%s",Zchan[i].c_str(),crStr[j].c_str()),";Z mass [GeV];Entries", 30, 76, 106 );
      h_Z_m_med[i][j] =new TH1F(Form("h_Z%s_mass_med%s",Zchan[i].c_str(),crStr[j].c_str()),";Z mass [GeV];Entries", 30, 76, 106 );
      h_Z_pt[i][j] = new TH1F( Form("h_Z%s_pt%s",Zchan[i].c_str(),crStr[j].c_str()),";Z p_{T} [GeV];Entries", 50, 0, 500 );
      h_Z_y[i][j] = new TH1F( Form("h_Z%s_y%s",Zchan[i].c_str(),crStr[j].c_str()),";Z rapidity;Entries", 30, -3., 3. );
      h_Z_dR[i][j] = new TH1F( Form("h_Z%s_dR%s",Zchan[i].c_str(),crStr[j].c_str()),";Z #Delta R(ll);Entries", 32, 0, 6.4 );
      
      h_Z_l1_pt[i][j] = new TH1F( Form("h_Z%s_leadingLep_pt%s",Zchan[i].c_str(),crStr[j].c_str()),";Z leading lepton p_{T} [GeV];Entries", 100, 0, 200 );
      h_Z_l2_pt[i][j] = new TH1F( Form("h_Z%s_subleadingLep_pt%s",Zchan[i].c_str(),crStr[j].c_str()),";Z subleading lepton p_{T} [GeV];Entries", 75, 0, 150 );
      
      h_Z_l1_eta[i][j] = new TH1F( Form("h_Z%s_leadingLep_eta%s",Zchan[i].c_str(),crStr[j].c_str()),";Z leading lepton #eta;Entries", 54, -2.7, 2.7 );
      h_Z_l2_eta[i][j] = new TH1F( Form("h_Z%s_subleadingLep_eta%s",Zchan[i].c_str(),crStr[j].c_str()),";Z subleading lepton #eta;Entries", 54, -2.7, 2.7 );
      
      h_Z_l1_phi[i][j] = new TH1F( Form("h_Z%s_leadingLep_phi%s",Zchan[i].c_str(),crStr[j].c_str()),";Z leading lepton #phi;Entries", 63, -3.15, 3.15 );
      h_Z_l2_phi[i][j] = new TH1F( Form("h_Z%s_subleadingLep_phi%s",Zchan[i].c_str(),crStr[j].c_str()),";Z subleading lepton #phi;Entries", 63, -3.15, 3.15 );

    }
    for(int j=0;j<2;j++){
      h_Z_m_mu[i][j] = new TH1F( Form("h_Z%s_mass_%s",Zchan[i].c_str(),muStr[j].c_str()),";Z mass [GeV];Entries", 30, 76, 106 );
    }
  }

  h_el_passLooseLHnoBL=new TH1F("h_el_passLooseLHnoBL",";electron pass LooseLHnoBL;Entries",3,-0.5,2.5);
  h_el_passLooseLH=new TH1F("h_el_passLooseLH",";electron pass LooseLH;Entries",3,-0.5,2.5);
  h_el_MCClass=new TH1F("h_el_MCClass",";electron MC truth classification;Entries",8,-0.5,7.5);

  h_Z_el_nInnerExpPix = (TH1F*) h_el_nInnerExpPix->Clone("h_Z_el_nInnerExpPix"); //wk()->addOutput(h_Z_el_nBL);
  h_el_nInnerExpPix_vs_MCType = new TH2F("h_el_nInnerExpPix_vs_MCType",";MC type;n_{Pix}",MCType::nMAX,0,MCType::nMAX,4,-0.5,3.5);

  double mctypebins[6]={0,1,2,3,4,5};
  double njetbins[5]={0,1,2,3,4};
  double pileupbins[8]={0,10,20,30,40,50,60,70}; 
  double njetbins2[3]={0,1,2};
  for(int i=0;i<5;i++){
    for(int j=0;j<MCType::nMAX+1;j++){
      h_el_ptvc20_vs_njet[i][j]=new TH1F(Form("h_el_ptvc20_vs_njet_%i_mctype_%i",i,j),"",40,0,2);
      h_el_ptvc30_vs_njet[i][j]=new TH1F(Form("h_el_ptvc30_vs_njet_%i_mctype_%i",i,j),"",40,0,2);
      h_el_tec20_vs_njet[i][j]=new TH1F(Form("h_el_tec20_vs_njet_%i_mctype_%i",i,j),"",40,0,2);
      h_lepton_id_vs_njet[i][j]=new TH1F(Form("h_lepton_id_vs_njet_%i_mctype_%i",i,j),"",11,-0.5,10.5);
    }
  }
  h_el_Pass_pt_Iso_vs_MCType=new TH2F("h_el_Pass_pt_Iso_vs_MCType",";MC type;p_{T} [GeV]",MCType::nMAX,0,MCType::nMAX,94, 6, 100);
  h_el_Fail_pt_Iso_vs_MCType=(TH2F*)h_el_Pass_pt_Iso_vs_MCType->Clone("h_el_Fail_pt_Iso_vs_MCType");
  for(int j=0;j<nElLHMax;j++){
    h_el_PassID_pt_vs_MCType[j]=new TH2F(Form("h_el_Pass%s_pt_vs_MCType",strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,0,MCType::nMAX,94, 6, 100);
    h_el_FailID_pt_vs_MCType[j]=(TH2F*)h_el_PassID_pt_vs_MCType[j]->Clone(Form("h_el_FailID%s_pt_vs_MCType",strLH[j].c_str()));
    h_el_passID_passIso_pt_vs_MCType[j]=(TH2F*)h_el_PassID_pt_vs_MCType[j]->Clone(Form("h_el_passID%s_passIso_pt_vs_MCType",strLH[j].c_str()));
    h_el_passID_failIso_pt_vs_MCType[j]=(TH2F*)h_el_PassID_pt_vs_MCType[j]->Clone(Form("h_el_passID%s_failIso_pt_vs_MCType",strLH[j].c_str()));
    h_el_passIso_passID_pt_vs_MCType[j]=(TH2F*)h_el_PassID_pt_vs_MCType[j]->Clone(Form("h_el_passIso_passId%s_pt_vs_MCType",strLH[j].c_str()));
    h_el_passIso_failID_pt_vs_MCType[j]=(TH2F*)h_el_PassID_pt_vs_MCType[j]->Clone(Form("h_el_passIso_failId%s_pt_vs_MCType",strLH[j].c_str()));
    for (int i=0;i<CREL::nMAX;i++){
      h_el_Pass_pt[j][i]= new TH1F(Form("h_el_%s_Pass%s_pt",CREL::strCREL[i].c_str(),strLH[j].c_str()),";electron pt [GeV];Entries",94, 6, 100);
      h_el_Fail_pt[j][i]= (TH1F*)h_el_Pass_pt[j][i]->Clone(Form("h_el_%s_Fail%s_pt",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Pass_eta[j][i]= new TH1F(Form("h_el_%s_Pass%s_eta",CREL::strCREL[i].c_str(),strLH[j].c_str()),";electron eta;Entries",100, -2.5, 2.5);
      h_el_Fail_eta[j][i]= (TH1F*)h_el_Pass_eta[j][i]->Clone(Form("h_el_%s_Fail%s_eta",CREL::strCREL[i].c_str(),strLH[j].c_str()));

      h_el_Pass_pt_vs_MCType_CR[j][i] = new TH2F(Form("h_el_%s_Pass%s_pt_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,0,MCType::nMAX,94, 6, 100);
      h_el_PassIDFailIso_pt_vs_MCType_CR[j][i]=(TH2F*)h_el_Pass_pt_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_PassIDFailIso%s_pt_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Pass_eta_vs_MCType_CR[j][i] = new TH2F(Form("h_el_%s_Pass%s_eta_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;#eta",MCType::nMAX,0,MCType::nMAX,24,0,2.4);
      h_el_Pass_njet_vs_MCType_CR[j][i] = new TH2F(Form("h_el_%s_Pass%s_njet_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;njets",MCType::nMAX,0,MCType::nMAX,5,0,5);
      h_el_Fail_pt_vs_MCType_CR[j][i] = (TH2F*) h_el_Pass_pt_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_pt_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str())); //wk()->addOutput(h_el_Fail_pt_vs_MCType_CR[j][i]);
      h_el_Fail_eta_vs_MCType_CR[j][i] = (TH2F*) h_el_Pass_eta_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_eta_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Fail_njet_vs_MCType_CR[j][i] = (TH2F*) h_el_Pass_njet_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_njet_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Pass_pt_eta_vs_MCType_CR[j][i] = new TH3F(Form("h_el_%s_Pass%s_pt_eta_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,mctypebins,3,ptBins,3,etaBins);
      h_el_Fail_pt_eta_vs_MCType_CR[j][i] = (TH3F*) h_el_Pass_pt_eta_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_pt_eta_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Pass_pt_njet_vs_MCType_CR[j][i] = new TH3F(Form("h_el_%s_Pass%s_pt_njet_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,mctypebins,3,ptBins,4,njetbins);
      h_el_Fail_pt_njet_vs_MCType_CR[j][i] = (TH3F*) h_el_Pass_pt_njet_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_pt_njet_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      h_el_Pass_pt_njet2_vs_MCType_CR[j][i] = new TH3F(Form("h_el_%s_Pass%s_pt_njet2_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,mctypebins,3,ptBins,2,njetbins2);
      h_el_Fail_pt_njet2_vs_MCType_CR[j][i] = (TH3F*) h_el_Pass_pt_njet2_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_pt_njet2_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
      //pileup
      h_el_Pass_pt_pileup_vs_MCType_CR[j][i] = new TH3F(Form("h_el_%s_Pass%s_pt_pileup_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()),";MC type;p_{T} [GeV]",MCType::nMAX,mctypebins,3,ptBins,7,pileupbins);
      h_el_Fail_pt_pileup_vs_MCType_CR[j][i] = (TH3F*) h_el_Pass_pt_pileup_vs_MCType_CR[j][i]->Clone(Form("h_el_%s_Fail%s_pt_pileup_vs_MCType",CREL::strCREL[i].c_str(),strLH[j].c_str()));
    }
  }

  h_el_fake_weight=new TH1F("h_el_fake_weight","",50,-50,50);
  h_el_run_pass=new TH1F("h_el_run_pass","",22400,341100,363500);
  h_lepton_id=new TH1F("h_lepton_id","",10,-0.5,9.5);
}
