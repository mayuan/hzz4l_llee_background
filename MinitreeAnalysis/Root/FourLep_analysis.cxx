#include <MinitreeAnalysis/FourLep_analysis.h>
#include <iostream>
#include <cmath>
#include "TString.h"
#include "TLorentzVector.h"
#include "TMath.h"

#define INFO(x)     std::cout << "-- <INFO>  FourLep_analysis: " << x << std::endl;
#define CHECK(X,S) { if ((X)==0) { std::cout<<"-- <ERROR>  FourLep_analysis: Failed to execute "<<S<<std::endl; return;} }
/*
FourLep_analysis::FourLep_analysis (string inFilename, string outFilename, string treename)
{
  TFile *f = TFile::Open(inFilename.c_str());
  TTree *tree;
  f->GetObject(treename.c_str(),tree);
  Init(tree);
  //FourLep::Init(tree);  
  //ZplusX::Init(tree);

  fout = TFile::Open(outFilename.c_str(),"RECREATE");
}
*/

FourLep_analysis::FourLep_analysis(TChain* chain) : FourLep(chain){}

void FourLep_analysis :: runAnalysis (string outFilename, bool isData, int cut)
{
  //fout = TFile::Open(outFilename.c_str(),"RECREATE");
  fout=new TFile(outFilename.c_str(),"RECREATE");
  bookHists(cut,outFilename);
  //setupTools();
  
  //int nEntries = FourLep::fChain->GetEntries();
  int nEntries = fChain->GetEntries();
  INFO("Entries:" << nEntries);
  //cout<<"entry check: "<<fChain->GetEntries("run<342700")<<endl;
  float iniEvents=0;
  //int jet0ev1=0,jet0ev2=0;
  int njetscut=2;
  if(outFilename.find("relax")!=string::npos) njetscut=1;
  bool useHFCut=false;
  if(outFilename.find("HF")!=string::npos) useHFCut=true;
  bool useLFCut=false;
  if(outFilename.find("LF")!=string::npos && outFilename.find("ThreeLplusX")!=string::npos) useLFCut=true;
  bool useGammaCut=false;
  bool is3LX=false,isInvD0=false,isEmu3=false,isInvIso=false,isSS=false,isRelaxed=false;
  if(outFilename.find("ThreeLplusX")!=string::npos) is3LX=true;
  else if(outFilename.find("invD0")!=string::npos) isInvD0=true;
  else if(outFilename.find("emu")!=string::npos) isEmu3=true;
  else if(outFilename.find("invIso")!=string::npos) isInvIso=true;
  else if(outFilename.find("sameSign")!=string::npos) isSS=true;
  else if(outFilename.find("relaxIsoD0")!=string::npos) isRelaxed=true;
  if(outFilename.find("Gamma")!=string::npos) useGammaCut=true;
  bool isSideband=false,relaxIso=false,enhanceLF=false;
  if(outFilename.find("Nominal")!=string::npos || outFilename.find("Sideband")!=string::npos) isSideband=true;
  if(useHFCut) cout<<"HF electrons"<<endl;
  if(useLFCut) cout<<"LF electrons"<<endl;
  if(useGammaCut) cout<<"gammas"<<endl;
  if(isSideband) cout<<"do sideband cut"<<endl;
  if(is3LX) cout<<"running on 3L+X"<<endl;
  else if(isInvD0) cout<<"running on InvD0"<<endl;
  else if(isEmu3) cout<<"running on emumumu"<<endl;
  else if(isInvIso) cout<<"running on InvIso"<<endl;
  else if(isRelaxed) cout<<"running on RelaxIsoD0"<<endl;
  if(relaxIso) cout<<"relaxing leading iso"<<endl;
  if(enhanceLF) cout<<"enhance LF"<<endl;
  cout<<"mass window cut "<<cut<<", njets cut (?)"<<njetscut<<endl;
  float hfTot=0,lfTot=0,gammaTot=0,tot=0;
  int nNoChi2=0;

  for (int ievt = 0; ievt < nEntries; ievt++) {
    //FourLep::fChain->GetEntry(ievt);
    fChain->GetEntry(ievt);
    
    //float w = FourLep::weight;
    float w=1;
    if(!isData) w=weight;
    if(run<364510 && run>364499 && w_xs==-1){
      if(run==364500) w*=-57619;
      else if(run==364501) w*=-34590;
      else if(run==364502) w*=-6285.6;
      else if(run==364503) w*=-491.86;
      else if(run==364504) w*=-62.987;
      else if(run==364505) w*=-57701;
      else if(run==364506) w*=-34588;
      else if(run==364507) w*=-6285.3;
      else if(run==364508) w*=-493.92;
      else if(run==364509) w*=-63.08;
    }
    //if(run==410289 || run==410470 || run==410472) w*=1.45; //ttbar NO! this is done in the stacking macro
    //float w_c=w;
    //if(isData) w_c=weight_couplings;
    /*
    else{
      TFile* effFile=new TFile("fakeFactors_d0.root");
      TH1F* fake=(TH1F*)effFile->Get("eff_pt_Loose_fake");
      TH1F* gamma=(TH1F*)effFile->Get("eff_pt_Loose_gamma");
      std::cout<<"pt: "<<lepton_pt[3]<<", nInner: "<<el_nInnerExpPix[3]<<std::endl;
      for(int i=0;i<3;i++){
	//std::cout<<"bin from "<<fake->GetXaxis()->GetBinLowEdge(i+1)<<" to "<<fake->GetXaxis()->GetBinUpEdge(i+1)<<std::endl;
	if(lepton_pt[3]>fake->GetXaxis()->GetBinLowEdge(i+1) && lepton_pt[3]<fake->GetXaxis()->GetBinUpEdge(i+1)){
	  if(el_nInnerExpPix[3]==0) w=gamma->GetBinContent(i+1);
	  else if(el_nInnerExpPix[3]>0) w=fake->GetBinContent(i+1);
	  else std::cout<<"no hit expected"<<std::endl;
	  break;
	}
      }
      if(w==1){
	if(el_nInnerExpPix[3]==0) w=gamma->GetBinContent(3);
	else if(el_nInnerExpPix[3]>0) w=fake->GetBinContent(3);
	else std::cout<<"no hit expected"<<std::endl;
      }
    }
    std::cout<<"set weight to "<<w<<std::endl;
    if(w==1) continue;
    */
    //INFO( FourLep::mZ1_unconstrained << " " << FourLep::weight  << " " << ZplusX::el_nBL->size());

    if(is3LX){
      if(!lepton_passd0Sig[0] || !lepton_passd0Sig[1] || !lepton_passd0Sig[2] || !lepton_passd0Sig[3]) continue;
      if(!lepton_passIso[2]) continue; //only relaxing iso on leading leptons
      if(!relaxIso){
	if(!lepton_passIso[0] || !lepton_passIso[1]) continue;
      }
      else{
	if(fChain->GetBranch("lepton_iso_weight")){
	  cout<<"weight: "<<w<<"; lepton iso weight 0: "<<lepton_isoWeight[0]<<"; "<<"lepton iso weight 1: "<<lepton_isoWeight[1]<<endl;
	  w=w/(lepton_isoWeight[0]*lepton_isoWeight[1]);
	  cout<<"corrected weight: "<<w<<endl;
	}
      }
      if(event_type<4){
	h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
	h_vtxChi2All->Fill(vtx4lchi2,w);
	if(vtx4lchi2>-999){
	  h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
	  h_vtxChi2All_FO->Fill(vtx4lchi2,w);
	}
      }
      if(!pass_vtx4lCut) continue;
    }
    else if(isInvD0 || isEmu3 || isSS || isRelaxed){
      if(!relaxIso){
        if(!lepton_passIso[0] || !lepton_passIso[1]) continue;
      }
      if(!lepton_passd0Sig[0] || !lepton_passd0Sig[1]) continue;
      //if(event_type==0 || event_type==3) cout<<"pt imbalances: "<<fabs(mspt[2]-idpt[2])/idpt[2]<<", "<<fabs(mspt[3]-idpt[3])/idpt[3]<<endl;
      if(enhanceLF && isSS && ((lepton_quality[2]!=0 || lepton_quality[3]!=0) || (fabs(mspt[2]-idpt[2])/idpt[2]<=0.2 && fabs(mspt[3]-idpt[3])/idpt[3]<=0.2))){
	//cout<<"event type "<<event_type<<" lep quality "<<lepton_quality[2]<<","<<lepton_quality[3]<<endl;
	//cout<<"reject event"<<endl;
	//if(fabs(mspt[2]-idpt[2])/idpt[2]>0.2 || fabs(mspt[3]-idpt[3])/idpt[3]>0.2) cout<<"lepton qualities: "<<lepton_quality[2]<<", "<<lepton_quality[3]<<endl;
	continue;
      }
      if(event_type<4){
	if(!isSideband || !((m4l_constrained>115 && m4l_constrained<130) || m4l_constrained>170)){
	  h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
	  if(lepton_passIso[2] && lepton_passIso[3] && lepton_passd0Sig[2] && lepton_passd0Sig[3]) h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
	  h_vtxChi2All->Fill(vtx4lchi2,w);
	  if(vtx4lchi2>-999){
	    h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
	    if(lepton_passIso[2] && lepton_passIso[3] && lepton_passd0Sig[2] && lepton_passd0Sig[3]) h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
	    h_vtxChi2All_FO->Fill(vtx4lchi2,w);
	  }
	}
      }
      else if(event_type==9){
	h_vtxChi2[0]->Fill(vtx4lchi2,w); //emumumu: just fill 4mu
	if(lepton_passIso[2] && lepton_passIso[3] && lepton_passd0Sig[2] && lepton_passd0Sig[3]) h_vtxChi2[0]->Fill(vtx4lchi2,w);
	h_vtxChi2All->Fill(vtx4lchi2,w);
	if(vtx4lchi2>-999){
	  h_vtxChi2_FO[3]->Fill(vtx4lchi2,w);
	  if(lepton_passIso[2] && lepton_passIso[3] && lepton_passd0Sig[2] && lepton_passd0Sig[3]) h_vtxChi2_FO[3]->Fill(vtx4lchi2,w);
	  h_vtxChi2All_FO->Fill(vtx4lchi2,w);
	}
      }
    }
    else if(isInvIso){
      if(!relaxIso){
        if(!lepton_passIso[0] || !lepton_passIso[1]) continue;
      }
      if(!lepton_passd0Sig[0] || !lepton_passd0Sig[1] || !lepton_passd0Sig[2] || !lepton_passd0Sig[3]) continue;
      if(enhanceLF && ((lepton_quality[2]!=0 || lepton_quality[3]!=0) || (fabs(mspt[2]-idpt[2])/idpt[2]<=0.2 && fabs(mspt[3]-idpt[3])/idpt[3]<=0.2))) continue;
      if(event_type<4){
        h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
        h_vtxChi2All->Fill(vtx4lchi2,w);
	if(vtx4lchi2>-999){
	  h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
	  h_vtxChi2All_FO->Fill(vtx4lchi2,w);
	}
      }
      //if(!pass_vtx4lCut) continue;
    }
    else if(isSideband){
      if(event_type<4){
	if(cut!=3 && !((m4l_constrained>115 && m4l_constrained<130) || m4l_constrained>170)){
	  h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
	  h_vtxChi2All->Fill(vtx4lchi2,w);
	  if(vtx4lchi2>-999){
            h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
            h_vtxChi2All_FO->Fill(vtx4lchi2,w);
          }
	}
	else if(cut==3 && !((m4l_constrained>110 && m4l_constrained<140) || m4l_constrained>=200)){
	  h_vtxChi2[event_type]->Fill(vtx4lchi2,w);
          h_vtxChi2All->Fill(vtx4lchi2,w);
	  if(vtx4lchi2>-999){
            h_vtxChi2_FO[event_type]->Fill(vtx4lchi2,w);
            h_vtxChi2All_FO->Fill(vtx4lchi2,w);
          }
        }
      }
      if(!pass_vtx4lCut) continue;
    }
    if(vtx4lchi2<0) nNoChi2++;

    if(w>0 && (event_type==0 || event_type==3 || event_type==9)) iniEvents+=w;
    int leptype=0; //0=el, 1=mu
    if(event_type==0 || event_type==3 || event_type==9) leptype=1;

    if(useHFCut){
      if(leptype!=0) continue; //only doing this for electrons
      if(!((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2) || (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2))){
	//cout<<"not HF, subleading electrons have pT "<<lepton_pt[2]<<", "<<lepton_pt[3]<<", and MCClass "<<el_MCClass[2]<<", "<<el_MCClass[3]<<endl;
	continue;
      }
      if(run==344171 || run==410000 || run==410009) w*=1.46; //ttbar
    }
    if(useLFCut){
      if(leptype!=0) continue; //only doing this for electrons
      tot+=w;
      if((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2) || (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2)){
	//cout<<"rejecting HF, subleading electrons have pT "<<lepton_pt[2]<<", "<<lepton_pt[3]<<", and MCClass "<<el_MCClass[2]<<", "<<el_MCClass[3]<<endl;
	hfTot+=w;
	continue;
      }
      if((lepton_pt[3]<lepton_pt[2] && el_nInnerExpPix[3]<1) || (lepton_pt[2]<lepton_pt[3] && el_nInnerExpPix[2]<1)){
	//cout<<"not LF, subleading electrons have pT "<<lepton_pt[2]<<", "<<lepton_pt[3]<<" and nInner "<<el_nInnerExpPix[2]<<", "<<el_nInnerExpPix[3]<<endl;
	gammaTot+=w;
	continue;
      }
      lfTot+=w;
      if(run==344171 || run==410000 || run==410009) w*=1.46; //ttbar
    }
    if(useGammaCut){
      if(leptype!=0) continue; //only doing this for electrons
      if((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2) || (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2)) continue;
      if((lepton_pt[3]<lepton_pt[2] && el_nInnerExpPix[3]>0) || (lepton_pt[2]<lepton_pt[3] && el_nInnerExpPix[2]>0)) continue;
      if(run==344171 || run==410000 || run==410009) w*=1.46; //ttbar
    }

    if(cut){
      cout<<"applying m4l cuts"<<endl;
      if(outFilename.find("relax")!=string::npos){
	if((m4l_constrained>118 && m4l_constrained<129) || m4l_constrained>170 || m4l_constrained<110){/*cout<<"m4l="<<m4l_constrained<<", skip"<<endl;*/ continue;}
      }
      else if(is3LX){
	//if(m4l_constrained<115 || m4l_constrained>130) continue;
      }
      else if(isSideband && cut!=3){
	if((m4l_constrained>115 && m4l_constrained<130) || m4l_constrained>170){
	  //cout<<"reject event with m4l "<<m4l_constrained<<endl;
	  continue;
	}
	else cout<<"accept event with m4l "<<m4l_constrained<<endl;
      }
      else{
	if(cut==1){
	  if(m4l_constrained<115 || m4l_constrained>140) continue;
	}
	else if(cut==2){
	  if(m4l_constrained<118 || m4l_constrained>129) continue;
	}
	else if(cut==3){
	  if((m4l_constrained>110 && m4l_constrained<140) || m4l_constrained>=200){ //blinded sidebands
	    continue;
	  }
	}
      }
    }

    h_m4l_fsr_all[leptype]->Fill(m4l_fsr,w);
    h_m4l_fsr_full_all[leptype]->Fill(m4l_fsr,w);
    h_m4l_fsr_full->Fill(m4l_fsr,w);
    int pileuptype=-1;
    if(pileupMu<45) pileuptype=0;
    else pileuptype=1;
    h_m4l_fsr_pileup_all[pileuptype]->Fill(m4l_fsr,w);
    if(pt4l_unconstrained<60) h_m4l_fsr_var[leptype][0][0]->Fill(m4l_fsr,w);
    else h_m4l_fsr_var[leptype][0][1]->Fill(m4l_fsr,w);
    if(n_jets<njetscut) h_m4l_fsr_var[leptype][1][0]->Fill(m4l_fsr,w);
    else h_m4l_fsr_var[leptype][1][1]->Fill(m4l_fsr,w);

    //else if(outFilename.find("relax")!=string::npos && ((m4l_constrained>118 && m4l_constrained<129) || m4l_constrained>170 || m4l_constrained<110)) continue;
    //if(!lepton_passIso[2] || !lepton_passIso[3]) continue;
    /*
    if((event_type==0 || event_type==3) && pt4l_unconstrained>150){
      if(event_type==0) cout<<"leading Z->mumu"<<endl;
      else if(event_type==3) cout<<"leading Z->ee"<<endl;
      for(int i=0;i<4;i++) cout<<"lepton "<<i<<" (pt,eta,phi)=("<<lepton_pt[i]<<","<<lepton_eta[i]<<","<<lepton_phi[i]<<")"<<endl;
    }
    */
    int njet=n_jets;
    int nbjet=n_jets_btag70;
    TLorentzVector jvec;
    TLorentzVector lepvec;
    float mindR[2]={10,10};
    for(int ilep=2;ilep<4;ilep++){
      lepvec.SetPtEtaPhiM(lepton_pt[ilep],lepton_eta[ilep],lepton_phi[ilep],lepton_m[ilep]);
      for(int i=0;i<n_jets;i++){
	jvec.SetPtEtaPhiM(jet_pt->at(i),jet_eta->at(i),jet_phi->at(i),jet_m->at(i));
	float dR=lepvec.DeltaR(jvec);
	if(dR<mindR[ilep-2]) mindR[ilep-2]=dR;
      }
    }
    if(lepton_pt[2]>lepton_pt[3]){
      h_minDR_leading_lep_jet_all[leptype]->Fill(mindR[0],w);
      h_minDR_subleading_lep_jet_all[leptype]->Fill(mindR[1],w);
    }
    else{
      h_minDR_leading_lep_jet_all[leptype]->Fill(mindR[1],w);
      h_minDR_subleading_lep_jet_all[leptype]->Fill(mindR[0],w);
    }
    /*
    for(int i=0;i<n_jets;i++){
      if(i>3) break;
      std::cout<<"jet "<<i<<" btag70: "<<jet_btag70[i]<<std::endl;
    }
    */
    for(int i=0;i<4;i++){
      h_lepton_pt->Fill(lepton_pt[i],w);
      h_lepton_eta->Fill(lepton_eta[i],w);
    }
    int lleptype=0; //0=e,1=mu
    if(event_type==0 || event_type==2) lleptype=1;
    for(int i=0;i<2;i++){
      h_pt_lep[i][lleptype]->Fill(lepton_pt[i],w);
      h_eta_lep[i][lleptype]->Fill(lepton_eta[i],w);
      h_caloIso_lep[i][lleptype]->Fill(lepton_topoetcone20[i],w);
      h_caloIso_comb_lep[lleptype]->Fill(lepton_topoetcone20[i],w);
      if(lleptype==0){
	h_trackIso_lep[i][lleptype]->Fill(lepton_ptvarcone20[i],w);
	h_trackIso_comb_lep[lleptype]->Fill(lepton_ptvarcone20[i],w);
      }
      else{
	h_trackIso_lep[i][lleptype]->Fill(lepton_ptvarcone30[i],w);
	h_trackIso_comb_lep[lleptype]->Fill(lepton_ptvarcone30[i],w);
      }
      h_d0Sig_lep[i][lleptype]->Fill(lepton_d0Sig[i],w);
    }
    h_mLeadingZ[lleptype]->Fill(mZ1_unconstrained,w);
    h_mLeadingZ_fullRange[lleptype]->Fill(mZ1_unconstrained,w);
    h_dRZ1[lleptype]->Fill(sqrt(pow(lepton_eta[0]-lepton_eta[1],2)+pow(lepton_phi[0]-lepton_phi[1],2)),w);
    h_met[lleptype]->Fill(met,w);
    if(pt4l_unconstrained>150){
      h_mZ1_highpT[leptype]->Fill(mZ1_unconstrained,w);
      h_mZ2_highpT[leptype]->Fill(mZ2_unconstrained,w);
      h_nbtag70_highpT[leptype]->Fill(n_jets_btag70,w);
      h_njets_highpT[leptype]->Fill(n_jets,w);
      if(lepton_pt[2]>lepton_pt[3]){
	h_caloIso_leading_highpT[leptype]->Fill(lepton_topoetcone20[2],w);
	h_trackIso_leading_highpT[leptype]->Fill(lepton_ptvarcone30[2],w);
	h_d0Sig_leading_highpT[leptype]->Fill(lepton_d0Sig[2],w);
	h_caloIso_subleading_highpT[leptype]->Fill(lepton_topoetcone20[3],w);
	h_trackIso_subleading_highpT[leptype]->Fill(lepton_ptvarcone30[3],w);
	h_d0Sig_subleading_highpT[leptype]->Fill(lepton_d0Sig[3],w);
	//if(lepton_topoetcone20[3]<0) cout<<"calo iso: "<<lepton_topoetcone20[3]<<", weight "<<endl;
	h_minDR_leading_lep_jet_highpT[leptype]->Fill(mindR[0]);
	h_minDR_subleading_lep_jet_highpT[leptype]->Fill(mindR[1]);
      }
      else{
	h_caloIso_leading_highpT[leptype]->Fill(lepton_topoetcone20[3],w);
        h_trackIso_leading_highpT[leptype]->Fill(lepton_ptvarcone30[3],w);
        h_d0Sig_leading_highpT[leptype]->Fill(lepton_d0Sig[3],w);
        h_caloIso_subleading_highpT[leptype]->Fill(lepton_topoetcone20[2],w);
        h_trackIso_subleading_highpT[leptype]->Fill(lepton_ptvarcone30[2],w);
        h_d0Sig_subleading_highpT[leptype]->Fill(lepton_d0Sig[2],w);
	//if(lepton_topoetcone20[2]<0) cout<<"calo iso: "<<lepton_topoetcone20[2]<<", weight "<<endl;
	h_minDR_leading_lep_jet_highpT[leptype]->Fill(mindR[1]);
	h_minDR_subleading_lep_jet_highpT[leptype]->Fill(mindR[0]);
      }
    }
    TLorentzVector lep1,lep2;
    lep1.SetPtEtaPhiM(lepton_pt[0],lepton_eta[0],lepton_phi[0],lepton_m[0]);
    lep2.SetPtEtaPhiM(lepton_pt[1],lepton_eta[1],lepton_phi[1],lepton_m[1]);
    TLorentzVector z1=lep1;
    z1+=lep2;
    h_Z1pT_all[leptype]->Fill(z1.Pt(),w);
    float HT=0;
    for(int i=0;i<n_jets;i++){
      if(i>3) break;
      HT+=jet_pt->at(i);
    }
    h_HT_all[leptype]->Fill(HT,w);
    int sample=run-344295;
    if(sample>3) sample-=19801;
    h_sample_pt4l_all[leptype]->Fill(sample,pt4l_unconstrained,w);

    h_dijet_invmass->Fill(dijet_invmass,w);
    h_dijet_deltaeta->Fill(dijet_deltaeta,w);
    //h_pt_all->Fill(pt4l_unconstrained
    string cat[11]={"low-p_{T4l} 0jet","high-p_{T4l} 0jet","low-p_{T4l} 1jet","med-p_{T4l} 1jet","high-p_{T4l} 1jet","VHhad","low-p_{Tj1} VBF","high-p_{T} VBF","VHlep", "ttH-Had","ttH-Lep"};
    for(int ibin=0;ibin<h_prod_type->GetNbinsX();ibin++) h_prod_type->GetXaxis()->SetBinLabel(ibin+1,cat[ibin].c_str());
    int prodbin=-1;
    if(prod_type==0){
      if(pt4l_unconstrained<100) prodbin=0;
      else prodbin=1;
    }
    else if(prod_type==1){
      if(pt4l_unconstrained<60) prodbin=2;
      else if(pt4l_unconstrained<120) prodbin=3;
      else prodbin=4;
    }
    else if(prod_type==2){
      prodbin=5;
    }
    else if(prod_type==3){
      if(jet_pt->at(0)<200) prodbin=6;
      else prodbin=7;
    }
    else if(prod_type==4){
      prodbin=8;
    }
    else if(prod_type==5){
      if(n_extraLep==0) prodbin=9;
      else prodbin=10;
    }
    h_prod_type->Fill(prodbin,w);
    if(event_type==0 || event_type==3) h_prod_type_mu->Fill(prodbin,w);
    else if(event_type==1 || event_type==2) h_prod_type_e->Fill(prodbin,w);
    if(prodbin==0) h_bdt[0]->Fill(BDT_discriminant);
    else if(prodbin==2) h_bdt[1]->Fill(BDT_1Jet_pt4l_60);
    else if(prodbin==3) h_bdt[1]->Fill(BDT_1Jet_pt4l_60_120);
    int bdtProdBins[6]={0,2,3,5,6,9};
    h_pt_all[leptype]->Fill(pt4l_unconstrained,w);
    h_eta_all[leptype]->Fill(eta4l_unconstrained,w);
    h_pt_all_binned[leptype]->Fill(pt4l_unconstrained,w);
    h_pt_all_binned_tot->Fill(pt4l_unconstrained,w);
    h_pt_all_tot->Fill(pt4l_unconstrained,w);
    h_eta_all_tot->Fill(eta4l_unconstrained,w);
    //cout<<"fill pt histogram with "<<pt4l_unconstrained<<", "<<w<<endl;
    h_y_all[leptype]->Fill(y4l_unconstrained,w);
    h_mZ1_all[leptype]->Fill(mZ1_unconstrained,w);
    h_mZ2_all[leptype]->Fill(mZ2_unconstrained,w);
    h_cts_all[leptype]->Fill(fabs(cthstr),w);
    h_njets[leptype]->Fill(n_jets,w);
    if(n_jets<4){
      h_njets_binned[leptype]->Fill(n_jets,w);
      h_njets_binned_tot->Fill(n_jets,w);
    }
    else{
      h_njets_binned[leptype]->Fill(3,w);
      h_njets_binned_tot->Fill(3,w);
    }
    h_nbjets_all[leptype]->Fill(n_jets_btag70,w);
    if(n_jets==0) h_pt_0jet[leptype]->Fill(pt4l_unconstrained,w);
    else if(n_jets==1) h_pt_1jet[leptype]->Fill(pt4l_unconstrained,w);
    else h_pt_2jet[leptype]->Fill(pt4l_unconstrained,w);
    if(pt4l_unconstrained<60){
      if(n_jets>0) h_leading_jet_pt_var[leptype][0][0]->Fill(jet_pt->at(0),w);
      h_y_var[leptype][0][0]->Fill(y4l_unconstrained,w);
      h_mZ1_var[leptype][0][0]->Fill(mZ1_unconstrained,w);
      h_mZ2_var[leptype][0][0]->Fill(mZ2_unconstrained,w);
      h_cts_var[leptype][0][0]->Fill(fabs(cthstr),w);
    }
    else{
      if(n_jets>0) h_leading_jet_pt_var[leptype][0][1]->Fill(jet_pt->at(0),w);
      h_y_var[leptype][0][1]->Fill(y4l_unconstrained,w);
      h_mZ1_var[leptype][0][1]->Fill(mZ1_unconstrained,w);
      h_mZ2_var[leptype][0][1]->Fill(mZ2_unconstrained,w);
      h_cts_var[leptype][0][1]->Fill(fabs(cthstr),w);
    }
    if(n_jets<njetscut){
      h_y_var[leptype][1][0]->Fill(y4l_unconstrained,w);
      h_pt_var[leptype][1][0]->Fill(pt4l_unconstrained,w);
      h_mZ1_var[leptype][1][0]->Fill(mZ1_unconstrained,w);
      h_mZ2_var[leptype][1][0]->Fill(mZ2_unconstrained,w);
      h_cts_var[leptype][1][0]->Fill(fabs(cthstr),w);
    }
    else if(n_jets>=njetscut){
      h_y_var[leptype][1][1]->Fill(y4l_unconstrained,w);
      h_pt_var[leptype][1][1]->Fill(pt4l_unconstrained,w);
      h_mZ1_var[leptype][1][1]->Fill(mZ1_unconstrained,w);
      h_mZ2_var[leptype][1][1]->Fill(mZ2_unconstrained,w);
      h_cts_var[leptype][1][1]->Fill(fabs(cthstr),w);
    }
    if(n_jets>0) h_leading_jet_pt_all[leptype]->Fill(jet_pt->at(0),w);
    if(n_jets==1) h_leading_jet_pt[0]->Fill(jet_pt->at(0),w);
    else if(n_jets>1){
      h_leading_jet_pt[1]->Fill(jet_pt->at(0),w);
      h_subleading_jet_pt->Fill(jet_pt->at(1),w);
      h_mjj_all[leptype]->Fill(dijet_invmass,w);
      h_detajj_all[leptype]->Fill(dijet_deltaeta,w);
      //float dijet_phi=0;
      //if(jet_eta[0]>jet_eta[1]) dijet_phi=jet_phi[0]-jet_phi[1];
      //if(jet_eta[0]<jet_eta[1]) dijet_phi=jet_phi[1]-jet_phi[0];
      //if(dijet_phi<0) dijet_phi = dijet_phi + 2*TMath::Pi();
      h_dphijj_all[leptype]->Fill(dijet_deltaphi,w);
    }

    for(int i=0;i<6;i++){
      if(prod_type==i){
	h_mZ1[i]->Fill(mZ1_unconstrained,w);
	h_mZ2[i]->Fill(mZ2_unconstrained,w);
	h_m4l[i]->Fill(m4l_unconstrained,w);
	h_m4l_fsr[i]->Fill(m4l_fsr,w);	
	h_m4l_const[i]->Fill(m4l_constrained,w);
      
	h_pt[i]->Fill(pt4l_unconstrained,w);
	h_y[i]->Fill(y4l_unconstrained,w);
	if(event_type==0 || event_type==3){
	  if(i==0) h_bdt[i]->Fill(BDT_discriminant,w);
	  else if(i==1){
	    if(pt4l_unconstrained<60) h_bdt[i]->Fill(BDT_1Jet_pt4l_60,w);
	    else if(pt4l_unconstrained>60 && pt4l_unconstrained<120) h_bdt[4]->Fill(BDT_1Jet_pt4l_60_120,w);
	    else h_bdt[5]->Fill(BDT_1Jet_pt4l_120,w);
	  }
	  else if(i==2) h_bdt[i]->Fill(BDT_VH,w);
	  else if(i==3) h_bdt[i]->Fill(BDT_VBF,w);
	}
      }
    }
  }

  INFO("njet 0 ratio: "<<h_mZ1_var[1][1][0]->Integral()/h_mZ1_all[1]->Integral()<<"; njet 1 ratio: "<<h_mZ1_var[1][1][1]->Integral()/h_mZ1_all[1]->Integral());
  INFO("m4l njet 0 ratio: "<<h_m4l_fsr_var[1][1][0]->Integral()/h_m4l_fsr_all[1]->Integral()<<"; njet 1 ratio: "<<h_m4l_fsr_var[1][1][1]->Integral()/h_m4l_fsr_all[1]->Integral());
  INFO("pt4l 0 ratio: "<<h_mZ1_var[1][0][0]->Integral()/h_mZ1_all[1]->Integral()<<"; pt4l 1 ratio: "<<h_mZ1_var[1][0][1]->Integral()/h_mZ1_all[1]->Integral());
  INFO("total: "<<tot<<" of which "<<lfTot<<" LF, "<<hfTot<<" HF, "<<gammaTot<<" gamma");
  cout<<"# of vertices w/o chi2: "<<nNoChi2<<endl;

  //INFO("0-jet events 1: "<<jet0ev1<<" and 2 "<<jet0ev2);
  //for(int i=0;i<5;i++) INFO("bin "<<i+1<<" bdt 0: "<<h_bdt[0]->GetBinContent(i+1));

  for(int i=0;i<6;i++){
    if(i==0){
      for(int j=0;j<h_m4l_const_all->GetNbinsX();j++){
	h_m4l_const_all->SetBinContent(j+1,h_m4l_const[i]->GetBinContent(j+1));
	h_m4l_const_all->SetBinError(j+1,h_m4l_const[i]->GetBinError(j+1));
      }
    }
    else h_m4l_const_all->Add(h_m4l_const[i]);
  }
  //for(int j=0;j<h_leading_jet_pt_all->GetNbinsX();j++){
  //h_leading_jet_pt_all->SetBinContent(j+1,h_leading_jet_pt[0]->GetBinContent(j+1));
  //h_leading_jet_pt_all->SetBinError(j+1,h_leading_jet_pt[0]->GetBinError(j+1));
  //}
  //h_leading_jet_pt_all->Add(h_leading_jet_pt[1]);
  
  INFO("finished, iniEvents="<<iniEvents);
  fout->Write();
  return;
}


void FourLep_analysis :: bookHists (int cut, string outFilename)
{
  //FIXME! gotta remember to change the ttHHad BDT name
  string bdtNames[6]={"BDT_discriminant","BDT_1Jet_pt4l_60","BDT_1Jet_pt4l_60_120","BDT_VH_noptHjj_discriminant","BDT_TwoJet_discriminant","BDT_discriminant"};
  for(int i=0;i<6;i++){ //by prod type
    h_mZ1[i] = new TH1F( Form("mZ1_%i",i),";m_{1} [GeV];Entries / 2 GeV",30,50.,110);
    h_mZ2[i] = new TH1F( Form("mZ2_%i",i),";m_{2} [GeV];Entries / 4 GeV",30,0.,120);
    h_m4l[i] = new TH1F( Form("m4l_%i",i),";m [GeV];Entries / 1 GeV",50,100.,150);
    h_m4l_fsr[i] = new TH1F( Form("m4l_fsr_%i",i),";m [GeV];Entries / 5 GeV",100,0.,500);
    h_m4l_const[i] = new TH1F( Form("m4l_const_%i",i),";m [GeV];Entries / 2 GeV",20,110,150);
  
    h_pt[i] = new TH1F( Form("pt_%i",i),";p_{T} [GeV];Entries / 6 GeV",50,0.,300);
    h_y[i] = new TH1F( Form("y_%i",i),";y;Entries / 0.1",100,-3,3);
    //FIXME this is just a lucky coincidence at this point
    h_bdt[i]=new TH1F(Form("bdt_%i",i),Form(";%s",bdtNames[i].c_str()),5,-1,1);
  }
  h_m4l_const_all=new TH1F("h_m4l_const_all",";m [GeV];Entries / 2 GeV",20,110,150);

  //h_njets=new TH1F("h_njets",";N_{jets};Entries",8,0,8);
  h_dijet_invmass=new TH1F("h_dijet_invmass",";M_{jj} (GeV);Entries / 5 GeV",25,0,500);
  h_dijet_deltaeta=new TH1F("h_dijet_deltaeta",";#delta#eta_{jj}",25,0,10);
  h_prod_type=new TH1F("h_prod_type","",11,-0.5,10.5);
  h_prod_type_mu=new TH1F("h_prod_type_mu","",11,-0.5,10.5);
  h_prod_type_e=new TH1F("h_prod_type_e","",11,-0.5,10.5);
  h_leading_jet_pt[0]=new TH1F("h_leading_jet_pt_1jet","; p_{T} (GeV); Entries/10 GeV",50,0,500);
  h_leading_jet_pt[1]=new TH1F("h_leading_jet_pt_2jet","; p_{T} (GeV); Entries/10 GeV",50,0,500);
  h_subleading_jet_pt=new TH1F("h_subleading_jet_pt","; p_{T} (GeV); Entries/10 GeV",50,0,500);

  string evtype[4]={"4mu","4e","2mu2e","2e2mu"};
  int chi2max=25;
  if(outFilename.find("invD0")!=string::npos || outFilename.find("emu")!=string::npos || outFilename.find("sameSign")!=string::npos) chi2max=100;
  for(int i=0;i<4;i++){
    h_vtxChi2[i]=new TH1F(Form("h_vtxChi2_%s",evtype[i].c_str()),"; 4l vtx #chi^{2}",200,0,chi2max);
    h_vtxChi2_IsoD0[i]=new TH1F(Form("h_vtxChi2_IsoD0_%s",evtype[i].c_str()),"; 4l vtx #chi^{2}",200,0,chi2max);
    h_vtxChi2_FO[i]=new TH1F(Form("h_vtxChi2_FO_%s",evtype[i].c_str()),"; 4l vtx #chi^{2}",200,0,chi2max);
    h_vtxChi2_IsoD0_FO[i]=new TH1F(Form("h_vtxChi2_IsoD0_FO_%s",evtype[i].c_str()),"; 4l vtx #chi^{2}",200,0,chi2max);
  }
  h_vtxChi2All=new TH1F("h_vtxChi2All","; 4l vtx #chi^{2}",200,0,chi2max);
  h_vtxChi2All_FO=new TH1F("h_vtxChi2All_FO","; 4l vtx #chi^{2}",200,0,chi2max);
  string lep[2]={"el","mu"};
  string var[2]={"pt4l","njets"};
  string Zchan[2]= {"ee","mumu"};
  string pileup[2]={"lomu","himu"};
  float maxPt=300;
  if(cut==0) maxPt=500;
  double pt4l_bins[11]={0,10,15,20,30,45,60,80,120,200,350};
  h_pt_all_binned_tot=new TH1F("h_pt_all_binned_tot",";p_{T} [GeV]",10,pt4l_bins);
  h_njets_binned_tot=new TH1F("h_njets_binned_tot",";N_{jet};Entries",4,0,4);
  h_m4l_fsr_full=new TH1F("h_m4l_fsr_full",";m_{4l} [GeV];Entries / 5 GeV",80,0,400);
  h_lepton_pt=new TH1F("h_lepton_pt",";lepton p_{T} [GeV]; Events/5 GeV",40,0,200);
  h_lepton_eta=new TH1F("h_lepton_eta",";lepton #eta; Events/0.2",25,-2.5,2.5);
  h_pt_all_tot=new TH1F("h_pt_all_tot","; p_{T,4l} [GeV]; Events/10 GeV",30,0,300);
  h_eta_all_tot=new TH1F("h_eta_all_tot","; #eta_{4l}; Events/0.5",20,-5,5);
  for(int ilep=0;ilep<2;ilep++){
    for(int i=0;i<2;i++){
      h_pt_lep[i][ilep]=new TH1F(Form("h_pt_lep_%i_%s",i,lep[ilep].c_str()),"; p_{T} (GeV);Entries/5 GeV",20,0,100);
      h_eta_lep[i][ilep]=new TH1F(Form("h_eta_lep_%i_%s",i,lep[ilep].c_str()),";#eta;Entries",20,-2.5,2.5);
      h_caloIso_lep[i][ilep]=new TH1F(Form("h_caloIso_lep_%i_%s",i,lep[ilep].c_str()),";Calo Isolation",20,-1,1);
      h_trackIso_lep[i][ilep]=new TH1F(Form("h_trackIso_lep_%i_%s",i,lep[ilep].c_str()),";Track Isolation",20,0,1);
      h_d0Sig_lep[i][ilep]=new TH1F(Form("h_d0Sig_lep_%i_%s",i,lep[ilep].c_str()),"; d0 significance",20,0,5);
    }
    h_caloIso_comb_lep[ilep]=new TH1F(Form("h_caloIso_comb_lep_%s",lep[ilep].c_str()),";Calo Isolation",20,-1,1);
    h_trackIso_comb_lep[ilep]=new TH1F(Form("h_trackIso_comb_lep_%s",lep[ilep].c_str()),";Track Isolation",20,0,1);
    h_mLeadingZ[ilep]=new TH1F(Form("h_mLeadingZ_%s",Zchan[ilep].c_str()),";m_{Z1}",30,76,106);
    h_mLeadingZ_fullRange[ilep]=new TH1F(Form("h_mLeadingZ_fullRange_%s",Zchan[ilep].c_str()),";m_{Z1}",30,46,106);
    h_dRZ1[ilep]=new TH1F(Form("h_dRZ1_%s",Zchan[ilep].c_str()),";#Delta R",32,0,6.4);
    h_met[ilep]=new TH1F(Form("h_met_%s",Zchan[ilep].c_str()),";MET",50,0,500);
    h_njets[ilep]=new TH1F(Form("h_njets_%s",lep[ilep].c_str()),";N_{jet};Entries",8,0,8);
    h_njets_binned[ilep]=new TH1F(Form("h_njets_binned_%s",lep[ilep].c_str()),";N_{jet};Entries",4,0,4);
    h_leading_jet_pt_all[ilep]=new TH1F(Form("h_leading_jet_pt_all_%s",lep[ilep].c_str()),"; p_{T} (GeV); Entries/20 GeV",25,0,500);
    h_m4l_fsr_all[ilep]=new TH1F(Form("h_m4l_fsr_all_%s",lep[ilep].c_str()),";m_{4l} [GeV];Entries / 1 GeV",15,115,130);
    h_m4l_fsr_full_all[ilep]=new TH1F(Form("h_m4l_fsr_full_all_%s",lep[ilep].c_str()),";m_{4l} [GeV];Entries / 5 GeV",80,0,400);
    h_m4l_fsr_pileup_all[ilep]=new TH1F(Form("h_m4l_fsr_pileup_all_%s",pileup[ilep].c_str()),";m_{4l} [GeV];Entries / 5 GeV",80,0,400);
    h_pt_all[ilep]=new TH1F(Form("h_pt_all_%s",lep[ilep].c_str()),";p_{T,4l} [GeV];Events / 12 GeV",25,0.,300);
    h_eta_all[ilep]=new TH1F(Form("h_eta_all_%s",lep[ilep].c_str()),"; #eta_{4l}; Events/0.5",20,-5,5);
    h_pt_all_binned[ilep]=new TH1F(Form("h_pt_all_binned_%s",lep[ilep].c_str()),";p_{T} [GeV]",10,pt4l_bins);
    h_y_all[ilep]=new TH1F(Form("h_y_all_%s",lep[ilep].c_str()),";y;Entries / 0.12",25,-3,3);
    h_mZ1_all[ilep]=new TH1F(Form("h_mZ1_all_%s",lep[ilep].c_str()),";m_{12} [GeV]",20,50,110);
    h_mZ2_all[ilep]=new TH1F(Form("h_mZ2_all_%s",lep[ilep].c_str()),";m_{34}",25,0,125);
    h_cts_all[ilep]=new TH1F(Form("h_cts_all_%s",lep[ilep].c_str()),";cos(#theta^{*})",25,0,1);
    double mjjbins[11]={0,60,90,120,200,300,400,500,750,1000,3000};
    h_mjj_all[ilep]=new TH1F(Form("h_mjj_all_%s",lep[ilep].c_str()),";m_{jj}",10,mjjbins);
    double detajjbins[10]={0,0.5,1,1.5,2,3,4,6,8,10};
    h_detajj_all[ilep]=new TH1F(Form("h_detajj_all_%s",lep[ilep].c_str()),";#Delta#eta_{jj}",9,detajjbins);
    h_dphijj_all[ilep]=new TH1F(Form("h_dphijj_all_%s",lep[ilep].c_str()),";#Delta#phi_{jj}",9,0,2*3.14159625);
    h_nbjets_all[ilep]=new TH1F(Form("h_nbjets_all_%s",lep[ilep].c_str()),"N_{b-jets}",4,0,4);
    h_pt_0jet[ilep]=new TH1F(Form("h_pt_0jet_%s",lep[ilep].c_str()),";p_{T} [GeV];Entries / 25 GeV",12,0.,maxPt);
    h_pt_1jet[ilep]=new TH1F(Form("h_pt_1jet_%s",lep[ilep].c_str()),";p_{T} [GeV];Entries / 25 GeV",12,0.,maxPt);
    h_pt_2jet[ilep]=new TH1F(Form("h_pt_2jet_%s",lep[ilep].c_str()),";p_{T} [GeV];Entries / 25 GeV",12,0.,maxPt);
    h_Z1pT_all[ilep]=new TH1F(Form("h_Z1pT_all_%s",lep[ilep].c_str()),"",25,0,maxPt);
    h_sample_pt4l_all[ilep]=new TH2F(Form("h_sample_pt4l_all_%s",lep[ilep].c_str()),"",46,-0.5,45.5,25,0.,maxPt);
    h_HT_all[ilep]=new TH1F(Form("h_HT_all_%s",lep[ilep].c_str()),"",50,0,1000);
    h_minDR_leading_lep_jet_all[ilep]=new TH1F(Form("h_minDR_leading_lep_jet_all_%s",lep[ilep].c_str()),"",40,0,20);
    h_minDR_subleading_lep_jet_all[ilep]=new TH1F(Form("h_minDR_subleading_lep_jet_all_%s",lep[ilep].c_str()),"",40,0,20);
    h_mZ1_highpT[ilep]=new TH1F(Form("h_mZ1_highpT_%s",lep[ilep].c_str()),"",8,0,120);
    h_mZ2_highpT[ilep]=new TH1F(Form("h_mZ2_highpT_%s",lep[ilep].c_str()),"",8,0,120);
    h_caloIso_leading_highpT[ilep]=new TH1F(Form("h_caloIso_leading_highpT_%s",lep[ilep].c_str()),"",16,-1,15);
    h_trackIso_leading_highpT[ilep]=new TH1F(Form("h_trackIso_leading_highpT_%s",lep[ilep].c_str()),"",16,-1,15);
    h_d0Sig_leading_highpT[ilep]=new TH1F(Form("h_d0Sig_leading_highpT_%s",lep[ilep].c_str()),"",20,0,20);
    h_caloIso_subleading_highpT[ilep]=new TH1F(Form("h_caloIso_subleading_highpT_%s",lep[ilep].c_str()),"",16,-1,15);
    h_trackIso_subleading_highpT[ilep]=new TH1F(Form("h_trackIso_subleading_highpT_%s",lep[ilep].c_str()),"",16,-1,15);
    h_d0Sig_subleading_highpT[ilep]=new TH1F(Form("h_d0Sig_subleading_highpT_%s",lep[ilep].c_str()),"",20,0,20);
    h_nbtag70_highpT[ilep]=new TH1F(Form("h_nbtag70_highpT_%s",lep[ilep].c_str()),"",5,0,5);
    h_njets_highpT[ilep]=new TH1F(Form("h_njets_highpT_%s",lep[ilep].c_str()),"",10,0,10);
    h_minDR_leading_lep_jet_highpT[ilep]=new TH1F(Form("h_minDR_leading_lep_jet_highpT_%s",lep[ilep].c_str()),"",16,0,4);
    h_minDR_subleading_lep_jet_highpT[ilep]=new TH1F(Form("h_minDR_subleading_lep_jet_highpT_%s",lep[ilep].c_str()),"",16,0,4);
    for(int ivar=0;ivar<2;ivar++){
      for(int j=0;j<2;j++){
	h_leading_jet_pt_var[ilep][ivar][j]=new TH1F(Form("h_leading_jet_pt_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),"; p_{T} (GeV); Entries/10 GeV",25,0,500);
	h_m4l_fsr_var[ilep][ivar][j]=new TH1F(Form("h_m4l_fsr_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),";m [GeV];Entries / 2 GeV",20,110,150);
	h_pt_var[ilep][ivar][j]=new TH1F(Form("h_pt_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),";p_{T} [GeV];Entries / 6 GeV",25,0.,maxPt);
	h_y_var[ilep][ivar][j]=new TH1F(Form("h_y_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),";y;Entries / 0.1",25,-3,3);
	h_mZ1_var[ilep][ivar][j]=new TH1F(Form("h_mZ1_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),"",25,0,125);
	h_mZ2_var[ilep][ivar][j]=new TH1F(Form("h_mZ2_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),"",25,0,125);
	h_cts_var[ilep][ivar][j]=new TH1F(Form("h_cts_var_%s_%s_%i",lep[ilep].c_str(),var[ivar].c_str(),j),"",25,0,1);
      }
    }
  }

  //h_mu_sub_d0sig=new TH1F("h_mu_sub_d0sig","",100,-10,10);  
  //for (int i=0; i<MCType::nMAX; i++){
  // for (int j=0; j<5; j++) {
  //   INFO( j ); // << " " << MCType::strMCType[i].c_str() << std::endl);
    // h_el_nBL_perMCType[j] = new TH1F(Form("h_el_%s_nBL",MCType::strMCType[j].c_str()),";n_{BL};Entries",4,-0.5,3.5);
    // h_el_eProbHT_perMCType[j] = new TH1F(Form("h_el_%s_eProbHT",MCType::strMCType[j].c_str()),";p^{e}_{TRT};Entries",21,-0.025,1.025);
    // h_el_rTRT_perMCType[j] = new TH1F(Form("h_el_%s_rTRT",MCType::strMCType[j].c_str()),";r_{TRT};Entries",21,-0.025,1.025);
    // h_el_f1_perMCType[j] = new TH1F(Form("h_el_%s_f1",MCType::strMCType[j].c_str()),";f_{1};Entries",30,-0.2,1);	
  //  }

}

void FourLep_analysis :: setupTools ()
{
}
