#include "MinitreeAnalysis/TreeReader_FourLep.h"
#include <iostream>
FourLep::FourLep(string fname, string tname, TTree *tree) : fChain(0) 
{
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fname.c_str());
    if (!f || !f->IsOpen()) {
      f = TFile::Open(fname.c_str());
    }
    f->GetObject(tname.c_str(),tree);
    
  }
  Init(tree);
}

FourLep::~FourLep()
{
   if (!fChain) return;
   // delete fChain->GetCurrentFile();
}

Int_t FourLep::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t FourLep::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void FourLep::Init(TTree *tree)
{
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("event_type", &event_type, &b_event_type);
   fChain->SetBranchAddress("fsr_associated_lepton", &fsr_associated_lepton, &b_fsr_associated_lepton);
   fChain->SetBranchAddress("fsr_type", &fsr_type, &b_fsr_type);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
   fChain->SetBranchAddress("n_jets_btag70", &n_jets_btag70, &b_n_jets_btag70);
   fChain->SetBranchAddress("n_jets_truth_bare", &n_jets_truth_bare, &b_n_jets_truth_bare);
   fChain->SetBranchAddress("n_jets_truth_fid", &n_jets_truth_fid, &b_n_jets_truth_fid);
   fChain->SetBranchAddress("dijet_invmass", &dijet_invmass, &b_dijet_invmass);
   fChain->SetBranchAddress("dijet_deltaeta", &dijet_deltaeta, &b_dijet_deltaeta);
   fChain->SetBranchAddress("dijet_deltaphi", &dijet_deltaphi, &b_dijet_deltaphi);
   fChain->SetBranchAddress("prod_type", &prod_type, &b_prod_type);
   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("BDT_discriminant", &BDT_discriminant, &b_BDT_discriminant);
   fChain->SetBranchAddress("BDT_VH_noptHjj_discriminant",&BDT_VH,&b_BDT_VH);
   fChain->SetBranchAddress("BDT_TwoJet_discriminant",&BDT_VBF,&b_BDT_VBF);
   fChain->SetBranchAddress("BDT_1Jet_pt4l_60",&BDT_1Jet_pt4l_60,&b_BDT_1Jet_pt4l_60);
   fChain->SetBranchAddress("BDT_1Jet_pt4l_60_120",&BDT_1Jet_pt4l_60_120,&b_BDT_1Jet_pt4l_60_120);
   fChain->SetBranchAddress("BDT_1Jet_pt4l_120",&BDT_1Jet_pt4l_120,&b_BDT_1Jet_pt4l_120);
   fChain->SetBranchAddress("KD_discriminant", &KD_discriminant, &b_KD_discriminant);
   fChain->SetBranchAddress("eta4l_fsr", &eta4l_fsr, &b_eta4l_fsr);
   fChain->SetBranchAddress("eta4l_truth_born", &eta4l_truth_born, &b_eta4l_truth_born);
   fChain->SetBranchAddress("eta4l_truth_matched_bare", &eta4l_truth_matched_bare, &b_eta4l_truth_matched_bare);
   fChain->SetBranchAddress("eta4l_unconstrained", &eta4l_unconstrained, &b_eta4l_unconstrained);
   fChain->SetBranchAddress("fsr_eta", &fsr_eta, &b_fsr_eta);
   fChain->SetBranchAddress("fsr_phi", &fsr_phi, &b_fsr_phi);
   fChain->SetBranchAddress("fsr_pt", &fsr_pt, &b_fsr_pt);
   fChain->SetBranchAddress("fsr_pt_constrained", &fsr_pt_constrained, &b_fsr_pt_constrained);
   fChain->SetBranchAddress("m4l_constrained", &m4l_constrained, &b_m4l_constrained);
   fChain->SetBranchAddress("m4l_fsr", &m4l_fsr, &b_m4l_fsr);
   fChain->SetBranchAddress("m4l_truth_born", &m4l_truth_born, &b_m4l_truth_born);
   fChain->SetBranchAddress("m4l_truth_matched_bare", &m4l_truth_matched_bare, &b_m4l_truth_matched_bare);
   fChain->SetBranchAddress("m4l_unconstrained", &m4l_unconstrained, &b_m4l_unconstrained);
   fChain->SetBranchAddress("m4lerr_fsr", &m4lerr_fsr, &b_m4lerr_fsr);
   fChain->SetBranchAddress("m4lerr_unconstrained", &m4lerr_unconstrained, &b_m4lerr_unconstrained);
   fChain->SetBranchAddress("mZ1_constrained", &mZ1_constrained, &b_mZ1_constrained);
   fChain->SetBranchAddress("mZ1_fsr", &mZ1_fsr, &b_mZ1_fsr);
   fChain->SetBranchAddress("mZ1_truth_born", &mZ1_truth_born, &b_mZ1_truth_born);
   fChain->SetBranchAddress("mZ1_truth_matched_bare", &mZ1_truth_matched_bare, &b_mZ1_truth_matched_bare);
   fChain->SetBranchAddress("mZ1_unconstrained", &mZ1_unconstrained, &b_mZ1_unconstrained);
   fChain->SetBranchAddress("mZ2_constrained", &mZ2_constrained, &b_mZ2_constrained);
   fChain->SetBranchAddress("mZ2_fsr", &mZ2_fsr, &b_mZ2_fsr);
   fChain->SetBranchAddress("mZ2_truth_born", &mZ2_truth_born, &b_mZ2_truth_born);
   fChain->SetBranchAddress("mZ2_truth_matched_bare", &mZ2_truth_matched_bare, &b_mZ2_truth_matched_bare);
   fChain->SetBranchAddress("mZ2_unconstrained", &mZ2_unconstrained, &b_mZ2_unconstrained);
   fChain->SetBranchAddress("outBDT_gauss", &outBDT_gauss, &b_outBDT_gauss);
   fChain->SetBranchAddress("pt4l_fsr", &pt4l_fsr, &b_pt4l_fsr);
   fChain->SetBranchAddress("pt4l_truth_born", &pt4l_truth_born, &b_pt4l_truth_born);
   fChain->SetBranchAddress("pt4l_truth_matched_bare", &pt4l_truth_matched_bare, &b_pt4l_truth_matched_bare);
   fChain->SetBranchAddress("pt4l_unconstrained", &pt4l_unconstrained, &b_pt4l_unconstrained);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("weight_corr", &weight_corr, &b_weight_corr);
   fChain->SetBranchAddress("weight_lumi", &weight_lumi, &b_weight_lumi);
   fChain->SetBranchAddress("weight_ptDown", &weight_ptDown, &b_weight_ptDown);
   fChain->SetBranchAddress("weight_ptUp", &weight_ptUp, &b_weight_ptUp);
   fChain->SetBranchAddress("weight_sampleoverlap", &weight_sampleoverlap, &b_weight_sampleoverlap);
   fChain->SetBranchAddress("weight_couplings", &weight_couplings, &b_weight_couplings);
   fChain->SetBranchAddress("y4l_fsr", &y4l_fsr, &b_y4l_fsr);
   fChain->SetBranchAddress("y4l_truth_born", &y4l_truth_born, &b_y4l_truth_born);
   fChain->SetBranchAddress("y4l_truth_matched_bare", &y4l_truth_matched_bare, &b_y4l_truth_matched_bare);
   fChain->SetBranchAddress("y4l_unconstrained", &y4l_unconstrained, &b_y4l_unconstrained);
   //fChain->SetBranchAddress("leading_jet_pt", &leading_jet_pt, &b_leading_jet_pt);
   //fChain->SetBranchAddress("subleading_jet_pt", &subleading_jet_pt, &b_subleading_jet_pt);
   fChain->SetBranchAddress("pass_vtx4lCut", &pass_vtx4lCut, &b_pass_vtx4lCut);

   fChain->SetBranchAddress("el_nInnerExpPix", &el_nInnerExpPix, &b_el_nInnerExpPix);
   fChain->SetBranchAddress("lepton_pt", &lepton_pt, &b_lepton_pt);

   jet_pt=new vector<float>;
   jet_eta=new vector<float>;
   jet_phi=new vector<float>;
   jet_m=new vector<float>;
   jet_btag70=new vector<int>;
   fChain->SetBranchAddress("jet_pt",&jet_pt);
   fChain->SetBranchAddress("lepton_phi", &lepton_phi, &b_lepton_phi);
   fChain->SetBranchAddress("jet_phi",&jet_phi);
   fChain->SetBranchAddress("lepton_eta", &lepton_eta, &b_lepton_eta);
   fChain->SetBranchAddress("jet_eta",&jet_eta);
   fChain->SetBranchAddress("lepton_m", &lepton_m, &b_lepton_m);
   fChain->SetBranchAddress("jet_m",&jet_m);
   fChain->SetBranchAddress("lepton_d0sig", &lepton_d0Sig, &b_lepton_d0Sig);
   fChain->SetBranchAddress("lepton_passD0sig", &lepton_passd0Sig, &b_lepton_passd0Sig);
   fChain->SetBranchAddress("lepton_ptvarcone30", &lepton_ptvarcone30, &b_lepton_ptvarcone30);
   fChain->SetBranchAddress("lepton_ptvarcone20", &lepton_ptvarcone20, &b_lepton_ptvarcone20);
   fChain->SetBranchAddress("lepton_topoetcone20", &lepton_topoetcone20, &b_lepton_topoetcone20);
   fChain->SetBranchAddress("jet_btag70",&jet_btag70);
   fChain->SetBranchAddress("lepton_passIsoCut",&lepton_passIso,&b_lepton_passIso);
   fChain->SetBranchAddress("cthstr",&cthstr,&b_cthstr);
   fChain->SetBranchAddress("el_MCClass",&el_MCClass,&b_el_MCClass);
   fChain->SetBranchAddress("met_et",&met,&b_met);
   fChain->SetBranchAddress("lepton_iso_weight",&lepton_isoWeight,&b_lepton_isoWeight);
   fChain->SetBranchAddress("w_xs",&w_xs,&b_w_xs);
   fChain->SetBranchAddress("vtx4l_chi2ndf",&vtx4lchi2,&b_vtx4lchi2);
   fChain->SetBranchAddress("ave_int_per_xing",&pileupMu,&b_pileupMu);
   fChain->SetBranchAddress("mu_IDpt",&idpt,&b_idpt);
   fChain->SetBranchAddress("mu_MSpt",&mspt,&b_mspt);
   fChain->SetBranchAddress("lepton_id",&lepton_quality,&b_lepton_quality);
   fChain->SetBranchAddress("n_extraLep",&n_extraLep,&b_n_extraLep);
}
