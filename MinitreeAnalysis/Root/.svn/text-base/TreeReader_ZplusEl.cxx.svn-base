#include "MinitreeAnalysis/TreeReader_ZplusEl.h"

ZplusEl::ZplusEl(string fname, string tname, TTree *tree) : fChain(0) 
{
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fname.c_str());
    if (!f || !f->IsOpen()) {
      f = TFile::Open(fname.c_str());
    }
    f->GetObject(tname.c_str(),tree);
    
  }
  Init(tree);
}

ZplusEl::~ZplusEl()
{
  if (!fChain) return;
  //delete fChain->GetCurrentFile();
}

Int_t ZplusEl::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ZplusEl::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void ZplusEl::Init(TTree *tree)
{
   // Set object pointer
   trigger = 0;

   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);


   // Set branch addresses and branch pointers

   fChain->SetBranchAddress("trigDecisionOK", &trigDecisionOK, &b_trigDecisionOK);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("event_type", &event_type, &b_event_type);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("avgMu", &avgMu, &b_avgMu);
   fChain->SetBranchAddress("npv", &npv, &b_npv);

   fChain->SetBranchAddress("trigger", &trigger, &b_trigger);
   
   fChain->SetBranchAddress("eventMET_et", &eventMET_et, &b_eventMET_et);
   fChain->SetBranchAddress("eventMET_sumet", &eventMET_sumet, &b_eventMET_sumet);
   fChain->SetBranchAddress("eventMET_phi", &eventMET_phi, &b_eventMET_phi);

   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("w_lumi", &w_lumi, &b_w_lumi);
   fChain->SetBranchAddress("w_MCw", &w_MCw, &b_w_MCw);
   fChain->SetBranchAddress("w_pileup", &w_pileup, &b_w_pileup);

   fChain->SetBranchAddress("mZ_unconstrained", &mZ_unconstrained, &b_mZ_unconstrained);

   fChain->SetBranchAddress("el_Deta1", &el_Deta1, &b_el_Deta1);
   fChain->SetBranchAddress("el_Reta", &el_Reta, &b_el_Reta);
   fChain->SetBranchAddress("el_Rphi", &el_Rphi, &b_el_Rphi);
   fChain->SetBranchAddress("lepton_d0sig", &lepton_d0Sig, &b_lepton_d0Sig);
   fChain->SetBranchAddress("el_eOverP", &el_eOverP, &b_el_eOverP);
   fChain->SetBranchAddress("el_eProbHT", &el_eProbHT, &b_el_eProbHT);
   fChain->SetBranchAddress("el_f1", &el_f1, &b_el_f1);
   fChain->SetBranchAddress("el_Rhad",el_Rhad,&b_el_Rhad);
   fChain->SetBranchAddress("el_f3",el_f3,&b_el_f3);

   fChain->SetBranchAddress("el_rTRT", &el_rTRT, &b_el_rTRT);
   fChain->SetBranchAddress("el_nTRT", &el_nTRT, &b_el_nTRT);
   fChain->SetBranchAddress("el_isLooseLHnoBL", &el_isLooseLHnoBL, &b_el_isLooseLHnoBL);

   fChain->SetBranchAddress("el_MCClass", &el_MCClass, &b_el_MCClass);
   fChain->SetBranchAddress("el_nBL", &el_nBL, &b_el_nBL);
   fChain->SetBranchAddress("el_nInnerExpPix", &el_nInnerExpPix, &b_el_nInnerExpPix);
   fChain->SetBranchAddress("el_isTrigMatched", &el_isTrigMatched, &b_el_isTrigMatched);
   fChain->SetBranchAddress("el_weight", &el_weight, &b_el_weight);      
   
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_nBL", &mu_nBL, &b_mu_nBL);
   fChain->SetBranchAddress("mu_nPix", &mu_nPix, &b_mu_nPix);
   fChain->SetBranchAddress("mu_nPrecHoleLayer", &mu_nPrecHoleLayer, &b_mu_nPrecHoleLayer);
   fChain->SetBranchAddress("mu_nPrecLayer", &mu_nPrecLayer, &b_mu_nPrecLayer);
   fChain->SetBranchAddress("mu_nSCT", &mu_nSCT, &b_mu_nSCT);
   fChain->SetBranchAddress("mu_nSiHoles", &mu_nSiHoles, &b_mu_nSiHoles);
   fChain->SetBranchAddress("mu_nTRT", &mu_nTRT, &b_mu_nTRT);
   fChain->SetBranchAddress("mu_type", &mu_type, &b_mu_type);

   fChain->SetBranchAddress("mu_CaloLRLikelihood", &mu_CaloLRLikelihood, &b_mu_CaloLRLikelihood);
   fChain->SetBranchAddress("mu_IDeta", &mu_IDeta, &b_mu_IDeta);
   fChain->SetBranchAddress("mu_IDpt", &mu_IDpt, &b_mu_IDpt);
   fChain->SetBranchAddress("mu_MSeta", &mu_MSeta, &b_mu_MSeta);
   fChain->SetBranchAddress("mu_MSpt", &mu_MSpt, &b_mu_MSpt);
   fChain->SetBranchAddress("mu_d0Sig", &mu_d0Sig, &b_mu_d0Sig);
   fChain->SetBranchAddress("mu_momBalanceSig", &mu_momBalanceSig, &b_mu_momBalanceSig);
   fChain->SetBranchAddress("mu_isTrigMatched", &mu_isTrigMatched, &b_mu_isTrigMatched);
   fChain->SetBranchAddress("mu_weight", &mu_weight, &b_mu_weight);

   fChain->SetBranchAddress("lepton_pt", &lepton_pt, &b_lepton_pt);
   fChain->SetBranchAddress("lepton_eta", &lepton_eta, &b_lepton_eta);
   fChain->SetBranchAddress("lepton_phi", &lepton_phi, &b_lepton_phi);
   fChain->SetBranchAddress("lepton_m", &lepton_m, &b_lepton_m);
   fChain->SetBranchAddress("lepton_id", &lepton_id, &b_lepton_id);
   fChain->SetBranchAddress("lepton_charge", &lepton_charge, &b_lepton_charge);
   fChain->SetBranchAddress("lepton_ptvarcone30", &lepton_ptvarcone30, &b_lepton_ptvarcone30);
   fChain->SetBranchAddress("lepton_ptvarcone20", &lepton_ptvarcone20, &b_lepton_ptvarcone20);
   fChain->SetBranchAddress("lepton_topoetcone20", &lepton_topoetcone20, &b_lepton_topoetcone20);
   fChain->SetBranchAddress("lepton_passD0sig", &lepton_passD0Sig, &b_lepton_passD0Sig);
   fChain->SetBranchAddress("lepton_passIsoCut", &lepton_passIsoCut, &b_lepton_passIsoCut);
   fChain->SetBranchAddress("lepton_quality", &lepton_quality, &b_lepton_quality);
   fChain->SetBranchAddress("random_run", &random_run, &b_random_run);
}
