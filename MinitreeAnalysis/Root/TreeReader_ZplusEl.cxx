#include "MinitreeAnalysis/TreeReader_ZplusEl.h"
#include <iostream>

ZplusEl::ZplusEl(string fname, string tname, TChain *tree) : fChain(0), isData(false)
{
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fname.c_str());
    if (!f || !f->IsOpen()) {
      f = TFile::Open(fname.c_str());
    }
    f->GetObject(tname.c_str(),tree);
    
  }
  Init(tree);
}

ZplusEl::~ZplusEl()
{
  if (!fChain) return;
  //delete fChain->GetCurrentFile();
}

Int_t ZplusEl::GetEntry(Long64_t entry)
{
// Read contents of entry.
  if (!fChain){
    std::cout<<"no tree!?!"<<std::endl;
    return 0;
  }
  return fChain->GetEntry(entry);
}
Long64_t ZplusEl::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void ZplusEl::Init(TChain *tree)
{
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   n_jets=0;
   //w_xs=0;
   lepton_pt=new vector<float>;
   lepton_eta=new vector<float>;
   lepton_phi=new vector<float>;
   lepton_m=new vector<float>;
   mu_type=new vector<int>;
   lepton_d0Sig=new vector<float>;
   mu_IDpt=new vector<float>;
   mu_MSpt=new vector<float>;
   lepton_charge=new vector<int>;
   lepton_ptvarcone30_tightTTVA=new vector<float>;
   lepton_ptcone20_tightTTVA=new vector<float>;
   lepton_neflowisol20=new vector<float>;
   lepton_passD0Sig=new vector<int>;
   lepton_passIsoCut=new vector<int>;
   lepton_id=new vector<int>;
   el_MCClass=new vector<int>;
   el_nInnerExpPix=new vector<int>;
   el_q=new vector<int>;

   // Set branch addresses and branch pointers

   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("event_type", &event_type, &b_event_type);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("run", &run, &b_run);

   fChain->SetBranchAddress("weight", &weight, &b_weight);
   //fChain->SetBranchAddress("w_lumi", &w_lumi, &b_w_lumi);
   //fChain->SetBranchAddress("w_MCw", &w_MCw, &b_w_MCw);
   //fChain->SetBranchAddress("w_pileup", &w_pileup, &b_w_pileup);

   fChain->SetBranchAddress("mZ_unconstrained", &mZ_unconstrained, &b_mZ_unconstrained);
   fChain->SetBranchAddress("mZ_constrained", &mZ_constrained, &b_mZ_constrained);

   fChain->SetBranchAddress("lepton_d0sig", &lepton_d0Sig, &b_lepton_d0Sig);
   if (!isData) fChain->SetBranchAddress("el_MCClass", &el_MCClass, &b_el_MCClass);
   fChain->SetBranchAddress("el_nInnerExpPix", &el_nInnerExpPix, &b_el_nInnerExpPix);
   fChain->SetBranchAddress("mu_type", &mu_type, &b_mu_type);

   fChain->SetBranchAddress("mu_IDpt", &mu_IDpt, &b_mu_IDpt);
   fChain->SetBranchAddress("mu_MSpt", &mu_MSpt, &b_mu_MSpt);

   fChain->SetBranchAddress("lepton_pt", &lepton_pt, &b_lepton_pt);
   fChain->SetBranchAddress("lepton_eta", &lepton_eta, &b_lepton_eta);
   fChain->SetBranchAddress("lepton_phi", &lepton_phi, &b_lepton_phi);
   fChain->SetBranchAddress("lepton_m", &lepton_m, &b_lepton_m);
   fChain->SetBranchAddress("lepton_id", &lepton_id, &b_lepton_id);
   fChain->SetBranchAddress("lepton_charge", &lepton_charge, &b_lepton_charge);
   //fChain->SetBranchAddress("lepton_ptvarcone30", &lepton_ptvarcone30, &b_lepton_ptvarcone30);
   fChain->SetBranchAddress("lepton_ptvarcone30_TightTTVA_pt500",&lepton_ptvarcone30_tightTTVA, &b_lepton_ptvarcone30_tightTTVA);
   fChain->SetBranchAddress("lepton_ptcone20_TightTTVA_pt500",&lepton_ptcone20_tightTTVA, &b_lepton_ptcone20_tightTTVA);
   //fChain->SetBranchAddress("lepton_ptvarcone20", &lepton_ptvarcone20, &b_lepton_ptvarcone20);
   //fChain->SetBranchAddress("lepton_topoetcone20", &lepton_topoetcone20, &b_lepton_topoetcone20);
   fChain->SetBranchAddress("lepton_neflowisol20", &lepton_neflowisol20, &b_lepton_neflowisol20);
   fChain->SetBranchAddress("lepton_passD0sig", &lepton_passD0Sig, &b_lepton_passD0Sig);
   fChain->SetBranchAddress("lepton_passIsoCut", &lepton_passIsoCut, &b_lepton_passIsoCut);
   fChain->SetBranchAddress("n_jets",&n_jets,&b_n_jets);
//   fChain->SetBranchAddress("lepton_iso_weight",&lepton_isoWeight,&b_lepton_isoWeight);
   //fChain->SetBranchAddress("w_xs",&w_xs,&b_w_xs);
   fChain->SetBranchAddress("ave_int_per_xing",&mu,&b_mu);
}
