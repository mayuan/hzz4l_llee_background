#!/bin/bash

#################################################################################
# This script with run the histogramming code on the minitrees, merge and copy the output.
# The minitrees must be organized in a specific structure that is the following:
#    PATH/SAMPLE/merge_final/BLAH.BLAH.BLAH_minitree.root
# where 'merge_final' is the directory coming out of the postProcess script.
# Just define the PATH below and the output directory and you are ready to go.

NTUPLES=/afs/cern.ch/work/i/inomidis/public/H4l/prod_reg6_v4
OUTPUT=/afs/cern.ch/user/i/inomidis/testarea/H4l/HZZxAODAnalysis/MinitreeAnalysis/run/hists/

#################################################################################

## clean up any previous histograms
rm ${NTUPLES}/*/merged_final/*_hists.root

## run the code
for i in ${NTUPLES}/*/merged_final/*_minitree.root; do runZplusXTreeAnalysis -i $i; done 

## copy the output to target directory
mkdir $OUTPUT
for SAMPLE in data ttbar WZ_Powheg ZZ_Powheg Zjets_Sherpa Zjets_Powheg; do
    echo "--> Merging output for === $SAMPLE ===";
    hadd -f $OUTPUT/hists.$SAMPLE.root $NTUPLES/$SAMPLE/merged_final/*_hists.root 
done

echo "All done. Created the following files under $OUTPUT"
ls $OUTPUT
