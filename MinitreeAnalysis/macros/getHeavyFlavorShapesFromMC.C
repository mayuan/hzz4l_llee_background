#include "../../H4lBackgroundReader/H4lBackgroundReader/H4lBkgEnums.h"

void getHeavyFlavorShapesFromMC(TString year){
  TH1::SetDefaultSumw2();
  TChain* chain=new TChain("tree_relaxIsoD0","chain");
  std::map<TString,TString> campaign;
  campaign["2016"]="mc16a";
  campaign["2017"]="mc16d";
  campaign["2018"]="mc16e";
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.342556.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4_m4l_100_150_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.343212.Powheggg2vvPythia8EvtGen_gg_ZZ_bkg_2e2mu_13TeV_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.343213.Powheggg2vvPythia8EvtGen_gg_ZZ_bkg_4l_noTau_13TeV_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.343232.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4_m4l_500_13000_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.361603.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.344171.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_4lMFilt_40_8_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.410009.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_dil_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.344295.Sherpa_NNPDF30NNLO_Zee_4lMassFilter40GeV8GeV_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.344296.Sherpa_NNPDF30NNLO_Zmumu_4lMassFilter40GeV8GeV_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.344297.Sherpa_NNPDF30NNLO_Zee_3lPtFilter4GeV_4lMassVeto40GeV8GeV_bkgCR.root");
  //chain->Add("/afs/cern.ch/atlas/groups/HSG2/H4l/run2/2016/MiniTrees/Prod_v09/mc/Background/BkgCR/mc15_13TeV.344298.Sherpa_NNPDF30NNLO_Zmumu_3lPtFilter4GeV_4lMassVeto40GeV8GeV_bkgCR.root");
  //chain->Add("/nfs/dust/atlas/user/naranjo/minitrees/prod_v17/mc16d/merged/BkgCR/Zjet.root");
  //chain->Add("/nfs/dust/atlas/user/naranjo/minitrees/prod_v17/mc16d/merged/BkgCR/ttbar.root");
  //chain->Add("/nfs/dust/atlas/user/naranjo/minitrees/prod_v17/mc16d/merged/BkgCR/WZ.root");
  //chain->Add(Form("Zjet_%s.root",year.Data()));
  //chain->Add(Form("ttbar_%s.root",year.Data()));
  //chain->Add(Form("WZ_%s.root",year.Data()));
  if(year.CompareTo("all")!=0){
    cout<<"run on year "<<year<<endl;
    chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+campaign[year]+"/Background/BkgCR/*ttbar*");
    chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+campaign[year]+"/Background/BkgCR/*Zee*");
    chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+campaign[year]+"/Background/BkgCR/*Zmumu*");
    chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+campaign[year]+"/Background/BkgCR/*WZlvll*");
    //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+campaign[year]+"/Background/BkgCR/*ttbar*");
    //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+campaign[year]+"/Background/BkgCR/*Zee*");
    //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+campaign[year]+"/Background/BkgCR/*Zmumu*");
    //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+campaign[year]+"/Background/BkgCR/*WZlvll*");
  }
  else{
    for(std::map<TString,TString>::iterator cit=campaign.begin();cit!=campaign.end();++cit){
      cout<<"add files for "<<(*cit).second<<endl;
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+(*cit).second+"/Background/BkgCR/*ttbar*");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+(*cit).second+"/Background/BkgCR/*Zee*");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+(*cit).second+"/Background/BkgCR/*Zmumu*");
      chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v25/AntiKt4EMPFlow/"+(*cit).second+"/Background/BkgCR/*WZlvll*");
      //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+(*cit).second+"/Background/BkgCR/*ttbar*");
      //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+(*cit).second+"/Background/BkgCR/*Zee*");
      //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+(*cit).second+"/Background/BkgCR/*Zmumu*");
      //chain->Add("/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/2018/MiniTrees/Prod_v20Fix/"+(*cit).second+"/Background/BkgCR/*WZlvll*");
    }
  }
  //chain->Add("/nfs/dust/atlas/user/naranjo/minitrees/newiso/mc16d/merged/BkgCR/ttbar.root");
  //chain->Add("/afs/cern.ch/work/i/inomidis/public/H4l/ntuples/Prod09/ntuple.ttbar.root");
  //chain->Add("/afs/cern.ch/work/i/inomidis/public/H4l/ntuples/Prod09/ntuple.Zjets.root");
  //chain->Add("/afs/cern.ch/work/i/inomidis/public/H4l/ntuples/Prod09/ntuple.ZZ.root");

  TString ch_llee = "(event_type==1 || event_type==2)";
  double weight=1.;
  float mZ1_fsr=0,mZ2_fsr=0,y4l_fsr=0,cthstr=0,m4l_fsr=0,dijet_invmass=0,dijet_deltaeta=0,dijet_deltaphi=0,cth1,cth2,phi1,phi,pt4l_fsr,met,m4lj,m4ljj,phi4l_unconstrained,eta4l_fsr,bdt,pt4lj,pt4ljj;
  int n_jets=0,quadtype=0,ptype,ptypefine,ptypesb,n_bjets,run=0,n_extraLep=-9,vtx=0,n_bjets60;
  std::vector<int>* in_mcClass=new std::vector<int>;
  //int in_mcClass[4];
  std::vector<float> *jetpt = new std::vector<float>;
  std::vector<float> *jetm = new std::vector<float>;
  std::vector<float> *jetphi = new std::vector<float>;
  std::vector<float> *jeteta = new std::vector<float>;
  std::vector<float> *lepphi = new std::vector<float>;
  std::vector<float> *lepeta = new std::vector<float>;
  std::vector<float> *leppt = new std::vector<float>;
  std::vector<int>* lepton_passD0sig=new std::vector<int>;
  std::vector<int>* lepton_passIsoCut=new std::vector<int>;
  //int lepton_passD0sig[4];
  //int lepton_passIsoCut[4];
  //std::vector<float>* lepton_ptvarcone30=new std::vector<float>;
  //std::vector<float>* lepton_ptvarcone20=new std::vector<float>; 
  //std::vector<float>* ptvarcone30_tightTTVA=new std::vector<float>;
  //std::vector<float>* ptvarcone20_tightTTVA=new std::vector<float>;
  //std::vector<float>* neflowisol20=new std::vector<float>;
  //std::vector<float>* topoetcone20=new std::vector<float>;
  //float ptvarcone30_tightTTVA[4];
  //float ptvarcone20_tightTTVA[4];
  //float neflowisol20[4];
  float mu;
  float NN_1Jet_pTLow_ZZ,NN_1Jet_pTMed_ZZ,NN_2Jet_Low_VH,NN_ttHHad_ttV;
  float cpobs1,cpobs2,cpobs3,cpobs4,cpobs5,cpobs6;
  float OO1_jj_tCzz,OO1_jj_tCza,OO1_jj_cHWtil,OO1_4l_tCzz,OO1_4l_tCza,OO1_4l_tCaa,OO1_4l_cHWtil,OO1_4l_cHBtil,OO1_4l_cHWBtil;
  float ptSortLep[4];
  float ptSortLepEta[4];
  float ptSortLepPhi[4];
  bool useTreeProdType=true;
  if(!chain->FindBranch("prod_type_fine")) useTreeProdType=false;
  if(useTreeProdType) cout<<"prod_type_fine in trees"<<endl;
  chain->SetBranchAddress("run",&run);
  chain->SetBranchAddress("weight",&weight);
  chain->SetBranchAddress("event_type",&quadtype);
  chain->SetBranchAddress("mZ1_fsr",&mZ1_fsr);
  chain->SetBranchAddress("el_MCClass",&in_mcClass);
  chain->SetBranchAddress("n_extraLep",&n_extraLep);
  chain->SetBranchAddress("mZ2_fsr",&mZ2_fsr);
  chain->SetBranchAddress("y4l_fsr",&y4l_fsr);
  chain->SetBranchAddress("cthstr_fsr",&cthstr);
  chain->SetBranchAddress("cth1_fsr",&cth1);
  chain->SetBranchAddress("cth2_fsr",&cth2);
  chain->SetBranchAddress("phi1_fsr",&phi1);
  chain->SetBranchAddress("phi_fsr",&phi);
  chain->SetBranchAddress("jet_pt",&jetpt);
  chain->SetBranchAddress("jet_m",&jetm);
  chain->SetBranchAddress("jet_phi",&jetphi);
  chain->SetBranchAddress("jet_eta",&jeteta);
  chain->SetBranchAddress("n_jets",&n_jets);
  chain->SetBranchAddress("m4l_fsr",&m4l_fsr);
  chain->SetBranchAddress("phi4l_unconstrained",&phi4l_unconstrained);
  chain->SetBranchAddress("pt4l_fsr",&pt4l_fsr);
  chain->SetBranchAddress("eta4l_fsr",&eta4l_fsr);
  chain->SetBranchAddress("lepton_passD0sig",&lepton_passD0sig);
  chain->SetBranchAddress("lepton_passIsoCut",&lepton_passIsoCut);
  //chain->SetBranchAddress("lepton_ptvarcone30",&lepton_ptvarcone30);
  //chain->SetBranchAddress("lepton_ptvarcone20",&lepton_ptvarcone20);
  //chain->SetBranchAddress("lepton_ptvarcone30_TightTTVA_pt1000",&ptvarcone30_tightTTVA);
  //chain->SetBranchAddress("lepton_topoetcone20",&topoetcone20);
  chain->SetBranchAddress("lepton_eta",&lepeta);
  chain->SetBranchAddress("lepton_phi",&lepphi);
  chain->SetBranchAddress("lepton_pt",&leppt);
  chain->SetBranchAddress("pass_vtx4lCut",&vtx);
  chain->SetBranchAddress("prod_type",&ptype);
  if(useTreeProdType){
    chain->SetBranchAddress("prod_type_fine",&ptypefine);
    chain->SetBranchAddress("prod_type_SB",&ptypesb);
  }
  chain->SetBranchAddress("NN_1Jet_pTLow_ZZ",&NN_1Jet_pTLow_ZZ);
  chain->SetBranchAddress("NN_1Jet_pTMed_ZZ",&NN_1Jet_pTMed_ZZ);
  chain->SetBranchAddress("NN_2Jet_Low_VH",&NN_2Jet_Low_VH);
  chain->SetBranchAddress("NN_ttHHad_ttV",&NN_ttHHad_ttV);
  chain->SetBranchAddress("dijet_invmass",&dijet_invmass);
  chain->SetBranchAddress("dijet_deltaeta",&dijet_deltaeta);
  chain->SetBranchAddress("dijet_deltaphi",&dijet_deltaphi);
  chain->SetBranchAddress("n_jets_btag70",&n_bjets);
  chain->SetBranchAddress("n_jets_btag60",&n_bjets60);
  chain->SetBranchAddress("met_et",&met);
  chain->SetBranchAddress("m4lj_fsr",&m4lj);
  chain->SetBranchAddress("m4ljj_fsr",&m4ljj);
  chain->SetBranchAddress("ave_int_per_xing",&mu);
  if(chain->FindBranch("BDT_Massdiscriminant")) chain->SetBranchAddress("BDT_Massdiscriminant",&bdt);
  else chain->SetBranchAddress("BDT_discriminant",&bdt);
  chain->SetBranchAddress("pt4lj_fsr",&pt4lj);
  chain->SetBranchAddress("pt4ljj_fsr",&pt4ljj);
  chain->SetBranchAddress("cp_obs1",&cpobs1);
  chain->SetBranchAddress("cp_obs2",&cpobs2);
  chain->SetBranchAddress("cp_obs3",&cpobs3);
  chain->SetBranchAddress("cp_obs4",&cpobs4);
  chain->SetBranchAddress("cp_obs5",&cpobs5);
  chain->SetBranchAddress("cp_obs6",&cpobs6);
  chain->SetBranchAddress("OO1_4l_cHWBtil1",&OO1_4l_cHWBtil);
  chain->SetBranchAddress("OO1_4l_cHWtil1",&OO1_4l_cHWtil);
  chain->SetBranchAddress("OO1_4l_cHBtil1",&OO1_4l_cHBtil);
  chain->SetBranchAddress("OO1_4l_tCzz1",&OO1_4l_tCzz);
  chain->SetBranchAddress("OO1_4l_tCza1",&OO1_4l_tCza);
  chain->SetBranchAddress("OO1_4l_tCaa1",&OO1_4l_tCaa);
  chain->SetBranchAddress("OO1_jj_tCzz1",&OO1_jj_tCzz);
  chain->SetBranchAddress("OO1_jj_tCza1",&OO1_jj_tCza);
  chain->SetBranchAddress("OO1_jj_cHWtil1",&OO1_jj_cHWtil);

  double pt4l_incl_bins[12]={0,10,20,30,45,60,80,120,200,300,650,1000};
  TH2F* pt4l_incl=new TH2F("pt4l_incl","",11,pt4l_incl_bins,11,105,160);
  TH2F* pt4l_jet[4];
  double pt4l_0jet_bins[5]={0,15,30,120,350};
  pt4l_jet[0]=new TH2F("pt4l_0jet","",4,pt4l_0jet_bins,11,105,160);
  double pt4l_1jet_bins[5]={0,60,80,120,350};
  pt4l_jet[1]=new TH2F("pt4l_1jet","",4,pt4l_1jet_bins,11,105,160);
  double pt4l_2jet_bins[3]={0,120,350};
  pt4l_jet[2]=new TH2F("pt4l_2jet","",2,pt4l_2jet_bins,11,105,160);
  pt4l_jet[3]=new TH2F("pt4l_3jet","",2,pt4l_2jet_bins,11,105,160);
  double m12_bins[5]={50,64,73,85,106};
  TH2F* m12=new TH2F("m12","",4,m12_bins,11,105,160);
  double m34_bins[8]={12,20,24,28,32,40,55,65};
  TH2F* m34=new TH2F("m34","",7,m34_bins,11,105,160);
  TH2F* m12_m34_2d=new TH2F("m12_m34_2d","",6,m12_bins,7,m34_bins);
  double m34_m4l_bins[5]={12,24,32,55,115};
  TH2F* m34_m4l=new TH2F("m34_m4l","",4,m34_m4l_bins,11,105,160);
  double y4l_bins[11]={0,0.15,0.3,0.45,0.6,0.75,0.9,1.2,1.6,2,2.5};
  TH2F* y4l=new TH2F("y4l","",10,y4l_bins,11,105,160);
  TH2F* njet=new TH2F("njet_diff","",4,0,4,11,105,160);
  //these are for setting normalizations for the 2d case
  //TH1F* njet_full=new TH1F("njet_full","",5,0,5);
  //double y4l_full_bins[5]={0,0.5,1,1.5,2.5};
  //TH1F* y4l_full=new TH1F("y4l_full","",5,y4l_full_bins);
  TH2F* cts=new TH2F("cts","",8,0,1,11,105,160);
  double jet1pt_bins[5]={29,30,60,120,350};
  TH2F* jet1pt=new TH2F("jet1pt","",4,jet1pt_bins,11,105,160);
  TH2F* jet2pt=new TH2F("jet2pt","",4,jet1pt_bins,11,105,160);
  TH2F* m12_m34=new TH2F("m12_m34","",5,0,5,11,105,160);
  TH1F* HFTot=new TH1F("HFTot","",2,-0.5,1.5);
  double m4l_bins[19]={60,80,100,115,130,140,160,180,200,220,240,260,280,300,320,340,360,380,400};
  TH2F* m4l=new TH2F("m4l","",23, 55.000000, 400.000000,1,0,1);
  TH2F* m4l_0jet=new TH2F("m4l_0jet","",23, 55.000000, 400.000000,1,0,1);
  TH2F* m4l_1jet=new TH2F("m4l_1jet","",23, 55.000000, 400.000000,1,0,1);
  TH2F* m4l_2jet=new TH2F("m4l_2jet","",23, 55.000000, 400.000000,1,0,1);
  //double m4l_diffxs_binning[18]={55,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,400};
  //TH2F* m4l_diffxs=new TH2F("m4l_diffxs","",17,m4l_diffxs_binning,1,0,1);
  double m4l_diffxs_binning[8]={55,115,118,121,124,127,130,400};
  TH2F* m4l_diffxs=new TH2F("m4l_diffxs","",7,m4l_diffxs_binning,1,0,1);
  double m4l_diffxs_binning2[18]={55,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,400}; //{55,115,118,129,400};
  TH2F* m4l_diffxs2=new TH2F("m4l_diffxs2","",17,m4l_diffxs_binning2,1,0,1);
  TH2F* h_cth1=new TH2F("cth1","",8,-1,1,11,105,160);
  TH2F* h_cth2=new TH2F("cth2","",8,-1,1,11,105,160);
  TH2F* h_phi1=new TH2F("phi1","",8,-3.14159265,3.14159265,11,105,160);
  TH2F* h_phi=new TH2F("phi","",8,-3.14159265,3.14159265,11,105,160);
  double pt4l_y4l_bins[4]={0,45,120,350};
  TH2F* pt4l_y4l[4];
  for(int i=0;i<4;i++) pt4l_y4l[i]=new TH2F(Form("pt4l_y4l%i",i),"",3,pt4l_y4l_bins,11,105,160);
  TH2F* pt4l_jet1pt=new TH2F("pt4l_jet1pt","",7,0,7,11,105,160);
  double m4lj_bins[8]={119,120,180,220,300,400,600,2000};
  TH2F* h_m4lj=new TH2F("m4lj","",7,m4lj_bins,11,105,160);
  double m4ljj_bins[7]={179,180,320,450,600,1000,2500};
  TH2F* h_m4ljj=new TH2F("m4ljj","",6,m4ljj_bins,11,105,160);
  double pt4lj_bins[5]={0,1,61,121,351};
  TH2F* h_pt4lj=new TH2F("pt4lj","",4,pt4lj_bins,11,105,160);
  double pt4ljj_bins[5]={0,1,61,121,351};
  TH2F* h_pt4ljj=new TH2F("pt4ljj","",4,pt4ljj_bins,11,105,160);

  TH1F* HFTest=new TH1F("HFTest","",1,0,1);
  TH1F* prod_type=new TH1F("prod_type","",H4lBkg::toInt(H4lBkg::Category::ProdType::Max),-0.5,H4lBkg::toInt(H4lBkg::Category::ProdType::Max)-0.5);
  TH2F* prod_type_m4l=new TH2F("prod_type_m4l","",2,-0.5,1.5,28,-0.5,27.5);
  double mjjbins[5]={0,1,2,4,7};
  TH2F* mjj=new TH2F("mjj","",4,mjjbins,11,105,160);
  double detajjbins[5]={0,1,2,3.5,10};
  TH2F* detajj=new TH2F("detajj","",4,detajjbins,11,105,160);
  double dphijj_bins[6]={0,1,3.14159265/2+1,3.14159265+1,3.14159265*3/2+1,2*3.14159265+1};
  TH2F* dphijj=new TH2F("dphijj","",5,dphijj_bins,11,105,160);
  TH2F* nbjet=new TH2F("nbjet","",3,0,3,11,105,160);
  TH2F* cp_obs1=new TH2F("cp_obs1","",8,-1,1,11,105,160);
  TH2F* cp_obs2=new TH2F("cp_obs2","",8,-1,1,11,105,160);
  TH2F* cp_obs3=new TH2F("cp_obs3","",8,-1,1,11,105,160);
  TH2F* cp_obs4=new TH2F("cp_obs4","",8,-1,1,11,105,160);
  TH2F* cp_obs5=new TH2F("cp_obs5","",8,-1,1,11,105,160);
  TH2F* cp_obs6=new TH2F("cp_obs6","",8,-1,1,11,105,160);
  TH2F* OO1jjtCzz=new TH2F("OO1jjtCzz","",5,0,5,11,105,160);
  TH2F* OO1jjtCza=new TH2F("OO1jjtCza","",5,0,5,11,105,160);
  TH2F* OO1jjcHWtil=new TH2F("OO1jjcHWtil","",5,0,5,11,105,160);
  double OO14ltCzz_bins[13]={-1.0, -0.024, -0.017, -0.011, -0.007, -0.003, 0.0, 0.003, 0.007, 0.011, 0.017, 0.024, 1.0};
  TH2F* OO14ltCzz=new TH2F("OO14ltCzz","",12,OO14ltCzz_bins,11,105,160);
  double OO14ltCza_bins[13]={-2.0, -0.25, -0.136, -0.077, -0.040, -0.016, 0.0, 0.016, 0.040, 0.077, 0.136, 0.25, 2.0};
  TH2F* OO14ltCza=new TH2F("OO14ltCza","",12,OO14ltCza_bins,11,105,160);
  double OO14ltCaa_bins[7]={-10.0, -0.15, -0.035, 0.0, 0.035, 0.15, 10.0};
  TH2F* OO14ltCaa=new TH2F("OO14ltCaa","",6,OO14ltCaa_bins,11,105,160);
  double OO14lcHWtil_bins[13]={-10.0, -0.135, -0.076, -0.043, -0.022, -0.008, 0.0, 0.008, 0.022, 0.043, 0.076, 0.135, 10.0};
  TH2F* OO14lcHWtil=new TH2F("OO14lcHWtil","",12,OO14lcHWtil_bins,11,105,160);
  double OO14lcHBtil_bins[7]={-20.0, -0.25, -0.065, 0.0, 0.065, 0.25, 20.0};
  TH2F* OO14lcHBtil=new TH2F("OO14lcHBtil","",6,OO14lcHBtil_bins,11,105,160);
  double OO14lcHWBtil_bins[7]={-15.0, -0.15, -0.035, 0.0, 0.035, 0.15, 15.0};
  TH2F* OO14lcHWBtil=new TH2F("OO14lcHWBtil","",6,OO14lcHWBtil_bins,11,105,160);
  TH2F* leptonpt[4];
  TH2F* leptoneta[4];
  TH2F* leptonphi[4];
  TH2F* leptonpt3njet[3];
  TH2F* leptonpt3mu[3];
  int nbins=19,hmax=100;
  for(int i=0;i<4;i++){
    leptonpt[i]=new TH2F(Form("lepton_pt%i",i),"",nbins-i*4,5,hmax-i*20,11,105,160);
    leptoneta[i]=new TH2F(Form("lepton_eta%i",i),"",20,-2.5,2.5,11,105,160);
    leptonphi[i]=new TH2F(Form("lepton_phi%i",i),"",20,-3.14159265,3.14159265,11,105,160);
  }
  for(int i=0;i<3;i++){
    leptonpt3njet[i]=new TH2F(Form("lepton_pt3_njet%i",i),"",7,5,40,11,105,160);
    leptonpt3mu[i]=new TH2F(Form("lepton_pt3_mu%i",i),"",7,5,40,11,105,160);
  }
  double bdt_bins[5]={-1,-0.5,0,0.5,1};
  double m4l_bins_main[12];
  for(int i=0;i<12;i++) m4l_bins_main[i]=105+i*5;
  double etype_bins[3]={0.5,1.5,2.5};
  TH3F* h_bdt=new TH3F("bdt","",4,bdt_bins,11,m4l_bins_main,2,etype_bins);
  TH2F* h_jet1pt_jet2pt=new TH2F("jet1pt_jet2pt","",6,0,6,11,105,160);
  TH2F* h_jet1pt_jet1y=new TH2F("jet1pt_jet1y","",6,0,6,11,105,160);
  TH2F* h_pt4l_pt4lj=new TH2F("pt4l_pt4lj","",5,0,5,11,105,160);
  TH2F* h_pt4lj_m4lj=new TH2F("pt4lj_m4lj","",5,0,5,11,105,160);
  TH1F* prod_type_hm=new TH1F("prod_type_hm","",2,0,2);
  TH1F* hmu=new TH1F("hmu","",7,0,70);
  int nev=0;
  float n2e2e=0,n2mu2e=0,nttbar=0;
  TString filename="";

  for (int i(0);i<chain->GetEntries();++i){

    if (i%10000==0) cout<<i<<"/"<<chain->GetEntries()<<endl;
    TString tempname(chain->GetCurrentFile()->GetName());
    chain->GetEntry(i);
    if(filename.CompareTo(tempname)!=0){
      std::cout<<"old file:"<<filename<<"; new file "<<tempname<<std::endl;
      filename=tempname;
    }
    //if((run==410289 || (run>=364500 && run<=364509)) && year=="2018") weight*=59.9/44.3; //while 4l-filtered ttbar and Zgamma are not available for mc16e
    /*
    if(lepton_ptvarcone30_TightTTVA_pt1000[0]/lepton_pt[0] < 0.15 && lepton_ptvarcone30_TightTTVA_pt1000[1]/lepton_pt[1] < 0.15 && lepton_ptvarcone30_TightTTVA_pt1000[2]/lepton_pt[2] < 0.15 && lepton_ptvarcone30_TightTTVA_pt1000[3]/lepton_pt[3] < 0.15 && event_type == 1 && ((run != 344297 && run != 344298 && run !=410470 && run !=410472 ))  && lepton_passD0sig[0] && lepton_passD0sig[1] && lepton_passD0sig[2] && lepton_passD0sig[3] && ((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2)|| (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2)) && pass_vtx4lCut==1 ) cout<<"pass cut w/ptvarcone30"<<endl;
    if(lepton_ptvarcone20_TightTTVA_pt1000[0]/lepton_pt[0] < 0.15 && lepton_ptvarcone20_TightTTVA_pt1000[1]/lepton_pt[1] < 0.15 && lepton_ptvarcone20_TightTTVA_pt1000[2]/lepton_pt[2] < 0.15 && lepton_ptvarcone20_TightTTVA_pt1000[3]/lepton_pt[3] < 0.15 && event_type == 1 && ((run != 344297 && run != 344298 && run !=410470 && run !=410472 ))  && lepton_passD0sig[0] && lepton_passD0sig[1] && lepton_passD0sig[2] && lepton_passD0sig[3] && ((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2)|| (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2)) && pass_vtx4lCut==1 ) cout<<"pass cut w/ptvarcone30"<<endl;
    */
    if(!(lepton_passD0sig->at(0) && lepton_passD0sig->at(1) && lepton_passD0sig->at(2) && lepton_passD0sig->at(3) && ((leppt->at(3)<leppt->at(2) && in_mcClass->at(3)==2) || (leppt->at(2)<leppt->at(3) && in_mcClass->at(2)==2)) && vtx==1)) continue;
    //if(!(lepton_passD0sig[0] && lepton_passD0sig[1] && lepton_passD0sig[2] && lepton_passD0sig[3] && ((leppt->at(3)<leppt->at(2) && in_mcClass[3]==2) || (leppt->at(2)<leppt->at(3) && in_mcClass[2]==2)) && vtx==1)) continue;
    //if(!(lepton_passD0sig[3] && lepton_passIsoCut[0] && lepton_passIsoCut[1] && lepton_passIsoCut[2] && lepton_passIsoCut[3])) continue;
    //if(!(lepton_passD0sig[0] && lepton_passD0sig[1] && lepton_passD0sig[2] && lepton_passD0sig[3] && lepton_passIsoCut[2] && lepton_passIsoCut[3] && ((leppt->at(3)<leppt->at(2) && in_mcClass[3]==2)|| (leppt->at(2)<leppt->at(3) && in_mcClass[2]==2)) && vtx==1)) continue;
    //if (quadtype==1 &&  (lepton_ptvarcone20[0] > 0.30 || lepton_ptvarcone20[1] > 0.30)) continue; //!)
    //if (quadtype==2 &&  (lepton_ptvarcone30[0] > 0.30 || lepton_ptvarcone30[1] > 0.30)) continue; //!)
    //temporary use of FCLoose+lepton dR cut
    /*    
    bool pass=true;
    for(int ilep=0;ilep<4;ilep++){
      //if(quadtype==2 && ptvarcone30_tightTTVA[ilep]/leppt->at(ilep)>0.15){ pass=false; break;}
      //if(quadtype==2 && ilep<=1 && topoetcone20[ilep]>0.3) {pass=false; break;}
      //if(quadtype==2 && ilep>1 && topoetcone20[ilep]>0.2) {pass=false; break;}
      if(quadtype==2){
	if(ilep==0 && !(topoetcone20[0] < 0.3  && ptvarcone30_tightTTVA[0]/leppt->at(0) < 0.15)) { pass=false; break;}
	if(ilep==1 && !(topoetcone20[1] < 0.3  && ptvarcone30_tightTTVA[1]/leppt->at(1) < 0.15)) { pass=false; break;}
	if(ilep==2 && !(topoetcone20[2] < 0.2  && ptvarcone30_tightTTVA[2]/leppt->at(2) < 0.15)) { pass=false; break;} 
	if(ilep==3 && !(topoetcone20[3] < 0.2  && ptvarcone30_tightTTVA[3]/leppt->at(3) < 0.15)) { pass=false; break;} 
      }
      //if(quadtype==1 && topoetcone20[ilep]>0.2) {pass=false; break;}
      if(quadtype==1 && !(topoetcone20[ilep] < 0.2 && ptvarcone30_tightTTVA[ilep]/leppt->at(ilep) < 0.15)){pass=false; break;}
      for(int jlep=ilep+1;jlep<4;jlep++){
	if(sqrt((lepeta->at(ilep)-lepeta->at(jlep))*(lepeta->at(ilep)-lepeta->at(jlep))+(lepphi->at(ilep)-lepphi->at(jlep))*(lepphi->at(ilep)-lepphi->at(jlep)))<0.3) {pass=false; break;}
      }
      if(!pass) break;
    }
    if(!pass) continue;
    */
    //if(!(lepton_passIsoCut[0] && lepton_passIsoCut[1] && lepton_passIsoCut[2] && lepton_passIsoCut[3])) continue;
    if(!(lepton_passIsoCut->at(0) && lepton_passIsoCut->at(1) && lepton_passIsoCut->at(2) && lepton_passIsoCut->at(3))) continue;
    nev++;
    float wt=weight;
    if(wt>1){
      cout<<"event from run "<<run<<" has weight "<<wt<<endl;
      continue;
    }
    for(int j=0;j<4;j++){
      ptSortLep[j]=leppt->at(0);
      ptSortLepEta[j]=lepeta->at(0);
      ptSortLepPhi[j]=lepphi->at(0);
    }
    for(int j=1;j<4;j++){
      bool isgt=false;
      for(int k=0;k<j;k++){
        if(leppt->at(j)>ptSortLep[k]){
          for(int m=j;m>=k+1;m--){
	    ptSortLep[m]=ptSortLep[m-1];
	    ptSortLepEta[m]=ptSortLepEta[m-1];
	    ptSortLepPhi[m]=ptSortLepPhi[m-1];
	  }
          ptSortLep[k]=leppt->at(j);
          ptSortLepEta[k]=lepeta->at(j);
          ptSortLepPhi[k]=lepphi->at(j);
          isgt=true;
          break;
        }
      } 
      if(!isgt){
	ptSortLep[j]=leppt->at(j);
	ptSortLepEta[j]=lepeta->at(j);
	ptSortLepPhi[j]=lepphi->at(j);
      }
    }
    //scale up ttbar according to results from llmumu estimate
    if(run == 410289 || run==410472 || run==410470){ wt*=1.2; nttbar+=wt;}
    m4l->Fill(m4l_fsr,0.5,wt);
    if (n_jets == 0) m4l_0jet->Fill(m4l_fsr,0.5,wt);
    if (n_jets == 1) m4l_1jet->Fill(m4l_fsr,0.5,wt); 
    if (n_jets > 1) m4l_2jet->Fill(m4l_fsr,0.5,wt);
    m4l_diffxs->Fill(m4l_fsr,0.5,wt);
    m4l_diffxs2->Fill(m4l_fsr,0.5,wt);
    HFTest->Fill(.5,wt);
    if(quadtype==1) n2e2e+=wt;
    else if(quadtype==2) n2mu2e+=wt;
    //if(wt>0.5) cout<<"wt: "<<wt<<"; m4l: "<<m4l_fsr<<"; lep: "<<lep<<"; file: "<<chain->GetFile()->GetName()<<endl;
    float cat;
    if(!useTreeProdType){
      if(ptype==0){
	if(pt4l_fsr<10) cat=0;
	else if(pt4l_fsr<100) cat=1;
	else cat=2;
      }
      else if(ptype==1){
	if(pt4l_fsr<60){
	  if(NN_1Jet_pTLow_ZZ<0.25) cat=3;
	  else cat=4;
	}
	else if(pt4l_fsr>60 && pt4l_fsr<120){ 
	  if(NN_1Jet_pTMed_ZZ<0.25) cat=5;
	  else cat=6;
	}
	else if(pt4l_fsr<200) cat=7;
	else cat=8;
      }
      else if(ptype==2){
	if(NN_2Jet_Low_VH<0.2) cat=9;
	else cat=10;
      }
      else if(ptype==3){
	if(pt4l_fsr<200){
	  if(NN_2Jet_Low_VH<0.2) cat=9;
	  else cat=10;
	}
	else cat=11;
      }
      else if(ptype==6){
	if(NN_ttHHad_ttV<0.4) cat=14;
	else cat=15;
      }
      else cat=ptype+8;
      prod_type->Fill(cat+H4lBkg::toInt(H4lBkg::Category::ProdType::_0JetPt4l10),wt);
    }
    else{
      //even with this, still the question of the categories that are split by NN variables
      //keep those next to the others to avoid confusion
      if(ptypefine<3) cat=ptypefine;
      else if(ptypefine==3){
	if(NN_1Jet_pTLow_ZZ>0.25) cat=4;
	else cat=3;
      }
      else if(ptypefine==4){
	if(NN_1Jet_pTMed_ZZ>0.25) cat=6;
	else cat=5;
      }
      else if(ptypefine<8) cat=ptypefine+2;
      else if(ptypefine==8){
	if(NN_2Jet_Low_VH > 0.2) cat=11;
	else cat=10;
      }
      else if(ptypefine<11) cat=ptypefine+3;
      else{
	if(NN_ttHHad_ttV>0.4) cat=15;
	else cat=14;
      }
      prod_type->Fill(cat+H4lBkg::toInt(H4lBkg::Category::ProdType::_0JetPt4l10),wt);
    }
    float sbcat=-1;
    if(useTreeProdType) sbcat=ptypesb;
    else{
      if(met>100 && n_bjets60>=1 && n_jets>=2) sbcat=3;
      else if(ptype==0) sbcat=0;
      else if(ptype==1) sbcat=1;
      else if(ptype==2) sbcat=2;
      else if(ptype==4) sbcat=4;
    }
    if(sbcat>-1) prod_type->Fill(sbcat+H4lBkg::toInt(H4lBkg::Category::ProdType::_SB_0jet),wt);
    if(n_jets==0) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_0Jet),wt);
    else if(n_jets==1) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_1Jet),wt);
    else if(n_jets==2) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_2Jet),wt);
    //else if(n_jets==3) prod_type->Fill(3.,wt);
    if(n_jets>=1) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_ge1Jet),wt);
    if(n_jets>=2) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_ge2Jet),wt);
    if(n_jets>=3) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_ge3Jet),wt);
    if(n_jets<=1) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_le1Jet),wt);
    //if(n_jets>=4) prod_type->Fill(7.,wt);
    if(fabs(y4l_fsr)<0.5) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l0),wt);
    else if(fabs(y4l_fsr)<1.0) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l1),wt);
    else if(fabs(y4l_fsr)<1.5) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l2),wt);
    else if(fabs(y4l_fsr)<2.5) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l3),wt);
    if(n_jets==0 || jetpt->at(0)<60) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_jet1pt0),wt);
    else if(jetpt->at(0)>60 && jetpt->at(0)<120) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_jet1pt1),wt);
    else if(jetpt->at(0)>120 && jetpt->at(0)<350) prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::_jet1pt2),wt);
    prod_type->Fill(H4lBkg::toInt(H4lBkg::Category::ProdType::prodInclusive),wt);
    if(dijet_invmass>300 && dijet_deltaeta>3.3) prod_type_hm->Fill(1.,wt);
    else prod_type_hm->Fill(0.,wt);
    hmu->Fill(mu,wt);
    /*
    if(ptype==0 || ptype>3) prod_type->Fill(ptype,wt);
    else if(ptype==1){
      if(pt4l_unconstrained<60) prod_type->Fill(1,wt);
      else if(pt4l_unconstrained<120) prod_type->Fill(6,wt);
      else prod_type->Fill(7,wt);
    }
    else if(ptype==2){
      if(jetpt[0]<150) prod_type->Fill(2,wt);
      else prod_type->Fill(8, wt);
    }
    else{
      if(jetpt[0]<200) prod_type->Fill(3,wt);
      else prod_type->Fill(9,wt);
    }
    */
    if(m4l_fsr<115 || m4l_fsr>130){
      prod_type_m4l->Fill(0.,cat+12,wt);
      //continue;
    }
    prod_type_m4l->Fill(1.,cat+12,wt);
    pt4l_incl->Fill(pt4l_fsr,m4l_fsr,wt);
    if(n_jets<4) pt4l_jet[n_jets]->Fill(pt4l_fsr,m4l_fsr,wt);
    else pt4l_jet[3]->Fill(pt4l_fsr,m4l_fsr,wt);
    if(fabs(y4l_fsr)<0.5) pt4l_y4l[0]->Fill(pt4l_fsr,m4l_fsr,wt);
    else if(fabs(y4l_fsr)<1.0) pt4l_y4l[1]->Fill(pt4l_fsr,m4l_fsr,wt);
    else if(fabs(y4l_fsr)<1.5) pt4l_y4l[2]->Fill(pt4l_fsr,m4l_fsr,wt);
    else if(fabs(y4l_fsr)<2.5) pt4l_y4l[3]->Fill(pt4l_fsr,m4l_fsr,wt);
    y4l->Fill(fabs(y4l_fsr),m4l_fsr,wt);
    //y4l_full->Fill(fabs(y4l_fsr),wt);
    if(n_jets<4){
      njet->Fill(n_jets,m4l_fsr,wt);
      //njet_full->Fill(n_jets,wt);
    }
    else{
      njet->Fill(3,m4l_fsr,wt);
      //njet_full->Fill(4,wt);
    }
    m12->Fill(mZ1_fsr,m4l_fsr,wt);
    m34->Fill(mZ2_fsr,m4l_fsr,wt);
    m34_m4l->Fill(mZ2_fsr,m4l_fsr,wt);
    cts->Fill(fabs(cthstr),m4l_fsr,wt);
    h_cth1->Fill(cth1,m4l_fsr,wt);
    h_cth2->Fill(cth2,m4l_fsr,wt);
    h_phi1->Fill(phi1,m4l_fsr,wt);
    h_phi->Fill(phi,m4l_fsr,wt);
    cp_obs1->Fill(cpobs1,m4l_fsr,wt);
    cp_obs2->Fill(cpobs2,m4l_fsr,wt);
    cp_obs3->Fill(cpobs3,m4l_fsr,wt);
    cp_obs4->Fill(cpobs4,m4l_fsr,wt);
    cp_obs5->Fill(cpobs5,m4l_fsr,wt);
    cp_obs6->Fill(cpobs6,m4l_fsr,wt);
    OO14ltCzz->Fill(OO1_4l_tCzz,m4l_fsr,wt);
    OO14ltCza->Fill(OO1_4l_tCza,m4l_fsr,wt);
    OO14ltCaa->Fill(OO1_4l_tCaa,m4l_fsr,wt);
    OO14lcHWtil->Fill(OO1_4l_cHWtil,m4l_fsr,wt);
    OO14lcHBtil->Fill(OO1_4l_cHBtil,m4l_fsr,wt);
    OO14lcHWBtil->Fill(OO1_4l_cHWBtil,m4l_fsr,wt);
    if(OO1_jj_tCzz<-10) OO1jjtCzz->Fill(0.5,m4l_fsr,wt);
    else if(OO1_jj_tCzz<-0.75) OO1jjtCzz->Fill(1.5,m4l_fsr,wt);
    else if(OO1_jj_tCzz<0) OO1jjtCzz->Fill(2.5,m4l_fsr,wt);
    else if(OO1_jj_tCzz<0.75) OO1jjtCzz->Fill(3.5,m4l_fsr,wt);
    else if(OO1_jj_tCzz<10) OO1jjtCzz->Fill(4.5,m4l_fsr,wt);
    if(OO1_jj_tCza<-3) OO1jjtCza->Fill(0.5,m4l_fsr,wt);
    else if(OO1_jj_tCza<-0.35) OO1jjtCza->Fill(1.5,m4l_fsr,wt);
    else if(OO1_jj_tCza<0) OO1jjtCza->Fill(2.5,m4l_fsr,wt);
    else if(OO1_jj_tCza<0.35) OO1jjtCza->Fill(3.5,m4l_fsr,wt);
    else if(OO1_jj_tCza<3) OO1jjtCza->Fill(4.5,m4l_fsr,wt);
    if(OO1_jj_cHWtil<-5) OO1jjcHWtil->Fill(0.5,m4l_fsr,wt);
    else if(OO1_jj_cHWtil<-0.4) OO1jjcHWtil->Fill(1.5,m4l_fsr,wt);
    else if(OO1_jj_cHWtil<0) OO1jjcHWtil->Fill(2.5,m4l_fsr,wt);
    else if(OO1_jj_cHWtil<0.4) OO1jjcHWtil->Fill(3.5,m4l_fsr,wt);
    else if(OO1_jj_cHWtil<5) OO1jjcHWtil->Fill(4.5,m4l_fsr,wt);
    //bjet binning, separate njets=0 and nbjets=0
    if(n_jets==0) nbjet->Fill(0.5,m4l_fsr,wt);
    else if(n_bjets==0) nbjet->Fill(1.5,m4l_fsr,wt);
    else nbjet->Fill(2.5,m4l_fsr,wt);
    h_bdt->Fill(bdt,m4l_fsr,quadtype,wt);
    float hbin;
    if(n_jets==0) hbin=0.5;
    else if(n_jets==1){
      if(jetpt->at(0)<60) hbin=1.5;
      else if(jetpt->at(0)<350) hbin=2.5;
    }
    else{
      if(jetpt->at(0)<60) hbin=3.5;
      else if(jetpt->at(0)<350){
	if(jetpt->at(1)<60) hbin=4.5;
	else hbin=5.5;
      }
    }
    h_jet1pt_jet2pt->Fill(hbin,m4l_fsr,wt);
    if(n_jets==0) hbin=0.5;
    else{
      if(pt4l_fsr<120){
	if(pt4lj<60) hbin=1.5;
	else if(pt4lj<350) hbin=3.5;
      }
      else if(pt4l_fsr<350){
	if(pt4lj<60) hbin=2.5;
	else if(pt4lj<350) hbin=4.5;
      }
    }
    h_pt4l_pt4lj->Fill(hbin,m4l_fsr,wt);
    if(n_jets==0) hbin=0.5;
    else{
      if(m4lj>120 && m4lj<220){
	if(pt4lj<350) hbin=1.5;
      }
      else if(m4lj<350){
	if(pt4lj<60) hbin=2.5;
	else if(pt4lj<350) hbin=3.5;
      }
      else if(m4lj<2000){
	if(pt4lj<350) hbin=4.5;
      }
    }
    h_pt4lj_m4lj->Fill(hbin,m4l_fsr,wt);
    if(n_jets==0) hbin=0.5;
    else{
      if(jetpt->at(0)<120){
	if(abs(jeteta->at(0))<0.8) hbin=1.5;
	else if(abs(jeteta->at(0))<1.7) hbin=2.5;
	else hbin=3.5;
      }
      else if(jetpt->at(0)<350){
	if(abs(jeteta->at(0))<1.7) hbin=4.5;
	else hbin=5.5;
      }
    }
    h_jet1pt_jet1y->Fill(hbin,m4l_fsr,wt);
    if(n_jets==0) hbin=0.5;
    else{
      if(jetpt->at(0)<60){
	if(pt4l_fsr<80) hbin=1.5;
	else if(pt4l_fsr<350) hbin=2.5;
      }
      else if(jetpt->at(0)<120){
	if(pt4l_fsr<120) hbin=3.5;
	else if(pt4l_fsr<350) hbin=4.5;
      }
      else if(jetpt->at(0)<350){
	if(pt4l_fsr<120) hbin=5.5;
	else if(pt4l_fsr<350) hbin=6.5;
      }
    }
    pt4l_jet1pt->Fill(hbin,m4l_fsr,wt);
    for(int ilep=0;ilep<4;ilep++){
      leptonpt[ilep]->Fill(ptSortLep[ilep],m4l_fsr,wt);
      leptoneta[ilep]->Fill(ptSortLepEta[ilep],m4l_fsr,wt);
      leptonphi[ilep]->Fill(ptSortLepPhi[ilep],m4l_fsr,wt);
    }
    if(n_jets==0) leptonpt3njet[0]->Fill(ptSortLep[3],m4l_fsr,wt);
    else if(n_jets==1) leptonpt3njet[1]->Fill(ptSortLep[3],m4l_fsr,wt);
    else leptonpt3njet[2]->Fill(ptSortLep[3],m4l_fsr,wt);
    if(mu<20) leptonpt3mu[0]->Fill(ptSortLep[3],m4l_fsr,wt);
    else if(mu<40) leptonpt3mu[1]->Fill(ptSortLep[3],m4l_fsr,wt);
    else leptonpt3mu[2]->Fill(ptSortLep[3],m4l_fsr,wt);
    TLorentzVector comb;
    comb.SetPtEtaPhiM(pt4l_fsr,eta4l_fsr,phi4l_unconstrained,m4l_fsr);
    if(n_jets>0){
      jet1pt->Fill(jetpt->at(0),m4l_fsr,wt);
      h_m4lj->Fill(m4lj,m4l_fsr,wt);
      TLorentzVector j1;
      j1.SetPtEtaPhiM(jetpt->at(0),jeteta->at(0),jetphi->at(0),jetm->at(0));
      comb+=j1;
      //h_pt4lj->Fill(comb.Pt()+1,m4l_fsr,wt);
      h_pt4lj->Fill(pt4lj,m4l_fsr,wt);
    }
    else{
      jet1pt->Fill(29.5,m4l_fsr,wt);
      h_m4lj->Fill(119.5,m4l_fsr,wt);
      h_pt4lj->Fill(0.5,m4l_fsr,wt);
    }
    if(n_jets>1){
      jet2pt->Fill(jetpt->at(1),m4l_fsr,wt);
      if(dijet_invmass<120) mjj->Fill(1.5,m4l_fsr,wt);
      else if(dijet_invmass<450) mjj->Fill(3,m4l_fsr,wt);
      else mjj->Fill(5.5,m4l_fsr,wt);
      detajj->Fill(dijet_deltaeta+1,m4l_fsr,wt);
      dphijj->Fill(dijet_deltaphi+1,m4l_fsr,wt);
      h_m4ljj->Fill(m4ljj,m4l_fsr,wt);
      TLorentzVector j2;
      j2.SetPtEtaPhiM(jetpt->at(1),jeteta->at(1),jetphi->at(1),jetm->at(1));
      comb+=j2;
      //h_pt4ljj->Fill(comb.Pt()+1,m4l_fsr,wt);
      h_pt4ljj->Fill(pt4lj,m4l_fsr,wt);
    }
    else{
      jet2pt->Fill(29.5,m4l_fsr,wt);
      mjj->Fill(0.5,m4l_fsr,wt);
      dphijj->Fill(0.5,m4l_fsr,wt);
      detajj->Fill(0.5,m4l_fsr,wt);
      h_m4ljj->Fill(179.5,m4l_fsr,wt);
      h_pt4ljj->Fill(0.5,m4l_fsr,wt);
    }
    HFTot->Fill(quadtype-1,wt);
    m12_m34_2d->Fill(mZ1_fsr,mZ2_fsr,wt);
    if(mZ2_fsr>32){
      if(mZ1_fsr<74 && mZ1_fsr>50) m12_m34->Fill(1.5,m4l_fsr,wt); //bin 2
      else if(mZ1_fsr>74) m12_m34->Fill(2.5,m4l_fsr,wt); //bin 3
    }
    else if(mZ2_fsr<32){
      if(mZ1_fsr<82) m12_m34->Fill(0.5,m4l_fsr,wt); //bin 1
      else if(mZ1_fsr>82){
	if(mZ2_fsr<24) m12_m34->Fill(4.5,m4l_fsr,wt); //bin 5
	else m12_m34->Fill(3.5,m4l_fsr,wt); //bin 4
      }
    }
  }
  double hftoterr,hftot;
  hftot=HFTot->IntegralAndError(1,2,hftoterr);
  //cout<<"heavy flavor: "<<HFTot->GetBinContent(1)<<"+/-"<<HFTot->GetBinError(1)<<endl;
  cout<<"heavy flavor: "<<hftot<<"+/-"<<hftoterr<<endl;
  //cout<<"total over the full mass range: "<<m4l->Integral()<<", from "<<nev<<" events"<<endl;
  cout<<"4e tot: "<<n2e2e<<"; 2mu2e tot: "<<n2mu2e<<endl;
  cout<<"ttbar contributes "<<nttbar<<endl;
  cout<<"test results: "<<HFTest->GetBinContent(1)<<endl;
  /*
  cout<<"mass window fraction: "<<prod_type->Integral()/m4l->Integral()<<endl;
  for(int i=0;i<prod_type->GetNbinsX();i++){
    //if(i>3) continue;
    float npass,ntot;
    if(i==0){
      npass=prod_type_m4l->GetBinContent(2,i+1);
      ntot=npass+prod_type_m4l->GetBinContent(1,i+1);
    }
    else if(i==1){
      npass=prod_type_m4l->GetBinContent(2,i+1)+prod_type_m4l->GetBinContent(2,7)+prod_type_m4l->GetBinContent(2,8);
      ntot=npass+prod_type_m4l->GetBinContent(1,i+1)+prod_type_m4l->GetBinContent(1,7)+prod_type_m4l->GetBinContent(1,8);
    }
    else if(i==2){
      npass=prod_type_m4l->GetBinContent(2,i+1)+prod_type_m4l->GetBinContent(2,9);
      ntot=npass+prod_type_m4l->GetBinContent(1,i+1)+prod_type_m4l->GetBinContent(1,9);
    }
    else if(i==3){
      npass=prod_type_m4l->GetBinContent(2,i+1)+prod_type_m4l->GetBinContent(2,10);
      ntot=npass+prod_type_m4l->GetBinContent(1,i+1)+prod_type_m4l->GetBinContent(1,10);
    }
    else{
      npass=prod_type_m4l->GetBinContent(2,i+1);
      ntot=npass+prod_type_m4l->GetBinContent(1,i+1);
    }
    cout<<"npass: "<<npass<<"; ntot: "<<ntot<<endl;
    cout<<"mass ratio for prod type "<<i<<"="<<npass/ntot<<endl;
  }
  */
  TFile* outFile=new TFile(Form("v25Files/heavyFlavorShapes_%s.root",year.Data()),"RECREATE");
  outFile->cd();
  m4l->Write();
  m4l_0jet->Write();
  m4l_1jet->Write();
  m4l_2jet->Write();
  pt4l_incl->Write();
  pt4l_jet1pt->Write();
  for(int i=0;i<4;i++){
    pt4l_jet[i]->Write();
    pt4l_y4l[i]->Write();
    leptonpt[i]->Write();
    leptoneta[i]->Write();
    leptonphi[i]->Write();
  }
  for(int i=0;i<3;i++){
    leptonpt3njet[i]->Write();
    leptonpt3mu[i]->Write();
  }
  m12->Write();
  m34->Write();
  njet->Write();
  //njet_full->Write();
  //y4l_full->Write();
  cts->Write();
  h_cth1->Write();
  h_cth2->Write();
  h_phi1->Write();
  h_phi->Write();
  y4l->Write();
  jet1pt->Write();
  jet2pt->Write();
  m34_m4l->Write();
  m12_m34_2d->Write();
  m12_m34->Write();
  prod_type->Write();
  /*
  hBDT0->Write();
  hBDT1l->Write();
  hBDT1h->Write();
  hBDT2H->Write();
  hBDT2F->Write();
  */
  mjj->Write();
  detajj->Write();
  dphijj->Write();
  h_m4lj->Write();
  h_m4ljj->Write();
  h_pt4lj->Write();
  h_pt4ljj->Write();
  m4l_diffxs->Write();
  m4l_diffxs2->Write();
  nbjet->Write();
  prod_type_hm->Write();
  HFTot->Write();
  hmu->Write();
  h_bdt->Write();
  h_jet1pt_jet2pt->Write();
  h_jet1pt_jet1y->Write();
  h_pt4l_pt4lj->Write();
  h_pt4lj_m4lj->Write();
  cp_obs1->Write();
  cp_obs2->Write();
  cp_obs3->Write();
  cp_obs4->Write();
  cp_obs5->Write();
  cp_obs6->Write();
  OO1jjtCzz->Write();
  OO1jjtCza->Write();
  OO1jjcHWtil->Write();
  OO14ltCzz->Write();
  OO14ltCza->Write();
  OO14ltCaa->Write();
  OO14lcHWtil->Write();
  OO14lcHWBtil->Write();
  OO14lcHBtil->Write();
  outFile->Write();
  outFile->Close();

}
