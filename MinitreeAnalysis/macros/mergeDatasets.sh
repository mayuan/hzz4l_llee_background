mkdir merged
for i in $1/user*; do
    sname=`echo $i | cut -d'.' -f1-8`;
    if [ -f $1/merged/$sname ] ; then
	continue;
    fi
    echo $sname;
    hadd $1/merged/$sname $1/$sname.*/*.root*;
done 
