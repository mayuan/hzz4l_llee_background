##############################################################
ver="v19"
reg="5"

out="reg${reg}_${ver}_D2AOD"
##############################################################

###### 25 ns
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodDFiles.txt" --output ${out} --region $reg --sample dataD --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodEFiles.txt" --output ${out} --region $reg --sample dataE --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodFFiles.txt" --output ${out} --region $reg --sample dataF --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodGFiles.txt" --output ${out} --region $reg --sample dataG --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodHFiles.txt" --output ${out} --region $reg --sample dataH --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodIFiles.txt" --output ${out} --region $reg --sample dataI --doAna false
runOnGrid --input "/afs/cern.ch/work/w/wleight/public/Higgs/MinitreeAnalysis/macros/periodJFiles.txt" --output ${out} --region $reg --sample dataJ --doAna false
##runOnGrid --input "data15_13TeV.00279284.physics_Main.merge.DAOD_HIGG2D2.f628_m1497_p2419/" --output ${out} --region $reg --sample dataHIGG2D2

#runOnGrid --input "mc15_13TeV.36110*.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Z*.merge.DAOD_HIGG2D1.e3601_s2576_s2132_r6765_r6282_p2425/" --output ${out} --region $reg --doAna false --sample Zjets

#runOnGrid --input "mc15_13TeV:mc15_13TeV.36150*.MadGraphPythia8EvtGen_A14NNPDF23LO_Z*_Np*.merge.DAOD_HIGG2D1.e3898_s2608_s2183_r6869_r6282_p2425/" --output ${out} --region $reg --doAna false --sample Zjets_Madgraph

#runOnGrid --input "mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_HIGG2D1.e3698_s2608_s2183_r6765_r6282_p2425/" --output ${out} --region $reg --sample ttbar

#runOnGrid --input "mc15_13TeV.361*.Sherpa_CT10_Z*_Pt*.merge.DAOD_HIGG2D1.*_r6869_r6282_p2425/" --output ${out} --region $reg --sample ZjetsSherpa

#runOnGrid --input "mc15_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4.merge.DAOD_HIGG2D1.e4475_s2726_r6869_r6282_p2425/" --output ${out} --region $reg --sample WZ

#runOnGrid --input "mc15_13TeV.1871*.PowhegPythia8_AU2CT10_W*Z_*DiLeptonFilter.merge.DAOD_HIGG2D1.e3059_s2608_s2183_r6630_r6264_p2419/" --output ${out} --region $reg --sample WZold

#runOnGrid --input "mc15_13TeV.1871*.PowhegPythia8_AU2CT10_ZZ_*.merge.DAOD_HIGG2D1.e3059_s2608_s2183_r6630_r6264_p2419/" --output ${out} --region $reg --sample qqZZold

#runOnGrid --input "mc15_13TeV.*.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4*merge.DAOD_HIGG2D1*s2608*r6869_r6282_p2425/" --output ${out} --region $reg --sample qqZZ

#runOnGrid --input "mc15_13TeV.34110*.Sherpa_CT10_Z*_4l*40GeV8GeV.merge.DAOD_HIGG2D1.e3929_s2608_s2183_r6869_r6282_p2425/" --output ${out} --region $reg --sample Zjets_Sherpa_Filtered

###### 50 ns
# runOnGrid --input "data15_13TeV.0026763*.physics_Main.merge.DAOD_HIGG2D1.r6848_p2358_p2375/" --output ${out} --region $reg --sample dataA4

# runOnGrid --input "data15_13TeV.0027*.physics_Main.merge.DAOD_HIGG2D1.f611_m1463_p2375/" --output ${out} --region $reg --sample dataC

# runOnGrid --input "data15_13TeV.0027*.physics_Main.merge.DAOD_HIGG2D1.f611_m1463_p2384/" --output ${out} --region $reg --sample dataC2

# runOnGrid --input "data15_13TeV.00271744.physics_Main.merge.DAOD_HIGG2D1.f611_m1463_p2384/" --output ${out} --region $reg --sample dataC22222

### mc
# runOnGrid --input "mc15_13TeV.36110*.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Z*.merge.DAOD_HIGG2D1.e3601_s2576_s2132_r6630_r6264_p2361/" --output ${out} --region $reg --sample Zjets

# runOnGrid --input "mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_HIGG2D1.e3698_s2608_s2183_r6630_r6264_p2375/" --output ${out} --region $reg --sample ttbar

# runOnGrid --input "mc15_13TeV.361*.Sherpa_CT10_Z*_Pt*.merge.DAOD_HIGG2D1.*_p2370/" --output ${out} --region $reg --sample ZjetsSherpa

# runOnGrid --input "mc15_13TeV.36107*.Sherpa_*lll*.merge.DAOD_HIGG2D1.e3836_s2608_s2183_r6793_r6264_p2370/" --output ${out} --region $reg --sample sherpaWZ

# runOnGrid --input "mc15_13TeV:mc15_13TeV.361063.Sherpa_CT10_llll.merge.DAOD_HIGG2D1.e3836_s2608_s2183_r6793_r6264_p2370/"  --output ${out} --region $reg --sample sherpaZZ

# runOnGrid --input "mc15_13TeV.1871*.PowhegPythia8_AU2CT10_W*Z_*DiLeptonFilter.merge.DAOD_HIGG2D1.e3059_s2608_s2183_r6630_r6264_p2361/" --output ${out} --region $reg --sample WZ

# runOnGrid --input "mc15_13TeV.1871*.PowhegPythia8_AU2CT10_ZZ_*.merge.DAOD_HIGG2D1.e3059_s2608_s2183_r6630_r6264_p2361/" --output ${out} --region $reg --sample qqZZ

# runOnGrid --input "mc15_13TeV.341505.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH125_ZZ4lep_noTau.merge.DAOD_HIGG2D1.e3951_s2608_s2183_r6630_r6264_p2361/" --output ${out} --region $reg --sample HZZnotau

