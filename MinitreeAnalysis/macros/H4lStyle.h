#ifndef  __H4LSTYLE_H
#define __H4LSTYLE_H

#include "AtlasStyle.C"
#include "AtlasLabels.C"
#include "TLegend.h"

TLegend* makeLeg(float xmin=0.605,float xmax=0.65,float ymin=0.90,float ymax=0.925);
void allLabels(float lumi=1.0, std::string channelOrOtherText="", float x=0.2,float y=0.875);

#endif // __H4LSTYLE_H
