#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "TSystem.h"
#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TCanvas.h"

#include "H4lStyle.C"
#include "../MinitreeAnalysis/CommonDefs.h"

void makeEfficiencyPlots(){

  gROOT->SetBatch(1);
  SetAtlasStyle();

  vector<int> mctypes = {MCType::f,MCType::gamma,MCType::q};
  enum ELLH{Loose=0,VeryLoose=1,nELLHMAX};
  const int nElLHMax=nELLHMAX;
  string strElLH[nElLHMax]={"Loose","VeryLoose"};

  TFile* file=new TFile("fakeFactors.root");
  //TFile* file[2];
  //file[1]=new TFile("fakeFactors.root");
  //file[0]=new TFile("fakeFactors_Loose.root"); //have to get loose from old file temporarily
  TH1F* elEffComp[2][3];
  TH1F* fakeEff;
  TH1F* gammaEff;
  TCanvas* c;
  TLegend* leg;
  c=new TCanvas("c","c",400,400);
  fakeEff=(TH1F*)file->Get("eff_pt_Loose_fake");
  fakeEff->SetMaximum(.002);
  fakeEff->Draw("e");
  c->SaveAs("effCompPlotsVert/fakeEff.pdf");
  delete c; c=0;
  c=new TCanvas("c","c",400,400);
  gammaEff=(TH1F*)file->Get("eff_pt_Loose_gamma");
  gammaEff->SetMaximum(.01);
  gammaEff->Draw("e");
  c->SaveAs("effCompPlotsVert/gammaEff.pdf");
  delete c; c=0;
  for(int i=0;i<3;i++){
    c=new TCanvas("c","c",400,400);
    gPad->SetLogx(1);
    leg=makeLeg(0.65,0.62,0.95,0.74);
    for(int iLH=0;iLH<nElLHMax;iLH++){
      elEffComp[iLH][i]=(TH1F*)file->Get(Form("eff_pt_%s_%s",strElLH[iLH].c_str(),MCType::strMCTypeLabel[mctypes[i]].c_str()));
      if(iLH==0){
	elEffComp[iLH][i]->SetStats(0);
	elEffComp[iLH][i]->SetLineColor(kRed);
	elEffComp[iLH][i]->SetMarkerColor(kRed);
	elEffComp[iLH][i]->SetLineWidth(2);
	elEffComp[iLH][i]->SetMarkerStyle(0);
	elEffComp[iLH][i]->SetMarkerSize(0);
	if(i==0) elEffComp[iLH][i]->SetMaximum(.0025);
	if(i==1) elEffComp[iLH][i]->SetMaximum(.015);
	if(i==2) elEffComp[iLH][i]->SetMaximum(.3);
	elEffComp[iLH][i]->SetMinimum(0);
	elEffComp[iLH][i]->GetXaxis()->SetMoreLogLabels();
	elEffComp[iLH][i]->Draw("e");
      }
      else{
	elEffComp[iLH][i]->SetStats(0);
	elEffComp[iLH][i]->SetLineColor(kBlack);
	elEffComp[iLH][i]->SetMarkerColor(kBlack);
	elEffComp[iLH][i]->SetLineWidth(2);
	elEffComp[iLH][i]->SetMarkerStyle(20);
	elEffComp[iLH][i]->SetMarkerSize(1.5);
	elEffComp[iLH][i]->Draw("esame");
      }
      leg->AddEntry(elEffComp[iLH][i],strElLH[iLH].c_str(),"lp");
    }
    leg->Draw();
    c->SaveAs(Form("effCompPlotsVert/el_eff_comp_%s.pdf",MCType::strMCTypeLabel[mctypes[i]].c_str()));
    delete c; c=0;
    delete leg; leg=0;
  }
  /*
  TH1F* muEffData=(TH1F*)file->Get("mu_eff_data");
  TH1F* muEffZjets=(TH1F*)file->Get("mu_eff_Zjets");
  c=new TCanvas("c","c",400,400);
  leg=makeLeg(0.65,0.62,0.95,0.74);
  muEffData->SetStats(0);
  muEffData->SetLineColor(kBlue);
  muEffData->SetMarkerColor(kBlue);
  muEffData->SetMarkerStyle(0);
  muEffData->SetMarkerSize(0);
  muEffData->Draw("e");
  muEffData->SetMinimum(0);
  muEffData->SetMaximum(.5);
  leg->AddEntry(muEffData,"data","lp");
  muEffZjets->SetStats(0);
  muEffZjets->SetLineColor(H4lStyle::LineColor[H4lStyle::Zjets]);
  muEffZjets->SetMarkerColor(H4lStyle::LineColor[H4lStyle::Zjets]);
  muEffZjets->SetMarkerStyle(20);
  muEffZjets->SetMarkerSize(1.5);
  muEffZjets->Draw("esame");
  leg->AddEntry(muEffZjets,"MC","lp");
  leg->Draw();
  c->SaveAs("effCompPlots/mu_eff_comp.pdf");
  */
  /*
  TFile* sf15=new TFile("efficiencySF15_d0.root");
  TFile* sf16=new TFile("efficiencySF16_d0.root");
  for(int i=0;i<2;i++){
    TH1F* h15=(TH1F*)sf15->Get(Form("efficiencySF_%i",i));
    TH1F* h16=(TH1F*)sf16->Get(Form("efficiencySF_%i",i));
    c=new TCanvas("c","c",400,400);
    gPad->SetLogx(1);
    h16->SetMarkerColor(kRed);
    h16->SetXTitle("p_{T}");
    h16->SetYTitle("Efficiency Scale Factor");
    h16->SetLineColor(kRed);
    leg=makeLeg(0.65,0.8,0.95,0.94);
    leg->AddEntry(h16,"2016 SF","p");
    leg->AddEntry(h15,"2015 SF","p");
    h16->Draw();
    h15->Draw("SAME");
    leg->Draw();
    if(i==0) c->Print("efficiencySFCompFake.eps");
    else c->Print("efficiencySFCompGamma.eps");
    delete c; c=0;
  }
  */
}
