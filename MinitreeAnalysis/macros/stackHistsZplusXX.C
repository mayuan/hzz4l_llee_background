/////////////////////////////////////////////////
//////////////// Compile the script to run it!!!!
/// root -b stackHists.C+
/////////////////////////////////////////////////
#include <vector>
#include <string>
#include <iostream>
using namespace std;

#include "TROOT.h"
#include "TSystem.h"
#include "TTree.h"
#include "TFile.h"
#include "TKey.h"
#include "TH1F.h"
#include "THStack.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TLine.h"
#include "TLegend.h"
#include "TLatex.h"

#include "AtlasStyle.C"

float LUMI = 84.967-0.325; //in pb-1, to scale the MC hists
double k_Zll_powheg = 1.026;
double k_Zll_sherpa = 0.9013;

//////////////////////////////////////////////////////////////////////////////////////
void setAtlasStyle()
{
  TStyle* atlasStyle(0); atlasStyle = AtlasStyle();
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();
  gStyle->SetPalette(1,0);
}

TLegend* makeLeg(float xmin,float xmax,float ymin,float ymax)
{
  TLegend *leg = new TLegend(xmin,xmax,ymin,ymax);  
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  leg->SetLineWidth(0);   
  leg->SetTextFont(42);
  return leg;
}
void decorate(TH1F* h,Color_t lcol,Color_t fcol,int mstyle,string xtitle="",string ytitle="")
{
  h->SetLineWidth(1);
  h->SetLineColor(lcol);
  h->SetFillColor(fcol);
  h->SetMarkerColor(fcol);
  h->SetMarkerStyle(mstyle);
  h->GetYaxis()->SetNoExponent(1);
  h->SetXTitle(xtitle.c_str());
  h->SetYTitle(ytitle.c_str());

  // ...............if you want rebin certain histograms:
  // if( ((string)h->GetName()).find("_m") != std::string::npos ) {
  //   h->GetXaxis()->SetRangeUser(76,116);
  // }
  // if ( ((string)h->GetName()).find("_mu_") != std::string::npos) {
  //   //    string dontRebinThese[]={"_n"}
  //   if ( ((string)h->GetName()).find("_n") == std::string::npos ) {
  //     h->Rebin(4);
  //   }
  // }

}
//////////////////////////////////////////////////////////////////////////////////////

void stackHistsZplusXX()
{
  gROOT->SetBatch(1);
  setAtlasStyle();
  
  ///DEFINE THE ORDER OF THE SAMPLES SHOWN IN THE STACK HERE:
  //enum SAMPLES {iDATA,iTTBAR,iZZ,iWZ,iZX,nSamplesMax};
  enum SAMPLES {iDATA,iTTBAR,iWZ,iZX,nSamplesMax};
  const int nFiles=nSamplesMax;

  /////////// DEFINE COLORS AND LEGEND LABELS FOR EACH HISTOGRAM
  Color_t cols[nFiles];
  string legTitles[nFiles];
  //legTitles[iSignal]="signal";   cols[iSignal]=kMagenta;
  legTitles[iZX]="Z+jets (Powheg)";       cols[iZX]=kCyan+1;
  legTitles[iDATA]="Data";     cols[iDATA]=kBlack;
  legTitles[iTTBAR]="t#bar{t}";     cols[iTTBAR]=8;
  legTitles[iWZ]="WZ";       cols[iWZ]=kMagenta-1;//kBlue+1;
  //legTitles[iZZ]="ZZ";       cols[iZZ]=kWhite;
	  
  //DEFINE INPUT FILE INFORMATION HERE
  string fnames[nFiles]; 
  string path="./minitree_mc15_13TeV_ZplusX_hists/"; //path to the input files
  string outputPath="./minitree_mc15_13TeV_ZplusX_plots/"; //all plots will be stored here
  gSystem->Exec(Form("mkdir %s",outputPath.c_str()));

  /////// GIVE THE NAME TAG FOR EACH SAMPLE AS IT APPEARS IN ITS FILENAME
  string sampleTag[nFiles];
  sampleTag[iDATA] = "data";
  sampleTag[iZX] = "Zjets_Powheg";
  //sampleTag[iZX] = "Zjets_Sherpa";
  sampleTag[iTTBAR] = "ttbar";
  sampleTag[iWZ] = "WZ";
  //sampleTag[iZZ] = "ZZ_Powheg";
  
  ////////////////IF YOU HAVE MULTIPLE HIST FILES PER SAMPLE, ONE PER CHANNEL, CHANGE NCHANNELS
  string channels[5]={"all","4mu","4e","2e2mu","2e2mu"};
  const int nChannels=1; //if one file includes histograms for all channels, set to 1


  //// 

  
  for (int iChannel=0; iChannel<nChannels; iChannel++) {
    
    TFile *f[nFiles];
    
    vector<string> hnames; //keep a vector with the hist names	
    vector<THStack*> hstacksMC; //a vector of stacks, one for each histogram
    vector<TH1F*> hdata; //a vector of hists, one for each histogram
    TLegend *leg = makeLeg(0.69,0.7,0.93,0.93);
    
    ////////////////////////////////////////////////////////////
    for (int i=0; i<nFiles; i++) {
      //open the files
      //   fnames[i] = path+"Hists_"+channels[iChannel]+".mc14_13TeV.HIGG2D1_region1_"+sampleTag[i]+".root";      
      if(i==0) fnames[i]=path+"minitree_data15_13TeV_ZplusXX_v4_hists.root";
      else fnames[i] = path+"minitree_mc15_13TeV_ZplusXX_"+sampleTag[i]+"_hists.root";      
      f[i]= TFile::Open(fnames[i].c_str());
      
      if (i==0) { //take the first file and loop over histograms to get the names and create the stacks
	TKey *key;
	TIter nextKey(f[0]->GetListOfKeys());

	while ((key = (TKey*)nextKey())) {
	  if (! gROOT->GetClass(key->GetClassName())->InheritsFrom("TH1")) continue;
	  if (gROOT->GetClass(key->GetClassName())->InheritsFrom("TH2")) continue;
	  TObject* obj = key->ReadObj();
	  TString hname = obj->GetName();

	  //..... skip certain histograms:
	  if ( hname.Contains("BL") && hname.Contains("type") ) continue;
	  
	  hnames.push_back( obj->GetName() );
	  hstacksMC.push_back(new THStack(Form("hstackMC_%s_%s",obj->GetName(),channels[iChannel].c_str()),obj->GetTitle()));
		  
	}    
      }
    }
    ////////////////////////////////////////////////////////////
    
    //all files opened. now fill the stacks
    for (int i=0; i<nFiles; i++) {
      for (unsigned int j=0; j<hnames.size(); j++) {
	cout << hnames[j].c_str() << " " << hstacksMC[j]->GetName() << endl;
	TH1F* h = (TH1F*) f[i]->Get(hnames[j].c_str());	
	string sUnits = ( ((TString)h->GetXaxis()->GetTitle()).Contains("GeV") )? " GeV": "";
	if (i==0) { //data
	  decorate(h,kBlack,kBlack,20,h->GetXaxis()->GetTitle(),Form("Entries / %.1f%s",h->GetBinWidth(1),sUnits.c_str()));	  
	  h->SetMarkerSize(0.15);
	  hdata.push_back((TH1F*)h->Clone(Form("hdata_%s",hnames[j].c_str())));
	  if (j==0) leg->AddEntry(hdata[j],legTitles[i].c_str(),"lp");
	}
	else { //mc
	  decorate(h,kBlack,cols[i],0,hstacksMC[j]->GetTitle(),Form("Entries / %.1f%s",h->GetBinWidth(1),sUnits.c_str()));
	  // >>>>>>>>>>>>>>>>>>>>>>>
	  h->Scale(LUMI);// pb-1
	  if (i==iZX) h->Scale(k_Zll_powheg);
	  // <<<<<<<<<<<<<<<<<<<<<<<
	  hstacksMC[j]->Add(h);

	  if (j==0) {
	    leg->AddEntry(h,legTitles[i].c_str(),"f");	  
	  }
	}
      }
    }


    //////////
    //Superimpose alternative Zjets MC
    string fname2 = path+"minitree_mc15_13TeV_ZplusXX_Zjets_Sherpa_hists.root";      
    TFile *fMC2 = TFile::Open(fname2.c_str());
    vector<TH1F*> hMC2;
    for (unsigned int j=0; j<hnames.size(); j++) {
      TH1F* h = (TH1F*) fMC2->Get(hnames[j].c_str());
      h->Scale(LUMI);// pb-1
      h->Scale(k_Zll_sherpa);
      decorate(h,kRed+1,0,0);
      //add other mc from stack
      TH1F* hmcsum = (TH1F*) (hstacksMC[j]->GetStack()->Last()->Clone());
      TH1F* hmclast = (TH1F*) (hstacksMC[j]->GetHists()->Last()->Clone());
      hmcsum->Add(hmclast,-1);
      h->Add(hmcsum);
      if (j==0) leg->AddEntry(h,"Z+jets (Sherpa)","lf");
      hMC2.push_back((TH1F*)h->Clone(Form("hMC2_%s",hnames[j].c_str())));
    }
    /////////////
    
    TCanvas c1("c1","",140,140);
    double padFrac=0.3;
    TPad *uppPad=new TPad("uppPad","",0,padFrac,1,1);
    TPad *lowPad=new TPad("lowPad","",0,0,1,padFrac);
    uppPad->SetBottomMargin(0.03);
    lowPad->SetTopMargin(0.01);	  
    lowPad->SetBottomMargin(0.3);
    uppPad->Draw();
    lowPad->SetFillColor(0);
    lowPad->Draw();
    
    TLatex t; t.SetNDC(); t.SetTextColor(1);
    for (unsigned int j=0; j<hstacksMC.size(); j++) {
      uppPad->cd();	  
      uppPad->Clear();
      hdata[j]->GetXaxis()->SetLabelSize(0);
      hdata[j]->Draw("e");

      hdata[j]->GetYaxis()->SetRangeUser(0,hdata[j]->GetMaximum()*1.5);
      uppPad->SetLogy(0);
      string doLog[]={"_n","_mass","_pt","Iso","d0Sig"};
      int ndoLog = sizeof(doLog)/sizeof(doLog[0]);
      for (int k=0; k<ndoLog; k++) {
      	if ( hnames[j].find(doLog[k]) != std::string::npos ) {
      	  hdata[j]->GetYaxis()->SetRangeUser(1,hdata[j]->GetMaximum()*3);
      	  uppPad->SetLogy(1);
      	  break;
      	}
      }
      hstacksMC[j]->Draw("histsame");
      hMC2[j]->Draw("histsame"); //the alternative MC
      hdata[j]->Draw("esame");
      leg->Draw();

      uppPad->RedrawAxis();
      // ------------------------------------
      /// ratio hist
      lowPad->cd();	  
      lowPad->Clear();

      TH1F* hmc = (TH1F*) hstacksMC[j]->GetStack()->Last();
      TH1F* hmc2 = (TH1F*) hMC2[j]->Clone();
      TH1F* hmcSyst = (TH1F*) hmc->Clone("hmcSyst");

      TH1F* hratio = (TH1F*) hmc->Clone("hratio");
      TH1F* hratio2 = (TH1F*) hmc2->Clone("hratio2");
      decorate(hratio,cols[iZX],cols[iZX],20,hdata[j]->GetXaxis()->GetTitle(),"MC / Data ");
      hratio->GetYaxis()->SetNdivisions(505);
      float sf=2.6;
      hratio->GetXaxis()->SetTitleSize(hdata[j]->GetYaxis()->GetTitleSize()*sf);
      hratio->GetXaxis()->SetTitleOffset(0.9);
      hratio->GetYaxis()->SetTitleSize(hdata[j]->GetYaxis()->GetTitleSize()*sf);
      hratio->GetYaxis()->SetTitleOffset(0.5);
      hratio->GetYaxis()->SetLabelSize(hdata[j]->GetYaxis()->GetLabelSize()*sf);
      hratio->GetXaxis()->SetLabelSize(hdata[j]->GetYaxis()->GetTitleSize()*sf);
      
      int nbins = hratio->GetXaxis()->GetNbins();
      for (int i=1; i<=nbins; i++) {
	hmcSyst->SetBinContent(i,1);
	hmcSyst->SetBinError(i,1.*hmc->GetBinError(i)/hmc->GetBinContent(i));
	hmc->SetBinError(i,0);
      }
      
      hratio->Divide( hdata[j] );
      hratio->GetYaxis()->SetRangeUser(0.4,1.6);
      hratio->Draw("");
      
      hmcSyst->SetLineColor(kOrange);
      hmcSyst->SetFillColor(kOrange);
      //hmcSyst->Draw("e2same");
      //hratio->Draw("esame");

      hratio2->Divide( hdata[j] );
      decorate(hratio2,kRed+1,kRed+1,20,hmc->GetXaxis()->GetTitle(),"MC / Data ");
      
      hratio->SetMarkerSize(0.15);
      hratio2->SetMarkerSize(0.15);
      hratio2->DrawCopy("esame");

      TLine lin(hratio->GetXaxis()->GetXmin(),1,hratio->GetXaxis()->GetXmax(),1);
      lin.SetLineColor(kBlack);
      lin.SetLineStyle(kDashed);
      lin.Draw();
      
      string plotname = Form("%s/plot_%s_%s.pdf",outputPath.c_str(),hnames[j].c_str(),channels[iChannel].c_str());
      c1.Print(plotname.c_str());
    }
    
    
   
  } // close loop over channels



}
