def GetNdim(tree):
  "Return the dimension of the last expression used with TTree::Draw"
  for i in xrange(100):
    if not tree.GetVar(i):
      return i

def GetValuesFromTree(tree, variable, cut = ''):
  """GetValuesFromTree(tree, variable, cut) ->
  Return a list with <variable> or a generator containing (value1,..valueN) for each event in case
  of multi-dimensional variable"""
  tree.SetEstimate(10*tree.GetEntries()) # default is 1M, can go beyond...
  if isinstance(variable, (list,tuple)):
    variable = ':'.join(variable)
  N = tree.Draw(variable, cut, 'goff')
  Ndim = GetNdim(tree)
  if Ndim == 1:
   
    from itertools import imap
    return [tree.GetV1().__getitem__(i) for i in xrange(N)]
    return imap(tree.GetV1().__getitem__, xrange(N))
  else:
    return ( (tree.GetVal(i)[j] for i in xrange(Ndim)) for j in xrange(N))
