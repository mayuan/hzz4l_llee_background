from ROOTDefs import GetValuesFromTree
from ROOT import *
import math
import numpy as np
path_to_files = '/nfs/dust/atlas/user/naranjo/minitrees/prod_v16/mc16a/merged/BkgCR/'

samples = [
 'ttbar.root',
 'Zjet.root',
 'WZ.root',
]

tchain = TChain("tree_relaxIsoD0")

for s in samples: tchain.Add(path_to_files+s)


ch_llee  = "(event_type==1 || event_type==2)";
ch_4e    = "(event_type==1)";
ch_2e2mu = "(event_type==2)";
all_Cuts = "lepton_passD0sig[0] && lepton_passD0sig[1] && lepton_passD0sig[2] && lepton_passIsoCut[0] && lepton_passIsoCut[1] && lepton_passIsoCut[2] && lepton_passD0sig[2] && lepton_passD0sig[3] &&  lepton_passIsoCut[3] && ((lepton_pt[3]<lepton_pt[2] && el_MCClass[3]==2)|| (lepton_pt[2]<lepton_pt[3] && el_MCClass[2]==2)) && pass_vtx4lCut==1"
channels ={'llee': ch_llee, '4e': ch_4e , '2mu2e': ch_2e2mu}
h = TH1F("h","counter",1,-50,50)

for name,ch in channels.iteritems():
  


  values = np.array(GetValuesFromTree(tchain,"y4l_unconstrained",all_Cuts + ' && ' + ch ))
  run    = np.array(GetValuesFromTree(tchain,"run",all_Cuts + ' && ' + ch ))
  weight = np.array(GetValuesFromTree(tchain,"weight",all_Cuts + ' && ' + ch ))
  
  print "Sanity check for %s ..."%(name) + "OK" if len(values) == len(run) and len(values) == len(weight)  else "FAIL" 
  for i in xrange(len(values)):
        _weight = weight[i]
        if (run[i] == 410501 or run[i]==344171 or run[i]==410000 or run[i]==410009 or run[i] == 410289): _weight = _weight*1.4
    #    if (run[i] == 410289 or run[i] == 410470): _weight = 1.21083*_weight 
        h.Fill(values[i], _weight)
  
  
       
  print name
 
  print h.GetBinContent(1) , "+-" , h.GetBinError(1) , " +- " , math.sqrt(pow(h.GetBinError(1)/h.GetBinContent(1),2) + 0.3*0.3)*h.GetBinContent(1) , "(",h.GetEntries(),")"
  h.Reset()



for s in samples: 
  print "Sample: ",s
  tchain.Reset()
  tchain.Add(path_to_files+s)   
  name= "llee"
  ch= ch_llee 



  values = np.array(GetValuesFromTree(tchain,"y4l_unconstrained",all_Cuts + ' && ' + ch ))
  run    = np.array(GetValuesFromTree(tchain,"run",all_Cuts + ' && ' + ch ))
  weight = np.array(GetValuesFromTree(tchain,"weight",all_Cuts + ' && ' + ch ))

  print "Sanity check for %s ..."%(name) + "OK" if len(values) == len(run) and len(values) == len(weight)  else "FAIL"
  for i in xrange(len(values)):
        _weight = weight[i]
        if (run[i] == 410501 or run[i]==344171 or run[i]==410000 or run[i]==410009 or run[i] == 410289): _weight = _weight*1.4
        if (run[i] == 410289 or run[i] == 410470 ): _weight = 1.21083*_weight
        h.Fill(values[i], _weight)



  print name

  print h.GetBinContent(1) , "+-" , h.GetBinError(1) , " +- " , math.sqrt(pow(h.GetBinError(1)/h.GetBinContent(1),2) + 0.3*0.3)*h.GetBinContent(1) , "(",h.GetEntries(),")"
  h.Reset()

