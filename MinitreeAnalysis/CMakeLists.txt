# The name of the package:
atlas_subdir (MinitreeAnalysis)

atlas_depends_on_subdirs(PUBLIC PhysicsAnalysis/AnalysisCommon/PATCore PhysicsAnalysis/AnalysisCommon/IsolationSelection Event/xAOD/xAODPrimitives)

find_package(ROOT)
find_package(Eigen)

# add the library itself:
atlas_add_library (MinitreeAnalysisLib
   MinitreeAnalysis/*.h Root/*.cxx
   PUBLIC_HEADERS MinitreeAnalysis
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS}
   LINK_LIBRARIES PATCoreLib IsolationSelectionLib xAODPrimitives ${ROOT_LIBRARIES} ${EIGEN_LIBRARIES})

atlas_add_executable (runZplusEl util/runZplusEl.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES MinitreeAnalysisLib ${ROOT_LIBRARIES})

atlas_add_executable (runFourLepTreeAnalysis util/runFourLepTreeAnalysis.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES MinitreeAnalysisLib ${ROOT_LIBRARIES})