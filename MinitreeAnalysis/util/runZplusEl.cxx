#include "MinitreeAnalysis/ZplusEl_analysis.h"
#include "MinitreeAnalysis/ArgInterpreter.h"
#include "TChain.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

int main( int argc, char* argv[]) {
  // //Interpret arguments
  ArgInterpreter ai;
  if (argc==1)
    ai.PrintOptions(1,1,1,1,1,1,1,0,1,1,1);
  ai.Interpret(argc,argv);
  if (!ai.Check(1,2,2,1)) return -1;
  ai.PrintConfig(1,1,1,1,1,1,1,0,1,1,1);

  TString inpar=(TString)ai.input;
  TChain* chain=new TChain("tree_ZplusEl");
  if (inpar.EndsWith(".root"))
    chain->Add(ai.input.c_str());
  else if(inpar.EndsWith(".list")){
    ifstream flist(inpar.Data());
    string line;
    if(flist.is_open()){
      while(getline(flist,line)){
        if(line.find(".root")==string::npos) continue;
        TString ifname(line.c_str());
        ifname=ifname.Strip();
	if(!ifname.Contains("Zpl")) ifname.ReplaceAll("CR","CRZpl");
        chain->Add(ifname);
	cout<<"file "<<ifname<<" added"<<endl;
      }
      flist.close();
    }
  }
  else
    chain->Add(Form("%s/*.root",ai.input.c_str()));

  string year="2016";
  if(inpar.Contains("mc16d") || inpar.Contains("data17")) year="2017";
  else if(inpar.Contains("mc16e") || inpar.Contains("data18")) year="2018";
  else if(inpar.Contains("all") || inpar.Contains("All")) year="all";

  bool vertex=true;
  if(!ai.useVertex) vertex=false;
  ZplusEl_analysis* alg=new ZplusEl_analysis(chain,vertex,ai.isData);
  alg->initialize(ai.output,year);
  for(int i=0;i<alg->fChain->GetEntries();i++){
    alg->execute(i);
  }
  alg->finalize();
  return 0;
}
