#include "MinitreeAnalysis/FourLep_analysis.h"

#include "TString.h"
#include "TChain.h"
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main(int argc, char* argv[]){

  TString          file_name("minitree.root");
  TString          out_name("");
  TString          chain_name("tree_incl_all");
  bool isData=false;
  TString treename="tree_relaxee";
  int masscut=0;

  if (argc==1) {
    printf("Usage: runFourLepTreeAnalysis.cxx --input (-i) <minitree.root> --output (-o) <minitree_hists.root> --treeName (-t) <tree_incl_all>  \n");
    return 0;
  }
  
  for (int i=1;i<=argc;++i) {
    //Grab next argument.
    TString arg=argv[i];
    //Figure out what it is.
    if       (arg=="--input"    || arg=="-i")  file_name = argv[i+1];
    else if  (arg=="--output"   || arg=="-o")  out_name = argv[i+1];
    else if  (arg=="--treeName" || arg=="-t")  chain_name = argv[i+1];
    else if  (arg=="--isData") isData=true;
    else if  (arg=="--useRelaxed") treename="tree_relaxIsoD0";
    else if  (arg=="--useNominal") treename="tree_incl_all";
    else if  (arg=="--useSameSign") treename="tree_ss";
    else if  (arg=="--useInvD0") treename="tree_invD0";
    else if  (arg=="--useInvIso") treename="tree_invIso";
    else if  (arg=="--useEmumumu") treename="tree_emu";
    else if  (arg=="--use3LX") treename="tree_relaxee";
    //else if  (arg=="--useEmuee") treename=
    else if  (arg=="--useMassCut1") masscut=1;
    else if  (arg=="--useMassCut2") masscut=2;
    else if  (arg=="--useMassCut3") masscut=3;
  }
  
  if (out_name=="") {
    out_name = file_name;
    out_name.ReplaceAll(".root","_hists.root");
  }
  else out_name=out_name+"hists.root";

  TChain* chain=new TChain(treename);
  if (file_name.EndsWith(".root"))
    chain->Add(file_name.Data());
  else if(file_name.EndsWith(".list")){
    ifstream flist(file_name.Data());
    string line;
    if(flist.is_open()){
      while(getline(flist,line)){
	if(line.find(".root")==string::npos) continue;
	TString ifname(line.c_str());
	ifname=ifname.Strip();
	chain->Add(ifname);
	//cout<<"file added"<<endl;
      }
    flist.close();
    }
  }
  else
    chain->Add(Form("%s/*.root",file_name.Data()));

  FourLep_analysis* alg=new FourLep_analysis(chain);

  std::cout << "input: " << file_name << std::endl;
  std::cout << "output: " << out_name << std::endl;
  std::cout << "chain: " << treename << std::endl;
  
  alg->runAnalysis(out_name.Data(),isData,masscut);
  return 0;

}
