# Implementation of fit for the 3L+X (e/mu) fit method.

Steps:

1) flattenCRTrees -> make a skimmed 3L+X ntuple from the official minitrees

2) buildBGWorkspace -> build the workspace for the fit

3) fitBGWorkspace -> read the workspace and do the fit


