#include "TFile.h"
#include "TTree.h"
#include "TObject.h"
#include "TMath.h"
#include "TChain.h"
#include "TBranch.h"

#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <array>

#include "TLorentzVector.h"
#include "TSystem.h"

int main(int argc, char* argv[]){

  using namespace std;

  string input;
  string region;
  //string isZZ="";
  bool useVtxCut=true;
  bool isData=true;
  string year;
  //int Xpos=-1;
  if (argc==1){
    cout<<"\ngive a directory:"<<endl;
    cout<<"--input /path/to/ZplusXTrees/"<<endl;
    cout<<"\nand a region:"<<endl;
    cout<<"--region ThreeLplusX"<<endl;
    cout<<"--useVtxCut {(t),f}"<<endl;
    cout<<"--year {2016,2017,2018}"<<endl;
    return -1;
  }

  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--input")){
      input=argv[++a];
      //clean name
      if (!(input[input.size()-1]=='/') && input.find(".root")==string::npos && input.find(".list")==string::npos) input.append("/");
    }
    else if (!strcmp(argv[a],"--region")){
      ++a;
      region=argv[a];
      cout<<"got a region:"<<region<<endl;
      //if (region=="ZplusX") Xpos=2;
      //else if (region=="ThreeLplusX") Xpos=3;
      //else Xpos=-1;
    }
    else if (!strcmp(argv[a],"--isMC")){
      isData=false;
      if(region=="ThreeLplusX" && !isData) cout<<"flattening 3L+X MC"<<endl;
      if(region=="relaxIsoD0" && !isData) cout<<"flattening relaxIsoD0 MC"<<endl;
    }
    else if (!strcmp(argv[a],"--useVtxCut")){
      if ((TString)argv[++a] == "t" || (TString)argv[++a] == "true")
	useVtxCut = true;
      else useVtxCut = false;
      cout << "do d0+vtx cut (unless relaxIsoD0): " << useVtxCut << endl;
    }
    else if(!strcmp(argv[a],"--year")){
      year=argv[++a];
      cout<<"year "<<year<<endl;
    }
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }
  if(region=="relaxIsoD0") useVtxCut=false;

  //if (Xpos==-1) { cout<<"invalid region! provide --region ZplusX or ThreeLplusX"<<endl; return -1;}

  string fsuffix="";
  if((region=="ThreeLplusX" || region=="relaxIsoD0") && !isData) fsuffix="_mc";
  TFile* outfile = new TFile(Form("v25Files/flattened%s%s_%s.root",region.c_str(),fsuffix.c_str(),year.c_str()),"RECREATE");
  
  //const char* user = getlogin();
  //gSystem->Exec(Form("rm -rf /tmp/%s/mergedtree",user));
  /*
  TFile* infile;
  if(input.find(".list")==string::npos){
    gSystem->Exec(Form("rm -rf /nfs/dust/atlas/user/naranjo/Development/HZZ4lBackground/build/mergedtree"));
    gSystem->Exec(Form("mkdir -p /nfs/dust/atlas/user/naranjo/Development/HZZ4lBackground/build/mergedtree"));
    if(input.find(".root")!=string::npos) gSystem->Exec(Form("hadd /nfs/dust/atlas/user/naranjo/Development/HZZ4lBackground/build/mergedtree/merged.root %s",input.c_str()));
    else gSystem->Exec(Form("hadd /nfs/dust/atlas/user/naranjo/Development/HZZ4lBackground/build/mergedtree/merged.root %s/*.root*",input.c_str()));
  }
  else if(doMerge){
    string line;
    //string command="hadd -f /eos/atlas/user/w/wleight/mergedFiles/";
    string command="hadd -f /afs/cern.ch/work/w/wleight/public/Higgs_2018_bkg/mergedFiles/";
    if(region=="ThreeLplusX"){
      if(isData) command=command+"ThreeLplusX_data_"+year+".root ";
      else command=command+"ThreeLplusX_MC_"+year+".root ";
    }
    else command=command+"ZplusX_"+year+".root ";
    ifstream flist(input.c_str());
    if(flist.is_open()){
      while(getline(flist,line)){
	if(line.find(".root")==string::npos) continue;
	command+=line;
	command+=" ";
      }
      flist.close();
    }
    gSystem->Exec(command.c_str());
  }

  if(input.find(".list")==string::npos){
    infile = new TFile(Form("/nfs/dust/atlas/user/naranjo/Development/HZZ4lBackground/build/mergedtree/merged.root"),"READ");
  }
  else{
    if(region=="ThreeLplusX"){
      //if(isData) infile=new TFile(Form("/eos/atlas/user/w/wleight/mergedFiles/ThreeLplusX_data_%s.root",year.c_str()),"READ");
      //else infile=new TFile(Form("/eos/atlas/user//w/wleight/mergedFiles/ThreeLplusX_MC_%s.root",year.c_str()),"READ");
      if(isData) infile=new TFile(Form("/afs/cern.ch/work/w/wleight/public/Higgs_2018_bkg/mergedFiles/ThreeLplusX_data_%s.root",year.c_str()),"READ");
      else infile=new TFile(Form("/afs/cern.ch/work/w/wleight/public/Higgs_2018_bkg/mergedFiles/ThreeLplusX_MC_%s.root",year.c_str()),"READ");
    }
    //else infile=new TFile(Form("/eos/atlas/user/w/wleight/mergedFiles/ZplusX_%s.root",year.c_str()),"READ");
    else infile=new TFile(Form("/afs/cern.ch/work/w/wleight/public/Higgs_2018_bkg/mergedFiles/ZplusX_%s.root",year.c_str()),"READ");
  }

  //Loop over trees in input
  TList* list = infile->GetListOfKeys();
  TIter next(list);
  while (TObject* obj = (TObject*)next()){

    std::cout<<"found object: "<<obj->GetName()<<" of type "<<obj->ClassName()<<std::endl;

    if (strcmp(obj->GetName(),"metatree")==0) {
      cout<<"infile contains metatree! Did you remember to postProcess? killing job"<<endl;
      //return -1;
    }

    TTree* intree = (TTree*)infile->Get(obj->GetName());
    if (!intree || intree->IsZombie()) {
      std::cout<<"not a TTree! skipping.."<<std::endl;
      continue;
    }

    std::string tname=intree->GetName();
    if(region=="ThreeLplusX" && tname.find("relaxee")==std::string::npos) continue;
    if(region=="ZplusX" && tname.find("ZplusEl")==std::string::npos) continue;

    cout<<"flattening "<<intree->GetName()<<endl;
  */

  TString tname="tree_";
  if(region=="ThreeLplusX") tname+="relaxee";
  else if(region=="relaxIsoD0") tname+="relaxIsoD0";
  else tname+="ZplusEl";
  TChain* intree=new TChain(tname,tname);
  string line;
  ifstream flist(input.c_str());
  if(flist.is_open()){    
    while(getline(flist,line)){
      if(line.find(".root")==string::npos) continue;
      cout<<"add file "<<line<<endl;
      intree->Add(line.data());
    }
    flist.close();
  }
    Int_t run=0;
    unsigned long long event=0;
    int event_type=1,prod_type=-1, prod_type_HM=-1,prod_type_fine=-1,prod_type_sb=-1;
    double weight=1.;
    //shape variables: only saved for 3L+X
    float mZ1_fsr=0, mZ2_fsr=0,y4l_fsr=0,pt4l_fsr=0,cthstr=0,m4l_HM,m4l_fsr=0,met,phi4l_unconstrained,eta4l_fsr,bdt,cp_obs1,cp_obs2,cp_obs3,cp_obs4,cp_obs5,cp_obs6;
    float OO1_jj_tCzz,OO1_jj_tCza,OO1_jj_cHWtil,OO1_4l_tCzz,OO1_4l_tCza,OO1_4l_tCaa,OO1_4l_cHWtil,OO1_4l_cHBtil,OO1_4l_cHWBtil;
    float phi,phi1,cth1,cth2;
    int n_jets=0,n_bjets=0,n_bjets60=-1;
    float mjj=0,detajj=0,dphijj=0,m4lj=0,m4ljj=0,pt4lj=0,pt4ljj=0;
    vector<int>* in_mcClass=new vector<int>;
    vector<int>* in_nInner=new vector<int>;
    //int in_mcClass[4];
    //int in_nInner[4];
    vector<float> *in_eta=new vector<float>;
    vector<float> *in_pt=new vector<float>;
    vector<float> *in_phi=new vector<float>;
    vector<float> *in_m=new vector<float>;
    //array<float,4>* in_res=new array<float,4>;
    //TBranch* b_res;
    vector<int>* in_lepton_passD0sig=new vector<int>;
    vector<int>* in_lepton_passIsoCut=new vector<int>;
    //int in_lepton_passD0sig[4];
    //int in_lepton_passIsoCut[4];
    //float lepton_ptvarcone20[4];
    //float lepton_ptvarcone30[4];
    //float ptvarcone30_tightTTVA[4];
    //float topoetcone20[4];
    vector<float> *jetpt = new vector<float>;
    vector<float> *jeteta = new vector<float>;
    vector<float> *jetphi = new vector<float>;
    vector<float> *jetm = new vector<float>;
    Int_t in_pass_vtx4lCut = 0;
    float mu = 0;
    float NN_1Jet_pTLow_ZZ,NN_1Jet_pTMed_ZZ,NN_2Jet_Low_VH,NN_ttHHad_ttV,NN_2Jet_Low_VBF;
    bool splitCatByNN=true;
    if(!intree->FindBranch("NN_1Jet_pTLow_ZZ")) splitCatByNN=false;
    bool useTreeProdType=true;
    if(!intree->FindBranch("prod_type_fine")) useTreeProdType=false;

    intree->SetBranchAddress("run",&run);
    intree->SetBranchAddress("ave_int_per_xing",&mu);
    intree->SetBranchAddress("weight",&weight);
    if(region=="ThreeLplusX" || region=="relaxIsoD0") intree->SetBranchAddress("mZ1_fsr",&mZ1_fsr);
    else intree->SetBranchAddress("mZ_fsr",&mZ1_fsr);
    intree->SetBranchAddress("el_MCClass",&in_mcClass);
    intree->SetBranchAddress("el_nInnerExpPix",&in_nInner);
    if(splitCatByNN){
      intree->SetBranchAddress("NN_1Jet_pTLow_ZZ",&NN_1Jet_pTLow_ZZ);
      intree->SetBranchAddress("NN_1Jet_pTMed_ZZ",&NN_1Jet_pTMed_ZZ);
      intree->SetBranchAddress("NN_2Jet_Low_VH",&NN_2Jet_Low_VH);
      intree->SetBranchAddress("NN_ttHHad_ttV",&NN_ttHHad_ttV);
    }
    intree->SetBranchAddress("NN_2Jet_Low_VBF",&NN_2Jet_Low_VBF);
    intree->SetBranchAddress("event_type",&event_type);
    intree->SetBranchAddress("lepton_pt",&in_pt);
    intree->SetBranchAddress("lepton_eta",&in_eta);
    intree->SetBranchAddress("lepton_phi",&in_phi);
    intree->SetBranchAddress("lepton_m",&in_m);
    //intree->SetBranchAddress("lepton_res",&in_res,&b_res);
    intree->SetBranchAddress("lepton_passD0sig",&in_lepton_passD0sig);
    intree->SetBranchAddress("lepton_passIsoCut",&in_lepton_passIsoCut);
    /*
    intree->SetBranchAddress("lepton_ptvarcone20",&lepton_ptvarcone20);
    intree->SetBranchAddress("lepton_ptvarcone30",&lepton_ptvarcone30);
    intree->SetBranchAddress("lepton_ptvarcone30_TightTTVA_pt1000",&ptvarcone30_tightTTVA);
    intree->SetBranchAddress("lepton_topoetcone20",&topoetcone20);
    */
    if(region=="ThreeLplusX" || region=="relaxIsoD0"){ //for building shapes from 3L+X CR, we want to save some additional variables
      intree->SetBranchAddress("pass_vtx4lCut",&in_pass_vtx4lCut);
      intree->SetBranchAddress("jet_pt",&jetpt);
      intree->SetBranchAddress("jet_phi",&jetphi);
      intree->SetBranchAddress("jet_eta",&jeteta);
      intree->SetBranchAddress("jet_m",&jetm);
      intree->SetBranchAddress("m4l_fsr",&m4l_fsr);
      intree->SetBranchAddress("m4l_constrained_HM",&m4l_HM);
      intree->SetBranchAddress("mZ2_fsr",&mZ2_fsr);
      intree->SetBranchAddress("event",&event);
      intree->SetBranchAddress("y4l_fsr",&y4l_fsr);
      intree->SetBranchAddress("cthstr_fsr",&cthstr);
      intree->SetBranchAddress("n_jets",&n_jets);
      intree->SetBranchAddress("pt4l_fsr",&pt4l_fsr);
      intree->SetBranchAddress("eta4l_fsr",&eta4l_fsr);
      intree->SetBranchAddress("phi4l_unconstrained",&phi4l_unconstrained);
      intree->SetBranchAddress("prod_type",&prod_type);
      if(useTreeProdType){
	intree->SetBranchAddress("prod_type_SB",&prod_type_sb);
	intree->SetBranchAddress("prod_type_fine",&prod_type_fine);
      }
      intree->SetBranchAddress("prod_type_HM",&prod_type_HM);
      intree->SetBranchAddress("dijet_invmass",&mjj);
      intree->SetBranchAddress("dijet_deltaeta",&detajj);
      intree->SetBranchAddress("dijet_deltaphi",&dphijj);
      intree->SetBranchAddress("n_jets_btag70",&n_bjets);
      intree->SetBranchAddress("n_jets_btag60",&n_bjets60);
      //intree->SetBranchAddress("jet_btag70",&jet_btag);
      intree->SetBranchAddress("phi_fsr",&phi);
      intree->SetBranchAddress("phi1_fsr",&phi1);
      intree->SetBranchAddress("cth1_fsr",&cth1);
      intree->SetBranchAddress("cth2_fsr",&cth2);
      intree->SetBranchAddress("met_et",&met);
      intree->SetBranchAddress("m4ljj_fsr",&m4ljj);
      intree->SetBranchAddress("m4lj_fsr",&m4lj);
      intree->SetBranchAddress("pt4ljj_fsr",&pt4ljj);
      intree->SetBranchAddress("pt4lj_fsr",&pt4lj);
      intree->SetBranchAddress("BDT_Massdiscriminant",&bdt);
      intree->SetBranchAddress("cp_obs1",&cp_obs1);
      intree->SetBranchAddress("cp_obs2",&cp_obs2);
      intree->SetBranchAddress("cp_obs3",&cp_obs3);
      intree->SetBranchAddress("cp_obs4",&cp_obs4);
      intree->SetBranchAddress("cp_obs5",&cp_obs5);
      intree->SetBranchAddress("cp_obs6",&cp_obs6);
      intree->SetBranchAddress("OO1_4l_cHWBtil1",&OO1_4l_cHWBtil);
      intree->SetBranchAddress("OO1_4l_cHWtil1",&OO1_4l_cHWtil);
      intree->SetBranchAddress("OO1_4l_cHBtil1",&OO1_4l_cHBtil);
      intree->SetBranchAddress("OO1_4l_tCzz1",&OO1_4l_tCzz);
      intree->SetBranchAddress("OO1_4l_tCza1",&OO1_4l_tCza);
      intree->SetBranchAddress("OO1_4l_tCaa1",&OO1_4l_tCaa);
      intree->SetBranchAddress("OO1_jj_tCzz1",&OO1_jj_tCzz);
      intree->SetBranchAddress("OO1_jj_tCza1",&OO1_jj_tCza);
      intree->SetBranchAddress("OO1_jj_cHWtil1",&OO1_jj_cHWtil);
    }
    outfile->cd();
    TTree* outtree = new TTree(intree->GetName(),intree->GetName());

    int MCClass,nInner;
    float eta,pt;
    //shape variables: only saved for 3L+X
    int njet,nbjet;
    float mZ1,mZ2,/*m4l,*/pt4l,y4l,jet1pt,jet2pt,cts,jet1y;
    int prod_type_cp;

    outtree->Branch("event_type",&event_type);
    outtree->Branch("run",&run);
    outtree->Branch("mZ1",&mZ1);

    outtree->Branch("weight",&weight);
    outtree->Branch("MCClass",&MCClass);
    outtree->Branch("nInner",&nInner);
    outtree->Branch("pt",&pt);
    outtree->Branch("eta",&eta);

    if(region=="ThreeLplusX" || region=="relaxIsoD0"){ //for building shapes in the 3L+X CR, we want to save some additional variables
      outtree->Branch("m4l",&m4l_fsr);
      outtree->Branch("m4l_constrained_HM",&m4l_HM);
      outtree->Branch("mZ2",&mZ2);
      outtree->Branch("njet",&njet);
      outtree->Branch("jet1pt",&jet1pt);
      outtree->Branch("jet2pt",&jet2pt);
      outtree->Branch("cts",&cts);
      //outtree->Branch("m4l",&m4l);
      outtree->Branch("pt4l",&pt4l);
      outtree->Branch("y4l",&y4l);
      //outtree->Branch("m4l_fsr",&m4l_fsr);
      outtree->Branch("prod_type",&prod_type);
      outtree->Branch("prod_type_HM",&prod_type_HM);
      outtree->Branch("prod_type_sb",&prod_type_sb);
      outtree->Branch("prod_type_cp",&prod_type_cp);
      outtree->Branch("mjj",&mjj);
      outtree->Branch("detajj",&detajj);
      outtree->Branch("dphijj",&dphijj);
      outtree->Branch("nbjet",&nbjet);
      outtree->Branch("cth1",&cth1);
      outtree->Branch("cth2",&cth2);
      outtree->Branch("phi",&phi);
      outtree->Branch("phi1",&phi1);
      outtree->Branch("m4lj",&m4lj);
      outtree->Branch("m4ljj",&m4ljj);
      outtree->Branch("pt4lj",&pt4lj);
      outtree->Branch("pt4ljj",&pt4ljj);
      outtree->Branch("lepton_pt",&in_pt);
      outtree->Branch("lepton_eta",&in_eta);
      outtree->Branch("lepton_phi",&in_phi);
      //outtree->Branch("lepton_res",&in_res);
      outtree->Branch("bdt",&bdt);
      outtree->Branch("mu",&mu);
      outtree->Branch("jet1y",&jet1y);
      outtree->Branch("cp_obs1",&cp_obs1);
      outtree->Branch("cp_obs2",&cp_obs2);
      outtree->Branch("cp_obs3",&cp_obs3);
      outtree->Branch("cp_obs4",&cp_obs4);
      outtree->Branch("cp_obs5",&cp_obs5);
      outtree->Branch("cp_obs6",&cp_obs6);
      outtree->Branch("OO1jjtCzz",&OO1_jj_tCzz);
      outtree->Branch("OO1jjtCza",&OO1_jj_tCza);
      outtree->Branch("OO1jjcHWtil",&OO1_jj_cHWtil);
      outtree->Branch("OO14ltCzz",&OO1_4l_tCzz);
      outtree->Branch("OO14ltCza",&OO1_4l_tCza);
      outtree->Branch("OO14ltCaa",&OO1_4l_tCaa);
      outtree->Branch("OO14lcHWtil",&OO1_4l_cHWtil);
      outtree->Branch("OO14lcHWBtil",&OO1_4l_cHWBtil);
      outtree->Branch("OO14lcHBtil",&OO1_4l_cHBtil);
      //outtree->Branch("NN_1Jet_pTLow_ZZ",&NN_1Jet_pTLow_ZZ);
      outtree->Branch("NNVBF",&NN_2Jet_Low_VBF);
    }

    //loop over input and flatten to output
    for (int i(0);i<intree->GetEntries();++i){

      if (i%10000==0) cout<<i<<"/"<<intree->GetEntries()<<endl;
      intree->GetEntry(i);

      //for relaxIsoD0, skip the 2l2mu
      if(region=="relaxIsoD0" && (event_type==0 || event_type==3)) continue;

      //if(run == 410289) weight = -4020.32*weight;
      //if(run == 410289 || run == 410470) weight = 1.21083*weight;

      int xloc=3;
      TLorentzVector Z[2];
      TLorentzVector elvec;
      if (region=="ZplusX") {
        if ( !in_lepton_passD0sig->at(0) || !in_lepton_passD0sig->at(1) || !in_lepton_passIsoCut->at(0) || !in_lepton_passIsoCut->at(1) ) continue;
	//if ( !in_lepton_passD0sig[0] || !in_lepton_passD0sig[1] || !in_lepton_passIsoCut[0] || !in_lepton_passIsoCut[1] ) continue;
        //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.30 || lepton_ptvarcone20[1] > 0.30 || lepton_topoetcone20[0] > 0.40 || lepton_topoetcone20[1] > 0.40)) continue; //!)
        //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.30 || lepton_ptvarcone30[1] > 0.30 || lepton_topoetcone20[0] > 0.60 || lepton_topoetcone20[1] > 0.60)) continue; //!)
        //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.15 || lepton_ptvarcone20[1] > 0.15 )) continue; //!)
        //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.15 || lepton_ptvarcone30[1] > 0.15 )) continue; //!)
        //if ( !in_lepton_passIsoCut[0] || !in_lepton_passIsoCut[1] ) continue;
	//temporary use of FCLoose+lepton dR cut
	/*
	bool pass=true;
	for(int ilep=0;ilep<3;ilep++){
	  if(ilep<2){
	    if(ptvarcone30_tightTTVA[ilep]/in_pt->at(ilep)>0.15){ pass=false; break;}
	    if(event_type==11 && topoetcone20[ilep]>0.3) {pass=false; break;}
	    if(event_type==13 && topoetcone20[ilep]>0.2) {pass=false; break;}
	  }
	  for(int jlep=ilep+1;jlep<3;jlep++){
	    if(sqrt((in_eta->at(ilep)-in_eta->at(jlep))*(in_eta->at(ilep)-in_eta->at(jlep))+(in_phi->at(ilep)-in_phi->at(jlep))*(in_phi->at(ilep)-in_phi->at(jlep)))<0.3) {pass=false; break;}
	  }
	  if(!pass) break;
	}
	if(!pass) continue;
	*/
	Z[0].SetPtEtaPhiM(in_pt->at(0),
			  in_eta->at(0),
			  in_phi->at(0),
			  in_m->at(0)
			  );
	Z[1].SetPtEtaPhiM(in_pt->at(1),
			  in_eta->at(1),
			  in_phi->at(1),
			  in_m->at(1)
			  );
	elvec.SetPtEtaPhiM(in_pt->at(2),
			in_eta->at(2),
			in_phi->at(2),
			in_m->at(2)
			);
	xloc=2;
      }
      else if(region=="ThreeLplusX") elvec.SetPtEtaPhiM(in_pt->at(3),in_eta->at(3),in_phi->at(3),in_m->at(3));
      else{ //relaxIsoD0, have to check for lowest-pT
	if(in_pt->at(2)<in_pt->at(3)){
	  xloc=2;
	  elvec.SetPtEtaPhiM(in_pt->at(2),in_eta->at(2),in_phi->at(2),in_m->at(2));
	}
	else elvec.SetPtEtaPhiM(in_pt->at(3),in_eta->at(3),in_phi->at(3),in_m->at(3));
      }
      
      if(!isData) MCClass = in_mcClass->at(xloc);
      nInner = in_nInner->at(xloc);
      //MCClass = in_mcClass[xloc];
      //nInner = in_nInner[xloc];
      eta = in_eta->at(xloc);
      pt = in_pt->at(xloc); //FIXME
      mZ1=mZ1_fsr;
      /*
      for(int ilep=0;ilep<4;ilep++){
	cout<<"run: "<<run<<", event type "<<event_type<<", lepton "<<ilep<<" pass iso="<<in_lepton_passIsoCut[ilep]<<", ptvarcone20: "<<lepton_ptvarcone20[ilep]<<", ptvarcone30: "<<lepton_ptvarcone30[ilep]<<", topoetcone: "<<topoetcone20[ilep]<<", pt: "<<in_pt->at(ilep)<<", eta: "<<in_eta->at(ilep)<<", phi: "<<in_phi->at(ilep)<<", pass d0 sig="<<in_lepton_passD0sig[ilep]<<endl;
      }
      */
      if(region=="ThreeLplusX" || region=="relaxIsoD0"){ //for building shapes in the 3L+X CR, we want to save some additional variables
	if ( !in_lepton_passD0sig->at(0)  || !in_lepton_passD0sig->at(1) ){
	//if ( !in_lepton_passD0sig[0]  || !in_lepton_passD0sig[1] ){
	  //cout<<"reject for fail d0 sig"<<endl;
	  continue;
	}

        if ( !in_lepton_passIsoCut->at(0) || !in_lepton_passIsoCut->at(1) ){ 
	//if ( !in_lepton_passIsoCut[0] || !in_lepton_passIsoCut[1] ){
	  //cout<<"reject for iso failure"<<endl;
	  continue;
	}

	if(region=="ThreeLplusX" && (!in_lepton_passD0sig->at(2) || !in_lepton_passIsoCut->at(2))) continue;
	//if(region=="ThreeLplusX" && (!in_lepton_passD0sig[2] || !in_lepton_passIsoCut[2])) continue;

        //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.30 || lepton_ptvarcone20[1] > 0.30 || lepton_topoetcone20[0] > 0.40 || lepton_topoetcone20[1] > 0.40)) continue; //!)
        //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.30 || lepton_ptvarcone30[1] > 0.30 || lepton_topoetcone20[0] > 0.60 || lepton_topoetcone20[1] > 0.60)) continue; //!)
        //if (event_type==1 &&  (lepton_ptvarcone20[0] > 0.15 || lepton_ptvarcone20[1] > 0.15 )) continue; //!)
        //if (event_type==2 &&  (lepton_ptvarcone30[0] > 0.15 || lepton_ptvarcone30[1] > 0.15 )) continue; //!)
        //if ((lepton_ptvarcone20[2] > 0.30 )) continue; //!)
        //if ((lepton_ptvarcone30[2] > 0.15 )) continue; //!)
	//temporary use of FCLoose+lepton dR cut
	/*
        bool pass=true;
        for(int ilep=0;ilep<4;ilep++){
	  //cout<<"run: "<<run<<", event type "<<event_type<<", lepton "<<ilep<<" ptvarcone: "<<ptvarcone30_tightTTVA[ilep]<<", topoetcone: "<<topoetcone20[ilep]<<", pt: "<<in_pt->at(ilep)<<", eta: "<<in_eta->at(ilep)<<", phi: "<<in_phi->at(ilep)<<endl;
	  if(ilep<3){
            if(ptvarcone30_tightTTVA[ilep]/in_pt->at(ilep)>0.15){pass=false; break;}
	    if(event_type==2 && ilep<=1 && topoetcone20[ilep]>0.3) {pass=false; break;}
	    if(event_type==1 && ilep<=1 && topoetcone20[ilep]>0.2) {pass=false; break;}
	    if(ilep==2 && topoetcone20[ilep]>0.2) {pass=false; break;}
          }
          for(int jlep=ilep+1;jlep<4;jlep++){
            if(sqrt((in_eta->at(ilep)-in_eta->at(jlep))*(in_eta->at(ilep)-in_eta->at(jlep))+(in_phi->at(ilep)-in_phi->at(jlep))*(in_phi->at(ilep)-in_phi->at(jlep)))<0.3) {pass=false; break;}
          }
          if(!pass) break;
        }
        if(!pass) continue;
	*/
	y4l=fabs(y4l_fsr);
	pt4l=pt4l_fsr;
	mZ2=mZ2_fsr;
	cts=fabs(cthstr);
	njet=n_jets;
	//cp prod type: 1=VBF (prod_type 3), 0 otherwise
	prod_type_cp=1;
	//if(prod_type==3) prod_type_cp=1;
	//shapes in VBF category for CP; OO1_jj variables set to -99 if Njet<2 and -49 if Njet>1 but still not VBF (to avoid efficiency issues in calculating the first bin)
	//NN changed to [0,2] with !VBF=0.5 (this will never be a shape, so no worries about efficiency issues)
	if(njet<2){
	  prod_type_cp=0;
	  NN_2Jet_Low_VBF=0.5;
	  OO1_jj_tCzz=-99;
	  OO1_jj_tCza=-99;
	  OO1_jj_cHWtil=-99;
	}
	else if(mjj<120){
	  prod_type_cp=0;
	  NN_2Jet_Low_VBF=0.5;
	  OO1_jj_tCzz=-49;
          OO1_jj_tCza=-49;
          OO1_jj_cHWtil=-49;
        }
	else NN_2Jet_Low_VBF+=1;
	if(prod_type_cp==3 && NN_2Jet_Low_VBF==0.5) std::cout<<"prod_type_cp mismatch!"<<std::endl;
	//nbjet binning: separate no jets and no bjets
	if(n_jets==0) nbjet=0;
	else if(n_bjets==0) nbjet=1;
	else nbjet=2;
	if(!useTreeProdType){
	  if(met>100 && n_bjets60>=1 && n_jets>=2) prod_type_sb=3;
	  else if(prod_type==0) prod_type_sb=0;
	  else if(prod_type==1) prod_type_sb=1;
	  else if(prod_type==2 || prod_type==3) prod_type_sb=2;
	  else if(prod_type==4) prod_type_sb=4;
	  else prod_type_sb=5; //overflow bin, for sPlot purposes
	  //if NN variables not present, 9, 11, 15, and 16 will be empty
	  if(prod_type==0){
	    if(pt4l>10 && pt4l<100) prod_type=7;
	    else if(pt4l>100) prod_type=8;
	  }
	  else if(prod_type==1){
	    if(pt4l<60){
	      if(splitCatByNN && NN_1Jet_pTLow_ZZ>0.25) prod_type=9;
	    }
	    else if(pt4l<120){
	      prod_type=10;
	      if(splitCatByNN && NN_1Jet_pTMed_ZZ>0.25) prod_type=11;
	    }
	    else if(pt4l<200) prod_type=12;
	    else prod_type=13;
	  }
	  else if(prod_type==2){
	    if(splitCatByNN && NN_2Jet_Low_VH > 0.2) prod_type=14;
	  }
	  else if(prod_type==3){
	    if(pt4l<200){
	      prod_type=2;
	      if(splitCatByNN && NN_2Jet_Low_VH > 0.2) prod_type=14;
	    }
	  }
	  else if(prod_type==6){
	    if(splitCatByNN && NN_ttHHad_ttV>0.4) prod_type=15;
	  }
	}
	else{
	  //even with this, still the question of the categories that are split by NN variables
	  //keep those next to the others to avoid confusion
	  if(prod_type_fine<3) prod_type=prod_type_fine;
	  else if(prod_type_fine==3){
	    if(splitCatByNN && NN_1Jet_pTLow_ZZ>0.25) prod_type=4;
	    else prod_type=3;
	  }
	  else if(prod_type_fine==4){
	    if(splitCatByNN && NN_1Jet_pTMed_ZZ>0.25) prod_type=6;
	    else prod_type=5;
	  }
	  else if(prod_type_fine<8) prod_type=prod_type_fine+2;
	  else if(prod_type_fine==8){
	    if(splitCatByNN && NN_2Jet_Low_VH > 0.2) prod_type=11;
	    else prod_type=10;
	  }
	  else if(prod_type_fine<11) prod_type=prod_type_fine+3;
	  else{
	    if(splitCatByNN && NN_ttHHad_ttV>0.4) prod_type=15;
	    else prod_type=14;
	  }
	}
	if(njet>0){
	  jet1pt=jetpt->at(0);
	  jet1y=abs(jeteta->at(0));
	  pt4lj+=1;
	}
	else{
	  jet1pt=29.5; //to fall in 29-30 bin (same for jet2pt in next line)
	  m4lj=119.5; //this and m4ljj below are handled similarly to jet1pt and jet2pt
	  jet1y=-1;
	  pt4lj=0.5;
	}
	//etajj and phijj overflow from 0-1 and will be filled with +1; mjj is more complex but this should work
	if(njet<2){detajj=0.5; mjj=-0.5; dphijj=0.5; jet2pt=29.5; m4ljj=179.5; pt4ljj=0.5;}
	else{
	  jet2pt=jetpt->at(1);
	  detajj+=1;
	  dphijj+=1;
	  pt4lj+=1;
	}
	//calculate pt4lj and pt4ljj
	//no longer necessary, but still modify to match the binning
	TLorentzVector comb;
      }
      if ( isnan(MCClass) || isnan(nInner)) {
	cout << "WARNING variable is null!!!" << endl;
	continue;
      }
      if (useVtxCut) {
	if (region=="ThreeLplusX" && !in_pass_vtx4lCut){
	  //cout<<"fails vertex cut: "<<in_pass_vtx4lCut<<endl;
	  continue;
	}
	if (!in_lepton_passD0sig->at(xloc)){
	//if (!in_lepton_passD0sig[xloc]){
	  //cout<<"X fails d0 sig"<<endl;
	  continue;
	}
      }
      

      outtree->Fill();
    }

    outtree->Write("",TObject::kOverwrite);
    //}

  outfile->Write("",TObject::kOverwrite);
  delete outfile;
  //delete infile;

}
