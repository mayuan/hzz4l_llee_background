#include "TFile.h"
#include "TTree.h"
#include "TObject.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TLegend.h"
#include "THStack.h"
#include "H4lBkgUtils/H4lStyle.h"
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cmath>
#include "../../H4lBackgroundReader/H4lBackgroundReader/H4lBkgEnums.h"

using namespace std;

int main(int argc, char* argv[])
{
  TString inputData;
  TString inputMC;
  float scaleMC=1.;
  bool isHM=false;
  TString year;
  bool splitCatMC=false;

  if (argc==1){
    cout<<"--inputData "<<endl;
    cout<<"--inputMC "<<endl;
    cout<<"--scaleMC "<<endl;
    cout<<"--year"<<endl;
    return -1;
  }

  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--inputData")){
      inputData=argv[++a];
    }
    else if (!strcmp(argv[a],"--inputMC")){
      inputMC=argv[++a];
    }
    else if (!strcmp(argv[a],"--scaleMC")){
      scaleMC=atof(argv[++a]);
    }
    else if(!strcmp(argv[a],"--HM")){
      isHM=true;
    }
    else if(!strcmp(argv[a],"--year")){
      year=argv[++a];
    }
    else if(!strcmp(argv[a],"--splitCatMC")){
      splitCatMC=true;
    }
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }

  cout<<"--inputData " << inputData <<endl;
  cout<<"--inputMC "<< inputMC <<endl;
  cout<<"--scaleMC "<< scaleMC <<endl;
  cout<<"year: "<<year<<endl;
  if(isHM) cout<<"HM categories"<<endl;

  std::map<TString,float> lumi;
  lumi["2016"]=36.2;
  lumi["2017"]=44.3;
  lumi["2018"]=59.9;
  lumi["all"]=139;

  TH1::SetDefaultSumw2();

  string hmString="";
  if(isHM) hmString="_hm";

  TFile *infile = TFile::Open(inputData,"READ");
  TH3F* h_f_pt_njet = (TH3F*) (infile->Get(Form("light_prod_type%s_pt_njet",hmString.c_str())))->Clone();
  TH3F* h_gamma_pt_njet = (TH3F*) (infile->Get(Form("gamma_prod_type%s_pt_njet",hmString.c_str())))->Clone();
  TH2F* h_f_njet_2d = (TH2F*) (infile->Get("light_pt_njet"))->Clone();
  TH2F* h_gamma_njet_2d = (TH2F*) (infile->Get("gamma_pt_njet"))->Clone();
  TH3F* h_f_y4l=(TH3F*)(infile->Get("light_y4l_2d_pt_njet"))->Clone();
  TH3F* h_gamma_y4l=(TH3F*)(infile->Get("gamma_y4l_2d_pt_njet"))->Clone();
  TH3F* h_sb_f_pt_njet = (TH3F*) (infile->Get("light_prod_type_sb_pt_njet"))->Clone();
  TH3F* h_sb_gamma_pt_njet = (TH3F*) (infile->Get("gamma_prod_type_sb_pt_njet"))->Clone();
  TH3F* h_jet1pt_f_pt_njet = (TH3F*) (infile->Get("light_jet1pt_pt_njet"))->Clone();
  TH3F* h_jet1pt_gamma_pt_njet = (TH3F*) (infile->Get("gamma_jet1pt_pt_njet"))->Clone();
  TH3F* h_jet1pt_f_pt_m4l = (TH3F*) (infile->Get("light_jet1pt_pt_m4l"))->Clone();
  TH3F* h_jet1pt_gamma_pt_m4l = (TH3F*) (infile->Get("gamma_jet1pt_pt_m4l"))->Clone();

  cout<<"fakes prod_type hist integral="<<h_f_pt_njet->Integral()<<"; njet hist integral="<<h_f_njet_2d->Integral()<<"; y4l hist integral: "<<h_f_y4l->Integral()<<"; sb hist integral: "<<h_sb_f_pt_njet->Integral()<<endl;

  //TH1F* m4l_light=(TH1F*)(infile->Get("light_m4l_detailed"))->Clone();
  //TH1F* m4l_gamma=(TH1F*)(infile->Get("gamma_m4l_detailed"))->Clone();
  //m4l_light->Scale(1/m4l_light->Integral());
  //m4l_gamma->Scale(1/m4l_gamma->Integral());
  //float mwfake=m4l_light->Integral(m4l_light->FindBin(118),m4l_light->FindBin(129));
  //float mwgamma=m4l_gamma->Integral(m4l_light->FindBin(118),m4l_light->FindBin(129));
  //float mwfake=m4l_light->GetBinContent(2)/m4l_light->Integral();
  //float mwgamma=m4l_gamma->GetBinContent(2)/m4l_gamma->Integral();
  //cout<<"mass window fraction for fakes: "<<mwfake<<"; gamma: "<<mwgamma<<endl;
  /*
  TH2F* m4l_light_prod=(TH2F*)(infile->Get("light_prodtype_m4l")->Clone());
  //TH2F* m4l_gamma_prod=(TH2F*)(infile->Get("gamma_prodtype_m4l")->Clone());
  float twojetpass=0,twojettot=0;
  float m4lratio[4];
  for(int i=0;i<m4l_light_prod->GetNbinsY();i++){
    if(i<4){
      float npass,ntot;
      if(i==0){
	npass=m4l_light_prod->GetBinContent(2,i+1);
	ntot=npass+m4l_light_prod->GetBinContent(1,i+1);
      }
      else if(i==1){
	npass=m4l_light_prod->GetBinContent(2,i+1)+m4l_light_prod->GetBinContent(2,7)+m4l_light_prod->GetBinContent(2,8);
	ntot=npass+m4l_light_prod->GetBinContent(1,i+1)+m4l_light_prod->GetBinContent(1,7)+m4l_light_prod->GetBinContent(1,8);
      }
      else if(i==2){
	npass=m4l_light_prod->GetBinContent(2,i+1)+m4l_light_prod->GetBinContent(2,9);
	ntot=npass+m4l_light_prod->GetBinContent(1,i+1)+m4l_light_prod->GetBinContent(1,9);
      }
      else if(i==3){
	npass=m4l_light_prod->GetBinContent(2,i+1)+m4l_light_prod->GetBinContent(2,10);
	ntot=npass+m4l_light_prod->GetBinContent(1,i+1)+m4l_light_prod->GetBinContent(1,10);
      }
      else{
	npass=m4l_light_prod->GetBinContent(2,i+1);
	ntot=npass+m4l_light_prod->GetBinContent(1,i+1);
      }
      //cout<<"mass ratio for prod type "<<i<<"="<<npass/ntot<<endl;
      m4lratio[i]=npass/ntot;
    }
    if(i>1 && i!=6 && i!=7){
      twojetpass+=m4l_light_prod->GetBinContent(2,i+1);
      twojettot+=m4l_light_prod->GetBinContent(2,i+1);
      twojettot+=m4l_light_prod->GetBinContent(1,i+1);
    }
  }
  float m4lratio2jettot=twojetpass/twojettot;
  */
  //cout<<"total 2-jet mass window fraction: "<<m4lratio2jettot<<endl;
  //h_f_pt_njet->Scale(mwfake);
  //h_gamma_pt_njet->Scale(mwgamma);
  /*
  for (int i=1; i<=h_f_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_f_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_f_pt_njet->GetZaxis()->GetNbins(); k++){
	cout<<"gamma bin "<<i<<","<<j<<",k"<<": "<<h_gamma_pt_njet->GetBinContent(i,j,k)<<"+/-"<<h_gamma_pt_njet->GetBinError(i,j,k)<<endl;
      }
    }
  }
  */
  cout<<"before subtraction, total contribution from fakes: "<<h_f_pt_njet->Integral()<<"; gammas: "<<h_gamma_pt_njet->Integral()<<endl;
  /*
  for (int i=1; i<=h_f_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_f_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_f_pt_njet->GetZaxis()->GetNbins(); k++){
	float binc=h_f_pt_njet->GetBinContent(i,j,k);
	float binerr=h_f_pt_njet->GetBinError(i,j,k);
	if(i==1){
	  binc*=m4lratio[0];
	  binerr*=m4lratio[0];
	}
	else if(i==2 || i==7 || i==8){
	  binc*=m4lratio[1];
          binerr*=m4lratio[1];
	}
	else if(i==3 || i==9){
	  binc*=m4lratio[2];
          binerr*=m4lratio[2];
        }
	else if(i==4 || i==10){
          binc*=m4lratio[3];
          binerr*=m4lratio[3];
        }
	else{
	  binc*=m4lratio2jettot;
	  binerr*=m4lratio2jettot;
	}
	//h_f_pt_njet->SetBinContent(i,j,k,binc);
	//h_f_pt_njet->SetBinError(i,j,k,binerr);
	//cout<<"prod type "<<i-1<<", pt bin "<<j<<", njet="<<k-1<<": f="<<h_f_pt_njet->GetBinContent(i,j,k)<<", gamma="<<h_gamma_pt_njet->GetBinContent(i,j,k)<<endl;
      }
    }
  }
  */

  TH3F* h_q_pt_njet = (TH3F*)h_f_pt_njet->Clone("h_q_pt_njet");
  TH3F* h_ZZf_pt_njet = (TH3F*)h_f_pt_njet->Clone("h_ZZf_pt_njet");
  TH3F* h_ZZgamma_pt_njet = (TH3F*)h_f_pt_njet->Clone("h_ZZgamma_pt_njet");
  for (int i=1; i<=h_q_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_q_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_q_pt_njet->GetZaxis()->GetNbins(); k++){
	h_q_pt_njet->SetBinContent(i,j,k,0);   h_q_pt_njet->SetBinError(i,j,k,0);
	h_ZZf_pt_njet->SetBinContent(i,j,k,0);   h_ZZf_pt_njet->SetBinError(i,j,k,0);
	h_ZZgamma_pt_njet->SetBinContent(i,j,k,0);    h_ZZgamma_pt_njet->SetBinError(i,j,k,0);
      }
    }
  }

  TH2F* h_q_njet_2d = (TH2F*)h_f_njet_2d->Clone("h_q_njet_2d");
  TH2F* h_ZZf_njet_2d = (TH2F*)h_f_njet_2d->Clone("h_ZZf_njet_2d");
  TH2F* h_ZZgamma_njet_2d = (TH2F*)h_f_njet_2d->Clone("h_ZZgamma_njet_2d");
  for (int i=1; i<=h_q_njet_2d->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_q_njet_2d->GetYaxis()->GetNbins(); j++){
      h_q_njet_2d->SetBinContent(i,j,0);   h_q_njet_2d->SetBinError(i,j,0);
      h_ZZf_njet_2d->SetBinContent(i,j,0);   h_ZZf_njet_2d->SetBinError(i,j,0);
      h_ZZgamma_njet_2d->SetBinContent(i,j,0);    h_ZZgamma_njet_2d->SetBinError(i,j,0);
    }
  }

  TH3F* h_q_y4l = (TH3F*)h_f_y4l->Clone("h_q_y4l");
  TH3F* h_ZZf_y4l = (TH3F*)h_f_y4l->Clone("h_ZZf_y4l");
  TH3F* h_ZZgamma_y4l = (TH3F*)h_f_y4l->Clone("h_ZZgamma_y4l");
  for (int i=1; i<=h_q_y4l->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_q_y4l->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_q_y4l->GetZaxis()->GetNbins(); k++){
        h_q_y4l->SetBinContent(i,j,k,0);   h_q_y4l->SetBinError(i,j,k,0);
        h_ZZf_y4l->SetBinContent(i,j,k,0);   h_ZZf_y4l->SetBinError(i,j,k,0);
        h_ZZgamma_y4l->SetBinContent(i,j,k,0);    h_ZZgamma_y4l->SetBinError(i,j,k,0);
      }
    }
  }

  TH3F* h_sb_q_pt_njet = (TH3F*)h_sb_f_pt_njet->Clone("h_sb_q_pt_njet");
  TH3F* h_sb_ZZf_pt_njet = (TH3F*)h_sb_f_pt_njet->Clone("h_sb_ZZf_pt_njet");
  TH3F* h_sb_ZZgamma_pt_njet = (TH3F*)h_sb_f_pt_njet->Clone("h_sb_ZZgamma_pt_njet");
  for (int i=1; i<=h_sb_q_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_sb_q_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_sb_q_pt_njet->GetZaxis()->GetNbins(); k++){
        h_sb_q_pt_njet->SetBinContent(i,j,k,0);   h_sb_q_pt_njet->SetBinError(i,j,k,0);
        h_sb_ZZf_pt_njet->SetBinContent(i,j,k,0);   h_sb_ZZf_pt_njet->SetBinError(i,j,k,0);
        h_sb_ZZgamma_pt_njet->SetBinContent(i,j,k,0);    h_sb_ZZgamma_pt_njet->SetBinError(i,j,k,0);
      }
    }
  }

  TH3F* h_jet1pt_q_pt_njet = (TH3F*)h_jet1pt_f_pt_njet->Clone("h_jet1pt_q_pt_njet");
  TH3F* h_jet1pt_ZZf_pt_njet = (TH3F*)h_jet1pt_f_pt_njet->Clone("h_jet1pt_ZZf_pt_njet");
  TH3F* h_jet1pt_ZZgamma_pt_njet = (TH3F*)h_jet1pt_f_pt_njet->Clone("h_jet1pt_ZZgamma_pt_njet");
  for (int i=1; i<=h_jet1pt_q_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_jet1pt_q_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_jet1pt_q_pt_njet->GetZaxis()->GetNbins(); k++){
        h_jet1pt_q_pt_njet->SetBinContent(i,j,k,0);   h_jet1pt_q_pt_njet->SetBinError(i,j,k,0);
        h_jet1pt_ZZf_pt_njet->SetBinContent(i,j,k,0);   h_jet1pt_ZZf_pt_njet->SetBinError(i,j,k,0);
        h_jet1pt_ZZgamma_pt_njet->SetBinContent(i,j,k,0);    h_jet1pt_ZZgamma_pt_njet->SetBinError(i,j,k,0);
      }
    }
  }

  TH3F* h_jet1pt_q_pt_m4l = (TH3F*)h_jet1pt_f_pt_m4l->Clone("h_jet1pt_q_pt_m4l");
  TH3F* h_jet1pt_ZZf_pt_m4l = (TH3F*)h_jet1pt_f_pt_m4l->Clone("h_jet1pt_ZZf_pt_m4l");
  TH3F* h_jet1pt_ZZgamma_pt_m4l = (TH3F*)h_jet1pt_f_pt_m4l->Clone("h_jet1pt_ZZgamma_pt_m4l");
  for (int i=1; i<=h_jet1pt_q_pt_m4l->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_jet1pt_q_pt_m4l->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_jet1pt_q_pt_m4l->GetZaxis()->GetNbins(); k++){
        h_jet1pt_q_pt_m4l->SetBinContent(i,j,k,0);   h_jet1pt_q_pt_m4l->SetBinError(i,j,k,0);
        h_jet1pt_ZZf_pt_m4l->SetBinContent(i,j,k,0);   h_jet1pt_ZZf_pt_m4l->SetBinError(i,j,k,0);
        h_jet1pt_ZZgamma_pt_m4l->SetBinContent(i,j,k,0);    h_jet1pt_ZZgamma_pt_m4l->SetBinError(i,j,k,0);
      }
    }
  }

  TFile *f_mc = TFile::Open(inputMC,"READ");
  TTree* t_mc = (TTree*)f_mc->Get("tree_relaxee");
  if (!t_mc || t_mc->IsZombie()) {
    std::cout<<"not a TTree!"<<std::endl;
  }

  Int_t run;
  int event_type;
  double weight;
  float pt,m4l,mjj,detajj,y4l,pt4l,jet1pt;
  int MCClass,ptype,njets,ptypesb;
  //float NN_1Jet_pTLow_ZZ,NN_2Jet_pTjLow_VH;
  //recall that when flattening, events with prod type 3+jet1pt<200 are given prod type 2
  TH3F* h_f_pt_njet_split=(TH3F*)h_q_pt_njet->Clone("h_f_pt_njet_split");
  TH3F* h_gamma_pt_njet_split=(TH3F*)h_q_pt_njet->Clone("h_gamma_pt_njet_split");
  t_mc->SetBranchAddress("run",&run);
  t_mc->SetBranchAddress("event_type",&event_type);
  t_mc->SetBranchAddress("weight",&weight);
  t_mc->SetBranchAddress("MCClass",&MCClass);
  t_mc->SetBranchAddress("pt",&pt);
  t_mc->SetBranchAddress("njet",&njets);
  t_mc->SetBranchAddress("prod_type",&ptype);
  t_mc->SetBranchAddress("m4l",&m4l);
  t_mc->SetBranchAddress("mjj",&mjj);
  t_mc->SetBranchAddress("detajj",&detajj);
  t_mc->SetBranchAddress("y4l",&y4l);
  //t_mc->SetBranchAddress("NN_1Jet_pTLow_ZZ",&NN_1Jet_pTLow_ZZ);
  //t_mc->SetBranchAddress("NN_2Jet_pTjLow_VH",&NN_2Jet_pTjLow_VH);
  t_mc->SetBranchAddress("pt4l",&pt4l);
  t_mc->SetBranchAddress("jet1pt",&jet1pt);
  t_mc->SetBranchAddress("prod_type_sb",&ptypesb);
  std::map<int,int> runNums;
  std::map<int,float> runWts;
  TH1F* vhLep_m4l=new TH1F("vhLep_m4l","",50,50,1000);

  long int nEntries = t_mc->GetEntries();
  for (int i(0);i<nEntries;++i){
    t_mc->GetEntry(i);

    //if(m4l<118 || m4l>129) continue;
    int njetcorr=njets;
    if(njetcorr>3) njetcorr=3;
    if(runNums.count(run)==0) runNums[run]=1;
    else runNums[run]++;
    float w = weight * scaleMC;
    if (MCClass==2){
      if(!isHM){
	if(pt<70){
	  //cout<<"not HM and pt="<<pt<<", fill h_q_pt_njet with "<<ptype<<", "<<pt<<", "<<njetcorr<<", "<<w<<endl;
	  h_q_pt_njet->Fill(ptype,pt,njetcorr,w);
	  h_q_njet_2d->Fill(pt,njetcorr,w);
	  if(y4l<2.5) h_q_y4l->Fill(y4l,pt,njetcorr,w);
	  h_sb_q_pt_njet->Fill(ptypesb,pt,njetcorr,w);
	  h_jet1pt_q_pt_njet->Fill(jet1pt,pt,njetcorr,w);
	  h_jet1pt_q_pt_m4l->Fill(jet1pt,pt,m4l,w);
	}
      }
      else{
	if(mjj>400 && detajj>3.3) h_q_pt_njet->Fill(1.,pt,njetcorr,w);
	else h_q_pt_njet->Fill(0.,pt,njetcorr,w);
      }
    }

    bool isZZ=(run==364250 || run==364251 || run==364252 || run==346340 || run==346341 || run==346342 || run==410155 || run == 410218 || run == 410219 || run == 345060 ||  run == 344235 || run == 345038 || run == 345066 || run ==  345039 || run == 345040 || run == 344973 || run == 344974 ||  run == 364243  || run ==  364245  || run == 364247 || run == 364248 || run == 342001 || run == 343273 || run == 345708 || run==345709 || run == 345937 || run==345938 || run==345939 || run==345966 || run==345967 );
    if ( isZZ ) {
      if (MCClass==5 || MCClass==7){
	if(!isHM){
	  if(pt<70){
	    if(ptype==12){
	      vhLep_m4l->Fill(m4l,w);
	      if(runWts.count(run)==0) runWts[run]=w;
	      else runWts[run]+=w;
	    }
	    h_ZZf_pt_njet->Fill(ptype,pt,njetcorr,w);
	    h_ZZf_njet_2d->Fill(pt,njetcorr,w);
	    if(y4l<2.5) h_ZZf_y4l->Fill(y4l,pt,njetcorr,w);
	    h_sb_ZZf_pt_njet->Fill(ptypesb,pt,njetcorr,w);
	    h_jet1pt_ZZf_pt_njet->Fill(jet1pt,pt,njetcorr,w);
	    h_jet1pt_ZZf_pt_m4l->Fill(jet1pt,pt,m4l,w);
	  }
	}
	else{
	  if(mjj>400 && detajj>3.3) h_ZZf_pt_njet->Fill(1.,pt,njetcorr,w);
	  else h_ZZf_pt_njet->Fill(0.,pt,njetcorr,w);
	}
      }
      else if (MCClass==3 || MCClass==4){
	if(!isHM){
	  if(pt<70){
	    h_ZZgamma_pt_njet->Fill(ptype,pt,njetcorr,w);
	    h_ZZgamma_njet_2d->Fill(pt,njetcorr,w);
	    if(y4l<2.5) h_ZZgamma_y4l->Fill(y4l,pt,njetcorr,w);
	    h_sb_ZZgamma_pt_njet->Fill(ptypesb,pt,njetcorr,w);
	    h_jet1pt_ZZgamma_pt_njet->Fill(jet1pt,pt,njetcorr,w);
	    h_jet1pt_ZZgamma_pt_m4l->Fill(jet1pt,pt,m4l,w);
	  }
	}
	else{
	  if(mjj>400 && detajj>3.3) h_ZZgamma_pt_njet->Fill(1.,pt,njetcorr,w);
	  else h_ZZgamma_pt_njet->Fill(0.,pt,njetcorr,w);
        }
      }
    }
    if(splitCatMC){ //if NN category splits are not done in data yet, count the MC numbers
      if(MCClass==5 || MCClass==7){ //fakes
	h_f_pt_njet_split->Fill(ptype,pt,njetcorr,w);
      }
      else if(MCClass==3 || MCClass==4){ //gammas
	h_gamma_pt_njet_split->Fill(ptype,pt,njetcorr,w);
      }
    }
  }

  TCanvas* cm4l=new TCanvas("cm4l","cm4l",400,400);
  vhLep_m4l->SetStats(0);
  vhLep_m4l->Draw("hist");
  cm4l->SaveAs("vhlep_zz_m4l.pdf");

  for(std::map<int,float>::iterator mit=runWts.begin();mit!=runWts.end();++mit) cout<<(*mit).first<<" contributes "<<(*mit).second<<endl;

  cout<<"from the fakes, subtract off "<<h_q_pt_njet->Integral()<<" HF and "<<h_ZZf_pt_njet->Integral()<<" ZZ"<<endl;

  if(splitCatMC){
    cout<<"divide categories by NN value"<<endl;
    for(int i=0;i<h_f_pt_njet->GetNbinsX();i++){
      int type1=i,type2;
      if(i==1 || i==10 || i==2 || i==6){
	if(i==1) type2=9; //split type 1 with type 9, 1-jet low-pT
	else if(i==10) type2=11; //split type 10 with type 11, 1-jet med-pT 
	else if(i==2) type2=14; //split type 2 with type 14, 2-jet low-pT
	else type2=15; //split type 6 with type 15, ttH-hadronic
      }
      else continue;
      for(int j=0;j<h_f_pt_njet->GetNbinsY();j++){
	for(int k=0;k<h_f_pt_njet->GetNbinsZ();k++){
	  float binc=h_f_pt_njet->GetBinContent(type1+1,j+1,k+1);
	  float bine=h_f_pt_njet->GetBinError(type1+1,j+1,k+1);
	  //cout<<"fake prod_type in bin "<<type1+1<<","<<j+1<<","<<k+1<<" was: "<<binc<<", and in bin "<<type2+1<<","<<j+1<<","<<k+1<<" was "<<h_f_pt_njet->GetBinContent(type2,j+1,k+1)<<endl;
	  float np1_f=h_f_pt_njet_split->GetBinContent(type1+1,j+1,k+1),np2_f=h_f_pt_njet_split->GetBinContent(type2+1,j+1,k+1);
	  if(np1_f<0) np1_f=0;
	  if(np2_f<0) np2_f=0;
	  //cout<<"from mc, we expect "<<np1_f<<" of type "<<type1<<" and "<<np2_f<<" of type "<<type2<<endl;
	  float frac1=1,frac2=1; //if nothing in the MC, don't change the data
	  if((np1_f+np2_f)>0){
	    frac1=np1_f/(np1_f+np2_f);
	    frac2=np2_f/(np1_f+np2_f);
	  }
	  h_f_pt_njet->SetBinContent(type1+1,j+1,k+1,binc*frac1);
	  h_f_pt_njet->SetBinError(type1+1,j+1,k+1,bine*frac1);
	  h_f_pt_njet->SetBinContent(type2+1,j+1,k+1,binc*frac2);
	  h_f_pt_njet->SetBinError(type2+1,j+1,k+1,bine*frac2);
	  //cout<<"fake prod_type in bin "<<type1+1<<","<<j+1<<","<<k+1<<" is now: "<<h_f_pt_njet->GetBinContent(type1+1,j+1,k+1)<<", and in bin "<<type2+1<<","<<j+1<<","<<k+1<<" is now "<<h_f_pt_njet->GetBinContent(type2+1,j+1,k+1)<<endl;
	  binc=h_gamma_pt_njet->GetBinContent(type1+1,j+1,k+1);
	  bine=h_gamma_pt_njet->GetBinError(type1+1,j+1,k+1);
	  float np1_g=h_gamma_pt_njet_split->GetBinContent(type1+1,j+1,k+1),np2_g=h_gamma_pt_njet_split->GetBinContent(type2+1,j+1,k+1);
	  if(np1_g<0) np1_g=0;
          if(np2_g<0) np2_g=0;
	  frac1=1; frac2=1;
	  if((np1_g+np2_g)>0){
	    frac1=np1_g/(np1_g+np2_g);
	    frac2=np2_g/(np1_g+np2_g);
	  }
	  h_gamma_pt_njet->SetBinContent(type1+1,j+1,k+1,binc*frac1);
	  h_gamma_pt_njet->SetBinError(type1+1,j+1,k+1,bine*frac1);
	  h_gamma_pt_njet->SetBinContent(type2+1,j+1,k+1,binc*frac2);
	  h_gamma_pt_njet->SetBinError(type2+1,j+1,k+1,bine*frac2);
	}
      }
    }
  }

  cout<<"after NN splitting but before subtraction, fakes prod_type hist integral="<<h_f_pt_njet->Integral()<<endl;

  for(int j=1; j<=h_q_pt_njet->GetYaxis()->GetNbins(); j++){
    for(int k=1; k<=h_q_pt_njet->GetZaxis()->GetNbins(); k++){
      cout<<"f bin 13,"<<j<<","<<k<<"="<<h_f_pt_njet->GetBinContent(13,j,k)<<"; HF="<<h_q_pt_njet->GetBinContent(13,j,k)<<", ZZ="<<h_ZZf_pt_njet->GetBinContent(13,j,k)<<endl;
    }
  }

  h_f_pt_njet->Add(h_q_pt_njet,-1);
  h_f_pt_njet->Add(h_ZZf_pt_njet,-1);
  h_gamma_pt_njet->Add(h_ZZgamma_pt_njet,-1*scaleMC);

  h_f_njet_2d->Add(h_q_njet_2d,-1);
  h_f_njet_2d->Add(h_ZZf_njet_2d,-1);
  h_gamma_njet_2d->Add(h_ZZgamma_njet_2d,-1*scaleMC);

  h_f_y4l->Add(h_q_y4l,-1);
  h_f_y4l->Add(h_ZZf_y4l,-1);
  h_gamma_y4l->Add(h_ZZgamma_y4l,-1*scaleMC);

  h_sb_f_pt_njet->Add(h_sb_q_pt_njet,-1);
  h_sb_f_pt_njet->Add(h_sb_ZZf_pt_njet,-1);
  h_sb_gamma_pt_njet->Add(h_sb_ZZgamma_pt_njet,-1*scaleMC);

  h_jet1pt_f_pt_njet->Add(h_jet1pt_q_pt_njet,-1);
  h_jet1pt_f_pt_njet->Add(h_jet1pt_ZZf_pt_njet,-1);
  h_jet1pt_gamma_pt_njet->Add(h_jet1pt_ZZgamma_pt_njet,-1*scaleMC);

  h_jet1pt_f_pt_m4l->Add(h_jet1pt_q_pt_m4l,-1);
  h_jet1pt_f_pt_m4l->Add(h_jet1pt_ZZf_pt_m4l,-1);
  h_jet1pt_gamma_pt_m4l->Add(h_jet1pt_ZZgamma_pt_m4l,-1*scaleMC);

  cout<<"after subtraction, fakes prod_type hist integral="<<h_f_pt_njet->Integral()<<"; njet hist integral="<<h_f_njet_2d->Integral()<<"; y4l hist integral: "<<h_f_y4l->Integral()<<"; sb hist integral: "<<h_sb_f_pt_njet->Integral()<<endl;
  cout<<"after subtraction, total contribution from fakes: "<<h_f_pt_njet->Integral()<<"; gammas: "<<h_gamma_pt_njet->Integral()<<endl;
  for (int i=1; i<=h_q_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_q_pt_njet->GetYaxis()->GetNbins(); j++){
      for(int k=1; k<=h_q_pt_njet->GetZaxis()->GetNbins(); k++){
	if(h_q_pt_njet->GetBinContent(i,j,k)<0){
	  //cout<<"f bin "<<i<<","<<j<<","<<k<<"="<<h_f_pt_njet->GetBinContent(i,j,k)<<"; ZZ="<<h_q_pt_njet->GetBinContent(i,j,k)<<", HF="<<h_ZZf_pt_njet->GetBinContent(i,j,k)<<endl;
	}
	if(h_gamma_pt_njet->GetBinContent(i,j,k)<0){
	  //cout<<"gamma bin "<<i<<","<<j<<","<<k<<"="<<h_gamma_pt_njet->GetBinContent(i,j,k)<<"; ZZ="<<h_ZZgamma_pt_njet->GetBinContent(i,j,k)<<endl;
	}
      }
    }
  }

  TFile* effFile=TFile::Open(Form("v23Files/fakeFactors_%s.root",year.Data()));
  TH2F* fakeEff=(TH2F*)effFile->Get("eff_pt_njet_Loose_fake");
  TH2F* gammaEff=(TH2F*)effFile->Get("eff_pt_njet_Loose_gamma");
  TFile* sfFile=TFile::Open(Form("v23Files/efficiencySF_pt_%s.root",year.Data()));
  TH1F* fakeSf=(TH1F*)sfFile->Get("efficiencySF_0");
  TH1F* gammaSf=(TH1F*)sfFile->Get("efficiencySF_1");
  /*
  TFile* f = TFile::Open("efficiencySF_ptup.root");
  TH1F* sf_up_gamma = (TH1F*) f->Get("efficiencySF_1")->Clone("sf_up_gamma");
  TH1F* sf_up_f = (TH1F*) f->Get("efficiencySF_0")->Clone("sf_up_f");
  f = TFile::Open("efficiencySF_ptdown.root");
  TH1F* sf_low_gamma = (TH1F*) f->Get("efficiencySF_1")->Clone("sf_low_gamma");
  TH1F* sf_low_f = (TH1F*) f->Get("efficiencySF_0")->Clone("sf_low_f");
  for (int i=1; i<=3; i++) {
    float x0 = fakeSf->GetBinContent(i);
    float x1 = sf_up_f->GetBinContent(i);
    float x2 = sf_low_f->GetBinContent(i);
    float dx1 = TMath::Abs(x1-x0);
    float dx2 = TMath::Abs(x2-x0);
    float dx = (dx1>dx2) ? dx1 : dx2;
    float sigmax0 = fakeSf->GetBinError(i) ;
    //cout << sigmax0 << "....." << dx << endl;
    fakeSf->SetBinError( i, sqrt( pow(sigmax0/x0,2) + pow(dx/x0,2) )*x0 );
    x0 = gammaSf->GetBinContent(i);
    x1 = sf_up_gamma->GetBinContent(i);
    x2 = sf_low_gamma->GetBinContent(i);
    dx1 = TMath::Abs(x1-x0);
    dx2 = TMath::Abs(x2-x0);
    dx = (dx1>dx2) ? dx1 : dx2;
    sigmax0 = gammaSf->GetBinError(i) ;
    gammaSf->SetBinError( i, sqrt( pow(sigmax0/x0,2) + pow(dx/x0,2) )*x0 );
  }
  */
  //TH2D* jetpthist=(TH2D*)h_f_pt_njet->Project3D("zye");
  //TH1F* fakeEffPt=(TH1F*)effFile->Get("eff_pt_Loose_fake");
  for(int i=0;i<fakeEff->GetNbinsX();i++){
    float fsf=fakeSf->GetBinContent(i+1);
    float gsf=gammaSf->GetBinContent(i+1);
    float fsferr=fakeSf->GetBinError(i+1)/fsf;
    float gsferr=gammaSf->GetBinError(i+1)/gsf;
    //cout<<"pT eff in this pT bin: "<<fakeEffPt->GetBinContent(i+1)<<endl;
    for(int j=0;j<fakeEff->GetNbinsY();j++){
      //cout<<"unscaled eff in bin "<<i+1<<", "<<j+1<<"="<<fakeEff->GetBinContent(i+1,j+1)<<", bin content "<<jetpthist->GetBinContent(i+1,j+1)<<endl;
      float relErr=fakeEff->GetBinError(i+1,j+1)/fakeEff->GetBinContent(i+1,j+1);
      fakeEff->SetBinContent(i+1,j+1,fsf*fakeEff->GetBinContent(i+1,j+1));
      fakeEff->SetBinError(i+1,j+1,sqrt(pow(fsferr,2)+pow(relErr,2))*fakeEff->GetBinContent(i+1,j+1));
      //cout<<"bin "<<i+1<<", "<<j+1<<", "<<" gamma eff="<<gammaEff->GetBinContent(i+1,j+1)<<endl;
      relErr=gammaEff->GetBinError(i+1,j+1)/gammaEff->GetBinContent(i+1,j+1);
      gammaEff->SetBinContent(i+1,j+1,gsf*gammaEff->GetBinContent(i+1,j+1));
      //cout<<"gsferr: "<<gsferr<<"; relErr: "<<relErr<<endl;
      gammaEff->SetBinError(i+1,j+1,sqrt(pow(gsferr,2)+pow(relErr,2))*gammaEff->GetBinContent(i+1,j+1));
      //cout<<"scaled 2-d fake efficiency in bin "<<i+1<<", "<<j+1<<" is "<<fakeEff->GetBinContent(i+1,j+1)<<endl;
    }
  }
  /*
  for(int i=0;i<h_f_njet_2d->GetNbinsY();i++){
    cout<<"before eff, fakes in njet="<<i<<" bin: "<<h_f_njet_2d->Integral(1,h_f_njet_2d->GetNbinsX(),i+1,i+1)<<endl;
  }
  */
  h_f_njet_2d->Multiply(fakeEff);
  h_gamma_njet_2d->Multiply(gammaEff);
  TH1D* h_f_njet=h_f_njet_2d->ProjectionY("h_f_njet",1,h_f_njet_2d->GetNbinsX(),"e");
  TH1D* h_gamma_njet=h_gamma_njet_2d->ProjectionY("h_gamma_njet",1,h_gamma_njet_2d->GetNbinsX(),"e");

  for(int i=0;i<h_f_y4l->GetNbinsX();i++){ //y4l bins
    for(int j=0;j<h_f_y4l->GetNbinsY();j++){ //pt bins
      for(int k=0;k<h_f_y4l->GetNbinsZ();k++){ //njet bins
	float relErrGammaEff=gammaEff->GetBinContent(j+1,k+1)==0 ? 0 : gammaEff->GetBinError(j+1,k+1)/gammaEff->GetBinContent(j+1,k+1);
	float relErrLightEff=fakeEff->GetBinContent(j+1,k+1)==0 ? 0 : fakeEff->GetBinError(j+1,k+1)/fakeEff->GetBinContent(j+1,k+1);
	float relErrGamma=h_gamma_y4l->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_gamma_y4l->GetBinError(i+1,j+1,k+1)/h_gamma_y4l->GetBinContent(i+1,j+1,k+1);
	float relErrLight=h_f_y4l->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_f_y4l->GetBinError(i+1,j+1,k+1)/h_f_y4l->GetBinContent(i+1,j+1,k+1);
	float bincGamma=h_gamma_y4l->GetBinContent(i+1,j+1,k+1);
	float bincLight=h_f_y4l->GetBinContent(i+1,j+1,k+1);
	//cout<<"bin "<<i+1<<","<<j+1<<","<<k+1<<": light eff "<<fakeEff->GetBinContent(j+1,k+1)<<", err "<<relErrLightEff<<", light "<<bincLight<<", err "<<relErrLight<<endl;
	h_gamma_y4l->SetBinContent(i+1,j+1,k+1,gammaEff->GetBinContent(j+1,k+1)*bincGamma);
	h_gamma_y4l->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrGammaEff,2)+pow(relErrGamma,2))*gammaEff->GetBinContent(j+1,k+1)*bincGamma);
	h_f_y4l->SetBinContent(i+1,j+1,k+1,fakeEff->GetBinContent(j+1,k+1)*bincLight);
	//cout<<"set bin "<<i+1<<","<<j+1<<","<<k+1<<" content to "<<fakeEff->GetBinContent(j+1,k+1)*bincLight<<"; actual content is "<<h_f_y4l->GetBinContent(i+1,j+1,k+1)<<endl;
        h_f_y4l->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrLightEff,2)+pow(relErrLight,2))*fakeEff->GetBinContent(j+1,k+1)*bincLight);
      }
    }
  }
  h_f_y4l->GetYaxis()->SetRange(1,h_f_y4l->GetNbinsY());
  h_f_y4l->GetZaxis()->SetRange(1,h_f_y4l->GetNbinsZ());
  TH1D* h_f_y4l2d=(TH1D*)h_f_y4l->Project3D("xe");
  //for(int i=0;i<h_f_y4l2d->GetNbinsX();i++) cout<<"y4l in bin "<<i+1<<" is "<<h_f_y4l2d->GetBinContent(i+1)<<endl;
  h_gamma_y4l->GetYaxis()->SetRange(1,h_gamma_y4l->GetNbinsY());
  h_gamma_y4l->GetZaxis()->SetRange(1,h_gamma_y4l->GetNbinsZ());
  TH1D* h_gamma_y4l2d=(TH1D*)h_gamma_y4l->Project3D("xe");

  for(int i=0;i<h_sb_f_pt_njet->GetNbinsX();i++){ //sb prod types
    for(int j=0;j<h_sb_f_pt_njet->GetNbinsY();j++){ //pt bins
      for(int k=0;k<h_sb_f_pt_njet->GetNbinsZ();k++){ //njet bins
	float relErrGammaEff=gammaEff->GetBinContent(j+1,k+1)==0 ? 0 : gammaEff->GetBinError(j+1,k+1)/gammaEff->GetBinContent(j+1,k+1);
        float relErrLightEff=fakeEff->GetBinContent(j+1,k+1)==0 ? 0 : fakeEff->GetBinError(j+1,k+1)/fakeEff->GetBinContent(j+1,k+1);
        float relErrGamma=h_sb_gamma_pt_njet->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_sb_gamma_pt_njet->GetBinError(i+1,j+1,k+1)/h_sb_gamma_pt_njet->GetBinContent(i+1,j+1,k+1);
        float relErrLight=h_sb_f_pt_njet->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_sb_f_pt_njet->GetBinError(i+1,j+1,k+1)/h_sb_f_pt_njet->GetBinContent(i+1,j+1,k+1);
        float bincGamma=h_sb_gamma_pt_njet->GetBinContent(i+1,j+1,k+1);
        float bincLight=h_sb_f_pt_njet->GetBinContent(i+1,j+1,k+1);
        //cout<<"bin "<<i+1<<","<<j+1<<","<<k+1<<": light eff "<<fakeEff->GetBinContent(j+1,k+1)<<", err "<<relErrLightEff<<", light "<<bincLight<<", err "<<relErrLight<<endl;
        h_sb_gamma_pt_njet->SetBinContent(i+1,j+1,k+1,gammaEff->GetBinContent(j+1,k+1)*bincGamma);
        h_sb_gamma_pt_njet->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrGammaEff,2)+pow(relErrGamma,2))*gammaEff->GetBinContent(j+1,k+1)*bincGamma);
        h_sb_f_pt_njet->SetBinContent(i+1,j+1,k+1,fakeEff->GetBinContent(j+1,k+1)*bincLight);
        //cout<<"set bin "<<i+1<<","<<j+1<<","<<k+1<<" content to "<<fakeEff->GetBinContent(j+1,k+1)*bincLight<<"; actual content is "<<h_sb_f_pt_njet->GetBinContent(i+1,j+1,k+1)<<endl;
        h_sb_f_pt_njet->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrLightEff,2)+pow(relErrLight,2))*fakeEff->GetBinContent(j+1,k+1)*bincLight);
      }
    }
  }
  h_sb_f_pt_njet->GetYaxis()->SetRange(1,h_sb_f_pt_njet->GetNbinsY());
  h_sb_f_pt_njet->GetZaxis()->SetRange(1,h_sb_f_pt_njet->GetNbinsZ());
  TH1D* h_sb_f=(TH1D*)h_sb_f_pt_njet->Project3D("xe");
  h_sb_gamma_pt_njet->GetYaxis()->SetRange(1,h_sb_gamma_pt_njet->GetNbinsY());
  h_sb_gamma_pt_njet->GetZaxis()->SetRange(1,h_sb_gamma_pt_njet->GetNbinsZ());
  TH1D* h_sb_gamma=(TH1D*)h_sb_gamma_pt_njet->Project3D("xe");

  for(int i=0;i<h_jet1pt_f_pt_njet->GetNbinsX();i++){ //jet1pt bins
    for(int j=0;j<h_jet1pt_f_pt_njet->GetNbinsY();j++){ //pt bins
      for(int k=0;k<h_jet1pt_f_pt_njet->GetNbinsZ();k++){ //njet bins
	float relErrGammaEff=gammaEff->GetBinContent(j+1,k+1)==0 ? 0 : gammaEff->GetBinError(j+1,k+1)/gammaEff->GetBinContent(j+1,k+1);
        float relErrLightEff=fakeEff->GetBinContent(j+1,k+1)==0 ? 0 : fakeEff->GetBinError(j+1,k+1)/fakeEff->GetBinContent(j+1,k+1);
        float relErrGamma=h_jet1pt_gamma_pt_njet->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_jet1pt_gamma_pt_njet->GetBinError(i+1,j+1,k+1)/h_jet1pt_gamma_pt_njet->GetBinContent(i+1,j+1,k+1);
        float relErrLight=h_jet1pt_f_pt_njet->GetBinContent(i+1,j+1,k+1)==0 ? 0 : h_jet1pt_f_pt_njet->GetBinError(i+1,j+1,k+1)/h_jet1pt_f_pt_njet->GetBinContent(i+1,j+1,k+1);
        float bincGamma=h_jet1pt_gamma_pt_njet->GetBinContent(i+1,j+1,k+1);
        float bincLight=h_jet1pt_f_pt_njet->GetBinContent(i+1,j+1,k+1);
        h_jet1pt_gamma_pt_njet->SetBinContent(i+1,j+1,k+1,gammaEff->GetBinContent(j+1,k+1)*bincGamma);
        h_jet1pt_gamma_pt_njet->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrGammaEff,2)+pow(relErrGamma,2))*gammaEff->GetBinContent(j+1,k+1)*bincGamma);
	h_jet1pt_f_pt_njet->SetBinContent(i+1,j+1,k+1,fakeEff->GetBinContent(j+1,k+1)*bincLight);
        h_jet1pt_f_pt_njet->SetBinError(i+1,j+1,k+1,sqrt(pow(relErrLightEff,2)+pow(relErrLight,2))*fakeEff->GetBinContent(j+1,k+1)*bincLight);
      }
    }
  }
  h_jet1pt_f_pt_njet->GetYaxis()->SetRange(1,h_jet1pt_f_pt_njet->GetNbinsY());
  h_jet1pt_f_pt_njet->GetZaxis()->SetRange(1,h_jet1pt_f_pt_njet->GetNbinsZ());
  TH1D* h_jet1pt_f=(TH1D*)h_jet1pt_f_pt_njet->Project3D("xe");
  h_jet1pt_gamma_pt_njet->GetYaxis()->SetRange(1,h_jet1pt_gamma_pt_njet->GetNbinsY());
  h_jet1pt_gamma_pt_njet->GetZaxis()->SetRange(1,h_jet1pt_gamma_pt_njet->GetNbinsZ());
  TH1D* h_jet1pt_gamma=(TH1D*)h_jet1pt_gamma_pt_njet->Project3D("xe");

  //jet1pt vs. pt vs. m4l, need the >=1 jet efficiency
  TH1D* effGamma0=gammaEff->ProjectionX("effGamma0",1,1);
  TH1D* effLight0=fakeEff->ProjectionX("effLight0",1,1);
  float ntotl=h_f_njet->GetBinContent(h_f_njet->GetNbinsX()),ntotg=h_gamma_njet->GetBinContent(h_gamma_njet->GetNbinsX());
  TH1D* effLightGE1=fakeEff->ProjectionX("effLightGE1",h_f_njet->GetNbinsX(),h_f_njet->GetNbinsX());
  effLightGE1->Scale(h_f_njet->GetBinContent(h_f_njet->GetNbinsX()));
  TH1D* effGammaGE1=gammaEff->ProjectionX("effGammaGE1",h_f_njet->GetNbinsX(),h_f_njet->GetNbinsX());
  effGammaGE1->Scale(h_gamma_njet->GetBinContent(h_f_njet->GetNbinsX()));
  for(int j=h_f_njet->GetNbinsX()-1;j>1;j--){
    ntotl+=h_f_njet->GetBinContent(j);
    ntotg+=h_gamma_njet->GetBinContent(j);
    effLightGE1->Add(fakeEff->ProjectionX(Form("templ_%i",j),j,j),h_f_njet->GetBinContent(j));
    effGammaGE1->Add(gammaEff->ProjectionX(Form("tempg_%i",j),j,j),h_gamma_njet->GetBinContent(j));
  }
  effLightGE1->Scale(1/ntotl);
  effGammaGE1->Scale(1/ntotg);
  for(int i=0;i<h_jet1pt_f_pt_m4l->GetNbinsY();i++){ //pt bins
    float relErrEffLight=effLightGE1->GetBinError(i+1)/effLightGE1->GetBinContent(i+1);
    float relErrEffGamma=effGammaGE1->GetBinError(i+1)/effGammaGE1->GetBinContent(i+1);
    for(int j=0;j<h_jet1pt_f_pt_m4l->GetNbinsX();j++){ //jet1pt bins
      if(j==0){
	//first jet1pt bin is actually njet=0 bin
	relErrEffLight=effLight0->GetBinError(i+1)/effLight0->GetBinContent(i+1);
	relErrEffGamma=effGamma0->GetBinError(i+1)/effGamma0->GetBinContent(i+1);
      }
      for(int k=0;k<h_jet1pt_f_pt_m4l->GetNbinsZ();k++){ //m4l bins
	float relErrLight=h_jet1pt_f_pt_m4l->GetBinContent(j+1,i+1,k+1)==0 ? 0 : h_jet1pt_f_pt_m4l->GetBinError(j+1,i+1,k+1)/h_jet1pt_f_pt_m4l->GetBinContent(j+1,i+1,k+1);
	float relErrGamma=h_jet1pt_gamma_pt_m4l->GetBinContent(j+1,i+1,k+1)==0 ? 0 : h_jet1pt_gamma_pt_m4l->GetBinError(j+1,i+1,k+1)/h_jet1pt_gamma_pt_m4l->GetBinContent(j+1,i+1,k+1);
	if(j==0){
	  h_jet1pt_f_pt_m4l->SetBinContent(j+1,i+1,k+1,effLight0->GetBinContent(i+1)*h_jet1pt_f_pt_m4l->GetBinContent(j+1,i+1,k+1));
	  h_jet1pt_gamma_pt_m4l->SetBinContent(j+1,i+1,k+1,effGamma0->GetBinContent(i+1)*h_jet1pt_gamma_pt_m4l->GetBinContent(j+1,i+1,k+1));
	}
	else{
	  h_jet1pt_f_pt_m4l->SetBinContent(j+1,i+1,k+1,effLightGE1->GetBinContent(i+1)*h_jet1pt_f_pt_m4l->GetBinContent(j+1,i+1,k+1));
	  h_jet1pt_gamma_pt_m4l->SetBinContent(j+1,i+1,k+1,effGammaGE1->GetBinContent(i+1)*h_jet1pt_gamma_pt_m4l->GetBinContent(j+1,i+1,k+1));
	}
	h_jet1pt_f_pt_m4l->SetBinError(j+1,i+1,k+1,h_jet1pt_f_pt_m4l->GetBinContent(j+1,i+1,k+1)*sqrt(pow(relErrEffLight,2)+pow(relErrLight,2)));
	h_jet1pt_gamma_pt_m4l->SetBinError(j+1,i+1,k+1,h_jet1pt_gamma_pt_m4l->GetBinContent(j+1,i+1,k+1)*sqrt(pow(relErrEffGamma,2)+pow(relErrGamma,2)));
      }
    }
  }
  h_jet1pt_f_pt_m4l->GetXaxis()->SetRange(1,4);
  h_jet1pt_f_pt_m4l->GetYaxis()->SetRange(1,h_jet1pt_f_pt_m4l->GetNbinsY());
  h_jet1pt_f_pt_m4l->GetZaxis()->SetRange(1,h_jet1pt_f_pt_m4l->GetNbinsZ());
  TH1D* h_jet1pt_f_m4l=(TH1D*)h_jet1pt_f_pt_m4l->Project3D("xe");
  for(int i=0;i<h_jet1pt_f_m4l->GetNbinsX();i++) cout<<"jet1pt in bin "<<i+1<<": "<<h_jet1pt_f_m4l->GetBinContent(i+1)<<endl;
  h_jet1pt_gamma_pt_m4l->GetXaxis()->SetRange(1,4);
  h_jet1pt_gamma_pt_m4l->GetYaxis()->SetRange(1,h_jet1pt_gamma_pt_m4l->GetNbinsY());
  h_jet1pt_gamma_pt_m4l->GetZaxis()->SetRange(1,h_jet1pt_gamma_pt_m4l->GetNbinsZ());
  TH1D* h_jet1pt_gamma_m4l=(TH1D*)h_jet1pt_gamma_pt_m4l->Project3D("xe");

  TH1F* prod_type_fake=new TH1F(Form("prod_type%s_fake",hmString.c_str()),"",h_f_pt_njet->GetNbinsX(),h_f_pt_njet->GetXaxis()->GetXbins()->GetArray());
  TH1F* prod_type_gamma=new TH1F(Form("prod_type%s_gamma",hmString.c_str()),"",h_gamma_pt_njet->GetNbinsX(),h_gamma_pt_njet->GetXaxis()->GetXbins()->GetArray());
  vector<TH2D*> prod_type_fake_dists;
  vector<TH2D*> prod_type_gamma_dists;
  TH2D* temp;
  double integral, error;
  for(int i=0;i<h_f_pt_njet->GetNbinsX();i++){
    h_f_pt_njet->GetXaxis()->SetRange(i+1,i+1);
    temp=(TH2D*)h_f_pt_njet->Project3D("zye");
    temp->SetName(Form("prod_type%s_fake_dists_%i",hmString.c_str(),i));
    //cout<<"before eff, f total in category "<<i<<" is "<<temp->Integral()<<endl;
    if(i==12){
      for(int j=0;j<temp->GetNbinsX();j++){
	for(int k=0;k<temp->GetNbinsY();k++){
	  cout<<"bin "<<i+1<<", "<<j+1<<", "<<k+1<<" content: "<<temp->GetBinContent(j+1,k+1)<<"; eff: "<<fakeEff->GetBinContent(j+1,k+1)<<endl;
	}
      }
    }
    prod_type_fake_dists.push_back(temp);
    prod_type_fake_dists[i]->Multiply(fakeEff);
    integral=prod_type_fake_dists[i]->IntegralAndError(1,prod_type_fake_dists[i]->GetNbinsX(),1,prod_type_fake_dists[i]->GetNbinsY(),error);
    prod_type_fake->SetBinContent(i+1,integral);
    prod_type_fake->SetBinError(i+1,error);
    h_gamma_pt_njet->GetXaxis()->SetRange(i+1,i+1);
    temp=(TH2D*)h_gamma_pt_njet->Project3D("zye");
    temp->SetName(Form("prod_type%s_gamma_dists_%i",hmString.c_str(),i));
    //cout<<"before eff, gamma total in category "<<i<<" is "<<temp->Integral()<<endl;
    prod_type_gamma_dists.push_back(temp);
    prod_type_gamma_dists[i]->Multiply(gammaEff);
    integral=prod_type_gamma_dists[i]->IntegralAndError(1,prod_type_gamma_dists[i]->GetNbinsX(),1,prod_type_gamma_dists[i]->GetNbinsY(),error);
    prod_type_gamma->SetBinContent(i+1,integral);
    prod_type_gamma->SetBinError(i+1,error);
  }
  cout<<"after efficiency, fakes prod_type hist integral="<<prod_type_fake->Integral()<<"; njet hist integral="<<h_f_njet_2d->Integral()<<"; y4l hist integral: "<<h_f_y4l->Integral()<<"; sb hist integral: "<<h_sb_f_pt_njet->Integral()<<endl;
  cout<<"gamma njet hist integral: "<<h_gamma_njet_2d->Integral()<<endl;
  //cout<<"after applying efficiency, have "<<prod_type_fake->Integral()<<" fakes and "<<prod_type_gamma->Integral()<<" gammas"<<endl;
  TFile* heavyFile=TFile::Open(Form("v23Files/heavyFlavorShapes_%s.root",year.Data()));
  TH1F* prod_type_heavy=(TH1F*)heavyFile->Get(Form("prod_type%s",hmString.c_str()));
  for(int i=H4lBkg::toInt(H4lBkg::Category::ProdType::_0Jet);i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_ge3Jet);i++){
    if(i==H4lBkg::toInt(H4lBkg::Category::ProdType::_ge1Jet) || i==H4lBkg::toInt(H4lBkg::Category::ProdType::_ge2Jet)) continue;
    cout<<"hf "<<H4lBkg::Category::toString((H4lBkg::Category::ProdType)i)<<"="<<prod_type_heavy->GetBinContent(i+1)<<endl;
  }

  //get the heavy sb yields for plotting purposes
  TH1D* h_sb_heavy=(TH1D*)h_sb_gamma->Clone("h_sb_heavy");
  h_sb_heavy->Reset();
  for(int i=0;i<h_sb_heavy->GetNbinsX();i++){
    h_sb_heavy->SetBinContent(i+1,prod_type_heavy->GetBinContent(i+H4lBkg::toInt(H4lBkg::Category::ProdType::_SB_0jet)+1));
    h_sb_heavy->SetBinError(i+1,prod_type_heavy->GetBinError(i+H4lBkg::toInt(H4lBkg::Category::ProdType::_SB_0jet)+1));
  }
  string sbcat[5]={"SB_0jet_4l_13TeV","SB_1jet_4l_13TeV","SB_2jet_4l_13TeV","SB_ttH_4l_13TeV","SB_VHLep_4l_13TeV"};
  TH1F* h_sb_dist=(TH1F*)h_sb_f->Clone("h_sb_dist");
  h_sb_dist->SetTitle("");
  h_sb_dist->Add(h_sb_gamma);
  h_sb_dist->Add(h_sb_heavy);
  h_sb_dist->GetXaxis()->SetRange(1,h_sb_dist->GetNbinsX()-1);
  h_sb_f->GetXaxis()->SetRange(1,h_sb_f->GetNbinsX()-1);
  h_sb_gamma->GetXaxis()->SetRange(1,h_sb_gamma->GetNbinsX()-1);
  h_sb_heavy->GetXaxis()->SetRange(1,h_sb_heavy->GetNbinsX()-1);
  TCanvas* csb=new TCanvas("csb","csb",400,400);
  TLegend* legsb=new TLegend(.7,.7,.9,.9);
  legsb->AddEntry(h_sb_f,"Light","F");
  legsb->AddEntry(h_sb_gamma,"Photons","F");
  legsb->AddEntry(h_sb_heavy,"Heavy flavor","F");
  h_sb_dist->SetFillColor(kBlack);
  h_sb_dist->SetFillStyle(3004);
  h_sb_dist->SetMarkerSize(0);
  h_sb_dist->SetStats(0);
  h_sb_dist->SetMinimum(0);
  legsb->AddEntry(h_sb_dist,"Uncertainty","F");

  THStack *hs_sb = new THStack("hs_sb","");
  h_sb_f->SetFillColor(kGreen+2);
  h_sb_f->SetLineColor(kGreen+2);
  h_sb_gamma->SetFillColor(kBlue+2);
  h_sb_gamma->SetLineColor(kBlue+2);
  h_sb_heavy->SetFillColor(kRed+1);
  h_sb_heavy->SetLineColor(kRed+1);
  //h_sb_dist->GetYaxis()->SetRangeUser(0,10);
  h_sb_dist->GetXaxis()->SetLabelSize(h_sb_dist->GetYaxis()->GetLabelSize());
  h_sb_dist->SetXTitle("");
  for(int i=0;i<5;i++) h_sb_dist->GetXaxis()->SetBinLabel(i+1,sbcat[i].c_str());
  h_sb_dist->Draw("E2 ");

  hs_sb->Add(h_sb_f);
  hs_sb->Add(h_sb_gamma);
  hs_sb->Add(h_sb_heavy);
  hs_sb->SetMaximum(20);
  hs_sb->Draw( "HIST SAME");
  h_sb_dist->Draw("E2 SAME");
  legsb->Draw();
  allLabels(lumi[year],"",.2,.8);
  csb->SaveAs(Form("catPlots/sb_dist_el_stack_%s.pdf",year.Data()));
  csb->SaveAs(Form("catPlots/sb_dist_el_stack_%s.eps",year.Data()));

  //TH1F* h_heavy_njet=(TH1F*)heavyFile->Get("njet_full");
  //TH1F* h_heavy_y4l2d=(TH1F*)heavyFile->Get("y4l_full");
  //cout<<"final contributions: heavy="<<prod_type_heavy->Integral()<<", light="<<prod_type_fake->Integral()<<", gamma="<<prod_type_gamma->Integral()<<endl;
  //for(int i=0;i<prod_type_fake->GetNbinsX();i++) cout<<"fake in prod_type "<<i<<"="<<prod_type_fake->GetBinContent(i+1)<<", gamma="<<prod_type_gamma->GetBinContent(i+1)<<endl;
  if(!isHM){
    string cat[16]={"low-p_{T4l} 0jet","med-p_{T4l} 0jet","high-p_{T4l} 0jet","low-p_{T4l} 1jet b1","low-p_{T4l} 1jet b2","med-p_{T4l} 1jet b1","med-p_{T4l} 1jet b2","higher-p_{T4l} 1jet","highest-p_{T4l} 1jet","high-p_{T} VBF","VHhad or low-p_{T} VBF b1","VHhad or low-p_{T} VBF b2","VHlep","ttH-Lep","ttH-Had b1","ttH-Had b2"};
    TH1F* prod_type_plot_fake=(TH1F*)prod_type_fake->Clone(Form("prod_type%s_plot_fake",hmString.c_str()));
    TH1F* prod_type_plot_gamma=(TH1F*)prod_type_gamma->Clone(Form("prod_type%s_plot_gamma",hmString.c_str()));
    TH1F* prod_type_plot_heavy=(TH1F*)prod_type_fake->Clone(Form("prod_type%s_plot_heavy",hmString.c_str()));
    //prod_type_plot_fake->Reset();
    //prod_type_plot_gamma->Reset();
    prod_type_plot_heavy->Reset();
    //finally gotten to the point where we shouldn't need this any more!
    //int binMap[16]={1,8,9,2,10,11,12,13,14,3,15,4,5,6,7,16};
    for(int ibin=0;ibin<prod_type_plot_fake->GetNbinsX();ibin++){
      /*
      prod_type_plot_fake->SetBinContent(ibin+1,prod_type_fake->GetBinContent(binMap[ibin]));
      prod_type_plot_fake->SetBinError(ibin+1,prod_type_fake->GetBinError(binMap[ibin]));
      prod_type_plot_gamma->SetBinContent(ibin+1,prod_type_gamma->GetBinContent(binMap[ibin]));
      prod_type_plot_gamma->SetBinError(ibin+1,prod_type_gamma->GetBinError(binMap[ibin]));
      */
      prod_type_plot_heavy->SetBinContent(ibin+1,prod_type_heavy->GetBinContent(ibin+14));
      prod_type_plot_heavy->SetBinError(ibin+1,prod_type_heavy->GetBinError(ibin+14));
    }
    TH1F* prod_type_plot_dist=(TH1F*)prod_type_plot_fake->Clone("prod_type_plot_dist");
    prod_type_plot_dist->Add(prod_type_plot_gamma);
    prod_type_plot_dist->Add(prod_type_plot_heavy);
    TCanvas* c=new TCanvas("c","c",400,400);
    TLegend* leg=new TLegend(.7,.7,.9,.9);
    prod_type_plot_dist->SetStats(0);
    prod_type_plot_dist->SetMinimum(0);
    prod_type_plot_dist->SetLineColor(kBlack);
    prod_type_plot_dist->SetMarkerColor(kBlack);
    leg->AddEntry(prod_type_plot_dist,"Total","l");
    //prod_type_plot_dist->GetXaxis()->SetRange(2,prod_type_plot_dist->GetNbinsX());
    for(int i=0;i<prod_type_plot_dist->GetNbinsX();i++) prod_type_plot_dist->GetXaxis()->SetBinLabel(i+1,cat[i].c_str());
    prod_type_plot_dist->Draw();
    prod_type_plot_fake->SetStats(0);
    prod_type_plot_fake->SetLineColor(kGreen);
    prod_type_plot_fake->SetMarkerColor(kGreen);
    leg->AddEntry(prod_type_plot_fake,"f","l");
    //prod_type_plot_fake->GetXaxis()->SetRange(2,prod_type_plot_dist->GetNbinsX());
    prod_type_plot_fake->Draw("SAME");
    prod_type_plot_gamma->SetStats(0);
    prod_type_plot_gamma->SetLineColor(kBlue);
    prod_type_plot_gamma->SetMarkerColor(kBlue);
    leg->AddEntry(prod_type_plot_gamma,"photons","l");
    //prod_type_plot_gamma->GetXaxis()->SetRange(2,prod_type_plot_dist->GetNbinsX());
    prod_type_plot_gamma->Draw("SAME");
    prod_type_plot_heavy->SetStats(0);
    prod_type_plot_heavy->SetLineColor(kRed);
    prod_type_plot_heavy->SetMarkerColor(kRed);
    leg->AddEntry(prod_type_plot_heavy,"HF","l");
    //prod_type_plot_heavy->GetXaxis()->SetRange(2,prod_type_plot_dist->GetNbinsX());
    prod_type_plot_heavy->Draw("SAME");
    prod_type_plot_dist->Draw("SAME");
    leg->Draw();
    c->SaveAs(Form("catPlots/prod_type_plot_dist_el_%s.pdf",year.Data()));
    c->SaveAs(Form("catPlots/prod_type_plot_dist_el_%s.pdf",year.Data()));
    //temporarily overwrite VHlep
    /*
    prod_type_fake->SetBinContent(5,.0018);
    prod_type_fake->SetBinError(5,.0003);
    prod_type_gamma->SetBinContent(5,0);
    prod_type_gamma->SetBinError(5,0);
    prod_type_heavy->SetBinContent(5,0);
    prod_type_heavy->SetBinError(5,0);
    */
    //prod_type_plot_dist->Scale(1/prod_type_plot_dist->Integral());
    TCanvas* c2=new TCanvas("c2","c2",400,400);
    TLegend* leg2=new TLegend(.7,.7,.9,.9);
    leg2->AddEntry(prod_type_plot_fake,"Light","F");
    leg2->AddEntry(prod_type_plot_gamma,"Photons","F");
    leg2->AddEntry(prod_type_plot_heavy,"Heavy flavor","F");
    prod_type_plot_dist->SetFillColor(kBlack);
    prod_type_plot_dist->SetFillStyle(3004);
    prod_type_plot_dist->SetMarkerSize(0);
    leg2->AddEntry(prod_type_plot_dist,"Uncertainty","F");

    //prod_type_plot_dist->Draw("E2A");
    THStack *hs = new THStack("hs","");
    prod_type_plot_fake->SetFillColor(kGreen+2);
    prod_type_plot_fake->SetLineColor(kGreen+2);
    prod_type_plot_gamma->SetFillColor(kBlue+2);
    prod_type_plot_gamma->SetLineColor(kBlue+2);
    prod_type_plot_heavy->SetFillColor(kRed+1);
    prod_type_plot_heavy->SetLineColor(kRed+1);
    //prod_type_plot_dist->GetYaxis()->SetRangeUser(0,10);
    prod_type_plot_dist->Draw("E2 ");
    
    hs->Add(prod_type_plot_fake);
    hs->Add(prod_type_plot_gamma);
    hs->Add(prod_type_plot_heavy);
    hs->SetMaximum(20);
    hs->Draw( "HIST SAME");
    prod_type_plot_dist->Draw("E2 SAME");
    leg2->Draw();
    allLabels(lumi[year],"",.2,.8);
    c2->SaveAs(Form("catPlots/prod_type_plot_dist_el_stack_%s.pdf",year.Data()));
    c2->SaveAs(Form("catPlots/prod_type_plot_dist_el_stack_%s.eps",year.Data()));
  }
  double totErr;
  //get njet shapes, normalize to inclusive total
  TFile* njetFile=TFile::Open(Form("shapeFiles/njets_incl_2l2e_%s_wide.root",year.Data()));
  TH1D* njetLight=(TH1D*)njetFile->Get("lightShape");
  TH1D* njetGamma=(TH1D*)njetFile->Get("gammaShape");
  TH1D* njetHeavy=(TH1D*)njetFile->Get("heavyShape");
  float lightTot=prod_type_fake->Integral(1,prod_type_fake->GetNbinsX());
  float gammaTot=prod_type_gamma->Integral(1,prod_type_gamma->GetNbinsX());
  float heavyTot=prod_type_heavy->GetBinContent(H4lBkg::toInt(H4lBkg::Category::ProdType::prodInclusive));
  njetLight->Scale(lightTot/njetLight->Integral());
  njetGamma->Scale(gammaTot/njetGamma->Integral());
  njetHeavy->Scale(heavyTot/njetHeavy->Integral());
  TFile* outfile=new TFile(Form("shapeFiles/prod_type%s_yields_2l2e_%s.root",hmString.c_str(),year.Data()),"RECREATE");
  if(!isHM){
    int ncat=H4lBkg::toInt(H4lBkg::Category::ProdType::Max); //njet, y4l (for 2d diffxs), jet1pt (ditto), stxs, sideband, and inclusive
    TH1F* lightYields=new TH1F("lightYields",0,ncat,-0.5,ncat-0.5);
    TH1F* gammaYields=new TH1F("gammaYields",0,ncat,-0.5,ncat-0.5);
    TH1F* heavyYields=(TH1F*)prod_type_heavy->Clone("heavyYields");
    //first 6 bins are 0jet, 1jet, 2jet, >=1jet, >=2jet, >=3jet; next four are the y4l bins; then three jet1pt bins; then the stxs categories; then the side band categories; last is inclusive
    for(int i=0;i<lightYields->GetNbinsX();i++){
      //cout<<"set for bin "<<i<<endl;
      if(i>=H4lBkg::toInt(H4lBkg::Category::ProdType::_0JetPt4l10) && i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_ttHHad_b2)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_0JetPt4l10)+1;
	lightYields->SetBinContent(i+1,prod_type_fake->GetBinContent(binNum));
	lightYields->SetBinError(i+1,prod_type_fake->GetBinError(binNum));
	gammaYields->SetBinContent(i+1,prod_type_gamma->GetBinContent(binNum));
	gammaYields->SetBinError(i+1,prod_type_gamma->GetBinError(binNum));
      }
      else if(i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_2Jet)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_0Jet)+1;
	lightYields->SetBinContent(i+1,njetLight->GetBinContent(binNum));
	//use these uncertainties since only have the combined ones in the shape file; scale appropriately
	lightYields->SetBinError(i+1,h_f_njet->GetBinError(binNum)*njetLight->GetBinContent(binNum)/h_f_njet->GetBinContent(binNum));
	gammaYields->SetBinContent(i+1,njetGamma->GetBinContent(binNum));
	gammaYields->SetBinError(i+1,h_gamma_njet->GetBinError(binNum)*njetGamma->GetBinContent(binNum)/h_gamma_njet->GetBinContent(binNum));
	cout<<"njets="<<i<<", light="<<lightYields->GetBinContent(i+1)<<", gamma="<<gammaYields->GetBinContent(i+1)<<", heavy="<<heavyYields->GetBinContent(i+1)<<endl;
      }
      else if(i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_ge3Jet)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_ge1Jet)+2;
	double error,integral;
	integral=h_f_njet->IntegralAndError(binNum,h_f_njet->GetNbinsX(),error);
	lightYields->SetBinContent(i+1,njetLight->Integral(binNum,njetLight->GetNbinsX()));
	//use these uncertainties since only have the combined ones in the shape file; scale appropriately
	lightYields->SetBinError(i+1,error*njetLight->Integral(binNum,njetLight->GetNbinsX())/integral);
	integral=h_gamma_njet->IntegralAndError(binNum,h_gamma_njet->GetNbinsX(),error);
	gammaYields->SetBinContent(i+1,njetGamma->Integral(binNum,njetGamma->GetNbinsX()));
	gammaYields->SetBinError(i+1,error*njetGamma->Integral(binNum,njetGamma->GetNbinsX())/integral);
	cout<<"njets>="<<i-2<<", light="<<lightYields->GetBinContent(i+1)<<", gamma="<<gammaYields->GetBinContent(i+1)<<", heavy="<<heavyYields->GetBinContent(i+1)<<endl;
      }
      else if(i==H4lBkg::toInt(H4lBkg::Category::ProdType::_le1Jet)){
	double error,integral;
	integral=h_f_njet->IntegralAndError(1,2,error);
	lightYields->SetBinContent(i+1,njetLight->Integral(1,2));
	lightYields->SetBinError(i+1,error*njetLight->Integral(1,2)/integral);
	integral=h_gamma_njet->IntegralAndError(1,2,error);
	gammaYields->SetBinContent(i+1,njetGamma->Integral(1,2));
	gammaYields->SetBinError(i+1,error*njetGamma->Integral(1,2)/integral);
      }
      else if(i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l3)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_y4l0)+1;
	lightYields->SetBinContent(i+1,h_f_y4l2d->GetBinContent(binNum));
	lightYields->SetBinError(i+1,h_f_y4l2d->GetBinError(binNum));
	//cout<<"set light y4l yield"<<endl;
	gammaYields->SetBinContent(i+1,h_gamma_y4l2d->GetBinContent(binNum));
	gammaYields->SetBinError(i+1,h_gamma_y4l2d->GetBinError(binNum));
	//cout<<"set gamma y4l yield"<<endl;
      }
      else if(i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_jet1pt2)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_jet1pt0)+2; //1st bin, which is 0-jet, gets merged with 2nd
	if(binNum>2){
	  lightYields->SetBinContent(i+1,h_jet1pt_f_m4l->GetBinContent(binNum));
	  cout<<"set "<<H4lBkg::Category::toString((H4lBkg::Category::ProdType)i)<<" to "<<h_jet1pt_f_m4l->GetBinContent(binNum)<<endl;
	  gammaYields->SetBinContent(i+1,h_jet1pt_gamma_m4l->GetBinContent(binNum));
	  lightYields->SetBinError(i+1,h_jet1pt_f_m4l->GetBinError(binNum));
	  gammaYields->SetBinError(i+1,h_jet1pt_gamma_m4l->GetBinError(binNum));
	} 
	else{
	  lightYields->SetBinContent(i+1,h_jet1pt_f_m4l->GetBinContent(1)+h_jet1pt_f_m4l->GetBinContent(2));
	  cout<<"set "<<H4lBkg::Category::toString((H4lBkg::Category::ProdType)i)<<" to "<<h_jet1pt_f_m4l->GetBinContent(1)+h_jet1pt_f_m4l->GetBinContent(2)<<endl;
	  gammaYields->SetBinContent(i+1,h_jet1pt_gamma_m4l->GetBinContent(1)+h_jet1pt_gamma_m4l->GetBinContent(2));
	  lightYields->SetBinError(i+1,sqrt(pow(h_jet1pt_f_m4l->GetBinError(1),2)+pow(h_jet1pt_f_m4l->GetBinError(2),2)));
	  gammaYields->SetBinError(i+1,sqrt(pow(h_jet1pt_gamma_m4l->GetBinError(1),2)+pow(h_jet1pt_gamma_m4l->GetBinError(2),2)));
	}
      }
      else if(i<=H4lBkg::toInt(H4lBkg::Category::ProdType::_SB_VHLep)){
	int binNum=i-H4lBkg::toInt(H4lBkg::Category::ProdType::_SB_0jet)+1;
	lightYields->SetBinContent(i+1,h_sb_f->GetBinContent(binNum));
	lightYields->SetBinError(i+1,h_sb_f->GetBinError(binNum));
	gammaYields->SetBinContent(i+1,h_sb_gamma->GetBinContent(binNum));
	gammaYields->SetBinError(i+1,h_sb_gamma->GetBinError(binNum));
      }
      else if(i==H4lBkg::toInt(H4lBkg::Category::ProdType::prodInclusive)){
	float tot=prod_type_fake->IntegralAndError(1,prod_type_fake->GetNbinsX(),totErr);
	lightYields->SetBinContent(i+1,tot);
	lightYields->SetBinError(i+1,totErr);
	cout<<"fake total is "<<tot<<"+/-"<<totErr<<endl;
	tot=prod_type_gamma->IntegralAndError(1,prod_type_gamma->GetNbinsX(),totErr);
	gammaYields->SetBinContent(i+1,tot);
	gammaYields->SetBinError(i+1,totErr);
	//tot=prod_type_heavy->IntegralAndError(13,prod_type_heavy->GetNbinsX()-1,totErr);
	//heavyYields->SetBinContent(i+1,tot);
	//heavyYields->SetBinError(i+1,totErr);
	cout<<"heavy total is "<<prod_type_heavy->GetBinContent(prod_type_heavy->GetNbinsX())<<"+/-"<<prod_type_heavy->GetBinError(prod_type_heavy->GetNbinsX())<<endl;
	cout<<"inclusive, light="<<lightYields->GetBinContent(i+1)<<", gamma="<<gammaYields->GetBinContent(i+1)<<", heavy="<<heavyYields->GetBinContent(i+1)<<endl;
      }
    }
    lightYields->Write();
    gammaYields->Write();
    heavyYields->Write();
  }
  else{
    TH1F* lightYields=(TH1F*)prod_type_fake->Clone("lightYields");
    TH1F* gammaYields=(TH1F*)prod_type_gamma->Clone("gammaYields");
    TH1F* heavyYields=(TH1F*)prod_type_heavy->Clone("heavyYields");
    lightYields->Write();
    gammaYields->Write();
    heavyYields->Write();
  }
  outfile->Write();
  outfile->Close();
  //for(int i=0;i<prod_type_dist->GetNbinsX();i++) cout<<"total in prod_type "<<i<<" is "<<prod_type_dist->GetBinContent(i+1)<<"+/-"<<prod_type_dist->GetBinError(i+1)<<endl;
  prod_type_fake->Scale(1/prod_type_fake->Integral(1,prod_type_fake->GetNbinsX()));
  for(int i=0;i<prod_type_fake->GetNbinsX();i++) cout<<"fraction in prod_type "<<i<<" for fakes is "<<prod_type_fake->GetBinContent(i+1)<<"+/-"<<prod_type_fake->GetBinError(i+1)<<endl;
  prod_type_gamma->Scale(1/prod_type_gamma->Integral(1,prod_type_fake->GetNbinsX()));
  for(int i=0;i<prod_type_gamma->GetNbinsX();i++) cout<<"fraction in prod_type "<<i<<" for gamma is "<<prod_type_gamma->GetBinContent(i+1)<<"+/-"<<prod_type_gamma->GetBinError(i+1)<<endl;
  prod_type_heavy->Scale(1/prod_type_heavy->GetBinContent(prod_type_heavy->GetNbinsX()));
  for(int i=0;i<13;i++) cout<<"fraction in prod_type "<<i<<" for heavy is "<<prod_type_heavy->GetBinContent(i+13)<<"+/-"<<prod_type_heavy->GetBinError(i+13)<<endl;

  /*
  TFile* v2file=new TFile("electronBackgroundComponents.root");
  TH1F* prod_type_dist2=(TH1F*)(v2file->Get("cat_0"))->Clone("prod_type_dist_2");
  prod_type_dist2->Add((TH1F*)v2file->Get("cat_1"));
  prod_type_dist2->Scale(.42/prod_type_dist2->Integral());
  TH1F* cat2=(TH1F*)v2file->Get("cat_2");
  cat2->Scale(.58/cat2->Integral());
  prod_type_dist2->Add(cat2);
  //prod_type_dist2->Scale(1/prod_type_dist2->Integral());
  delete c; c=0;
  c=new TCanvas("c","c",400,400);
  prod_type_dist->Draw();
  prod_type_dist2->SetStats(0);
  prod_type_dist2->SetLineColor(kMagenta);
  prod_type_dist2->SetMarkerColor(kMagenta);
  prod_type_dist2->Draw("SAME");
  c->SaveAs("testproddist.pdf");
  */
}
