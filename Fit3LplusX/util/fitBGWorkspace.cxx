#include "TFile.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "THStack.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLegend.h"
#include "TLine.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

#include "RooWorkspace.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooArgSet.h"
#include "RooGaussian.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooPlot.h"
#include "RooBinning.h"
#include "RooKeysPdf.h"
#include "RooStats/SPlot.h"
#include "RooNDKeysPdf.h"
#include "RooArgList.h"
#include "Fit3LplusX/Vars.h"

#include "H4lBkgUtils/H4lStyle.h"
#include "H4lBkgUtils/AtlasStyle.h"
float LUMI = 36.1;

void myText(Double_t x,Double_t y,Color_t color,const char *text, float tsize) 
{
  TLatex l;
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}

void printToPDF(TH1F* h_raw,TH1F* h,int );

void printToPDF(TH1F* h_raw,TH1F* h,int rebin)
{
  h_raw->Scale(1./h_raw->Integral());
  //h_raw->SetLineStyle(kDashed);
  //h_raw->Rebin(rebin);
  //h_raw->SetMarkerSize(0);
  //h_raw->GetXaxis()->SetRangeUser(0,1000);
  h_raw->Draw("e");
  h_raw->GetYaxis()->SetRangeUser(0.000001,h_raw->GetMaximum()*1.4);
  h_raw->GetYaxis()->SetTitle(Form("Events / %g GeV",h_raw->GetBinWidth(1)));
  h_raw->GetXaxis()->SetTitle("m_{4l} [GeV]");
  
  //h->GetXaxis()->SetRangeUser(0,1000);
  h->SetLineColor(kRed+1);
  h->SetLineStyle(kDashed);
  h->Scale(rebin);
  h->DrawCopy("histsame");

  h->SetLineColor(kRed+1);
  h->SetLineStyle(1);
  h->Scale(1./h->Integral());
  //h->Rebin(rebin);
  h->Draw("histsame");
  h_raw->Draw("esame");
  
  TLegend *leg = makeLeg(0.605,0.65,0.90,0.85);
  leg->AddEntry(h_raw,"Raw","lp");
  leg->AddEntry(h,"Smoothed","l");
  leg->Draw();
}

int main(int argc, char* argv[]){

  using namespace std;
  using namespace RooFit;

  string treename="tree_relaxee";
  string output="plots";
  bool do_nInner=false;
  bool do_rTRT=false;
  bool do_f1=false;
  bool do_eProbHT=false;
  bool isData=false;
  bool do2e2e=false;
  bool do2mu2e=false;
  string year;
  
  if (argc==1){
    cout<<"give args:"<<endl;
    cout<<"--treename [{tree_relaxee}, ...]"<<endl;
    cout<<"--vars [nInner, rTRT, f1, eProbHT]"<<endl;
    cout<<"--isData [t,f]"<<endl;
    cout<<"--do2e2e"<< endl;
    cout<<"--do2mu2e" << endl; 
    cout<<"--year [2016,2017,2018]"<<endl;
    return -1;
  }

  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--treename")){
      cout<<"setting treename"<<endl;
      treename=argv[++a];
    }
    else if (!strcmp(argv[a],"--vars")){
      cout<<"setting vars"<<endl;
      while (a+1<argc){
        string next = argv[++a];
        if (next.find("--")==string::npos){
          if (next=="nInner") do_nInner=true;
          else if (next=="rTRT") do_rTRT=true;
          else if (next=="f1") do_f1=true;
	  else if (next=="eProbHT") do_eProbHT=true;
          else {
            cout<<"unrecognized param given to --vars: "<<next<<endl;
            return -1;
          }
        }
        else {
          --a;
          break;
        }
      }
    }
    else if (!strcmp(argv[a],"--output")){
      cout<<"setting output dir"<<endl;
      output=argv[++a];
    }
    else if(!strcmp(argv[a],"--isData")){
      cout<<"setting isData";
      if(!strcmp(argv[++a],"t")) isData=true;
      if(isData) cout<<" to true"<<endl;
      else cout<<" to false "<<endl;
    }
    else if(!strcmp(argv[a],"--do2e2e")){
      cout<<"setting channel ee+ee";
      do2e2e=true;
    }
    else if(!strcmp(argv[a],"--do2mu2e")){
      cout<<"setting channel mumu+ee";
      do2mu2e=true;
    }
    else if(!strcmp(argv[a],"--year")){
      year=argv[++a];
      cout<<"year is "<<year<<endl;
      if(year.compare("2017")==0) LUMI=44.3;
      if(year.compare("2018")==0) LUMI=59.9;
      if(year.compare("all")==0) LUMI=139;
    }
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }

  cout<<"using tree = "<<treename<<endl;
  cout<<"nInner: "<<(do_nInner?"yes":"no")<<endl;
  cout<<"rTRT: "<<(do_rTRT?"yes":"no")<<endl;
  cout<<"f1: "<<(do_f1?"yes":"no")<<endl;
  cout<<"eProbHT: "<<(do_eProbHT?"yes":"no")<<endl;
  cout<<"channel: "<<( do2e2e?"ee+ee": (do2mu2e?"mumu+ee":"ee/mumu+ee"))<<endl;

  TString finalState="";
  if(do2e2e) finalState="_2e2e";
  if(do2mu2e) finalState="_2mu2e";

  vector<Vars::Var> FitVars;
  vector<string> FitVarNames;
  if (do_nInner) {FitVars.push_back(Vars::nInner); FitVarNames.push_back("n_{BL}");}
  if (do_rTRT) {FitVars.push_back(Vars::rTRT); FitVarNames.push_back("r_{TRT}");}
  if (do_f1) {FitVars.push_back(Vars::f1); FitVarNames.push_back("f_{1}");}
  if (do_eProbHT) {FitVars.push_back(Vars::eProbHT); FitVarNames.push_back("p_{TRT}");}

  TString label=Form("%s/wsp",output.c_str());
  for (auto v: FitVars) { label+="_"; label+=v.name; label+="_"; label+=year;}
  TFile* infile = TFile::Open(Form("%s.root",label.Data()),"READ");
  RooWorkspace* wsp = (RooWorkspace*)infile->Get("bgDiscriminantWorkspace");
  wsp->Print();

  RooRealVar* n_gamma = wsp->var("n_gamma");
  RooRealVar* n_light = wsp->var("n_light");
  RooRealVar* n_heavy = wsp->var("n_heavy");
  RooRealVar* n_real = wsp->var("n_real");

  RooArgSet *nVars = 0;
  if (FitVars.size()==1) 
    nVars = new RooArgSet(*n_gamma,*n_light);
  else if (FitVars.size()>1) 
    nVars = new RooArgSet(*n_gamma,*n_light,*n_heavy,*n_real); 
  else {
    std::cout << "Please define fit variables!" << std::endl;
  }

  RooRealVar* v_eta = new RooRealVar("eta","eta",-2.47,2.47);
  v_eta->setBins(9);
  RooRealVar* v_pt = new RooRealVar("pt","pt",7,70);
  double ptBins[4]={7,10,15,70};
  //double ptBins[5]={5,7,10,15,70};
  RooBinning ptBinning(3,ptBins);
  v_pt->setBinning(ptBinning);
  RooRealVar* v_m4l = new RooRealVar("m4l","m4l",55,400);
  
  //double m4l_bins[19]={60,80,100,115,130,140,160,180,200,220,240,260,280,300,320,340,360,380,400};
  //RooBinning m4l_binning(18,m4l_bins);
  v_m4l->setBins(23);
  RooRealVar* v_m4l_0jet = new RooRealVar("m4l_0jet","m4l_0jet",55,400);
  v_m4l_0jet->setBins(23);
  RooRealVar* v_m4l_1jet = new RooRealVar("m4l_1jet","m4l_1jet",55,400);
  v_m4l_1jet->setBins(23);
  RooRealVar* v_m4l_2jet = new RooRealVar("m4l_2jet","m4l_2jet",55,400);
  v_m4l_2jet->setBins(23);
  RooRealVar* v_m4l_smoothed = new RooRealVar("m4l_smoothed","m4l_smoothed",55,400);
  v_m4l_smoothed->setBins(620);
  RooRealVar* v_m4l_smoothed2 = new RooRealVar("m4l_smoothed2","m4l_smoothed2",55,400);
  v_m4l_smoothed2->setBins(620);
  RooRealVar* v_pt4l_incl=new RooRealVar("pt4l_incl","pt4l_incl",0,1000);
  double pt4l_incl_bins[12]={0,10,20,30,45,60,80,120,200,300,650,1000};
  RooBinning pt4l_incl_binning(11,pt4l_incl_bins);
  v_pt4l_incl->setBinning(pt4l_incl_binning);
  /*
  RooRealVar* v_pt4l_incl_mcut=new RooRealVar("pt4l_incl_mcut","pt4l_incl_mcut",-1,1000);
  double pt4l_incl_mcut_bins[13]={-1,0,10,15,20,30,45,60,80,120,200,350,1000};
  RooBinning pt4l_incl_mcut_binning(12,pt4l_incl_mcut_bins);
  v_pt4l_incl_mcut->setBinning(pt4l_incl_mcut_binning);
  */
  RooRealVar* v_pt4l_0jet=new RooRealVar("pt4l_0jet","pt4l_0jet",0,10000);
  double pt4l_0jet_bins[6]={0,15,30,120,350,10000};
  RooBinning pt4l_0jet_binning(5,pt4l_0jet_bins);
  v_pt4l_0jet->setBinning(pt4l_0jet_binning);
  //RooRealVar* v_pt4l_0jet_mcut=(RooRealVar*)v_pt4l_0jet->Clone("pt4l_0jet_mcut"); //only have to clone this one since it already has a -1 bin
  RooRealVar* v_pt4l_1jet=new RooRealVar("pt4l_1jet","pt4l_1jet",0,10000);
  double pt4l_1jet_bins[6]={0,60,80,120,350,10000};
  RooBinning pt4l_1jet_binning(5,pt4l_1jet_bins);
  v_pt4l_1jet->setBinning(pt4l_1jet_binning);
  RooRealVar* v_pt4l_2jet=new RooRealVar("pt4l_2jet","pt4l_2jet",0,10000);
  double pt4l_2jet_bins[4]={0,120,350,10000};
  RooBinning pt4l_2jet_binning(3,pt4l_2jet_bins);
  v_pt4l_2jet->setBinning(pt4l_2jet_binning);
  RooRealVar* v_pt4l_3jet=new RooRealVar("pt4l_3jet","pt4l_3jet",0,10000);
  RooBinning pt4l_3jet_binning(3,pt4l_2jet_bins);
  v_pt4l_3jet->setBinning(pt4l_3jet_binning);
  //RooRealVar* v_pt4l_1jet_mcut=(RooRealVar*)v_pt4l_1jet->Clone("pt4l_1jet_mcut");
  RooRealVar* v_m12=new RooRealVar("m12","m12",50,106);
  double m12_bins[5]={50,64,73,85,106};
  RooBinning m12_binning(4,m12_bins);
  v_m12->setBinning(m12_binning);
  /*
  RooRealVar* v_m12_mcut=new RooRealVar("m12_mcut","m12_mcut",-1,1000);
  double m12_mcut_bins[10]={-1,0,50,65,74,82,88,94,106,1000};
  RooBinning m12_mcut_binning(9,m12_mcut_bins);
  v_m12_mcut->setBinning(m12_mcut_binning);
  */
  /*
  RooRealVar* v_m12_2d_mcut=new RooRealVar("m12_2d_mcut","m12_2d_mcut",-1,1000);
  double m12_2d_mcut_bins[7]={-1,0,50,75,82,106,1000};
  RooBinning m12_2d_mcut_binning(6,m12_2d_mcut_bins);
  v_m12_2d_mcut->setBinning(m12_2d_mcut_binning);
  */
  RooRealVar* v_m34=new RooRealVar("m34","m34",12,65);
  double m34_bins[8]={12,20,24,28,32,40,55,65};
  RooBinning m34_binning(7,m34_bins);
  v_m34->setBinning(m34_binning);
  /*
  RooRealVar* v_m34_mcut=new RooRealVar("m34_mcut","m34_mcut",-1,1000);
  double m34_mcut_bins[11]={-1,0,12,20,24,28,32,40,55,65,1000};
  RooBinning m34_mcut_binning(10,m34_mcut_bins);
  v_m34_mcut->setBinning(m34_mcut_binning);
  RooRealVar* v_m34_m4l=new RooRealVar("m34_rebin","m34_rebin",-1,1000);
  double m34_m4l_bins[8]={-1,0,12,24,32,55,115,1000};
  RooBinning m34_m4l_binning(7,m34_m4l_bins);
  v_m34_m4l->setBinning(m34_m4l_binning);
  */
  /*
  RooRealVar* v_m34_2d_mcut=new RooRealVar("m34_2d_mcut","m34_2d_mcut",-1,1000);
  double m34_2d_mcut_bins[7]={-1,0,12,24,32,65,1000};
  RooBinning m34_2d_mcut_binning(6,m34_2d_mcut_bins);
  v_m34_2d_mcut->setBinning(m34_2d_mcut_binning);
  */
  /*
  RooRealVar* v_m12_m34_mcut=new RooRealVar("m12_m34_mcut","m12_m34_mcut",-1,6); //flatten to 1d and assign directly to the bin, easier to handle than funky 2d binning
  v_m12_m34_mcut->setBins(7);
  */
  RooRealVar* v_m12_m34=new RooRealVar("m12_m34","m12_m34",0,6); //flatten to 1d and assign directly to the bin, easier to handle than funky 2d binning
  v_m12_m34->setBins(6);
  RooRealVar* v_m4l_constrained_HM=new RooRealVar("m4l_constrained_HM","m4l_constrained_HM",55,2500);
  v_m4l_constrained_HM->setBins(163);
  RooRealVar* v_y4l=new RooRealVar("y4l","y4l",0,2.5);
  double y4l_bins[11]={0,0.15,0.3,0.45,0.6,0.75,0.9,1.2,1.6,2,2.5};
  RooBinning y4l_binning(10,y4l_bins);
  v_y4l->setBinning(y4l_binning);
  RooRealVar* v_y4l_2d=new RooRealVar("y4l_2d","y4l_2d",0,2.5);
  double y4l_2d_bins[5]={0,0.5,1.0,1.5,2.5};
  RooBinning y4l_2d_binning(4,y4l_2d_bins);
  v_y4l_2d->setBinning(y4l_2d_binning);
  /*
  RooRealVar* v_y4l_mcut=new RooRealVar("y4l_mcut","y4l_mcut",-1,10);
  double y4l_mcut_bins[10]={-1,0,0.3,0.6,0.9,1.2,1.6,2,2.5,10};
  RooBinning y4l_mcut_binning(9,y4l_mcut_bins);
  v_y4l_mcut->setBinning(y4l_mcut_binning);
  */
  RooRealVar* v_njet=new RooRealVar("njet","njet",0,4);
  double njet_bins[5]={0,1,2,3,4};
  RooBinning njet_binning(4,njet_bins);
  v_njet->setBinning(njet_binning);
  RooRealVar* v_njet_diff=new RooRealVar("njet_diff","njet_diff",0,4);
  double njet_diff_bins[5]={0,1,2,3,4};
  RooBinning njet_diff_binning(4,njet_diff_bins);
  v_njet_diff->setBinning(njet_diff_binning);
  RooRealVar* v_njet2=new RooRealVar("njet2","njet2",0,10);
  double njet2_bins[3]={0,1,10};
  RooBinning njet2_binning(2,njet2_bins);
  v_njet2->setBinning(njet2_binning);
  /*
  RooRealVar* v_njet_mcut=new RooRealVar("njet_mcut","njet_mcut",-1,10);
  double njet_mcut_bins[6]={-1,0,1,2,3,10};
  RooBinning njet_mcut_binning(5,njet_mcut_bins);
  v_njet_mcut->setBinning(njet_mcut_binning);
  RooRealVar* v_njet2_mcut=new RooRealVar("njet2_mcut","njet2_mcut",0,10);
  double njet2_mcut_bins[3]={0,1,10};
  RooBinning njet2_mcut_binning(2,njet2_mcut_bins);
  v_njet2->setBinning(njet2_mcut_binning);
  */
  RooRealVar* v_njet_incl=new RooRealVar("njet_incl","njet_incl",0,3); //0,ge1,ge2,ge3 jets
  v_njet_incl->setBins(4);
  RooRealVar* v_cts=new RooRealVar("cts","cts",0,1);
  v_cts->setBins(8);
  //RooRealVar* v_cts_mcut=new RooRealVar("cts_mcut","cts_mcut",-0.125,1);
  //v_cts_mcut->setBins(9);
  RooRealVar* v_jet1pt=new RooRealVar("jet1pt","jet1pt",29,1000);
  double jet1pt_bins[6]={29,30,60,120,350,1000};
  RooBinning jet1pt_binning(5,jet1pt_bins);
  v_jet1pt->setBinning(jet1pt_binning);
  RooRealVar* v_jet2pt=new RooRealVar("jet2pt","jet2pt",29,1000);
  RooBinning jet2pt_binning(5,jet1pt_bins);
  v_jet2pt->setBinning(jet2pt_binning);
  /*
  RooRealVar* v_jet1pt_mcut=new RooRealVar("jet1pt_mcut","jet1pt_mcut",-1,4000);
  double jet1pt_mcut_bins[9]={-1,0,30,40,55,75,120,350,4000};
  RooBinning jet1pt_mcut_binning(8,jet1pt_mcut_bins);
  v_jet1pt_mcut->setBinning(jet1pt_mcut_binning);
  */
  RooRealVar* v_m4l_mwindow=new RooRealVar("m4l_mwindow","m4l_mwindow",55,400);
  //double m4l_mwindow_bins[26]={55,70,85,100,105,110,115,120,125,130,135,140,145,150,155,160,175,190,205,220,235,250,265,280,295,310,325,340,355,370,385,400};
  double m4l_mwindow_bins[14]={55,105,110,115,120,125,130,135,140,145,150,155,160,400};
  RooBinning m4l_mwindow_binning(13,m4l_mwindow_bins);
  v_m4l_mwindow->setBinning(m4l_mwindow_binning);
  RooRealVar* v_m4l_diffxs=new RooRealVar("m4l_diffxs","m4l_diffxs",55,400);
  //double m4l_diffxs_bins[18]={55,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,400};
  //RooBinning m4l_diffxs_binning(17,m4l_diffxs_bins);
  double m4l_diffxs_bins[8]={55,115,118,121,124,127,130,400};
  RooBinning m4l_diffxs_binning(7,m4l_diffxs_bins);
  v_m4l_diffxs->setBinning(m4l_diffxs_binning);

  RooRealVar* v_m4l_diffxs2=new RooRealVar("m4l_diffxs2","m4l_diffxs2",55,400);
  double m4l_diffxs_bins2[18]={55,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,400};
  RooBinning m4l_diffxs_binning2(17,m4l_diffxs_bins2);
  v_m4l_diffxs2->setBinning(m4l_diffxs_binning2);
  
  RooRealVar* v_prod_type=new RooRealVar("prod_type","prod_type",-0.5,15.5);
  v_prod_type->setBins(16);
  RooRealVar* v_prod_type_sb=new RooRealVar("prod_type_sb","prod_type_sb",-0.5,5.5);
  v_prod_type_sb->setBins(6);
  RooRealVar* v_prod_type_cp=new RooRealVar("prod_type_cp","prod_type_cp",-0.5,1.5);
  v_prod_type_cp->setBins(2);
  //RooRealVar* v_m4l_detailed=new RooRealVar("m4l_detailed","m4l_detailed",-0.5,1.5);
  //v_m4l_detailed->setBins(2);
  /*
  RooRealVar* v_BDT_0jet=new RooRealVar("BDT_0jet","BDT_0jet",-999,1);
  double BDT_0jet_bins[8]={-999,-1,-0.68,-0.3333,0,0.4,0.88,1};
  RooBinning BDT_0jet_binning(7,BDT_0jet_bins);
  v_BDT_0jet->setBinning(BDT_0jet_binning);
  RooRealVar* v_BDT_1jet_low=new RooRealVar("BDT_1jet_low","BDT_1jet_low",-999,1);
  double BDT_1jet_low_bins[6]={-999,-1,-0.1,0.22,0.5,1};
  RooBinning BDT_1jet_low_binning(5,BDT_1jet_low_bins);
  v_BDT_1jet_low->setBinning(BDT_1jet_low_binning);
  RooRealVar* v_BDT_1jet_high=new RooRealVar("BDT_1jet_high","BDT_1jet_high",-999,1);
  double BDT_1jet_high_bins[6]={-999,-1,0.02,0.4,0.66,1};
  RooBinning BDT_1jet_high_binning(5,BDT_1jet_high_bins);
  v_BDT_1jet_high->setBinning(BDT_1jet_high_binning);
  RooRealVar* v_BDT_2jet_VBF=new RooRealVar("BDT_2jet_VBF","BDT_2jet_VBF",-999,1);
  double BDT_2jet_VBF_bins[7]={-999,-1,-0.1,0.34,0.68,0.86,1};
  RooBinning BDT_2jet_VBF_binning(6,BDT_2jet_VBF_bins);
  v_BDT_2jet_VBF->setBinning(BDT_2jet_VBF_binning);
  RooRealVar* v_BDT_2jet_VH=new RooRealVar("BDT_2jet_VH","BDT_2jet_VH",-999,1);
  double BDT_2jet_VH_bins[8]={-999,-1,-0.26,0.22,0.56,0.72,0.82,1};
  RooBinning BDT_2jet_VH_binning(7,BDT_2jet_VH_bins);
  v_BDT_2jet_VH->setBinning(BDT_2jet_VH_binning);
  */
  RooRealVar* v_mjj=new RooRealVar("mjj","mjj",0,7);
  //double mjj_bins[5]={-1,0,120,450,3000};
  double mjj_bins[5]={0,1,2,4,7};
  RooBinning mjj_binning(4,mjj_bins);
  v_mjj->setBinning(mjj_binning);
  RooRealVar* v_detajj=new RooRealVar("detajj","detajj",0,10);
  double detajj_bins[5]={0,1,2,3.5,10};
  RooBinning detajj_binning(4,detajj_bins);
  v_detajj->setBinning(detajj_binning);
  RooRealVar* v_dphijj=new RooRealVar("dphijj","dphijj",0,2*3.14159265);
  double dphijj_bins[6]={0,1,3.14159265/2+1,3.14159265+1,3.14159265*1.5+1,2*3.14159265+1};
  RooBinning dphijj_binning(5,dphijj_bins);
  v_dphijj->setBinning(dphijj_binning);
  RooRealVar* v_nbjet=new RooRealVar("nbjet","nbjet",0,3);
  v_nbjet->setBins(3);
  RooRealVar* v_prod_type_hm=new RooRealVar("prod_type_hm","prod_type_hm",0,2);
  v_prod_type_hm->setBins(2);
  RooRealVar* v_phi=new RooRealVar("phi","phi",-3.14159265,3.14159265);
  v_phi->setBins(8);
  RooRealVar* v_phi1=new RooRealVar("phi1","phi1",-3.14159265,3.14159265);
  v_phi1->setBins(8);
  RooRealVar* v_cth1=new RooRealVar("cth1","cth1",-1,1);
  v_cth1->setBins(8);
  RooRealVar* v_cth2=new RooRealVar("cth2","cth2",-1,1);
  v_cth2->setBins(8);
  RooRealVar* v_pt4l_y4l0=new RooRealVar("pt4l_y4l0","pt4l_y4l0",0,10000);
  double pt4l_y4l_bins[5]={0,45,120,350,10000};
  RooBinning pt4l_y4l_binning(4,pt4l_y4l_bins);
  v_pt4l_y4l0->setBinning(pt4l_y4l_binning);
  RooRealVar* v_pt4l_y4l1=new RooRealVar("pt4l_y4l1","pt4l_y4l1",0,10000);
  v_pt4l_y4l1->setBinning(pt4l_y4l_binning);
  RooRealVar* v_pt4l_y4l2=new RooRealVar("pt4l_y4l2","pt4l_y4l2",0,10000);
  v_pt4l_y4l2->setBinning(pt4l_y4l_binning);
  RooRealVar* v_pt4l_y4l3=new RooRealVar("pt4l_y4l3","pt4l_y4l3",0,10000);
  v_pt4l_y4l3->setBinning(pt4l_y4l_binning);
  RooRealVar* v_pt4l_jet1pt=new RooRealVar("pt4l_jet1pt","pt4l_jet1pt",0,8);
  v_pt4l_jet1pt->setBins(8);
  RooRealVar* v_m4lj=new RooRealVar("m4lj","m4lj",119,10000);
  double m4lj_bins[9]={119,120,180,220,300,400,600,2000,10000};
  RooBinning m4lj_binning(8,m4lj_bins);
  v_m4lj->setBinning(m4lj_binning);
  RooRealVar* v_m4ljj=new RooRealVar("m4ljj","m4ljj",179,10000);
  double m4ljj_bins[8]={179,180,320,450,600,1000,2500,10000};
  RooBinning m4ljj_binning(7,m4ljj_bins);
  v_m4ljj->setBinning(m4ljj_binning);
  RooRealVar* v_pt4lj=new RooRealVar("pt4lj","pt4lj",0,10000);
  double pt4lj_bins[6]={0,1,61,121,351,10000};
  RooBinning pt4lj_binning(5,pt4lj_bins);
  v_pt4lj->setBinning(pt4lj_binning);
  RooRealVar* v_pt4ljj=new RooRealVar("pt4ljj","pt4ljj",0,10000);
  double pt4ljj_bins[6]={0,1,61,121,351,10000};
  RooBinning pt4ljj_binning(5,pt4ljj_bins);
  v_pt4ljj->setBinning(pt4ljj_binning);
  RooRealVar* v_leptonpt0=new RooRealVar("lepton_pt0","lepton_pt0",5,100);
  v_leptonpt0->setBins(19);
  RooRealVar* v_leptonpt1=new RooRealVar("lepton_pt1","lepton_pt1",5,80);
  v_leptonpt1->setBins(15);
  RooRealVar* v_leptonpt2=new RooRealVar("lepton_pt2","lepton_pt2",5,60);
  v_leptonpt2->setBins(11);
  RooRealVar* v_leptonpt3=new RooRealVar("lepton_pt3","lepton_pt3",5,40);
  v_leptonpt3->setBins(7);
  RooRealVar* v_leptonpt3_njet[3];
  RooRealVar* v_leptonpt3_mu[3];
  for(int i=0;i<3;i++){
    v_leptonpt3_njet[i]=new RooRealVar(Form("lepton_pt3_njet%i",i),Form("lepton_pt3_njet%i",i),0,40);
    v_leptonpt3_njet[i]->setBins(8);
    v_leptonpt3_mu[i]=new RooRealVar(Form("lepton_pt3_mu%i",i),Form("lepton_pt3_mu%i",i),0,40);
    v_leptonpt3_mu[i]->setBins(8);
  }
  RooRealVar* v_leptoneta0=new RooRealVar("lepton_eta0","lepton_eta0",-2.5,2.5);
  v_leptoneta0->setBins(20);
  RooRealVar* v_leptoneta1=new RooRealVar("lepton_eta1","lepton_eta1",-2.5,2.5);
  v_leptoneta1->setBins(20);
  RooRealVar* v_leptoneta2=new RooRealVar("lepton_eta2","lepton_eta2",-2.5,2.5);
  v_leptoneta2->setBins(20);
  RooRealVar* v_leptoneta3=new RooRealVar("lepton_eta3","lepton_eta3",-2.5,2.5);
  v_leptoneta3->setBins(20);
  RooRealVar* v_leptonphi0=new RooRealVar("lepton_phi0","lepton_phi0",-3.14159265,3.14159265);
  v_leptonphi0->setBins(20);
  RooRealVar* v_leptonphi1=new RooRealVar("lepton_phi1","lepton_phi1",-3.14159265,3.14159265);
  v_leptonphi1->setBins(20);
  RooRealVar* v_leptonphi2=new RooRealVar("lepton_phi2","lepton_phi2",-3.14159265,3.14159265);
  v_leptonphi2->setBins(20);
  RooRealVar* v_leptonphi3=new RooRealVar("lepton_phi3","lepton_phi3",-3.14159265,3.14159265);
  v_leptonphi3->setBins(20);
  RooRealVar* v_bdt=new RooRealVar("bdt","bdt",-1,1);
  //double bdt_bins[5]={-1,-0.399,0.109,0.45,1};
  //RooBinning bdt_binning(4,bdt_bins);
  //v_bdt->setBinning(bdt_binning);
  v_bdt->setBins(4);
  RooRealVar* v_jet1pt_jet2pt=new RooRealVar("jet1pt_jet2pt","jet1pt_jet2pt",0,7);
  v_jet1pt_jet2pt->setBins(7);
  RooRealVar* v_pt4l_pt4lj=new RooRealVar("pt4l_pt4lj","pt4l_pt4lj",0,6);
  v_pt4l_pt4lj->setBins(6);
  RooRealVar* v_pt4lj_m4lj=new RooRealVar("pt4lj_m4lj","pt4lj_m4lj",0,6);
  v_pt4lj_m4lj->setBins(6);
  RooRealVar* v_jet1pt_jet1y=new RooRealVar("jet1pt_jet1y","jet1pt_jet1y",0,7);
  v_jet1pt_jet1y->setBins(7);
  RooRealVar* v_cp_obs1=new RooRealVar("cp_obs1","cp_obs1",-1,1);
  v_cp_obs1->setBins(8);
  RooRealVar* v_cp_obs2=new RooRealVar("cp_obs2","cp_obs2",-1,1);
  v_cp_obs2->setBins(8);
  RooRealVar* v_cp_obs3=new RooRealVar("cp_obs3","cp_obs3",-1,1);
  v_cp_obs3->setBins(8);
  RooRealVar* v_cp_obs4=new RooRealVar("cp_obs4","cp_obs4",-1,1);
  v_cp_obs4->setBins(8);
  RooRealVar* v_cp_obs5=new RooRealVar("cp_obs5","cp_obs5",-1,1);
  v_cp_obs5->setBins(8);
  RooRealVar* v_cp_obs6=new RooRealVar("cp_obs6","cp_obs6",-1,1);
  v_cp_obs6->setBins(8);
  RooRealVar* v_shape_OO1jjtCzz=new RooRealVar("shape_OO1jjtCzz","shape_OO1jjtCzz",0,6);
  v_shape_OO1jjtCzz->setBins(6);
  RooRealVar* v_shape_OO1jjtCza=new RooRealVar("shape_OO1jjtCza","shape_OO1jjtCza",0,6);
  v_shape_OO1jjtCza->setBins(6);
  RooRealVar* v_shape_OO1jjcHWtil=new RooRealVar("shape_OO1jjcHWtil","shape_OO1jjcHWtil",0,6);
  v_shape_OO1jjcHWtil->setBins(6);
  RooRealVar* v_OO1jjtCzz=new RooRealVar("OO1jjtCzz","OO1jjtCzz",0,5);
  v_OO1jjtCzz->setBins(5);
  RooRealVar* v_OO1jjtCza=new RooRealVar("OO1jjtCza","OO1jjtCza",0,5);
  v_OO1jjtCza->setBins(5);
  RooRealVar* v_OO1jjcHWtil=new RooRealVar("OO1jjcHWtil","OO1jjcHWtil",0,5);
  v_OO1jjcHWtil->setBins(5);
  RooRealVar* v_OO14ltCzz=new RooRealVar("OO14ltCzz","OO14ltCzz",-1,1);
  double OO14ltCzz_bins[13]={-1.0, -0.024, -0.017, -0.011, -0.007, -0.003, 0.0, 0.003, 0.007, 0.011, 0.017, 0.024, 1.0};
  RooBinning OO14ltCzz_binning(12,OO14ltCzz_bins);
  v_OO14ltCzz->setBinning(OO14ltCzz_binning);
  RooRealVar* v_OO14ltCza=new RooRealVar("OO14ltCza","OO14ltCza",-2,2);
  double OO14ltCza_bins[13]={-2.0, -0.25, -0.136, -0.077, -0.040, -0.016, 0.0, 0.016, 0.040, 0.077, 0.136, 0.25, 2.0};
  RooBinning OO14ltCza_binning(12,OO14ltCza_bins);
  v_OO14ltCza->setBinning(OO14ltCza_binning);
  RooRealVar* v_OO14ltCaa=new RooRealVar("OO14ltCaa","OO14ltCaa",-10,10);
  double OO14ltCaa_bins[7]={-10.0, -0.15, -0.035, 0.0, 0.035, 0.15, 10.0};
  RooBinning OO14ltCaa_binning(6,OO14ltCaa_bins);
  v_OO14ltCaa->setBinning(OO14ltCaa_binning);
  RooRealVar* v_OO14lcHWtil=new RooRealVar("OO14lcHWtil","OO14lcHWtil",-10,10);
  double OO14lcHWtil_bins[13]={-10.0, -0.135, -0.076, -0.043, -0.022, -0.008, 0.0, 0.008, 0.022, 0.043, 0.076, 0.135, 10.0};
  RooBinning OO14lcHWtil_binning(12,OO14lcHWtil_bins);
  v_OO14lcHWtil->setBinning(OO14lcHWtil_binning);
  RooRealVar* v_OO14lcHBtil=new RooRealVar("OO14lcHBtil","OO14lcHBtil",-20,20);
  double OO14lcHBtil_bins[7]={-20.0, -0.25, -0.065, 0.0, 0.065, 0.25, 20.0};
  RooBinning OO14lcHBtil_binning(6,OO14lcHBtil_bins);
  v_OO14lcHBtil->setBinning(OO14lcHBtil_binning);
  RooRealVar* v_OO14lcHWBtil=new RooRealVar("OO14lcHWBtil","OO14lcHWBtil",-15,15);
  double OO14lcHWBtil_bins[7]={-15.0, -0.15, -0.035, 0.0, 0.035, 0.15, 15.0};
  RooBinning OO14lcHWBtil_binning(6,OO14lcHWBtil_bins);
  v_OO14lcHWBtil->setBinning(OO14lcHWBtil_binning);
  RooRealVar* v_NNVBF=new RooRealVar("NNVBF","NNVBF",0,2);
  double NNVBF_bins[7]={0,1,1.5,1.75,1.9,1.95,2};
  RooBinning NNVBF_binning(6,NNVBF_bins);
  v_NNVBF->setBinning(NNVBF_binning);

  std::cout<<"created RooRealVars"<<std::endl;

  RooRealVar* weight = wsp->var("weight");
  //weight->setRange(-0.1,0.5);
  vector<RooRealVar*> rrvars;
  for (auto v:FitVars) rrvars.push_back(wsp->var(v.name));
  rrvars.push_back(v_eta);
  rrvars.push_back(v_pt);
  rrvars.push_back(v_m4l_smoothed);
  rrvars.push_back(v_m4l_constrained_HM);
  rrvars.push_back(v_m4l_smoothed2);
  rrvars.push_back(v_m4l);
  rrvars.push_back(v_m4l_0jet);
  rrvars.push_back(v_m4l_1jet);
  rrvars.push_back(v_m4l_2jet);
  rrvars.push_back(v_pt4l_incl);
  rrvars.push_back(v_pt4l_0jet);
  rrvars.push_back(v_pt4l_1jet);
  rrvars.push_back(v_pt4l_2jet);
  rrvars.push_back(v_pt4l_3jet);
  rrvars.push_back(v_m12);
  rrvars.push_back(v_m34);
  rrvars.push_back(v_y4l);
  rrvars.push_back(v_y4l_2d);
  rrvars.push_back(v_njet);
  rrvars.push_back(v_njet_diff);
  rrvars.push_back(v_njet2);
  rrvars.push_back(v_cts);
  rrvars.push_back(v_jet1pt);
  rrvars.push_back(v_jet2pt);
  /*
  rrvars.push_back(v_pt4l_incl_mcut);
  rrvars.push_back(v_pt4l_0jet_mcut);
  rrvars.push_back(v_pt4l_1jet_mcut);
  rrvars.push_back(v_m12_mcut);
  rrvars.push_back(v_m34_mcut);
  rrvars.push_back(v_y4l_mcut);
  rrvars.push_back(v_njet_mcut);
  rrvars.push_back(v_njet2_mcut);
  rrvars.push_back(v_cts_mcut);
  rrvars.push_back(v_jet1pt_mcut);
  rrvars.push_back(v_m12_m34_mcut);
  */
  rrvars.push_back(v_m12_m34);
  rrvars.push_back(v_m4l_mwindow);
  //rrvars.push_back(v_m34_m4l);
  rrvars.push_back(v_prod_type);
  rrvars.push_back(v_prod_type_hm);
  rrvars.push_back(v_prod_type_sb);
  rrvars.push_back(v_prod_type_cp);
  //rrvars.push_back(v_m4l_detailed);
  /*
  rrvars.push_back(v_BDT_0jet);
  rrvars.push_back(v_BDT_1jet_low);
  rrvars.push_back(v_BDT_1jet_high);
  rrvars.push_back(v_BDT_2jet_VBF);
  rrvars.push_back(v_BDT_2jet_VH);
  */
  rrvars.push_back(v_mjj);
  rrvars.push_back(v_detajj);
  rrvars.push_back(v_dphijj);
  rrvars.push_back(v_m4l_diffxs);
  rrvars.push_back(v_m4l_diffxs2);
  rrvars.push_back(v_nbjet);
  rrvars.push_back(v_phi);
  rrvars.push_back(v_phi1);
  rrvars.push_back(v_cth1);
  rrvars.push_back(v_cth2);
  rrvars.push_back(v_pt4l_y4l0);
  rrvars.push_back(v_pt4l_y4l1);
  rrvars.push_back(v_pt4l_y4l2);
  rrvars.push_back(v_pt4l_y4l3);
  rrvars.push_back(v_pt4l_jet1pt);
  rrvars.push_back(v_m4lj);
  rrvars.push_back(v_m4ljj);
  rrvars.push_back(v_pt4lj);
  rrvars.push_back(v_pt4ljj);
  rrvars.push_back(v_leptonpt0);
  rrvars.push_back(v_leptonpt1);
  rrvars.push_back(v_leptonpt2);
  rrvars.push_back(v_leptonpt3);
  rrvars.push_back(v_bdt);
  rrvars.push_back(v_jet1pt_jet2pt);
  rrvars.push_back(v_pt4l_pt4lj);
  rrvars.push_back(v_pt4lj_m4lj);
  rrvars.push_back(v_jet1pt_jet1y);
  rrvars.push_back(v_leptoneta0);
  rrvars.push_back(v_leptoneta1);
  rrvars.push_back(v_leptoneta2);
  rrvars.push_back(v_leptoneta3);
  rrvars.push_back(v_leptonphi0);
  rrvars.push_back(v_leptonphi1);
  rrvars.push_back(v_leptonphi2);
  rrvars.push_back(v_leptonphi3);
  for(int i=0;i<3;i++){
    rrvars.push_back(v_leptonpt3_njet[i]);
    rrvars.push_back(v_leptonpt3_mu[i]);
  }
  rrvars.push_back(v_cp_obs1);
  rrvars.push_back(v_cp_obs2);
  rrvars.push_back(v_cp_obs3);
  rrvars.push_back(v_cp_obs4);
  rrvars.push_back(v_cp_obs5);
  rrvars.push_back(v_cp_obs6);
  rrvars.push_back(v_OO1jjtCzz);
  rrvars.push_back(v_OO1jjtCza);
  rrvars.push_back(v_OO1jjcHWtil);
  rrvars.push_back(v_shape_OO1jjtCzz);
  rrvars.push_back(v_shape_OO1jjtCza);
  rrvars.push_back(v_shape_OO1jjcHWtil);
  rrvars.push_back(v_OO14ltCzz);
  rrvars.push_back(v_OO14ltCza);
  rrvars.push_back(v_OO14ltCaa);
  rrvars.push_back(v_OO14lcHWtil);
  rrvars.push_back(v_OO14lcHBtil);
  rrvars.push_back(v_OO14lcHWBtil);
  rrvars.push_back(v_NNVBF);
  RooArgSet vars("vars");
  vars.add(*weight);
  for (auto v: rrvars) vars.add(*v);

  //std::cout<<"now, open the files"<<std::endl;

  RooDataSet* fitData = new RooDataSet("data3lx","data3lx",vars,"weight");

  TFile* dataFile = TFile::Open(Form("%s/flattenedThreeLplusX_%s.root",output.c_str(),year.c_str()),"READ");
  TTree* dataTree = (TTree*)dataFile->Get(treename.c_str());

  float t_mu;
  double t_weight;
  float t_eta,t_pt;
  int t_nInner, t_MCClass, t_event_type, t_prod_type, t_prod_type_HM, t_prod_type_sb, t_prod_type_cp;
  Int_t t_run;
  float t_m12,t_m34,t_m4l,t_pt4l,t_y4l,t_jet1pt,t_jet2pt,t_cts,t_mjj,t_detajj,t_dphijj,t_m4l_constrained_HM,t_cth1,t_cth2,t_phi,t_phi1,t_m4lj,t_m4ljj,t_pt4lj,t_pt4ljj,t_bdt,t_jet1y;
  //float t_BDT0,t_BDT1l,t_BDT1h,t_BDT2F,t_BDT2H;
  float t_cp_obs1,t_cp_obs2,t_cp_obs3,t_cp_obs4,t_cp_obs5,t_cp_obs6,t_OO1jjtCzz,t_OO1jjtCza,t_OO1jjcHWtil,t_OO14ltCzz,t_OO14ltCza,t_OO14ltCaa,t_OO14lcHWtil,t_OO14lcHBtil,t_OO14lcHWBtil,t_NNVBF;
  int t_njet,t_nbjet;
  vector<float> *t_lep_pt=new vector<float>;
  vector<float> *t_lep_eta=new vector<float>;
  vector<float> *t_lep_phi=new vector<float>;
  //bool isMC=false;
  //if(dataTree->FindBranch("MCClass")
  if (!isData)
    dataTree->SetBranchAddress("weight",&t_weight);
  else t_weight = 1;
  dataTree->SetBranchAddress("MCClass",&t_MCClass);
  dataTree->SetBranchAddress("event_type",&t_event_type);
  dataTree->SetBranchAddress("prod_type",&t_prod_type);
  dataTree->SetBranchAddress("prod_type_sb",&t_prod_type_sb);
  dataTree->SetBranchAddress("prod_type_cp",&t_prod_type_cp);
  dataTree->SetBranchAddress("prod_type_HM",&t_prod_type_HM);
  dataTree->SetBranchAddress("run",&t_run);
  dataTree->SetBranchAddress("nInner",&t_nInner);
  dataTree->SetBranchAddress("eta",&t_eta);
  dataTree->SetBranchAddress("pt",&t_pt);
  dataTree->SetBranchAddress("mZ1",&t_m12);
  dataTree->SetBranchAddress("mZ2",&t_m34);
  dataTree->SetBranchAddress("m4l",&t_m4l);
  //dataTree->SetBranchAddress("m4l_constrained",&t_m4lc);
  dataTree->SetBranchAddress("m4l_constrained_HM",&t_m4l_constrained_HM);
  //dataTree->SetBranchAddress("m4l_fsr",&t_m4l);
  dataTree->SetBranchAddress("y4l",&t_y4l);
  dataTree->SetBranchAddress("pt4l",&t_pt4l);
  dataTree->SetBranchAddress("jet1pt",&t_jet1pt);
  dataTree->SetBranchAddress("jet2pt",&t_jet2pt);
  dataTree->SetBranchAddress("jet1y",&t_jet1y);
  dataTree->SetBranchAddress("cts",&t_cts);
  dataTree->SetBranchAddress("cth1",&t_cth1);
  dataTree->SetBranchAddress("cth2",&t_cth2);
  dataTree->SetBranchAddress("phi",&t_phi);
  dataTree->SetBranchAddress("phi1",&t_phi1);
  dataTree->SetBranchAddress("njet",&t_njet);
  dataTree->SetBranchAddress("lepton_pt",&t_lep_pt);
  dataTree->SetBranchAddress("lepton_eta",&t_lep_eta);
  dataTree->SetBranchAddress("lepton_phi",&t_lep_phi);
  /*
  dataTree->SetBranchAddress("BDT_discriminant", &t_BDT0);
  dataTree->SetBranchAddress("BDT_VH_noptHjj_discriminant",&t_BDT2H);
  dataTree->SetBranchAddress("BDT_TwoJet_discriminant",&t_BDT2F);
  dataTree->SetBranchAddress("BDT_1Jet_pt4l_60",&t_BDT1l);
  dataTree->SetBranchAddress("BDT_1Jet_pt4l_60_120",&t_BDT1h);
  */
  dataTree->SetBranchAddress("mjj",&t_mjj);
  dataTree->SetBranchAddress("detajj",&t_detajj);
  dataTree->SetBranchAddress("dphijj",&t_dphijj);
  dataTree->SetBranchAddress("nbjet",&t_nbjet);
  dataTree->SetBranchAddress("m4lj",&t_m4lj);
  dataTree->SetBranchAddress("m4ljj",&t_m4ljj);
  dataTree->SetBranchAddress("pt4lj",&t_pt4lj);
  dataTree->SetBranchAddress("pt4ljj",&t_pt4ljj);
  dataTree->SetBranchAddress("bdt",&t_bdt);
  dataTree->SetBranchAddress("mu",&t_mu);
  dataTree->SetBranchAddress("cp_obs1",&t_cp_obs1);
  dataTree->SetBranchAddress("cp_obs2",&t_cp_obs2);
  dataTree->SetBranchAddress("cp_obs3",&t_cp_obs3);
  dataTree->SetBranchAddress("cp_obs4",&t_cp_obs4);
  dataTree->SetBranchAddress("cp_obs5",&t_cp_obs5);
  dataTree->SetBranchAddress("cp_obs6",&t_cp_obs6);
  dataTree->SetBranchAddress("OO1jjtCzz",&t_OO1jjtCzz);
  dataTree->SetBranchAddress("OO1jjtCza",&t_OO1jjtCza);
  dataTree->SetBranchAddress("OO1jjcHWtil",&t_OO1jjcHWtil);
  dataTree->SetBranchAddress("OO14ltCzz",&t_OO14ltCzz);
  dataTree->SetBranchAddress("OO14ltCza",&t_OO14ltCza);
  dataTree->SetBranchAddress("OO14ltCaa",&t_OO14ltCaa);
  dataTree->SetBranchAddress("OO14lcHWtil",&t_OO14lcHWtil);
  dataTree->SetBranchAddress("OO14lcHBtil",&t_OO14lcHBtil);
  dataTree->SetBranchAddress("OO14lcHWBtil",&t_OO14lcHWBtil);
  dataTree->SetBranchAddress("NNVBF",&t_NNVBF);

  float n_actual_gamma=0;
  float n_actual_light=0;
  float n_actual_heavy=0;
  float n_actual_real=0;
  float n_actual_total=0;
  float ptSortLep[4];
  float ptSortLepEta[4];
  float ptSortLepPhi[4];

  TH1F* truth_gamma_pt=new TH1F("truth_gamma_pt","",3,ptBins);
  TH1F* truth_heavy_pt=new TH1F("truth_heavy_pt","",3,ptBins);
  TH1F* truth_light_pt=new TH1F("truth_light_pt","",3,ptBins);

  float pt4lval, m4lval;

  for (unsigned int i(0);i<dataTree->GetEntries();++i){

    dataTree->GetEntry(i);
    
    if (do2e2e && t_event_type!=1) continue;
    if (do2mu2e && t_event_type!=2) continue;
    
    weight->setVal( t_weight);
    for(int j=0;j<4;j++){
      ptSortLep[j]=t_lep_pt->at(0);
      ptSortLepEta[j]=t_lep_eta->at(0);
      ptSortLepPhi[j]=t_lep_phi->at(0);
    }
    for(int j=1;j<4;j++){
      bool isgt=false;
      for(int k=0;k<j;k++){
	if(t_lep_pt->at(j)>ptSortLep[k]){
	  for(int m=j;m>=k+1;m--){
	    ptSortLep[m]=ptSortLep[m-1];
	    ptSortLepEta[m]=ptSortLepEta[m-1];
	    ptSortLepPhi[m]=ptSortLepPhi[m-1];
	  }
	  ptSortLep[k]=t_lep_pt->at(j);
	  ptSortLepEta[k]=t_lep_eta->at(j);
	  ptSortLepPhi[k]=t_lep_phi->at(j);
	  isgt=true;
	  break;
	}
      }
      if(!isgt){
	ptSortLep[j]=t_lep_pt->at(j);
	ptSortLepEta[j]=t_lep_eta->at(j);
	ptSortLepPhi[j]=t_lep_phi->at(j);
      }
    }
    /*
    cout<<"entry: "<<i<<" ";
    for(int j=0;j<4;j++) cout<<"lep v1,"<<j<<"="<<t_lep_pt->at(j)<<". "<<t_lep_eta->at(j)<<" ";
    for(int j=0;j<4;j++) cout<<"lep v2,"<<j<<"="<<ptSortLep[j]<<", "<<ptSortLepEta[j]<<" ";
    cout<<endl;
    */
    for (auto v: rrvars){
      if (!strcmp(v->GetName(),"nInner")) v->setVal(t_nInner);
      else if (!strcmp(v->GetName(),"eta")) v->setVal(t_eta);
      else if (!strcmp(v->GetName(),"pt")) v->setVal(t_pt);
      else if (!strcmp(v->GetName(),"pt4l_incl")) v->setVal(t_pt4l);
      else if (!strcmp(v->GetName(),"pt4l_0jet")){if(t_njet==0) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);} //9999 means that it goes into the acceptance bin
      else if (!strcmp(v->GetName(),"pt4l_1jet")){if(t_njet==1) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);} //needs to be handled separately from mass cut
      else if (!strcmp(v->GetName(),"pt4l_2jet")){if(t_njet==2) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);}
      else if (!strcmp(v->GetName(),"pt4l_3jet")){if(t_njet>2) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);}
      else if (!strcmp(v->GetName(),"pt4l_y4l0")){if(fabs(t_y4l)<0.5) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);} //9999 means that it goes into the acceptance bin
      else if (!strcmp(v->GetName(),"pt4l_y4l1")){if(fabs(t_y4l)>0.5 && fabs(t_y4l)<1) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);} //needs to be handled separately from mass cut
      else if (!strcmp(v->GetName(),"pt4l_y4l2")){if(fabs(t_y4l)>1 && fabs(t_y4l)<1.5) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);}
      else if (!strcmp(v->GetName(),"pt4l_y4l3")){if(fabs(t_y4l)>1.5 && fabs(t_y4l)<2.5) pt4lval=t_pt4l; else pt4lval=9999; v->setVal(pt4lval);}
      else if (!strcmp(v->GetName(),"m4l_smoothed")) v->setVal(t_m4l);
      else if (!strcmp(v->GetName(),"m4l")){ v->setVal(t_m4l);}
      else if (!strcmp(v->GetName(),"m4l_constrained_HM")){v->setVal(t_m4l_constrained_HM);}
      else if (!strcmp(v->GetName(),"m4l_0jet")){if(t_njet==0) m4lval=t_m4l; else m4lval=999; v->setVal(m4lval);} //999 means that it goes into the acceptance bin
      else if (!strcmp(v->GetName(),"m4l_1jet")){if(t_njet==1) m4lval=t_m4l; else m4lval=999; v->setVal(m4lval);} //needs to be handled separately from mass cut
      else if (!strcmp(v->GetName(),"m4l_2jet")){if(t_njet>1) m4lval=t_m4l; else m4lval=999; v->setVal(m4lval);}
      else if (!strcmp(v->GetName(),"m4l_mwindow")) v->setVal(t_m4l);
      else if (!strcmp(v->GetName(),"m4l_diffxs")) v->setVal(t_m4l);
      else if (!strcmp(v->GetName(),"m4l_diffxs2")) v->setVal(t_m4l);
      else if (!strcmp(v->GetName(),"y4l")) v->setVal(t_y4l);
      else if (!strcmp(v->GetName(),"y4l_2d")) v->setVal(t_y4l);
      else if (!strcmp(v->GetName(),"m12")) v->setVal(t_m12);
      else if (!strcmp(v->GetName(),"m34")) v->setVal(t_m34);
      else if (!strcmp(v->GetName(),"cts")) v->setVal(t_cts);
      else if (!strcmp(v->GetName(),"cth1")) v->setVal(t_cth1);
      else if (!strcmp(v->GetName(),"cth2")) v->setVal(t_cth2);
      else if (!strcmp(v->GetName(),"phi")) v->setVal(t_phi);
      else if (!strcmp(v->GetName(),"phi1")) v->setVal(t_phi1);
      else if (!strcmp(v->GetName(),"jet1pt"))v->setVal(t_jet1pt);
      else if (!strcmp(v->GetName(),"jet2pt"))v->setVal(t_jet2pt);
      else if (!strcmp(v->GetName(),"njet")){if(t_njet<4) v->setVal(t_njet); else v->setVal(3);}
      else if (!strcmp(v->GetName(),"njet_diff")){if(t_njet<4) v->setVal(t_njet); else v->setVal(3);}
      else if (!strcmp(v->GetName(),"njet2")) v->setVal(t_njet);
      else if (!strcmp(v->GetName(),"prod_type")) v->setVal(t_prod_type);
      else if (!strcmp(v->GetName(),"prod_type_sb")) v->setVal(t_prod_type_sb);
      else if (!strcmp(v->GetName(),"prod_type_cp")) v->setVal(t_prod_type_cp);
      else if (!strcmp(v->GetName(),"prod_type_hm")) v->setVal(t_prod_type_HM);
      else if (!strcmp(v->GetName(),"m12_m34")){ //bit tricky here
	if(t_m34>32){
	  if(t_m12<74) v->setVal(1.5); //bin 2
	  else if(t_m12>74) v->setVal(2.5); //bin 3
	  else v->setVal(5.5); //overflow
	}
	else if(t_m34<32){
	  if(t_m12<82) v->setVal(0.5); //bin 1
	  else if(t_m12>82){
	    if(t_m34<24) v->setVal(4.5); //bin 5
	    else v->setVal(3.5); //bin 4
	  }
	  else v->setVal(5.5); //overflow
	}
	else v->setVal(5.5); //overflow
      }
      //else if(!strcmp(v->GetName(),"m4l_detailed")){if(t_m4lc>118 && t_m4lc<129) v->setVal(1); else v->setVal(0);}
      /*
      else if(!strcmp(v->GetName(),"BDT_0jet")){ if(t_m4lc>118 && t_m4lc<129 && t_prod_type==0) v->setVal(t_BDT0); else v->setVal(-999);}
      else if(!strcmp(v->GetName(),"BDT_1jet_low")){ if(t_m4lc>118 && t_m4lc<129 && t_prod_type==1 && t_pt4l<60) v->setVal(t_BDT1l); else v->setVal(-999);}
      else if(!strcmp(v->GetName(),"BDT_1jet_high")){ if(t_m4lc>118 && t_m4lc<129 && t_prod_type==1 && t_pt4l>60 && t_pt4l<120) v->setVal(t_BDT1h); else v->setVal(-999);}
      else if(!strcmp(v->GetName(),"BDT_2jet_VBF")){ if(t_m4lc>118 && t_m4lc<129 && t_prod_type==3) v->setVal(t_BDT2F); else v->setVal(-999);}
      else if(!strcmp(v->GetName(),"BDT_2jet_VH")){ if(t_m4lc>118 && t_m4lc<129 && t_prod_type==2) v->setVal(t_BDT2H); else v->setVal(-999);}
      */
      else if(!strcmp(v->GetName(),"detajj")) v->setVal(t_detajj);
      else if(!strcmp(v->GetName(),"mjj")){ //a bit tricky here
	if(t_mjj<0) v->setVal(0.5); //means 0 or 1 jets in the event
	else if(t_mjj<120) v->setVal(1.5); //0-120 bin
	else if(t_mjj<450) v->setVal(3); //120-450 bin
	else if(t_mjj<3000) v->setVal(5.5); //450-3000 bin
      }
      else if(!strcmp(v->GetName(),"dphijj")) v->setVal(t_dphijj);
      else if(!strcmp(v->GetName(),"nbjet")){v->setVal(t_nbjet);}
      else if(!strcmp(v->GetName(),"pt4l_jet1pt")){ //bit tricky
	if(t_njet==0) v->setVal(0.5);
	else{
	  if(t_jet1pt<60){
	    if(t_pt4l<80) v->setVal(1.5);
	    else if(t_pt4l<350) v->setVal(2.5);
	    else v->setVal(7.5); //overflow
	  }
	  else if(t_jet1pt<120){
	    if(t_pt4l<120) v->setVal(3.5);
	    else if(t_pt4l<350) v->setVal(4.5);
	    else v->setVal(7.5); //overflow
	  }
	  else if(t_jet1pt<350){
            if(t_pt4l<120) v->setVal(5.5);
            else if(t_pt4l<350) v->setVal(6.5);
            else v->setVal(7.5); //overflow
          }
	  else v->setVal(7.5);
	}
      }
      else if(!strcmp(v->GetName(),"m4lj")) v->setVal(t_m4lj);
      else if(!strcmp(v->GetName(),"m4ljj")) v->setVal(t_m4ljj);
      else if(!strcmp(v->GetName(),"pt4lj")) v->setVal(t_pt4lj);
      else if(!strcmp(v->GetName(),"pt4ljj")) v->setVal(t_pt4ljj);
      else if(!strcmp(v->GetName(),"lepton_pt0")) v->setVal(ptSortLep[0]);
      else if(!strcmp(v->GetName(),"lepton_pt1")) v->setVal(ptSortLep[1]);
      else if(!strcmp(v->GetName(),"lepton_pt2")) v->setVal(ptSortLep[2]);
      else if(!strcmp(v->GetName(),"lepton_pt3")) v->setVal(ptSortLep[3]);
      else if(strstr(v->GetName(),"lepton_pt3_njet")){
	if(strstr(v->GetName(),"0")){
	  if(t_njet==0) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
	else if(strstr(v->GetName(),"1")){
	  if(t_njet==1) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
	else{
	  if(t_njet>=2) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
      }
      else if(strstr(v->GetName(),"lepton_pt3_mu")){
        if(strstr(v->GetName(),"0")){
	  if(t_mu<20) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
        else if(strstr(v->GetName(),"1")){
	  if(t_mu>=20 && t_mu<40) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
        else{
	  if(t_mu>=40) v->setVal(ptSortLep[3]);
	  else v->setVal(2); //underflow
	}
      } 
      else if(!strcmp(v->GetName(),"lepton_eta0")) v->setVal(ptSortLepEta[0]);
      else if(!strcmp(v->GetName(),"lepton_eta1")) v->setVal(ptSortLepEta[1]);
      else if(!strcmp(v->GetName(),"lepton_eta2")) v->setVal(ptSortLepEta[2]);
      else if(!strcmp(v->GetName(),"lepton_eta3")) v->setVal(ptSortLepEta[3]);
      else if(!strcmp(v->GetName(),"lepton_phi0")) v->setVal(ptSortLepPhi[0]);
      else if(!strcmp(v->GetName(),"lepton_phi1")) v->setVal(ptSortLepPhi[1]);
      else if(!strcmp(v->GetName(),"lepton_phi2")) v->setVal(ptSortLepPhi[2]);
      else if(!strcmp(v->GetName(),"lepton_phi3")) v->setVal(ptSortLepPhi[3]);
      else if(!strcmp(v->GetName(),"bdt")) v->setVal(t_bdt);
      else if(!strcmp(v->GetName(),"jet1pt_jet2pt")){ //tricky binning
	if(t_njet==0) v->setVal(0.5);
	else if(t_njet==1){
	  if(t_jet1pt<60) v->setVal(1.5);
	  else if(t_jet1pt<350) v->setVal(2.5);
	  else v->setVal(6.5); //overflow
	}
	else{
	  if(t_jet1pt<60) v->setVal(3.5);
	  else if(t_jet1pt<350){
	    if(t_jet2pt<60) v->setVal(4.5);
	    else v->setVal(5.5);
	  }
	  else v->setVal(6.5); //overflow
	}
      }
      else if(!strcmp(v->GetName(),"pt4l_pt4lj")){ //tricky binning
	if(t_njet==0) v->setVal(0.5);
	else{
	  if(t_pt4l<120){
	    if(t_pt4lj<60) v->setVal(1.5);
	    else if(t_pt4lj<350) v->setVal(3.5);
	    else v->setVal(5.5); //overflow
	  }
	  else if(t_pt4l<350){
	    if(t_pt4lj<60) v->setVal(2.5);
            else if(t_pt4lj<350) v->setVal(4.5);
            else v->setVal(5.5); //overflow
          }
	  else v->setVal(5.5); //overflow
	}
      }
      else if(!strcmp(v->GetName(),"pt4lj_m4lj")){ //tricky binning
	if(t_njet==0) v->setVal(0.5);
	else{
	  if(t_m4lj>120 && t_m4lj<220){
	    if(t_pt4lj<350) v->setVal(1.5);
	    else v->setVal(5.5); //overflow
	  }
	  else if(t_m4lj<350){
	    if(t_pt4lj<60) v->setVal(2.5);
	    else if(t_pt4lj<350) v->setVal(3.5);
	    else v->setVal(5.5); //overflow
	  }
	  else if(t_m4lj<2000){
	    if(t_pt4lj<350) v->setVal(4.5);
	    else v->setVal(5.5); //overflow
	  }
	  else v->setVal(5.5); //overflow
	}
      }
      else if(!strcmp(v->GetName(),"jet1pt_jet1y")){ //binning
	if(t_njet==0) v->setVal(0.5);
	else{
	  if(t_jet1pt<120){
	    if(t_jet1y<0.8) v->setVal(1.5);
	    else if(t_jet1y<1.7) v->setVal(2.5);
	    else v->setVal(3.5);
	  }
	  else if(t_jet1pt<350){
	    if(t_jet1y<1.7) v->setVal(4.5);
            else v->setVal(5.5);
          }
	  else v->setVal(6.5); //overflow
	}
      }
      else if(!strcmp(v->GetName(),"cp_obs1")) v->setVal(t_cp_obs1);
      else if(!strcmp(v->GetName(),"cp_obs2")) v->setVal(t_cp_obs2);
      else if(!strcmp(v->GetName(),"cp_obs3")) v->setVal(t_cp_obs3);
      else if(!strcmp(v->GetName(),"cp_obs4")) v->setVal(t_cp_obs4);
      else if(!strcmp(v->GetName(),"cp_obs5")) v->setVal(t_cp_obs5);
      else if(!strcmp(v->GetName(),"cp_obs6")) v->setVal(t_cp_obs6);
      else if(!strcmp(v->GetName(),"shape_OO1jjtCzz")){
	if(t_OO1jjtCzz<-50) v->setVal(0.5);
	else if(t_OO1jjtCzz<-10) v->setVal(1.5);
	else if(t_OO1jjtCzz<-0.75) v->setVal(2.5);
	else if(t_OO1jjtCzz<0) v->setVal(3.5);
	else if(t_OO1jjtCzz<0.75) v->setVal(4.5);
	else if(t_OO1jjtCzz<10) v->setVal(5.5);
      }
      else if(!strcmp(v->GetName(),"OO1jjtCzz")){
        if(t_OO1jjtCzz<-10) v->setVal(0.5);
        else if(t_OO1jjtCzz<-0.75) v->setVal(1.5);
        else if(t_OO1jjtCzz<0) v->setVal(2.5);
        else if(t_OO1jjtCzz<0.75) v->setVal(3.5);
        else if(t_OO1jjtCzz<10) v->setVal(4.5);
      }
      else if(!strcmp(v->GetName(),"shape_OO1jjtCza")){
	if(t_OO1jjtCza<-50) v->setVal(0.5);
        else if(t_OO1jjtCza<-3) v->setVal(1.5);
        else if(t_OO1jjtCza<-0.35) v->setVal(2.5);
        else if(t_OO1jjtCza<0) v->setVal(3.5);
        else if(t_OO1jjtCza<0.35) v->setVal(4.5);
        else if(t_OO1jjtCza<3) v->setVal(5.5);
      }
      else if(!strcmp(v->GetName(),"OO1jjtCza")){
        if(t_OO1jjtCzz<-3) v->setVal(0.5);
        else if(t_OO1jjtCzz<-0.35) v->setVal(1.5);
        else if(t_OO1jjtCzz<0) v->setVal(2.5);
        else if(t_OO1jjtCzz<0.35) v->setVal(3.5);
        else if(t_OO1jjtCzz<3) v->setVal(4.5);
      }
      else if(!strcmp(v->GetName(),"shape_OO1jjcHWtil")){
	if(t_OO1jjcHWtil<-50) v->setVal(0.5);
        else if(t_OO1jjcHWtil<-5) v->setVal(1.5);
        else if(t_OO1jjcHWtil<-0.4) v->setVal(2.5);
        else if(t_OO1jjcHWtil<0) v->setVal(3.5);
        else if(t_OO1jjcHWtil<0.4) v->setVal(4.5);
        else if(t_OO1jjcHWtil<5) v->setVal(5.5);
      }
      else if(!strcmp(v->GetName(),"OO1jjcHWtil")){
	if(t_OO1jjtCzz<-5) v->setVal(0.5);
        else if(t_OO1jjtCzz<-0.4) v->setVal(1.5);
        else if(t_OO1jjtCzz<0) v->setVal(2.5);
        else if(t_OO1jjtCzz<0.4) v->setVal(3.5);
        else if(t_OO1jjtCzz<5) v->setVal(4.5);
      }
      else if(!strcmp(v->GetName(),"OO14ltCzz")){
        if(t_OO14ltCzz<-1) v->setVal(-.99);
	else if(t_OO14ltCzz>1) v->setVal(.99);
	else v->setVal(t_OO14ltCzz);
      }
      else if(!strcmp(v->GetName(),"OO14ltCza")){
        if(t_OO14ltCza<-2) v->setVal(-1.99);
        else if(t_OO14ltCza>2) v->setVal(1.99);
        else v->setVal(t_OO14ltCza);
      }
      else if(!strcmp(v->GetName(),"OO14ltCaa")){
        if(t_OO14ltCaa<-10) v->setVal(-9.99);
        else if(t_OO14ltCaa>10) v->setVal(9.99);
        else v->setVal(t_OO14ltCaa);
      }
      else if(!strcmp(v->GetName(),"OO14lcHWtil")){
        if(t_OO14lcHWtil<-10) v->setVal(-9.99);
        else if(t_OO14lcHWtil>10) v->setVal(9.99);
        else v->setVal(t_OO14lcHWtil);
      }
      else if(!strcmp(v->GetName(),"OO14lcHBtil")){
        if(t_OO14lcHBtil<-20) v->setVal(-19.99);
        else if(t_OO14lcHBtil>20) v->setVal(19.99);
        else v->setVal(t_OO14lcHBtil);
      }
      else if(!strcmp(v->GetName(),"OO14lcHWBtil")){
        if(t_OO14lcHWBtil<-15) v->setVal(-14.99);
        else if(t_OO14lcHWBtil>15) v->setVal(14.99);
        else v->setVal(t_OO14lcHWBtil);
      }
      else if(!strcmp(v->GetName(),"NNVBF")) v->setVal(t_NNVBF);
      //else if (!strcmp(v->GetName(),"prod_type_hm")){
      //if(t_mjj>400 && t_detajj>3.3) v->setVal(0);
      //else v->setVal(1);
      //}
      /*     
      if(t_m4l>115 && t_m4l<130){
	if (!strcmp(v->GetName(),"pt4l_incl_mcut")) v->setVal(t_pt4l);
	else if (!strcmp(v->GetName(),"pt4l_0jet_mcut")){if(t_njet==0) pt4lval=t_pt4l; else pt4lval=999; v->setVal(pt4lval);}
	else if (!strcmp(v->GetName(),"pt4l_1jet_mcut")){if(t_njet>0) pt4lval=t_pt4l; else pt4lval=999; v->setVal(pt4lval);}
	else if (!strcmp(v->GetName(),"y4l_mcut")) v->setVal(t_y4l);
	else if (!strcmp(v->GetName(),"m12_mcut")) v->setVal(t_m12);
	else if (!strcmp(v->GetName(),"m34_mcut")) v->setVal(t_m34);
	else if (!strcmp(v->GetName(),"m34_m4l")) v->setVal(t_m34);
	else if (!strcmp(v->GetName(),"cts_mcut")) v->setVal(t_cts);
	else if (!strcmp(v->GetName(),"jet1pt_mcut")){if(t_njet>0) v->setVal(t_jet1pt); else v->setVal(999);}
	else if (!strcmp(v->GetName(),"njet_mcut")) v->setVal(t_njet);
	else if (!strcmp(v->GetName(),"njet2_mcut")) v->setVal(t_njet);
	else if (!strcmp(v->GetName(),"m12_m34_mcut")){ //bit tricky here
	  //std::cout<<"m12: "<<t_m12<<"; m34: "<<t_m34<<std::endl;
	  if(t_m34>32 && t_m34<65){
	    if(t_m12<74 && t_m12>50) v->setVal(1.5); //bin 2
	    else if(t_m12>74 && t_m12<106) v->setVal(2.5); //bin 3
	    else v->setVal(5.5); //overflow
	  }
	  else if(t_m34<32 && t_m34>12){
	    if(t_m12<82 && t_m12>50) v->setVal(0.5); //bin 1
	    else if(t_m12>82 && t_m12<106){
	      if(t_m34<24) v->setVal(4.5); //bin 5
	      else v->setVal(3.5); //bin 4
	    }
	    else v->setVal(5.5); //overflow
	  }
	  else v->setVal(5.5); //overflow
	}
      }
      else{
	if (!strcmp(v->GetName(),"pt4l_incl_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"pt4l_0jet_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"pt4l_1jet_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"y4l_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"m12_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"m34_mcut")) v->setVal(-1);
	else if (!strcmp(v->GetName(),"m34_m4l")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"cts_mcut")) v->setVal(-1);
        else if (!strcmp(v->GetName(),"jet1pt_mcut")) v->setVal(-1);
	else if (!strcmp(v->GetName(),"njet_mcut")) v->setVal(-1);
	else if (!strcmp(v->GetName(),"njet2_mcut")) v->setVal(-1);
	else if (!strcmp(v->GetName(),"m12_m34_mcut")) v->setVal(-1);
      }
      */
    }

    fitData->add(vars, t_weight);

    if (t_MCClass==3 || t_MCClass==4){ n_actual_gamma+=t_weight; truth_gamma_pt->Fill(t_pt,t_weight);}
    if (t_MCClass==5){ n_actual_light+=t_weight; truth_light_pt->Fill(t_pt,t_weight);}
    if (t_MCClass==2){ n_actual_heavy+=t_weight; truth_heavy_pt->Fill(t_pt,t_weight);}
    n_actual_total+=t_weight;
  }

  v_prod_type_cp->Print();

  std::cout<<"ngamma: "<<n_actual_gamma<<"; nlight: "<<n_actual_light<<"; nheavy: "<<n_actual_heavy<<"; ntot: "<<n_actual_total<<std::endl;

  RooAbsPdf* pdf = wsp->pdf("addpdf");

  // add constraints for real electron contributions (q or ZZ) -- NOT NEEDED ANYMORE...
  if(isData) {
    n_actual_heavy = 0.29*LUMI;
    n_actual_real = 0.26*LUMI;
  }
  //RooGaussian heavyConstraint("heavyConstraint","heavyConstraint",*n_heavy,RooConst(.04*n_actual_total),RooConst(.04*n_actual_total*.25));
  RooGaussian heavyConstraint("heavyConstraint","heavyConstraint",*n_heavy,RooConst(n_actual_heavy),RooConst(n_actual_heavy*.25));
  RooGaussian realConstraint("realConstraint","realConstraint",*n_real,RooConst(n_actual_real),RooConst(n_actual_real*0.1));

  fitData->Print();

  //optimal seed:
  n_gamma->setRange(0,2000);
  n_light->setRange(0,15000);
  n_heavy->setRange(0,1.5*n_actual_heavy); //1.5*n_actual_total
  //n_heavy->setRange(0.75*n_actual_heavy,1.25*n_actual_heavy);
  n_real->setRange(0.95*n_actual_real,1.05*n_actual_real);
    
  n_gamma->setVal(n_actual_total*.05);
  n_light->setVal(n_actual_total*.85);
  n_heavy->setVal(n_actual_heavy);
  n_real->setVal(n_actual_real);

  nVars->Print("v");
  cout<<"total = "<<n_actual_total<<endl;

  RooFitResult* r=NULL;
  if (nVars->getSize()==2)
    r = pdf->fitTo(*fitData,Extended(),SumW2Error(((isData)? false : true)),Save());
  else //fitting more pdfs, must apply the constraints on q, e
    r = pdf->fitTo(*fitData,Extended(),SumW2Error(((isData)? false : true)),Save(),ExternalConstraints(RooArgSet(heavyConstraint,realConstraint)));
  
  for (unsigned int i(0);i<FitVars.size();++i) rrvars[i]->Print();
  nVars->Print("v");

  
  //Plotting discriminating variabnles (nInner, etc.) distributions:
  //
  SetAtlasStyle();
  TCanvas* c2 = new TCanvas("c2","fitPlot",600,600);
  double padFrac=0.3;
  TPad *uppPad=new TPad("uppPad","",0,padFrac,1,1);
  TPad *lowPad=new TPad("lowPad","",0,0,1,padFrac);
  uppPad->SetBottomMargin(0.03);
  lowPad->SetTopMargin(0.01);       lowPad->SetBottomMargin(0.3);
  uppPad->Draw();
  lowPad->SetFillColor(0);
  lowPad->Draw();
  uppPad->cd();
  TCanvas* c = new TCanvas("c","fitPlot",600,600);
  TLegend *leg;

  TLatex t; t.SetNDC(); t.SetTextColor(1); t.SetTextSize(0.038);
  int i=0;
  
    c->Clear();
    RooRealVar* v = rrvars[i];

    TFile *f =0;
    TH1F* h_ZZ_all=0;
    /*
    /// ---- Get the ZZ+q component.. only for show
    f = TFile::Open("flattenedThreeLplusX_mc.root","READ");
    f->cd();
    //////exclude f+gamma contributions from ZZ:
    TString isZZ="(run==343212 || run==343213 || run==343232 || run==361603 || run==342556)";
    // to get q, e, and ZZ contributions to f,gamma from MC:
    TH1F* h_q = new TH1F("h_q","h_q",4,-0.5,3.5);
    TH1F* h_ZZf = new TH1F("h_ZZf","h_ZZf",4,-0.5,3.5);  
    TH1F* h_ZZgamma = new TH1F("h_ZZgamma","h_ZZgamma",4,-0.5,3.5);

    TTree *t_mc = (TTree*) f->Get("tree_relaxee");
    t_mc->Draw(Form("%s>>h_q",rrvars[i]->GetName()),"weight*(MCClass==2)"); 
    t_mc->Draw(Form("%s>>h_ZZf",rrvars[i]->GetName()),Form("weight*((MCClass==5 || MCClass==7) && %s)",isZZ.Data()));
    t_mc->Draw(Form("%s>>h_ZZgamma",rrvars[i]->GetName()),Form("weight*((MCClass==3 || MCClass==4) && %s)",isZZ.Data()));

    c->Clear();
    
    h_ZZ_all = (TH1F*) h_ZZf->Clone();
    h_ZZ_all->Add(h_ZZgamma);
    h_ZZ_all->Add(h_q);
    h_ZZ_all->SetLineColor(kAzure+1);
    h_ZZ_all->SetLineWidth(1);
    h_ZZ_all->SetFillColor(kAzure+1);
    h_ZZ_all->SetFillStyle(3004);
    */
    
    //Plotting for show
    RooPlot* frame = v->frame("");//Title(v->GetName()));
    //fitData->plotOn(frame,DataError(RooAbsData::SumW2));
    //fitData->plotOn(frame,DataError(RooAbsData::SumW2));
    //for (int ii=1; ii<=frame->GetXaxis()->GetNbins(); ii++) frame->GetXaxis()->SetBinLabel(ii,Form("%i",ii-1));
    frame->GetXaxis()->SetNdivisions(4);
    fitData->plotOn(frame);
    pdf->plotOn(frame,DrawOption("F"),FillColor(kOrange),MoveToBack(),Components(*wsp->pdf(Form("histpdf_%s_gamma",v->GetName()))));
    pdf->plotOn(frame,LineColor(kGreen+3),LineStyle(kDashed),Components(*wsp->pdf(Form("histpdf_%s_light",v->GetName()))));
    pdf->plotOn(frame,LineColor(kRed+1));
    fitData->plotOn(frame);
    //    frame->addTH1(h_ZZ_all,"hist");
    
    frame->GetYaxis()->SetTitle("Events");
    frame->GetXaxis()->SetTitle(FitVars[i].label);
    frame->GetYaxis()->SetRangeUser(0,frame->GetMaximum()*1.5);

    c->cd();	
    frame->Draw();     ///<<<<<<<<<<<<
  
    leg = makeLeg(0.65,0.65,0.98,0.92);
    TH1F *hdata = new TH1F(); hdata->SetDrawOption("e");
    TH1F *hfit = new TH1F(); hfit->SetDrawOption("hist"); hfit->SetLineColor(kRed+1);
    TH1F *hfit_f = new TH1F(); hfit_f->SetDrawOption("hist"); hfit_f->SetLineColor(kGreen+3);hfit_f->SetLineStyle(kDashed);hfit_f->SetLineWidth(3);
    TH1F *hfit_g = new TH1F(); hfit_g->SetDrawOption("hist"); hfit_g->SetLineColor(kOrange);    //TH1F *hfit_q = new TH1F(); hfit_q->SetDrawOption("hist"); hfit_q->SetLineColor(kAzure+1);

    leg->AddEntry(hdata,"Data","lp");
    leg->AddEntry(hfit,"Fit","l");
    leg->AddEntry(hfit_f,"f","l");
    leg->AddEntry(hfit_g,"#gamma","f");
    //    leg->AddEntry(h_ZZ_all,"ZZ","lf");
    leg->Draw();

    c->GetPad(0)->RedrawAxis();

    if(!do2e2e && ! do2mu2e) allLabels(LUMI, "ll + ee CR" );
    else if(do2e2e) allLabels(LUMI,"ee+ee");
    else if(do2mu2e) allLabels(LUMI,"mumu+ee");
    c->SetLogy(0);
    c->Print(Form("%s/plots/Fit_llee_%s_data%s_%s.pdf",output.c_str(),v->GetName(),finalState.Data(),year.c_str()));
    c->Print(Form("%s/plots/Fit_llee_%s_data%s_%s.eps",output.c_str(),v->GetName(),finalState.Data(),year.c_str()));
    c2->cd();
    uppPad->cd();
    frame->GetYaxis()->SetRangeUser(0.8,frame->GetMaximum()*10);
    frame->Draw();

    uppPad->cd();
    uppPad->SetLogy(1);
    leg->Draw();
    uppPad->RedrawAxis();
    if(!do2e2e && ! do2mu2e) allLabels(LUMI, "ll + ee CR" );
    else if(do2e2e) allLabels(LUMI,"ee+ee");
    else if(do2mu2e) allLabels(LUMI,"mumu+ee");
    frame->GetXaxis()->SetLabelSize(0);
    lowPad->cd();
    RooPlot* frame2 = v->frame(Title(""));
    RooHist *hpull = frame->pullHist();
    frame2->addPlotable((RooPlotable*)hpull,"P");
    frame2->SetYTitle("(Data-Fit)/Err");
    frame2->SetXTitle(FitVars[i].label);
    if (((TString)v->GetName())=="nInner") frame2->GetXaxis()->SetNdivisions(FitVars[i].bins);
    frame2->GetYaxis()->SetNdivisions(505);
    frame2->GetYaxis()->SetRangeUser(-1.8,1.8);
    float sf=2.4;
    frame2->GetXaxis()->SetTitleSize(frame->GetYaxis()->GetTitleSize()*sf);
    frame2->GetXaxis()->SetTitleOffset(0.9);
    frame2->GetYaxis()->SetTitleSize(frame->GetYaxis()->GetTitleSize()*sf);
    frame2->GetYaxis()->SetTitleOffset(0.5);
    frame2->GetYaxis()->SetLabelSize(frame->GetYaxis()->GetLabelSize()*sf);
    frame2->GetXaxis()->SetLabelSize(frame->GetYaxis()->GetTitleSize()*sf);
    frame2->Draw();
    TLine lin(frame2->GetXaxis()->GetXmin(),0,frame2->GetXaxis()->GetXmax(),0);
    lin.SetLineColor(kBlack);
    lin.SetLineStyle(kDashed);
    lin.Draw();
    c2->Print(Form("%s/plots/Fit_llee_%s_data_LOG%s_%s.pdf",output.c_str(),v->GetName(),finalState.Data(),year.c_str()));
    c2->Print(Form("%s/plots/Fit_llee_%s_data_LOG%s_%s.eps",output.c_str(),v->GetName(),finalState.Data(),year.c_str()));
    if (h_ZZ_all) delete h_ZZ_all;
    if (f) f->Close();
    
    infile->cd();
    // }
  if (r) {
    std::cout << "FIT RESULT ========================== " << std::endl;
    r->Print("v");
  }

  //-------sPlot stuff
  //
  //only yields should be non-constant
  pdf->getParameters(fitData)->setAttribAll("Constant",true);
  nVars->setAttribAll("Constant",false);

  RooStats::SPlot* sData = new RooStats::SPlot("sData","SPlot",*fitData, pdf, *nVars);

  std::cout << "@@@ @@@ " << std::endl;
  std::cout << "@@@ @@@ " << std::endl;
  std::cout<<"\nsanity checks on sData:" <<std::endl;
  std::cout<<"Yield of gamma = "<<n_gamma->getVal()<<", from sWeights is "<<sData->GetYieldFromSWeight("n_gamma")<<std::endl;
  std::cout<<"Yield of light = "<<n_light->getVal()<<", from sWeights is "<<sData->GetYieldFromSWeight("n_light")<<std::endl;
  std::cout<<"Yield of heavy = "<<n_heavy->getVal()<<", from sWeights is "<<sData->GetYieldFromSWeight("n_heavy")<<std::endl;
  std::cout<<"done sanity checks\n\n";
  std::cout << "@@@ @@@ " << std::endl;
  std::cout << "@@@ @@@ " << std::endl;
  
  
  //Plotting eta/pt distributions:
  RooDataSet* data_gamma = new RooDataSet(fitData->GetName(),fitData->GetTitle(),fitData,*fitData->get(),0,"n_gamma_sw");
  RooDataSet* data_light = new RooDataSet(fitData->GetName(),fitData->GetTitle(),fitData,*fitData->get(),0,"n_light_sw");
  //RooKeysPdf *keys_m4l_gamma, *keys_m4l_light;
  //TH1 *hist_m4l_gamma,*hist_m4l_light;
  //TH1 *hist_m4l_RAW_gamma,*hist_m4l_RAW_light;

  //this is where output histograms are saved
  std::cout << "Saving output..." << std::endl;
//  TCanvas* c1;
//  c1 = new TCanvas("c1", "c1",600,600);
//
//  printToPDF(hist_m4l_RAW_gamma,hist_m4l_gamma,1);
//  allLabels(0,"gamma");
//  c1->Print(Form("shapesResults/%s.pdf",tag_gamma.Data()));
//  c1->SetLogy();
//  c1->Print(Form("shapesResults/%s_log.pdf",tag_gamma.Data()));
//  c1->Print(Form("shapesResults/%s.eps",tag_gamma.Data()));
//
//
//  c1->Clear();
//  printToPDF(hist_m4l_RAW_light,hist_m4l_light,1);
//  allLabels(0,"light");
//  c1->Print(Form("shapesResults/%s.pdf",tag_light.Data()));
//  c1->SetLogy();
//  c1->Print(Form("shapesResults/%s_log.pdf",tag_light.Data()));
//  c1->Print(Form("shapesResults/%s.eps",tag_light.Data()));

      
    //
  if(label.Contains("/")){
    int pos=label.Index("/");
    label.Replace(0,pos+1,"");
    cout<<label<<endl;
  }
  TFile* fileout = TFile::Open(Form("%s/postfit_kinematics_%s%s.root",output.c_str(),label.Data(),finalState.Data()),"RECREATE");

  vector<TH1F*> hist_gamma_vec;
  vector<TH1F*> hist_light_vec;
  for(auto v: rrvars){
    //only for shape variables, of course
    if(!strcmp(v->GetName(),"nInner")) continue;
    if(!strcmp(v->GetName(),"m4l_smoothed")) continue;
    if(!strcmp(v->GetName(),"m4l_smoothed2")) continue;
    else if (!strcmp(v->GetName(),"rTRT")) continue;
    else if (!strcmp(v->GetName(),"f1")) continue;
    else if (!strcmp(v->GetName(),"eProbHT")) continue;
       
    TH1F* hist_gamma = (TH1F*)data_gamma->createHistogram("gamma",*v);
    hist_gamma->SetMarkerColor(kOrange);
    hist_gamma->SetLineColor(kOrange);
    //hist_gamma->SetMarkerStyle(21);
    hist_gamma->SetName(Form("gamma_%s",v->GetName()));
    hist_gamma_vec.push_back(hist_gamma);

    TH1F* hist_light = (TH1F*)data_light->createHistogram("light",*v);
    hist_light->SetMarkerColor(8);
    hist_light->SetLineColor(8);
    //hist_light->SetMarkerStyle(21);
    hist_light->SetName(Form("light_%s",v->GetName()));
    hist_light_vec.push_back(hist_light);

    if(!strcmp(v->GetName(),"m4l_detailed")){
      TH1* hist_prodtype_m4l_gamma=data_gamma->createHistogram("gamma_prodtype_m4l",*v,Binning(v->getBinning()),YVar(*v_prod_type,Binning(v_prod_type->getBinning())));
      hist_prodtype_m4l_gamma->SetName("gamma_prodtype_m4l");
      TH1* hist_prodtype_m4l_light=data_light->createHistogram("light_prodtype_m4l",*v,Binning(v->getBinning()),YVar(*v_prod_type,Binning(v_prod_type->getBinning())));
      hist_prodtype_m4l_light->SetName("light_prodtype_m4l");
      continue;
    }

    if(strncmp(v->GetName(),"njet",4)!=0){
      TH1* hist_njet_gamma = data_gamma->createHistogram("njet_gamma",*v,Binning(v->getBinning()),YVar(*v_njet,Binning(njet_binning)));
      hist_njet_gamma->SetName(Form("gamma_%s_njet",v->GetName()));
      TH1* hist_njet_light = data_light->createHistogram("njet_light",*v,Binning(v->getBinning()),YVar(*v_njet,Binning(njet_binning)));
      hist_njet_light->SetName(Form("light_%s_njet",v->GetName()));
      
      TH1* hist_njet2_gamma = data_gamma->createHistogram("njet2_gamma",*v,Binning(v->getBinning()),YVar(*v_njet2,Binning(njet2_binning)));
      hist_njet2_gamma->SetName(Form("gamma_%s_njet2",v->GetName()));
      TH1* hist_njet2_light = data_light->createHistogram("njet2_light",*v,Binning(v->getBinning()),YVar(*v_njet2,Binning(njet2_binning)));
      hist_njet2_light->SetName(Form("light_%s_njet2",v->GetName()));
    }

    if(strcmp(v->GetName(),"pt")!=0){
      TH1* hist_pt_gamma = data_gamma->createHistogram("pt_gamma",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)));
      hist_pt_gamma->SetName(Form("gamma_%s_pt",v->GetName()));
      TH1* hist_pt_light = data_light->createHistogram("pt_light",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)));
      hist_pt_light->SetName(Form("light_%s_pt",v->GetName()));
    }
    /*
    if(!strcmp(v->GetName(),"m4l")){
      double lightFrac=hist_light->GetBinContent(5)/hist_light->Integral();
      double gammaFrac=hist_gamma->GetBinContent(5)/hist_gamma->Integral();
      cout<<"mass window fraction for light is "<<lightFrac<<" and for gamma "<<gammaFrac<<"; test: "<<hist_light->GetBinLowEdge(5)<<endl;
    }
    */
    if(!strstr(v->GetName(),"mcut") && strcmp(v->GetName(),"pt")!=0 && (!strstr(v->GetName(),"m4l") || strstr(v->GetName(),"j"))){ //2d histograms vs. m4l
      TH1* hist2d_gamma=data_gamma->createHistogram("hist",*v,Binning(v->getBinning()),YVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
      hist2d_gamma->SetName(Form("gamma_%s_m4l",v->GetName()));
      TH1* hist2d_light=data_light->createHistogram("hist",*v,Binning(v->getBinning()),YVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
      hist2d_light->SetName(Form("light_%s_m4l",v->GetName()));
      //3d histograms!  vs. m4l and pt, vs. m4l and njet (except for njet)
      TH1* hist_3d_gamma_pt=data_gamma->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
      hist_3d_gamma_pt->SetName(Form("gamma_%s_pt_m4l",v->GetName()));
      TH1* hist_3d_light_pt=data_light->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
      hist_3d_light_pt->SetName(Form("light_%s_pt_m4l",v->GetName()));
      if(!strstr(v->GetName(),"njet")){
	TH1* hist_3d_gamma_njet=data_gamma->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_njet,Binning(njet_binning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
	hist_3d_gamma_njet->SetName(Form("gamma_%s_njet_m4l",v->GetName()));
	TH1* hist_3d_light_njet=data_light->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_njet,Binning(njet_binning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
	hist_3d_light_njet->SetName(Form("light_%s_njet_m4l",v->GetName()));
	TH1* hist_3d_gamma_njet2=data_gamma->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_njet2,Binning(njet2_binning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
        hist_3d_gamma_njet2->SetName(Form("gamma_%s_njet2_m4l",v->GetName()));
        TH1* hist_3d_light_njet2=data_light->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_njet2,Binning(njet2_binning)),ZVar(*v_m4l_mwindow,Binning(m4l_mwindow_binning)));
        hist_3d_light_njet2->SetName(Form("light_%s_njet2_m4l",v->GetName()));
      }
    }

    if(strstr(v->GetName(),"prod_type") || strcmp(v->GetName(),"y4l_2d")==0 || strcmp(v->GetName(),"jet1pt")==0 || strcmp(v->GetName(),"NNVBF")==0){ //for categories/2d shapes
      TH1* hist_3d_gamma_pt_njet=data_gamma->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_njet,Binning(njet_binning)));
      hist_3d_gamma_pt_njet->SetName(Form("gamma_%s_pt_njet",v->GetName()));
      TH1* hist_3d_light_pt_njet=data_light->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_njet,Binning(njet_binning)));
      hist_3d_light_pt_njet->SetName(Form("light_%s_pt_njet",v->GetName()));
      //TH1* hist_3d_gamma_pt=data_gamma->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_m4l_detailed,Binning(m4l_detailed_binning)));
      //hist_3d_gamma_pt->SetName(Form("gamma_%s_pt_m4l",v->GetName()));
      //TH1* hist_3d_light_pt=data_light->createHistogram("h",*v,Binning(v->getBinning()),YVar(*v_pt,Binning(ptBinning)),ZVar(*v_m4l_detailed,Binning(m4l_detailed_binning)));
      //hist_3d_light_pt->SetName(Form("light_%s_pt_m4l",v->GetName()));
    }
    /*
    TH1F* hist_heavy = (TH1F*)data_heavy->createHistogram("heavy",*v);
    hist_heavy->SetMarkerColor(kAzure);
    hist_heavy->SetLineColor(kAzure);
    //hist_heavy->SetMarkerStyle(21);
    hist_heavy->SetName(Form("heavy_%s",v->GetName()));
    hist_heavy_vec.push_back(hist_heavy);
    */
    /*
    if(!strcmp(v->GetName(),"eta")){
      hist_incl_eta = new TH1F("inclusive_eta","inclusive_eta",v_eta->getBins(),v_eta->getMin(),v_eta->getMax());
      hist_incl_eta->Add(hist_gamma);
      hist_incl_eta->Add(hist_light);
      hist_incl_eta->Add(hist_heavy);
      hist_incl_eta->SetMarkerColor(kBlack);
      hist_incl_eta->SetMarkerStyle(21);

      stack_eta = new THStack();
      stack_eta->Add(hist_incl_eta);
      stack_eta->Add(hist_gamma);
      stack_eta->Add(hist_light);
      stack_eta->Add(hist_heavy);
      stack_eta->SetTitle("Electron #eta (no stack);#eta;# Electrons");
    }

    if(!strcmp(v->GetName(),"pt")){
      hist_incl_pt = new TH1F("inclusive_pt","inclusive_pt",3,ptBins);
      hist_incl_pt->Add(hist_gamma);
      hist_incl_pt->Add(hist_light);
      hist_incl_pt->Add(hist_heavy);
      hist_incl_pt->SetMarkerColor(kBlack);
      hist_incl_pt->SetMarkerStyle(24);

      stack_pt = new THStack();
      stack_pt->Add(hist_gamma);
      stack_pt->Add(hist_light);
      stack_pt->Add(hist_incl_pt);
      //stack_pt->Add(hist_heavy_pt);
      stack_pt->SetTitle("Electron p_{T} (no stack);p_{T};# Electrons");
    }
    */
  }

  //TH1* hist2d_gamma=data_gamma->createHistogram("m12_mcut,m34_mcut");
  //TH1* m12_m34_hist2d_gamma=data_gamma->createHistogram("hist",*v_m12_mcut,Binning(m12_mcut_binning),YVar(*v_m34_mcut,Binning(m34_mcut_binning)));
  //m12_m34_hist2d_gamma->SetName("gamma_m12_m34_2d_mcut");
  //TH1* m12_m34_hist2d_light=data_light->createHistogram("hist",*v_m12_mcut,Binning(m12_mcut_binning),YVar(*v_m34_mcut,Binning(m34_mcut_binning)));
  //m12_m34_hist2d_light->SetName("light_m12_m34_2d_mcut");

  //TH1F* h_gamma_pt = (TH1F*)data_gamma->createHistogram("h_gamma",*v_pt);
  //h_gamma_pt->SetName("h_gamma_pt");
  //TH1F* h_f_pt = (TH1F*)data_light->createHistogram("h_f",*v_pt);
  //h_f_pt->SetName("h_f_pt");
  fileout->Write();
  fileout->Close();
  std::cout << "Done." << std::endl;

}
     
