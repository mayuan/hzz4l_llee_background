#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TH1F.h"

#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <vector>
#include <math.h>

#include "RooProdPdf.h"
#include "RooWorkspace.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooHistPdf.h"
#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooPlot.h"

#include "Fit3LplusX/Vars.h"

#include "TSystem.h"
#include "H4lBkgUtils/H4lStyle.h"
#include "H4lBkgUtils/AtlasStyle.h"
#include "H4lBkgUtils/AtlasLabels.h"

struct Var{
  const char* name;
  const char* label;
  int bins;
  float min;
  float max;
};

int main(int argc, char* argv[]){

  using namespace std;
  using namespace RooFit;

  SetAtlasStyle();
  
  TString treename="tree_ZplusEl";
  string output="";
  string year;
  bool do_nInner=false;
  bool do_rTRT=false;
  bool do_f1=false;
  bool do_eProbHT=false;
  /*
  bool do_f3=false;
  bool do_Rhad=false;
  bool do_Rphi=false;
  bool do_Reta=false;
  bool do_Deta1=false;
  */
  bool do_pdfMergeFQ=true;
  bool do_pdfCorr_f=false;
  bool do_pdfCorr_gamma=false;
  
  if (argc==1){
    cout<<"give args:"<<endl;
    cout <<"--input [\"flattenedZplusX.Zjets.root flattenedZplusX.WZ.root ...\"]"<<endl;
    cout<<"--mergePdfFQ [{true}, false]"<<endl;
    //cout<<"--usePdfCorr [{true}, false]"<<endl;
    cout<<"--usePdfCorr [f, gamma]"<<endl;
    cout<<"--treename [{tree_ZplusEl}, tree_ZplusMu, tree_incl_all]"<<endl;
    cout<<"--vars [nInner, ptBalance, d0sig, rTRT, f1, eProbHT, f3, Rhad, Reta, Rphi, Deta1]"<<endl;
    cout<<"--year [2016,2017,2018]"<<endl;
    cout<<"--output [\"output directory\"]"<<endl;
    return -1;
  }

  std::vector<TString> inputfiles;
  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--input")){
      std::stringstream ss(argv[++a]);
      string buf;      
      while (ss>>buf) inputfiles.push_back(buf);
    }
    else if (!strcmp(argv[a],"--mergePdfFQ")){
      std::stringstream ss(argv[++a]);
      ss >> std::boolalpha >> do_pdfMergeFQ;
    }
    else if (!strcmp(argv[a],"--usePdfCorr")){
      while (a+1<argc){
        std::string next = argv[++a];
        if (next.find("--")==std::string::npos){
          if (next=="f") do_pdfCorr_f=true;
          else if (next=="gamma") do_pdfCorr_gamma=true;
          else {
            cout<<"unrecognized param given to --usePdfCorr: "<<next<<endl;
            return -1;
          }
        }
        else {
          --a;
          break;
        }
      }
    }
    else if (!strcmp(argv[a],"--treename")){
      cout<<"setting treename"<<endl;
      treename=argv[++a];
    }
    else if (!strcmp(argv[a],"--vars")){
      cout<<"settings vars"<<endl;
      while (a+1<argc){
        std::string next = argv[++a];
        if (next.find("--")==std::string::npos){
          if (next=="nInner") do_nInner=true;
          else if (next=="rTRT") do_rTRT=true;
          else if (next=="f1") do_f1=true;
	  else if (next=="eProbHT") do_eProbHT=true;
          else {
            cout<<"unrecognized param given to --vars: "<<next<<endl;
            return -1;
          }
        }
        else {
          --a;
          break;
        }
      }
    }
    else if (!strcmp(argv[a],"--output")){
      cout<<"setting output dir"<<endl;
      output=argv[++a];
    }
    else if(!strcmp(argv[a],"--year")){
      cout<<"setting year to "<<year<<endl;
      year=argv[++a];
    }
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }

  cout<<"using merged templates for f/q = " << do_pdfMergeFQ <<endl;
  cout<<"using template corrections (f,gamma)= " << do_pdfCorr_f << " " << do_pdfCorr_gamma <<endl;
  cout<<"using tree = "<<treename<<endl;
  cout<<"nInner: "<<(do_nInner?"yes":"no")<<endl;
  cout<<"rTRT: "<<(do_rTRT?"yes":"no")<<endl;
  cout<<"f1: "<<(do_f1?"yes":"no")<<endl;
  cout<<"eProbHT: "<<(do_eProbHT?"yes":"no")<<endl;

  vector<Vars::Var> FitVars;
  if (do_nInner) FitVars.push_back(Vars::nInner);
  if (do_rTRT) FitVars.push_back(Vars::rTRT);
  if (do_f1) FitVars.push_back(Vars::f1);
  if (do_eProbHT) FitVars.push_back(Vars::eProbHT);

  //Build Template Pdfs
  RooWorkspace* wsp = new RooWorkspace("bgDiscriminantWorkspace","bgDiscriminantWorkspace");
  wsp->factory("weight[-10.,10000.]");
  wsp->factory("MCClass[-0.5,7.5]");
  for (auto v: FitVars) wsp->factory(Form("%s[%f,%f]",v.name,v.min,v.max));
  
  wsp->factory("n_gamma[50,0,9999999999]");
  wsp->factory("n_light[700,0,9999999999]");
  wsp->factory("n_heavy[10,0,9999999999]");
  wsp->factory("n_real[10,0,9999999999]");

  RooRealVar* n_gamma = wsp->var("n_gamma");
  RooRealVar* n_light = wsp->var("n_light");
  RooRealVar* n_heavy = wsp->var("n_heavy");
  RooRealVar* n_real = wsp->var("n_real");

  RooRealVar* weight = wsp->var("weight");
  RooRealVar* MCClass = wsp->var("MCClass");

  vector<RooRealVar*> rrvars;
  for (auto v:FitVars) rrvars.push_back(wsp->var(v.name));
  for (unsigned int i(0);i<FitVars.size();++i) rrvars[i]->setBins(FitVars[i].bins);

  RooArgSet vars("vars");

  vars.add(*weight);
  vars.add(*MCClass);
  for (auto v: rrvars) vars.add(*v);

  RooArgList pdfs_gamma("pdfs_gamma");
  RooArgList pdfs_light("pdfs_light");
  RooArgList pdfs_heavy("pdfs_heavy");
  RooArgList pdfs_real("pdfs_real");

  RooDataSet* templateData = new RooDataSet("dataZx","dataZx",vars,"weight");
  
  //Set up reading in file
  TChain* tree = new TChain(treename);
  for (auto file : inputfiles) tree->Add(file);  
  if (tree->GetEntries()<=0) {
    std::cout << "Error: tree not found, " << treename << std::endl;
    exit(0);
  }
  
  double t_weight;
  int t_nInner, t_MCClass;
  
  tree->SetBranchAddress("weight",&t_weight);
  tree->SetBranchAddress("MCClass",&t_MCClass);
  tree->SetBranchAddress("nInner",&t_nInner);

  TFile* corrfile = 0;
  if (do_pdfCorr_f || do_pdfCorr_gamma) corrfile = TFile::Open(Form("pdf_corrections_%s.root",year.c_str()),"READ");

  for (unsigned int i(0);i<tree->GetEntries();++i){

    tree->GetEntry(i);

    if (t_nInner<0) continue;

    weight->setVal(t_weight);
    MCClass->setVal(t_MCClass);
    for (auto v: rrvars)
      if (!strcmp(v->GetName(),"nInner")) v->setVal(t_nInner);
    templateData->add(vars, t_weight);

  }

  templateData->Print();

  TCanvas* c  = new TCanvas("c1","c1",600,600);
  //c->Divide(FitVars.size());
  TString label="wsp";
  for (auto v: FitVars) { label+="_"; label+=v.name; }

  for (unsigned int i(0);i<FitVars.size();++i) {
    c->Clear();
    Vars::Var a = FitVars[i];
    RooRealVar* v = rrvars[i];

    RooDataSet* red_gamma = 0;    
    RooDataSet* red_light = 0;
    RooDataSet* red_heavy = 0;

    if (treename.Contains("ZplusMu")) {      
      red_light = (RooDataSet*)templateData->reduce(*v, Form("(MCClass==5)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max));
      red_heavy = (do_pdfMergeFQ)?
	(RooDataSet*)templateData->reduce(*v, Form("(MCClass==1 || MCClass==2)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max)):
	(RooDataSet*)templateData->reduce(*v, Form("(MCClass==2)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max));
    }
    else { //ZplusEl
      red_gamma = (RooDataSet*)templateData->reduce(*v, Form("(MCClass==3||MCClass==4)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max));
      red_light = (do_pdfMergeFQ) ?
	(RooDataSet*)templateData->reduce(*v, Form("(MCClass==5 || MCClass==1 || MCClass==2 || MCClass==7)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max)): 
	(RooDataSet*)templateData->reduce(*v, Form("(MCClass==5 || MCClass==7)&& %f<=%s && %s<=%f",a.min,a.name,a.name,a.max));
      red_heavy = (RooDataSet*)templateData->reduce(*v, Form("(MCClass==2)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max));
    }        
    RooDataSet* red_real = (RooDataSet*)templateData->reduce(*v, Form("(MCClass==1)&&%f<=%s&&%s<=%f",a.min,a.name,a.name,a.max));

    RooDataHist* bin_gamma = (red_gamma)? red_gamma->binnedClone("bin_gamma") : 0;
    RooDataHist* bin_light = red_light->binnedClone("bin_light");
    RooDataHist* bin_heavy = red_heavy->binnedClone("bin_heavy");
    RooDataHist* bin_real = red_real->binnedClone("bin_real");

    int smoothing=0;
    TH1F* h_corr=0;	
    if (do_pdfCorr_gamma || do_pdfCorr_f) {
      std::cout << "Retrieving pdf corrections... " << std::endl;
      h_corr = (TH1F*) corrfile->Get(Form("hcorr_%s",v->GetName()))->Clone();
      h_corr->SetDirectory(0);
      //.... if separate corrections for photons/fakes...
      // TH1F* h_corr_gamma = (TH1F*) corrfile->Get(Form("hcorr_%s_Gamma",v->GetName()))->Clone();
      // TH1F* h_corr_light = (!strcmp(v->GetName(),"nInner")) ?
      // 	(TH1F*) corrfile->Get(Form("hcorr_%s_All",v->GetName()))->Clone():
      // 	(TH1F*) corrfile->Get(Form("hcorr_%s_Fake",v->GetName()))->Clone();  
    }
    
    RooHistPdf* pdf_heavy = new RooHistPdf(Form("histpdf_%s_heavy",v->GetName()),"",*v,*bin_heavy, smoothing);
    RooHistPdf* pdf_real = new RooHistPdf(Form("histpdf_%s_real",v->GetName()),"",*v,*bin_real, smoothing);

    RooHistPdf* pdf_uncorr_gamma = (bin_gamma)? new RooHistPdf(Form("histpdf_uncorr_%s_gamma",v->GetName()),"",*v,*bin_gamma, smoothing) :0;
    RooHistPdf* pdf_uncorr_light =  new RooHistPdf(Form("histpdf_uncorr_%s_light",v->GetName()),"",*v,*bin_light, smoothing);
    
    RooHistPdf* pdf_gamma = (!do_pdfCorr_gamma && bin_gamma)? (RooHistPdf*)pdf_uncorr_gamma->Clone(Form("histpdf_%s_gamma",v->GetName())) :0; 
    RooHistPdf* pdf_light = (!do_pdfCorr_f)? (RooHistPdf*)pdf_uncorr_light->Clone(Form("histpdf_%s_light",v->GetName())) :0; 

    TH1* hpdfcorr_gamma = (pdf_uncorr_gamma)? pdf_uncorr_gamma->createHistogram("hpdfcorr_gamma", *v) :0;
    TH1* hpdfcorr_light = (pdf_uncorr_light)? pdf_uncorr_light->createHistogram("hpdfcorr_light", *v) :0;

    ///// apply corrections to pdfs
    if(do_pdfCorr_f) {
      for (int j=1; j<hpdfcorr_light->GetXaxis()->GetNbins(); j++) {cout << hpdfcorr_light->GetBinContent(j) << " ";} cout << endl;
      hpdfcorr_light->Multiply(h_corr);
      for (int j=1; j<hpdfcorr_light->GetXaxis()->GetNbins(); j++) {cout << hpdfcorr_light->GetBinContent(j) << " ";} cout << endl;
      //RooDataHist* dhcorr_light = new RooDataHist( Form("dhcorr_%s_light",v->GetName()), "", *v, Import(*hpdfcorr_light));
      bin_light->Delete();
      bin_light = new RooDataHist( Form("dhcorr_%s_light",v->GetName()), "", *v, Import(*hpdfcorr_light));     
      pdf_light = new RooHistPdf(Form("histpdf_%s_light",v->GetName()),"",*v,*bin_light, smoothing);
    } 
    if(do_pdfCorr_gamma) {
      hpdfcorr_gamma->Multiply(h_corr);
      //RooDataHist* dhcorr_gamma = new RooDataHist( Form("dhcorr_%s_gamma",v->GetName()), "", *v, Import(*hpdfcorr_gamma));
      bin_gamma->Delete();	    
      bin_gamma = new RooDataHist( Form("dhcorr_%s_gamma",v->GetName()), "", *v, Import(*hpdfcorr_gamma));
      pdf_gamma = new RooHistPdf(Form("histpdf_%s_gamma",v->GetName()),"",*v,*bin_gamma, smoothing);      
    }
    
    pdfs_gamma.add(*pdf_gamma);
    pdfs_light.add(*pdf_light);
    pdfs_heavy.add(*pdf_heavy);
    pdfs_real.add(*pdf_real);

    std::cout<<"...added pdfs"<<std::endl;

    ////////////////////////////////////////////////////////////
    //Plotting for show
    RooPlot* frame = v->frame(Title(""));
    frame->GetYaxis()->SetTitle("Fraction of events");
    frame->GetXaxis()->SetTitle(FitVars[i].label);
    //    TLegend *leg = makeLeg(0.6,0.58,0.94,0.86);
    TLegend *leg = makeLeg(0.64,0.58,0.94,0.80);
    
    
    if (pdf_gamma) {
      cout<<"plot gamma pdf"<<endl;
      if (do_pdfCorr_gamma) {
	pdf_uncorr_gamma->plotOn(frame,LineColor(kOrange+2),LineStyle(kDashed),LineWidth(3));
	TH1* hpdfuncorr_gamma = pdf_uncorr_gamma->createHistogram("hpdfuncorr_gamma", *v);
	hpdfuncorr_gamma->SetLineColor(kOrange+2);
	hpdfuncorr_gamma->SetLineStyle(3);
	leg->AddEntry(hpdfuncorr_gamma->Clone(),"#gamma","l");	
      }
      pdf_gamma->plotOn(frame,DrawOption("F"),FillColor(kOrange),MoveToBack());
      hpdfcorr_gamma->SetFillColor(kOrange);
      hpdfcorr_gamma->SetLineColor(kOrange);
      leg->AddEntry(hpdfcorr_gamma->Clone(),"#gamma","f");
    }    

    if (pdf_light) {
      cout<<"plot light pdf"<<endl;
      /*
      if (do_pdfCorr_f) {
	pdf_uncorr_light->plotOn(frame,LineColor(kGreen),LineWidth(2));
	TH1* hpdfuncorr_light = pdf_uncorr_light->createHistogram("hpdfuncorr_light", *v);
	hpdfuncorr_light->SetLineColor(kGreen);
	hpdfuncorr_light->SetLineStyle(1);
	hpdfuncorr_light->SetLineWidth(2);          
        leg->AddEntry(hpdfuncorr_light->Clone(),"f","l");
	//if (treename.Contains("ZplusMu")) 
	//  leg->AddEntry(hpdfuncorr_light->Clone(),"LF","l");
	//else 
	//  leg->AddEntry(hpdfuncorr_light->Clone(),"f","l"); 
      }
      */
      pdf_light->plotOn(frame,LineColor(kGreen),LineWidth(3));
      hpdfcorr_light->SetLineColor(kGreen);
      leg->AddEntry(hpdfcorr_light->Clone(),"fake","f");
      //hpdfcorr_light->SetLineColor(kGreen+3);
      //hpdfcorr_light->SetLineStyle(1);
      //hpdfcorr_light->SetLineWidth(2);
      //if (treename.Contains("ZplusMu")) 
	//leg->AddEntry(hpdfcorr_light->Clone(),"LF","l");
      //else
    }
    cout<<"plot heavy pdf"<<endl;
    pdf_heavy->plotOn(frame,LineColor(kOrange+10),LineWidth(3),LineStyle(kDotted));    
   
    frame->GetYaxis()->SetRangeUser(0.,frame->GetMaximum()*1.3);
    if (treename.Contains("ZplusEl")) frame->GetXaxis()->SetNdivisions(4);
    frame->Draw();

    float x=0.2, y = 0.875;
    ATLASLabel(x,y, " Simulation Internal", 1);
    y-=0.065;
    TLatex tMain;
    tMain.SetNDC();
    tMain.SetTextSize(0.0425);
    tMain.SetTextFont(42);
    if (treename.Contains("ZplusEl"))
      tMain.DrawLatex(x, y,"Z+e");
    else if (treename.Contains("ZplusMu"))
      tMain.DrawLatex(x, y,"Z+#mu");

    TH1* hpdfuncorr_real = pdf_real->createHistogram("hpdfuncorr_real", *v);
    hpdfuncorr_real->SetLineColor(kOrange);
    hpdfuncorr_real->SetLineWidth(3);

    TH1* hpdfcorr_heavy = pdf_heavy->createHistogram("hpdf_heavy", *v);
    hpdfcorr_heavy->SetLineColor(kOrange+10);
    hpdfcorr_heavy->SetLineStyle(kDotted);
    hpdfcorr_heavy->SetLineWidth(3);
    if (treename.Contains("ZplusMu")) {
      leg->AddEntry(hpdfcorr_heavy,"HF","l");
      leg->AddEntry(hpdfuncorr_real,"Prompt isolated","l");
    }else {
      leg->AddEntry(hpdfcorr_heavy,"q","l");
    }

    leg->Draw();

    c->GetPad(0)->RedrawAxis();

    TString channel = treename;
    channel.ReplaceAll("tree_","");
    c->Print(Form("%s/plots/Pdf_%s_%s_%s.eps",output.c_str(),channel.Data(),a.name,year.c_str()));
    c->Print(Form("%s/plots/Pdf_%s_%s_%s.pdf",output.c_str(),channel.Data(),a.name,year.c_str()));
  }
  std::cout<<"plotted"<<std::endl;

  RooProdPdf* prodpdf_gamma = (pdfs_gamma.getSize()>0) ? new RooProdPdf(Form("prodpdf_gamma"),Form("prodpdf_gamma"),pdfs_gamma) :0;
  RooProdPdf* prodpdf_light = (pdfs_light.getSize()>0) ? new RooProdPdf(Form("prodpdf_light"),Form("prodpdf_light"),pdfs_light) :0;
  RooProdPdf* prodpdf_heavy = (pdfs_heavy.getSize()>0) ? new RooProdPdf(Form("prodpdf_heavy"),Form("prodpdf_heavy"),pdfs_heavy) :0;
  RooProdPdf* prodpdf_real = (pdfs_real.getSize()>0)? new RooProdPdf(Form("prodpdf_real"),Form("prodpdf_real"),pdfs_real) :0;

  std::cout<<"building final pdf"<<std::endl;

  RooAddPdf *finalPdf = 0;
  if (do_pdfMergeFQ) {
    finalPdf = (treename.Contains("ZplusMu"))?
      new RooAddPdf("addpdf","addpdf",RooArgList(*prodpdf_light,*prodpdf_heavy),RooArgList(*n_light,*n_heavy)):
      new RooAddPdf("addpdf","addpdf",RooArgList(*prodpdf_gamma,*prodpdf_light),RooArgList(*n_gamma,*n_light));
  } else {
    finalPdf = (treename.Contains("ZplusMu")) ?
      new RooAddPdf("addpdf","addpdf",RooArgList(*prodpdf_light,*prodpdf_heavy,*prodpdf_real),RooArgList(*n_light,*n_heavy,*n_real)):
      new RooAddPdf("addpdf","addpdf",RooArgList(*prodpdf_gamma,*prodpdf_light,*prodpdf_heavy,*prodpdf_real),RooArgList(*n_gamma,*n_light,*n_heavy,*n_real));
  }
  std::cout<<"importing final pdf to ws"<<std::endl;
  wsp->import(*finalPdf);
  wsp->writeToFile(Form("%s/%s_%s.root",output.c_str(),label.Data(),year.c_str()));

  cout<<endl<<endl;
  wsp->Print();

  for (unsigned int i(0);i<FitVars.size();++i) rrvars[i]->Print();
}
     
