#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TObject.h"
#include "TString.h"
#include "TH1F.h"

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char* argv[])
{
  TString inputData;
  TString inputMC;
  float scaleMC=1.;
  TString treename;
  
  if (argc==1){
    cout<<"--inputData "<<endl;
    cout<<"--inputMC "<<endl;
    cout<<"--scaleMC "<<endl;
    cout<<"--treename "<<endl;
    return -1;
  }

  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--inputData")){
      inputData=argv[++a];
    }
    else if (!strcmp(argv[a],"--inputMC")){
      inputMC=argv[++a];
    }
    else if (!strcmp(argv[a],"--scaleMC")){
      scaleMC=atof(argv[++a]);
    }
    else if (!strcmp(argv[a],"--treename")){
      treename=argv[++a];
    }      
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }

  cout<<"--inputData " << inputData <<endl;
  cout<<"--inputMC "<< inputMC <<endl;
  cout<<"--scaleMC "<< scaleMC <<endl;
  cout<<"--treename "<< treename <<endl;
    
  TH1::SetDefaultSumw2();
  
  TFile *infile = TFile::Open(inputData,"READ");
  //the data...
  TH1F* h_f_pt = (TH1F*) (infile->Get("h_f_pt"))->Clone();
  TH1F* h_gamma_pt = (TH1F*) (infile->Get("h_gamma_pt"))->Clone();
  TH1F* h_q_pt = 0;
  TH1F* h_ZZq_pt = 0;
  if (treename.Contains("relaxmumu")) {
    h_q_pt = (TH1F*) (infile->Get("h_q_pt"))->Clone();
    h_ZZq_pt = (TH1F*)h_f_pt->Clone("h_ZZq_pt");
  }
  else  h_q_pt = (TH1F*)h_f_pt->Clone("h_q_pt");

  //clone the data hists to fill with mc events
  TH1F* h_ZZreal_pt = (TH1F*)h_f_pt->Clone("h_ZZreal_pt");

  TH1F* h_ZZf_pt = (TH1F*)h_f_pt->Clone("h_ZZf_pt");
  TH1F* h_ZZgamma_pt = (TH1F*)h_f_pt->Clone("h_ZZgamma_pt");
  TH1F* h_ttVf_pt = (TH1F*)h_f_pt->Clone("h_ttVf_pt");
  TH1F* h_ttVgamma_pt = (TH1F*)h_f_pt->Clone("h_ttVgamma_pt");
    
  for (int i=1; i<=h_q_pt->GetXaxis()->GetNbins(); i++) {
    if (treename.Contains("relaxee")) { h_q_pt->SetBinContent(i,0);   h_q_pt->SetBinError(i,0); }    
    h_ZZf_pt->SetBinContent(i,0);   h_ZZf_pt->SetBinError(i,0);
    h_ZZgamma_pt->SetBinContent(i,0);    h_ZZgamma_pt->SetBinError(i,0);
    h_ZZreal_pt->SetBinContent(i,0);   h_ZZreal_pt->SetBinError(i,0);

    h_ttVf_pt->SetBinContent(i,0);   h_ttVf_pt->SetBinError(i,0);
    h_ttVgamma_pt->SetBinContent(i,0);    h_ttVgamma_pt->SetBinError(i,0);

    if (treename.Contains("relaxmumu")) {h_ZZq_pt->SetBinContent(i,0);   h_ZZq_pt->SetBinError(i,0); }
  }

  if (treename.Contains("relaxmumu")) {
    TString inputMC = inputData.ReplaceAll(".root","_ZZ.root");
    TFile *infileMC = TFile::Open(inputMC,"READ");
    h_ZZf_pt = (TH1F*) (infileMC->Get("h_f_pt"))->Clone();
    h_ZZq_pt = (TH1F*) (infileMC->Get("h_q_pt"))->Clone();
    h_ZZf_pt->SetDirectory(0);
    h_ZZq_pt->SetDirectory(0);
  }

  /*

  // TFile *f_mc = TFile::Open(inputMC,"READ");
  // TTree* t_mc = (TTree*)f_mc->Get(treename);
  TChain* t_mc = new TChain(treename);
  std::stringstream ss(inputMC.Data());
  string buf;
  std::vector<string> inputfiles;
  while (ss>>buf) {
    std::cout << "Adding file " << buf << std::endl;
    t_mc->AddFile(buf.c_str());
  }
  if (!t_mc || t_mc->IsZombie()) {
    std::cout<<"not a TTree!"<<std::endl;
  }
  t_mc->Lookup();
  
  Int_t run;
  int event_type;
  float weight;
  float pt;
  int MCClass;
  t_mc->SetBranchAddress("run",&run);
  t_mc->SetBranchAddress("event_type",&event_type);
  t_mc->SetBranchAddress("weight",&weight);
  t_mc->SetBranchAddress("MCClass",&MCClass);
  t_mc->SetBranchAddress("pt",&pt);

  long int nEntries = t_mc->GetEntries();
  for (int i(0);i<nEntries;++i){    
    //if (i%10000==0) cout<<i<<"/"<<t_mc->GetEntries()<<endl;
    t_mc->GetEntry(i);

    float w = weight * scaleMC;
    if (MCClass==2 && treename.Contains("relaxee"))
      h_q_pt->Fill(pt,w);

    bool isZZ=(run==343212 || run==343213 || run==343232 || run==361603 || run==342556); //qq/gg->ZZ
    if ( isZZ ) {
      if (treename.Contains("relaxee")) {
	if (MCClass==5 || MCClass==7)
	  h_ZZf_pt->Fill(pt,w);
	else if (MCClass==3 || MCClass==4)
	  h_ZZgamma_pt->Fill(pt,w);
      }
      else if (treename.Contains("relaxmumu") && MCClass==1)
	//h_ZZreal_pt->Fill(pt,w);
	h_ZZq_pt->Fill(pt,w);
    }
  }
  */
  
  ////// subtractions...
  Double_t err;
  Double_t integ;
  int nbins = h_f_pt->GetXaxis()->GetNbins();

  if (treename.Contains("relaxee")) {
    cout << "                 	 fakes  |  gammas " << endl;
    cout << "---------------------------------------------------- " << endl;
    integ = h_f_pt->IntegralAndError(1,nbins,err);
    cout << " data : " << integ << " +- " << err;
    integ = h_gamma_pt->IntegralAndError(1,nbins,err);
    cout << "\t| " << integ << " +- " << err << endl;

    integ = h_ZZf_pt->IntegralAndError(1,nbins,err);
    cout << " ZZmc : " << integ << " +- " << err;
    integ = h_ZZgamma_pt->IntegralAndError(1,nbins,err);
    cout << "\t| " << integ << " +- " << err << endl;

    integ = h_q_pt->IntegralAndError(1,nbins,err);
    cout << " HFmc : " << integ << " +- " << err << "\t|  n/a "  << endl;
    cout << "----------------------------------------------------- " <<endl;
  }
  else {
    cout << "                 	 fakes  |  q/e " << endl;
    cout << "---------------------------------------------------- " << endl;
    integ = h_f_pt->IntegralAndError(1,nbins,err);
    cout << " data : " << integ << " +- " << err;
    integ = h_q_pt->IntegralAndError(1,nbins,err);
    cout << "\t| " << integ << " +- " << err << endl;

    integ = h_ZZf_pt->IntegralAndError(1,nbins,err);
    cout << " ZZmc : " << integ << " +- " << err;
    integ = h_ZZq_pt->IntegralAndError(1,nbins,err);
    cout << "\t| " << integ << " +- " << err << endl;

    cout << "----------------------------------------------------- " <<endl;
  }

  
  //fakes: subtract lll+q and ZZ->lll+f
  if (treename.Contains("relaxee")) h_f_pt->Add(h_q_pt,-1);
  else  h_q_pt->Add(h_ZZq_pt,-1*scaleMC); 
  h_f_pt->Add(h_ZZf_pt,-1);
  //photons: subtract ZZ->lll+gamma
  h_gamma_pt->Add(h_ZZgamma_pt,-1*scaleMC);

  integ = h_f_pt->IntegralAndError(1,nbins,err);
  cout << "data-mc : " << integ << " +- " << err;
  if (treename.Contains("relaxee")) {
    integ = h_gamma_pt->IntegralAndError(1,nbins,err);
    cout << " | " << integ << " +- " << err << endl;
  } else {
    integ = h_q_pt->IntegralAndError(1,nbins,err);
    cout << " | " << integ << " +- " << err << endl;
  }

  TFile *fout = TFile::Open("pt_ZZsubtracted.root","recreate");
  h_gamma_pt->Write();
  h_f_pt->Write();
  h_q_pt->Write();
  fout->Write();
  fout->Close();
  infile->Close();
  //delete t_mc;
}
