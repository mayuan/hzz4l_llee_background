#include "TFile.h"
#include "TTree.h"
#include "TObject.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"

#include <iostream>
#include <string>
#include <map>
//#include <stdio.h>

using namespace std;

int main(int argc, char* argv[])
{
  TString inputData;
  TString inputMC;
  float scaleMC=1.;
  TString year;
  
  if (argc==1){
    cout<<"--inputData "<<endl;
    cout<<"--inputMC "<<endl;
    cout<<"--scaleMC "<<endl;
    cout<<"--year"<<endl;
    return -1;
  }

  for (int a(1);a<argc;++a){
    if (!strcmp(argv[a],"--inputData")){
      inputData=argv[++a];
    }
    else if (!strcmp(argv[a],"--inputMC")){
      inputMC=argv[++a];
    }
    else if (!strcmp(argv[a],"--scaleMC")){
      scaleMC=atof(argv[++a]);
    }
    else if(!strcmp(argv[a],"--year")){
      year=argv[++a];
    }
    else {
      cout<<"invalid arg received: "<<argv[a]<<endl;
      return -1;
    }
  }
  bool use2e2e=false;
  bool use2mu2e=false;
  if(inputData.Contains("2e2e")) use2e2e=true;
  else if(inputData.Contains("2mu2e")) use2mu2e=true;

  cout<<"--inputData " << inputData <<endl;
  cout<<"--inputMC "<< inputMC <<endl;
  cout<<"--scaleMC "<< scaleMC <<endl;
  cout<<"year: "<<year<<endl;
  
  TH1::SetDefaultSumw2();
  
  TFile *infile = TFile::Open(inputData,"READ");
  //the data...
  TH2F* h_f_pt_njet = (TH2F*) (infile->Get("light_pt_njet"))->Clone();
  TH2F* h_gamma_pt_njet = (TH2F*) (infile->Get("gamma_pt_njet"))->Clone();

  //clone the data hists to fill with mc events
  TH2F* h_q_pt_njet = (TH2F*)h_f_pt_njet->Clone("h_q_pt_njet");
  TH2F* h_ZZf_pt_njet = (TH2F*)h_f_pt_njet->Clone("h_ZZf_pt_njet");
  TH2F* h_ZZgamma_pt_njet = (TH2F*)h_f_pt_njet->Clone("h_ZZgamma_pt_njet");
  for (int i=1; i<=h_q_pt_njet->GetXaxis()->GetNbins(); i++) {
    for(int j=1; j<=h_q_pt_njet->GetYaxis()->GetNbins(); j++){
      h_q_pt_njet->SetBinContent(i,j,0);   h_q_pt_njet->SetBinError(i,j,0);
      h_ZZf_pt_njet->SetBinContent(i,j,0);   h_ZZf_pt_njet->SetBinError(i,j,0);
      h_ZZgamma_pt_njet->SetBinContent(i,j,0);    h_ZZgamma_pt_njet->SetBinError(i,j,0);
    }
  }


  TFile *f_mc = TFile::Open(inputMC,"READ");
  TTree* t_mc = (TTree*)f_mc->Get("tree_relaxee");
  if (!t_mc || t_mc->IsZombie()) {
    std::cout<<"not a TTree!"<<std::endl;
  }

  Int_t run,njets;
  int event_type;
  double weight;
  float pt;
  int MCClass,qtype;
  t_mc->SetBranchAddress("run",&run);
  t_mc->SetBranchAddress("event_type",&event_type);
  t_mc->SetBranchAddress("weight",&weight);
  t_mc->SetBranchAddress("MCClass",&MCClass);
  t_mc->SetBranchAddress("pt",&pt);
  t_mc->SetBranchAddress("njet",&njets);
  t_mc->SetBranchAddress("event_type",&qtype);
  std::map<int,int> runNums;

  long int nEntries = t_mc->GetEntries();
  //std::cout<<"there are "<<nEntries<<" mc entries"<<std::endl;
  for (int i(0);i<nEntries;++i){    
    //if (i%10000==0) cout<<i<<"/"<<t_mc->GetEntries()<<endl;
    t_mc->GetEntry(i);

    if(use2e2e && qtype!=1) continue;
    if(use2mu2e && qtype!=2) continue;

    if(runNums.count(run)==0) runNums[run]=1;
    else runNums[run]++;

    //if(run>363101 && run<363115) std::cout<<"Ztautau weight: "<<weight<<"; pt: "<<pt<<"; MCClass: "<<MCClass<<std::endl;

    float w = weight * scaleMC;
    if (MCClass==2)
      h_q_pt_njet->Fill(pt,njets,w);

    bool isZZ=(run==364250 || run==364251 || run==364252 || run==346340 || run==346341 || run==346342 || run==410155 || run == 410218 || run == 410219 || run == 345060 ||  run == 344235 || run == 345038 || run == 345066 || run ==  345039 || run == 345040 || run == 344973 || run == 344974 ||  run == 364243  || run ==  364245  || run == 364247 || run == 364248 || run == 342001 || run == 343273 || run == 345708 || run==345709 || run == 345937 || run==345938 || run==345939 || run==345966 || run==345967 );
    if ( isZZ ) {
      if (MCClass==5 || MCClass==7)
	h_ZZf_pt_njet->Fill(pt,njets,w);
      else if (MCClass==3 || MCClass==4)
	h_ZZgamma_pt_njet->Fill(pt,njets,w);
    }
  }

  //for(std::map<int,int>::iterator it=runNums.begin();it!=runNums.end();++it) std::cout<<"run "<<(*it).first<<" has "<<(*it).second<<" entries"<<std::endl;

  ////// subtractions...
  Double_t err;
  Double_t integ;

  cout << "                 	 fakes  |  gammas " << endl;
  cout << "---------------------------------------------------- " << endl;
  integ = h_f_pt_njet->IntegralAndError(1,h_f_pt_njet->GetNbinsX(),1,h_f_pt_njet->GetNbinsY(),err);
  cout << " data : " << integ << " +- " << err;
  integ = h_gamma_pt_njet->IntegralAndError(1,h_gamma_pt_njet->GetNbinsX(),1,h_gamma_pt_njet->GetNbinsY(),err);
  cout << "\t| " << integ << " +- " << err << endl;

  integ = h_ZZf_pt_njet->IntegralAndError(1,h_ZZf_pt_njet->GetNbinsX(),1,h_ZZf_pt_njet->GetNbinsY(),err);
  cout << " ZZmc : " << integ << " +- " << err;
  integ = h_ZZgamma_pt_njet->IntegralAndError(1,h_ZZgamma_pt_njet->GetNbinsX(),1,h_ZZgamma_pt_njet->GetNbinsY(),err);
  cout << "\t| " << integ << " +- " << err << endl;

  integ = h_q_pt_njet->IntegralAndError(1,h_q_pt_njet->GetNbinsX(),1,h_q_pt_njet->GetNbinsY(),err);
  cout << " HFmc : " << integ << " +- " << err << "\t|  n/a "  << endl;
  cout << "----------------------------------------------------- " <<endl;

  //fakes: subtract lll+q and ZZ->lll+f
  h_f_pt_njet->Add(h_q_pt_njet,-1);
  h_f_pt_njet->Add(h_ZZf_pt_njet,-1);
  //photons: subtract ZZ->lll+gamma
  h_gamma_pt_njet->Add(h_ZZgamma_pt_njet,-1*scaleMC); 

  integ = h_f_pt_njet->IntegralAndError(1,h_f_pt_njet->GetNbinsX(),1,h_f_pt_njet->GetNbinsY(),err);
  cout << "data-mc : " << integ << " +- " << err;
  integ = h_gamma_pt_njet->IntegralAndError(1,h_gamma_pt_njet->GetNbinsX(),1,h_gamma_pt_njet->GetNbinsY(),err);
  cout << " | " << integ << " +- " << err << endl;

  TString finalState;
  if(use2e2e) finalState="_2e2e";
  if(use2mu2e) finalState="_2mu2e";
  TFile *fout = TFile::Open(Form("pt_njet_ZZsubtracted%s_%s.root",finalState.Data(),year.Data()),"recreate");
  h_gamma_pt_njet->Write();
  h_f_pt_njet->Write();
  fout->Write();
  fout->Close();
  infile->Close();
}
