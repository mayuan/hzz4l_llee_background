#!/usr/bin/env python

import ROOT
from ROOT import *
#TFile, TH1F, TTree
import sys
import argparse
import re
import os, fnmatch
import math


f = TFile.Open('postfit_kinematics_wsp_ptBalance.root')
h_f_pt = f.Get('h_f_pt').Clone('h_f_pt')
h_q_pt = f.Get('h_q_pt').Clone('h_q_pt')
h_f_pt.SetDirectory(0)
h_q_pt.SetDirectory(0)

f2 = TFile.Open('postfit_kinematics_wsp_ptBalance_ZZ.root')
h_f_pt_ZZ = f2.Get('h_f_pt').Clone('h_f_pt_ZZ')
h_q_pt_ZZ = f2.Get('h_q_pt').Clone('h_q_pt_ZZ')
h_f_pt_ZZ.SetDirectory(0)
h_q_pt_ZZ.SetDirectory(0)

h_f_pt.Add(h_f_pt_ZZ,-1)
h_q_pt.Add(h_q_pt_ZZ,-1)
fout = TFile.Open('pt_ZZsubtracted.root','RECREATE')
h_f_pt.Write()
h_q_pt.Write()
fout.Close()
