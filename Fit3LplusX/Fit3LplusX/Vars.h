#ifndef Fit3LplusX_Vars_H
#define Fit3LplusX_Vars_H

namespace Vars{

  struct Var{
    const char* name;
    const char* label;
    int bins;
    float min;
    float max;
  };

  Var nInner{"nInner","n_{InnerPix}",4,-0.5,3.5};
  Var rTRT{"rTRT","r_{TRT}",21,-0.025,1.025};
  Var f1{"f1","f_{1}",40,-0.01,1.01};
  Var eProbHT{"eProbHT","p^{e}_{TRT}",21,-0.025,1.025};
  Var f3{"f3","f_{3}",40,-0.1,1.01};
  Var Rhad{"Rhad","Rhad",40,-0.1,2.01};
  Var Reta{"Reta","Reta",40,-0.1,1.01};
  Var Rphi{"Rphi","Rphi",40,-0.1,1.01};
  Var Deta1{"Deta1","Deta1",40,-0.1,1.01};

  Var ptBalance{"ptBalance","1-p_{T}^{MS}/p_{T}^{ID}",13,-0.5,0.8};
  Var d0sig{"d0sig","d_{0}/#sigma_{d_{0}}",6,0,3};

}

#endif
