#include "H4lStyle.C"
void plotFSRcorr()
{
  SetAtlasStyle();
  TFile *f = TFile::Open("minitrees/ntuple.signal.root");
  TTree* t = (TTree*)f->Get("tree_incl_all");
  TH1F* h = new TH1F("h",";m_{4l} [GeV];Entries / 2 GeV",35,70,140);
  TH1F* h2 = (TH1F*) h->Clone("h2");
  t->Draw("m4l_unconstrained>>h2","weight*(m4l_fsr!=m4l_unconstrained && event_type==2)","");
  t->Draw("m4l_fsr>>h","weight*(m4l_fsr!=m4l_unconstrained && event_type==2)","");
  cout << h->Integral() << endl;
  h->Scale(1/36.47);
  h2->Scale(1/36.47);
  h2->SetFillColor(kOrange);
  TCanvas *c1 = new TCanvas("c1","",600,600);
  h->Draw("hist");
  h2->Draw("histsame");
  h->Draw("histsame");
  allLabels(1,"all channels");
  TLegend *leg = makeLeg(0.205,0.4,0.65,0.6);
  leg->AddEntry(h2,"before FSR correction","lf");
  leg->AddEntry(h,"after FSR correction","l");
  leg->Draw();
}
