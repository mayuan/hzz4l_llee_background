#include "H4lBkgUtils/H4lStyle.h"
#include "H4lBkgUtils/AtlasLabels.h"
#include "TLine.h"
#include "TString.h"
#include "TLatex.h"
#include <string>

TLegend* makeLeg(float xmin,float ymin,float xmax,float ymax)
{
  TLegend *leg = new TLegend(xmin,ymin,xmax,ymax);  
  leg->SetShadowColor(0);
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
    
  leg->SetLineWidth(0);   
  leg->SetTextFont(42);
  return leg;
}

void allLabels(float lumi, std::string channelOrOtherText,float x,float y)
{
  ATLASLabel(x, y, "Internal", 1);

  y-=0.065;
  if (channelOrOtherText!="") {
    TLatex tMain;
    tMain.SetNDC();
    tMain.SetTextSize(0.0425);
    tMain.SetTextFont(42);    
    tMain.DrawLatex(x, y,channelOrOtherText.c_str());
    y-=0.05;
  }
  TLatex lumiInfo;
  lumiInfo.SetNDC();
  lumiInfo.SetTextSize(0.03);
  lumiInfo.SetTextFont(42);
  //lumiInfo.DrawLatex(x, y, Form("#sqrt{s} = 13 TeV #intLdt = %g fb^{-1}",lumi));
  lumiInfo.DrawLatex(x, y, Form("13 TeV, %g fb^{-1}",lumi));  
}
