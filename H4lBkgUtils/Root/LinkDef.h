#include <H4lBkgUtils/H4lBkgUtils.h>
#include <H4lBkgUtils/AtlasStyle.h>
#include <H4lBkgUtils/H4lStyle.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class H4lBkgUtils+;
#pragma link C++ class H4lStyle+;
//#pragma link C++ class AtlasStyle+;

#endif
