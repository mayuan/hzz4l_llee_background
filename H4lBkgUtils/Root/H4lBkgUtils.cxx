#include "H4lBkgUtils/H4lBkgUtils.h"

#include "TString.h"
#include "TChain.h"

#include <iostream>
using namespace std;

TChain* getChainForSample(TString samplesToAdd, TString treename, TString path, bool verbose)
{
  map<TString, vector<TString> > defs;

  // defs["Zbb"] = {"mc*.363*.Sherpa_NNPDF30NNLO_Z*BFilter*.root"};
  // defs["Zcc"] = {"mc*.363*.Sherpa_NNPDF30NNLO_Z*CFilterBVeto*.root"};
  // defs["Zqq"] = {"mc*.363*.Sherpa_NNPDF30NNLO_Z*CVetoBVeto*.root"};

  defs["data"] = {"data*.root"};
  
  defs["ZjetsIncl"] = {"mc*.3641*Sherpa_221*.root"};
  defs["ZjetsFilt"] = {"mc*.3442*.Sherpa_NNPDF30NNLO_Z*.root"};

  defs["ZjetsLF"] = {"mc*.3641*Sherpa_221*CVetoBVeto*.root"};
  defs["ZjetsHF"] = {"mc*.3641*Sherpa_221*BFilter*.root","mc*.3641*Sherpa_221*CFilterBVeto*.root","mc*.3442*.Sherpa_NNPDF30NNLO_Z*.root"};

  //define Zjets = ZjetsIncl+ZjetsFilt
  vector<TString> vZjets;
  for (auto i: defs["ZjetsIncl"]) vZjets.push_back(i);
  for (auto i: defs["ZjetsFilt"]) vZjets.push_back(i);
  defs["Zjets"] = vZjets;
  
  defs["ZjetsMG"] = {"mc*.36150*.*.root","mc*.36151*.*.root"};

  defs["ttbar"] = {"mc*.410000.*.root","mc*.410009.*.root","mc*.344171.*.root"};
  
  defs["ttV"] = {"mc*.410144.*.root", "mc*.410142.*.root"};
  defs["VVV"] = {"mc*.361621.*.root","mc*.361623.*.root","mc*.361625.*.root","mc*.361626.*.root"};

  //define MultiLep = ttV+VVV
  vector<TString> vMultiLep;
  for (auto i: defs["ttV"]) vMultiLep.push_back(i);
  for (auto i: defs["VVV"]) vMultiLep.push_back(i);
  defs["MultiLep"] = vMultiLep;
  
  defs["ZZ"] = {"mc*.361603.*.root","mc*.343232.*.root","mc*.342556.*.root","mc*.343212.*.root","mc*.343213.*.root"};
  defs["WZ"] = {"mc*.361601.*.root"};

  defs["signal"] = {"mc*.345060.*.root","mc*.344235.*.root","mc*.341947.*.root","mc*.341964.*.root","mc*.342561.*.root","mc*.344973.*.root"};

  
  TChain *ch = new TChain(treename);

  //Break down "Sample1+Sample2" to a vector of strings
  vector<TString> samples;
  if (samplesToAdd.Contains("+")) {
    while (samplesToAdd.Contains("+")) {
      if (samplesToAdd=="" || samplesToAdd=="+") break;
      unsigned int posPlus = samplesToAdd.First("+");
      TString s0 = samplesToAdd(0, posPlus);
      samples.push_back(s0);
      samplesToAdd.Remove(0, posPlus+1);
    }
    samples.push_back(samplesToAdd);
  }
  else samples.push_back(samplesToAdd);

  for (auto s : samples) {
    if (defs[s].size()==0) {
      cout << "ERROR, list of files not defined for " << s << ". Available options:"<< endl;
      for (const auto k : defs) if (k.second.size()) cout << k.first << " ";
      cout << endl;
      continue;
    }
   
    for ( auto defs_i : defs[s] ) ch->Add( path+"/"+defs_i );    
  }

  if (verbose) {
    ch->Lookup();
    cout << "Files added:" << endl;
    for (auto f: *(ch->GetListOfFiles()))
      cout << f->GetTitle() << endl;
    cout << endl;
  }
  return ch;
}
  
