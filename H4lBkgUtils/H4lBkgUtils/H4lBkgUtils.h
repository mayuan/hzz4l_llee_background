#ifndef H4lBkgUtils_H
#define H4lBkgUtils_H

#include "TString.h"
#include "TChain.h"

TChain* getChainForSample(TString s, TString treename, TString path, bool verbose=true);

#endif
