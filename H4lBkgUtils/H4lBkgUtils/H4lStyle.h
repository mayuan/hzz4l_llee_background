#ifndef  __H4LSTYLE_H
#define __H4LSTYLE_H

#include "TLegend.h"
#include "TColor.h"
#include <vector>
using namespace std;

namespace H4lStyle
{
  enum SampleEnums{
    Data = 0,
    Signal,
    ZZ,
    RedBkg,
    WZ,
    ttbar,
    Zjets,
    Zgamma,
    MultiLep,
    nMax
  };

  //const
  //vector<Color_t> LineColor = {
  const Color_t LineColor[ ] = {
    kBlack,
    (Color_t)TColor::GetColor("#487D9E"),
    (Color_t)TColor::GetColor("#A10000"),
    kViolet+1,
    kGray+3,
    kOrange+4,
    kGreen+3,  //kCyan+1
    kYellow+1,
    kCyan-1
  };

  const Color_t FillColor[ ]={
    kBlack,
    (Color_t)TColor::GetColor("#74CCFF"),
    (Color_t)TColor::GetColor("#ee0000"),
    kViolet-1,
    kGray,
    kOrange-3,
    kGreen+1, //kCyan+1
    kYellow,
    kCyan-2
  };

  const TString LegName[ ]={
    "Data",
    "H125",
    "ZZ*",
    "Z+jets, t#bar{t}", //"Reducible backgound"
    "WZ",
    "t#bar{t}",
    "Z+jets",
    "Z+#gamma",
    "VVV+ttV"
  };
}

TLegend* makeLeg(float xmin=0.755,float ymin=0.65,float xmax=0.95,float ymax=0.925);
void allLabels(float lumi=1.0, std::string channelOrOtherText="", float x=0.2,float y=0.875);

#endif // __H4LSTYLE_H
