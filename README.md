# Background estimations for the H->ZZ->4l analysis 

## To setup:

mkdir source build run

cd source

git clone
https://:@gitlab.cern.ch:8443/HZZ/HZZSoftware/HZZ4lBackground.git ./

asetup 21.2.4,AnalysisBase

cd ../build

cmake ../source

make

source ./x86*/setup.sh

